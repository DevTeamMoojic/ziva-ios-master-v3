//
//  TransactionWithDeleteCell.h
//  Ziva
//
//  Created by Leena on 20/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionWithDeleteCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *titleSubL;
@property (weak, nonatomic) IBOutlet UILabel *dateL;
@property (weak, nonatomic) IBOutlet UILabel *orderIDL;
@property (weak, nonatomic) IBOutlet UIButton *callB;
@property (weak, nonatomic) IBOutlet UIButton *downArrowB;

@property (weak, nonatomic) IBOutlet UILabel *countL;
@property (weak, nonatomic) IBOutlet UILabel *priceL;
@property (weak, nonatomic) IBOutlet UIButton *deleteB;
@property (weak, nonatomic) IBOutlet UIButton *rebookB;

@property (weak, nonatomic) IBOutlet UIView *rebookv;
@property (weak, nonatomic) IBOutlet UIView *callv;


- (void) configureDataForCell : (NSDictionary *) item;

@end
