//
//  TransactionWithoutDeleteCell.m
//  Ziva
//
//  Created by Leena on 20/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "TransactionWithoutDeleteCell.h"

@implementation TransactionWithoutDeleteCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void) configureDataForCell : (NSDictionary *) item
{
    // NSLog(@"%@", [item allKeys]);
    NSArray *arr = [[item valueForKey:@"Items"] objectAtIndex:0];
    // NSLog(@"%@", arr);
    NSString *storeName = [arr valueForKey:KEY_NAME];
    // NSDictionary *dict1 = [ReadData dictionaryFromDictionary:item forKey:@"Items"];
    
    self.titleL.text = storeName;
    //self.titleL.text = [ReadData arrayFromDictionary:arr1 forKey:KEY_NAME];
    //self.titleL.text = [ReadData stringValueFromDictionary:dict1 forKey:KEY_NAME];
    self.titleSubL.text = [ReadData stringValueFromDictionary:item forKey:KEY_CTXN_SUB_TITLE];
    
    //Date formate.
    NSString *dateStr = [ReadData stringValueFromDictionary:item forKey:KEY_CTXN_DATE];
    NSString *FNSTR = [dateStr stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    
    //Set formate.
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:FORMAT_DATE_TIME];
    NSDate *date  = [dateFormatter dateFromString:FNSTR];
    
    // Convert to new Date Format
    [dateFormatter setDateFormat:@"yyyy-MM-dd h:mm a"];
    NSString *newDateStr = [dateFormatter stringFromDate:date];
    //NSLog(@"%@",newDate);
    self.dateL.text = newDateStr;
    
    //self.dateL.text = [ReadData stringValueFromDictionary:item forKey:KEY_CTXN_DATE];
    //self.orderIDL.text = [ReadData stringValueFromDictionary:item forKey:KEY_CTXN_ORDERID];
    
    self.orderIDL.text =[NSString stringWithFormat:@"Order ID - %@",[ReadData stringValueFromDictionary:item forKey:KEY_CTXN_ORDERID]];
}

@end
