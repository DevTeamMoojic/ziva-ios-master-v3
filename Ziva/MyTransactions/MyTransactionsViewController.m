//
//  MyTransactionsViewController.m
//  Ziva
//
//  Created by Leena on 20/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//


#import "MyTransactionsViewController.h"
#import "TransactionWithDeleteCell.h"
#import "TransactionWithoutDeleteCell.h"
#import "KLCPopup.h"
#import "LandingPageViewController.h"


#define ROW_HEIGHT 40
#define ROW_HEIGHT_DELETE 140

@interface MyTransactionsViewController ()<APIDelegate,MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
    
    NSArray *listTransaction;
    NSMutableArray *listDeletedTransaction;
    
    CGSize layoutsize;
    
    
    // For Current and Past Transation button.
    __weak IBOutlet UIView *toggleCV;
    __weak IBOutlet UIButton *currentTransactionsB;
    __weak IBOutlet UIButton *pastTransactionB;
    
    
    __weak IBOutlet UIView *optionsV;
    __weak IBOutlet UIView *currentPastV;
    __weak IBOutlet UILabel *screenTitleL;
    
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIView *topImageV;
    __weak IBOutlet UIImageView *mainImgV;
    
    __weak IBOutlet UIScrollView *scrollviewOnTop;
    
    __weak IBOutlet UIScrollView *tabcontentscrollView;
    
    __weak IBOutlet UITableView *listTblV;
    __weak IBOutlet UILabel *noresultsL;
    
    CGRect cachedImageViewSize;
    CGFloat yscrollOffset;
    
    BOOL isBackFromOtherView;
    BOOL isContentSizeUpdated;
    
    NSInteger pageWidth;
    
    CGFloat contentHeight;
    
    BOOL isCurrenctOrPast;
    
    NSString *pickstr;
    
    UIButton *contB,*wrongB,*notcomfortB,*noreasonB;
    AppDelegate *appD;
    MyCartController *cartC;
    
    NSInteger sectionCount;
    NSMutableArray *arrayForBool;
    NSMutableDictionary *finalListTransactionDict;
    
    NSString  *itemID;
    
}
@end

@implementation MyTransactionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    listDeletedTransaction = [[NSMutableArray alloc]init];
    
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     cartC = appD.sessionDelegate.mycartC;
       arrayForBool = [[NSMutableArray alloc]init];
    finalListTransactionDict = [[NSMutableDictionary alloc]init];
    
    [self setupLayout];
}


#pragma mark - Layout

- (void) setupLayout
{
    
    // button enable and disabled.
    isCurrenctOrPast = YES;
    currentTransactionsB.selected = YES;
    pastTransactionB.selected = NO;

    _selLatitude = [appD getUserCurrentLocationLatitude];
    _selLongitude = [appD getUserCurrentLocationLongitude];
    _selGenderId = [appD getLoggedInUserGender];
    
    // For Current and Past button rounded rect.
    toggleCV.layer.borderColor = [UIColor grayColor].CGColor;
    
    currentTransactionsB.backgroundColor = currentTransactionsB.selected?[UIColor colorWithRed:230.0/255.0 green:76.0/255.0 blue:60.0/255.0 alpha:1.0] : [UIColor whiteColor];
    pastTransactionB.backgroundColor = pastTransactionB.selected?[UIColor colorWithRed:230.0/255.0 green:76.0/255.0 blue:60.0/255.0 alpha:1.0] : [UIColor whiteColor];
    
    [currentTransactionsB setTitleColor:currentTransactionsB.selected?[UIColor whiteColor] : [UIColor grayColor] forState:UIControlStateNormal];
    [pastTransactionB setTitleColor:pastTransactionB.selected?[UIColor whiteColor] : [UIColor grayColor] forState:UIControlStateNormal];
    /*
    //pageWidth = CGRectGetWidth(self.view.bounds);
    
    [self.view addGestureRecognizer:scrollviewOnTop.panGestureRecognizer];
    [listTblV removeGestureRecognizer:listTblV.panGestureRecognizer];
    
    //Position elements
    {
        topImageV.frame = [UpdateFrame setSizeForView:topImageV usingHeight:[ResizeToFitContent getHeightForParallaxImage:self.view.frame]];
        
        NSLog(@"x=%f,y=%f,h=%f,w=%f",topImageV.frame.origin.x,topImageV.frame.origin.y, topImageV.frame.size.height, topImageV.frame.size.width);
        
        yscrollOffset = CGRectGetHeight(topImageV.frame) - IMAGE_SCROLL_OFFSET_Y - CGRectGetHeight(optionsV.frame);
        mainImgV.frame = topImageV.frame ;
        
        containerView.frame = [UpdateFrame setSizeForView:containerView usingHeight:(CGRectGetHeight(self.view.bounds) + yscrollOffset)];
        
        //  tabcontentscrollView.frame= [UpdateFrame setPositionForView:tabcontentscrollView usingPositionY: CGRectGetMaxY(topImageV.frame)];
        
        tabcontentscrollView.frame = [UpdateFrame setSizeForView:tabcontentscrollView usingHeight: CGRectGetHeight(containerView.frame) - CGRectGetMaxY(topImageV.frame)];
        
        NSLog(@"x=%f,y=%f,h=%f,w=%f",tabcontentscrollView.frame.origin.x,tabcontentscrollView.frame.origin.y, tabcontentscrollView.frame.size.height, tabcontentscrollView.frame.size.width);
        
    }
    
    
    CGPoint scrollviewOrigin = scrollviewOnTop.frame.origin;
    scrollviewOnTop.scrollIndicatorInsets = UIEdgeInsetsMake(-scrollviewOrigin.y, 0, scrollviewOrigin.y, scrollviewOrigin.x);
    */
    [self reloadList];
}


#pragma mark - Events

- (IBAction) currentTransactionsBPressed:(UIButton *)sender{
    if(!sender.selected){
        
        currentTransactionsB.selected = YES;
        pastTransactionB.selected = NO;
        
        [self toggleListing];
        
        // calling service
        isCurrenctOrPast = YES;
        [self reloadList];
    }
}

- (IBAction) pastTransactionBPressed:(UIButton *)sender{
    if(!sender.selected){
        currentTransactionsB.selected = NO;
        pastTransactionB.selected = YES;
        [self toggleListing];
        
        // clling servisce for past.
        isCurrenctOrPast = NO;
        [self reloadList];
    }
    
}

- (void) toggleListing
{
    if(currentTransactionsB.selected){
        //noresultsL.hidden = ([storesArray count] > 0);
        // storesListCV.hidden = ([storesArray count] == 0);
    }
    else{
        //noresultsL.hidden = ([keywordsArray count] > 0);
        // servicesListCV.hidden = ([keywordsArray count] == 0);
    }
    
    currentTransactionsB.backgroundColor = currentTransactionsB.selected?[UIColor colorWithRed:230.0/255.0 green:76.0/255.0 blue:60.0/255.0 alpha:1.0] : [UIColor whiteColor];
    pastTransactionB.backgroundColor = pastTransactionB.selected?[UIColor colorWithRed:230.0/255.0 green:76.0/255.0 blue:60.0/255.0 alpha:1.0] : [UIColor whiteColor];
    
    [currentTransactionsB setTitleColor:currentTransactionsB.selected?[UIColor whiteColor] : [UIColor grayColor] forState:UIControlStateNormal];
    [pastTransactionB setTitleColor:pastTransactionB.selected?[UIColor whiteColor] : [UIColor grayColor] forState:UIControlStateNormal];
    
}


#pragma mark - Methods

- (void) reloadList
{
    if ([InternetCheck isOnline]){
        
        if (isCurrenctOrPast) {
            
            
            [self hudShowWithMessage:@"Loading"];
            NSMutableArray *replacementArray = [NSMutableArray array];
            NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
            [paramDict setObject:@"CUSTOMER_ID" forKey:@"key"];
            //[paramDict setObject:@"5620" forKey:@"value"];
            [paramDict setObject: [appD getLoggedInUserId] forKey:@"value"];
            [replacementArray addObject:paramDict];
            
            [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_CURRENTAPPOINTMENTS Param:nil replacementStrings:replacementArray];
            
        }else
        {

            [self hudShowWithMessage:@"Loading"];
            NSMutableArray *replacementArray = [NSMutableArray array];
            NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
            [paramDict setObject:@"CUSTOMER_ID" forKey:@"key"];
            //[paramDict setObject:@"5620" forKey:@"value"];
            [paramDict setObject: [appD getLoggedInUserId] forKey:@"value"];
            [replacementArray addObject:paramDict];
            
            [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_PASTAPPOINTMENTS Param:nil replacementStrings:replacementArray];
            
            
        }
        
    }else{
         [self showErrorMessage:NETWORKERROR];
    }
}

- (void) showErrorMessage : (NSString *)message
{
    UIAlertView *errorNotice = [[UIAlertView alloc] initWithTitle:ALERT_TITLE_ERROR message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorNotice show];
}

-(CGFloat) calculateHeightOfList
{
    return (listTransaction.count  * ROW_HEIGHT);
}

- (void) processResponse : (NSDictionary *) dict
{
    [finalListTransactionDict removeAllObjects];
    [arrayForBool removeAllObjects];
    listTransaction = [ReadData arrayFromDictionary:dict forKey:RESPONSEKEY_DATA];
   
    for (NSDictionary *d in listTransaction) {
        
        NSInteger strID = [[d valueForKey:@"Id"] integerValue];
        //NSLog(@"%ld",(long)strID);
        NSMutableArray *arr = [[NSMutableArray alloc]init];

        if (strID == [[d valueForKey:@"Id"] integerValue]) {
            NSMutableArray *itemArray = [d valueForKey:@"Items"];
           // NSLog(@"item array %@", itemArray);
           // [arr addObject:d];
            //NSLog(@"the d is --%@",arr );
            [finalListTransactionDict setObject:itemArray forKey:[NSString stringWithFormat:@"%ld",(long)strID]];
            //NSLog(@"%@subarray is",subArray );
            //[walletFinalDict setObject:subArray forKey:[NSString stringWithFormat:@"%ld",(long)salonID]];
            
        }
        
    }
    //NSLog(@"final dictonary %@",finalListTransactionDict);
    sectionCount = finalListTransactionDict.count;
   // NSLog(@" count is = %ld",(long)finalListTransactionDict);

    // This logic for initial headder decollaps array.
    for (int  i = 0; i< finalListTransactionDict.count; i ++) {
        [arrayForBool addObject:[NSNumber numberWithBool:NO]];
    }

    if (finalListTransactionDict.count > 0) {
        listTblV.hidden = NO;
         noresultsL.hidden = YES;
         [listTblV reloadData];
    }else{
        listTblV.hidden = YES;
        noresultsL.hidden = NO;
    }
    
//    noresultsL.hidden = ([finalListTransactionDict count] > 0);
//    listTblV.hidden = ([finalListTransactionDict count] == 0);

     [HUD hide:YES];
   
    
    /*
    listTransaction = [ReadData arrayFromDictionary:dict forKey:RESPONSEKEY_DATA];
    //NSLog(@"----- Response is %@", listTransaction);
     listTblV.scrollEnabled = YES;
    
    [listTblV reloadData];
    [listTblV setContentOffset:CGPointZero animated:NO];
    listTblV.hidden = (10 == 0);
    noresultsL.hidden = !listTblV.hidden ;
    
    noresultsL.hidden = !listTblV.hidden ;
    
    noresultsL.hidden = ([listTransaction count] > 0);
    listTblV.hidden = ([listTransaction count] == 0);

    [HUD hide:YES];
     */
}
/*
- (void) tabChanged
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    
    if (oldOffset.y >= yscrollOffset)
    {
        CGPoint offset = [self getScrollContentOffset];
        offset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:offset animated:NO];
    }
    isContentSizeUpdated = NO;
}
*/
#pragma mark - HUD & Delegate methods

- (void)hudShowWithMessage:(NSString *)message
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    HUD.dimBackground = YES;
    HUD.labelText = message;
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    [HUD show:YES];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Events

- (IBAction)backBPressed:(UIButton *)sender{
    
    LandingPageViewController *landVC = [[LandingPageViewController alloc]init];
    [landVC callAPI];
    //NSArray *rrr = [self.navigationController viewControllers];
    //NSLog(@"%@",rrr);
    [self.navigationController popToRootViewControllerAnimated:YES];
   // [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Expand on Scroll animation
/*
- (CGSize) setContentSize
{
    CGFloat height = CGRectGetHeight(mainImgV.frame) + contentHeight;
    return CGSizeMake(pageWidth, height);
}

- (void) setScrollContentOffset: (CGPoint) offset
{
    listTblV.contentOffset = offset;
}

- (CGPoint) getScrollContentOffset
{
    return [listTblV contentOffset];
}

- (void) resetOffsetAllTabs
{
    CGPoint offset = CGPointZero;
    [self setScrollContentOffset : offset];
}

-(void) updateContentSize : (CGPoint) contentOffset;
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        contentOffset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    }
    [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    
    isContentSizeUpdated = NO;
    
}

#pragma mark - Scroll Events

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == scrollviewOnTop)
    {
        {
            if (isContentSizeUpdated)  return;
            
            CGRect scrolledBoundsForContainerView = containerView.bounds;
            if (scrollView.contentOffset.y <= yscrollOffset)
            {
                
                scrolledBoundsForContainerView.origin.y = scrollView.contentOffset.y ;
                containerView.bounds = scrolledBoundsForContainerView;
                
                //Reset offset for all tabs
                if (scrollView.contentOffset.y <= 0) [self resetOffsetAllTabs];
                
                
                CGFloat y = -scrollView.contentOffset.y;
                CGFloat alphaLevel = 1;
                CGFloat BLUR_MAX_Y = IMAGE_SCROLL_OFFSET_Y + CGRectGetHeight(topImageV.frame) - CGRectGetMinY(optionsV.frame);
                if (fabs(y) < BLUR_MAX_Y)
                {
                    alphaLevel = (BLUR_MAX_Y - fabs(y))/(BLUR_MAX_Y);
                }
                else
                {
                    alphaLevel = 0;
                }
                
                //[screenTitleL setAlpha:alphaLevel];
                if (y > 0)
                {
                    mainImgV.frame = CGRectInset(cachedImageViewSize, 0, -y/2);
                    mainImgV.center = CGPointMake(mainImgV.center.x, mainImgV.center.y + y/2);
                }
                else
                {
                    mainImgV.frame = [UpdateFrame setPositionForView:mainImgV usingPositionY:y];
                }
                return;
            }
            
            scrolledBoundsForContainerView.origin.y = yscrollOffset ;
            containerView.bounds = scrolledBoundsForContainerView;
            
            [self setScrollContentOffset:CGPointMake(0, scrollView.contentOffset.y - yscrollOffset)  ];
        }
    }
}

*/
#pragma mark - Table view data source

-(CGFloat) calculateheightForRow:(NSInteger)row{
    /*
    if ([listDeletedTransaction containsObject:[listTransaction objectAtIndex:row]])
        return ROW_HEIGHT_DELETE;
    else
        return ROW_HEIGHT;*/
    return 40;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //return 1;
    return sectionCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
   // return listTransaction.count ;
    if ([[arrayForBool objectAtIndex:section] boolValue])
    {
        NSArray *arr = [finalListTransactionDict allKeys];
        NSString *strrrr = [arr objectAtIndex:section];
        NSArray *addada = [finalListTransactionDict objectForKey:strrrr];
        
        return addada.count;
    }
    else{
        return 0;

    }

   
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    // This block of code for cancel status 1 then header height 0 else to height is normal height.
    if (isCurrenctOrPast){
        
        NSArray *arrkeys = [finalListTransactionDict allKeys];
        NSString *sectionStr = [arrkeys objectAtIndex:section];
        // Get headaer item
        NSDictionary *headerDict = [[NSDictionary alloc]init];
        
        for (NSDictionary *dict in listTransaction) {
            
            //NSLog(@"%@",[dict valueForKey:@"Id"]);
            //NSLog(@"%@",sectionStr);
            NSInteger strFlot = [[dict valueForKey:@"Id"] integerValue];
            
            if ([sectionStr integerValue] == strFlot) {
                headerDict = dict;
            }
        }
        
        // new implemtation for Cancel appointment.
        NSArray * headerArray = [headerDict valueForKey:@"Items"];
        
        int cancelCount = 0;
        
        for (NSDictionary *itemDcit in headerArray) {
            int cancelStatus = [[itemDcit valueForKey:@"CancelStatus"] integerValue];
            
            if (cancelStatus == 1) {
                cancelCount = cancelCount + 1;
            }
        }
        
        float height;
        
            if (cancelCount == [headerArray count]) {
                height = 0.0f;
                
            }else{
                height =  91.0f;
            }
        
        return  height;

        
        
    }else{
        return 91.0f;

    }
    
  
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSArray *arrkeys = [finalListTransactionDict allKeys];
    NSString *sectionStr = [arrkeys objectAtIndex:section];
    NSArray *arr = [finalListTransactionDict objectForKey:sectionStr];
    
    // Get headaer item
    NSDictionary *headerDict = [[NSDictionary alloc]init];
    
    for (NSDictionary *dict in listTransaction) {
        
        //NSLog(@"%@",[dict valueForKey:@"Id"]);
        //NSLog(@"%@",sectionStr);
        NSInteger strFlot = [[dict valueForKey:@"Id"] integerValue];

        if ([sectionStr integerValue] == strFlot) {
            headerDict = dict;
        }
    }
    
     UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,CGRectGetWidth(tableView.frame), 91)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    // title
    UILabel *titleHeadderL = [[UILabel alloc]initWithFrame:CGRectMake(10, 8, 250, 20)];
    titleHeadderL.text = [NSString stringWithFormat:@"%@",[ReadData stringValueFromDictionary:headerDict forKey:KEY_CTXN_SUB_TITLE]];
    titleHeadderL.font = [UIFont semiboldFontOfSize:12.0f];
    
    // sub title.
    UILabel *subtitleL = [[UILabel alloc]initWithFrame:CGRectMake(10, 25, CGRectGetWidth(tableView.frame)- 130, 20)];
    subtitleL.text = [NSString stringWithFormat:@"%@",[ReadData stringValueFromDictionary:headerDict forKey:KEY_CTXN_SUB_TITLE]];
    subtitleL.textColor = [UIColor lightGrayColor];
    subtitleL.font = [UIFont regularFontOfSize:10.0f];
    
    
    // callrin and redirecdtor button.
    UIButton *callBORRedirect = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetWidth(tableView.frame)- 110, 30, 61, 32)];
    [callBORRedirect setTag:section];
    
    if (isCurrenctOrPast) {
       
        [callBORRedirect setImage:[UIImage imageNamed:@"mytransactions_call.png"] forState:UIControlStateNormal];
        [callBORRedirect addTarget:self action:@selector(callAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }else{
        
        [callBORRedirect setImage:[UIImage imageNamed:@"mytransactions_rebook.png"] forState:UIControlStateNormal];
        [callBORRedirect addTarget:self action:@selector(rebookAction:) forControlEvents:UIControlEventTouchUpInside];
  }
    
   
    
    
    // Down arraow imga.
    UIButton *expandB = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetWidth(tableView.frame)-41, 32, 30, 30)];
    [expandB setImage:[UIImage imageNamed:@"mytransactions_appointment_downarrow"] forState:UIControlStateNormal];
   [expandB addTarget:self action:@selector(downArrowaction:) forControlEvents:UIControlEventTouchUpInside];
    [expandB setTag:section];
    
    if ([[arrayForBool objectAtIndex:section] boolValue])
    {
        [expandB setImage:[UIImage imageNamed:@"mytransactions_appointment_uparrow.png"] forState:UIControlStateNormal];
    }

    // gray view aftert the deleter the service.
    
    UIView *trnsprentv = [[ UIView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 91)];
    trnsprentv.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5f];
    
    //grayV.backgroundColor = [UIColor colorWithCGColor:<#(nonnull CGColorRef)#>];
  
    // title
    UILabel *cancelL = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetWidth(tableView.frame)-48, 8, 40, 20)];
    cancelL.text = @"Cancel";
    cancelL.textColor = [UIColor blackColor];
    cancelL.font = [UIFont regularFontOfSize:10.0f];

    
        // view transprent after button submit Action.
        if (isCurrenctOrPast) {
            if ([[ReadData stringValueFromDictionary:headerDict forKey:KEY_CTXN_CANCELSTATUS] isEqualToString:@"1"]) {
               trnsprentv.hidden = NO;
                cancelL.hidden = NO;
            
            }else{
               trnsprentv.hidden = YES;
                cancelL.hidden = YES;

            }
            
        }else{
            trnsprentv.hidden = YES;
             cancelL.hidden = YES;
        }

   
    /*
    //Down arrow images.
    UIImageView *headderDownArrow = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetWidth(tableView.frame)-35, 30, 20, 20)];
    headderDownArrow.image = [UIImage imageNamed:@"icon_wallet_downarrow"];
    if ([[arrayForBool objectAtIndex:section] boolValue])
    {
        headderDownArrow.image = [UIImage imageNamed:@"icon_walletuparraow"];
    }*/

    

    // date title.
    UILabel *dateL = [[UILabel alloc]initWithFrame:CGRectMake(10, 50, CGRectGetWidth(tableView.frame)-130, 20)];
    //Date formate.
    NSString *dateStr = [ReadData stringValueFromDictionary:headerDict forKey:KEY_CTXN_DATE];
    NSString *FNSTR = [dateStr stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    
    //Set formate.
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:FORMAT_DATE_TIME];
    NSDate *date  = [dateFormatter dateFromString:FNSTR];
    
    // Convert to new Date Format.
    [dateFormatter setDateFormat:@"yyyy-MM-dd h:mm a"];
    NSString *newDateStr = [dateFormatter stringFromDate:date];
    //NSLog(@"%@",newDate);
    dateL.text = newDateStr;
    dateL.font = [UIFont semiboldFontOfSize:12.0f];
    dateL.textColor = [UIColor redColor];

    
    // order title.
    UILabel *orderL = [[UILabel alloc]initWithFrame:CGRectMake(10, 70, 200, 20)];
    //self.countL.text = [ReadData stringValueFromDictionary:item forKey:KEY_CTXN_COUNT];
    orderL.text = [NSString stringWithFormat:@"Order ID - %@",[ReadData stringValueFromDictionary:headerDict forKey:KEY_CTXN_ORDERID]];
    orderL.font = [UIFont semiboldFontOfSize:11.0f];
    orderL.textColor = [[UIColor lightGrayColor] colorWithAlphaComponent:50.0f];
    
    // line
    UIView * lineV = [[UIView alloc]initWithFrame:CGRectMake(0, 90, 414, 1)];
    lineV.backgroundColor = [UIColor lightGrayColor];
    
    
    
    [headerView addSubview:lineV];
    [headerView addSubview:callBORRedirect];
    //[headerView addSubview:headderDownArrow];
    [headerView addSubview:orderL];
    [headerView addSubview:expandB];
    [headerView addSubview:dateL];
    [headerView addSubview:subtitleL];
    [headerView addSubview:titleHeadderL];
    [headerView addSubview:cancelL];
    //[headerView bringSubviewToFront:trnsprentv];
    [headerView addSubview:trnsprentv];
    [self.view bringSubviewToFront:trnsprentv];
    
    /*
    static NSString *CellIdentifier = @"CellWithoutDelete";
    TransactionWithoutDeleteCell *withOutDeleteCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    return withOutDeleteCell.contentView;*/
    
    
    return headerView;
}
-(void)downArrowaction:(UIButton*) sender{
    
    //NSLog(@"%lu",(long)sender.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
   // NSLog(@"%lu", (long) indexPath);
    
    if (indexPath.row == 0)
    {
        BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        for (int i=0; i<[finalListTransactionDict count]; i++)
        {
            if (indexPath.section==i)
            {
                [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!collapsed]];
            }
        }
        
        [listTblV reloadSections:[NSIndexSet indexSetWithIndex:sender.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
        
    }
    
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[arrayForBool objectAtIndex:indexPath.section] boolValue]) {
        return [self calculateheightForRow:indexPath.row];
        
    }else
        return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TransactionWithDeleteCell *withDeleCell = (TransactionWithDeleteCell *)[tableView dequeueReusableCellWithIdentifier:@"CellWithDelete" forIndexPath:indexPath];
    
    NSArray *allkeysCell = [finalListTransactionDict allKeys];
    NSString *strkeyCell = [allkeysCell objectAtIndex:indexPath.section];
    NSArray  *arraaddedItemCell = [finalListTransactionDict objectForKey:strkeyCell];
    NSDictionary *dict = [arraaddedItemCell objectAtIndex:indexPath.row];
    
    
    
    // delete button action
    // Delete button hide and unhide Action.
    if (isCurrenctOrPast) {
        withDeleCell.deleteB.hidden = NO;
        
    }else{
        
        withDeleCell.deleteB.hidden = YES;
        
    }
    // delete button Action.
    withDeleCell.deleteB.tag = indexPath.row;
    [withDeleCell.deleteB addTarget:self action:@selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];

    [withDeleCell configureDataForCell:dict];
    
    //NSDictionary *item = listTransaction[indexPath.row];
    
    /*
    TransactionWithDeleteCell *withDeleCell;
    TransactionWithoutDeleteCell *withOutDeleteCell;
    
    // with delete button.
    if ([listDeletedTransaction containsObject:[listTransaction objectAtIndex:indexPath.row]]){
        withDeleCell = (TransactionWithDeleteCell *)[tableView dequeueReusableCellWithIdentifier:@"CellWithDelete" forIndexPath:indexPath];
        NSDictionary *item = listTransaction[indexPath.row];
        
        // downArrow button Action.
        withDeleCell.downArrowB.tag = indexPath.row;
        [withDeleCell.downArrowB addTarget:self action:@selector(downArrowAction:) forControlEvents:UIControlEventTouchUpInside];
        
        // calling button Action.
        if (isCurrenctOrPast) {
            
            withDeleCell.callB.tag = indexPath.row;
            [withDeleCell.callB addTarget:self action:@selector(callAction:) forControlEvents:UIControlEventTouchUpInside];
            withDeleCell.rebookB.hidden = YES;
            withDeleCell.callv.hidden = NO;
            withDeleCell.rebookv.hidden = YES;
            

        }else{
            withDeleCell.rebookB.tag = indexPath.row;
            [withDeleCell.rebookB addTarget:self action:@selector(rebookAction:) forControlEvents:UIControlEventTouchUpInside];
            withDeleCell.rebookB.hidden = NO;
            withDeleCell.callv.hidden = YES;
            withDeleCell.rebookv.hidden = NO;

        }
              // delete button Action.
        withDeleCell.deleteB.tag = indexPath.row;
        [withDeleCell.deleteB addTarget:self action:@selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];
        
        // Delete button hide and unhide Action.
        if (isCurrenctOrPast) {
             withDeleCell.deleteB.hidden = NO;
            
        }else{
            
            withDeleCell.deleteB.hidden = YES;
            
        }
        
        [withDeleCell configureDataForCell :item];
      return withDeleCell;
        
    }else{ // Wtith out delete button
        withOutDeleteCell = (TransactionWithoutDeleteCell *)[tableView dequeueReusableCellWithIdentifier:@"CellWithoutDelete" forIndexPath:indexPath];
        NSDictionary *item = listTransaction[indexPath.row];
       // withOutDeleteCell.trnsprentv.hidden = YES;

        
        // delete button Action.
        withOutDeleteCell.downArrowB.tag = indexPath.row;
        [withOutDeleteCell.downArrowB addTarget:self action:@selector(downArrowAction:) forControlEvents:UIControlEventTouchUpInside];
       
         // calling button Action.
        if (isCurrenctOrPast) {
           
            withOutDeleteCell.callB.tag = indexPath.row;
            [withOutDeleteCell.callB addTarget:self action:@selector(callAction:) forControlEvents:UIControlEventTouchUpInside];
            //withOutDeleteCell.rebookB.hidden = YES;
           // withOutDeleteCell.callB.hidden = NO;
           // withOutDeleteCell.rebookB.hidden = YES;
            withOutDeleteCell.callv.hidden = NO;
            withOutDeleteCell.rebookv.hidden = YES;

            
        }else{
            withOutDeleteCell.rebookB.tag = indexPath.row;
            [withOutDeleteCell.rebookB addTarget:self action:@selector(rebookAction:) forControlEvents:UIControlEventTouchUpInside];
            //withOutDeleteCell.callB.hidden = YES;
            //withOutDeleteCell.rebookB.hidden = NO;
            //withOutDeleteCell.rebookB.hidden = NO;
            
            withOutDeleteCell.callv.hidden = YES;
            withOutDeleteCell.rebookv.hidden = NO;

        }
        
        // view transprent after button submit Action.
        if (isCurrenctOrPast) {
            if ([[ReadData stringValueFromDictionary:item forKey:KEY_CTXN_CANCELSTATUS] isEqualToString:@"1"]) {
                withOutDeleteCell.trnsprentv.hidden = NO;
            }else{
                withOutDeleteCell.trnsprentv.hidden = YES;
            }
            
        }else{
            withOutDeleteCell.trnsprentv.hidden = YES;
        }
        

     [withOutDeleteCell configureDataForCell :item];
        
        return withOutDeleteCell;
        
    }
    
    */
    withDeleCell.selectionStyle = UITableViewCellSelectionStyleNone;
    return withDeleCell;
}

// Called when user click on downarrow button,
-(void)downArrowAction:(UIButton*) sender{
    
    
    if ([listDeletedTransaction containsObject:[listTransaction objectAtIndex:sender.tag]]) {
        [listDeletedTransaction removeObject:[listTransaction objectAtIndex:sender.tag]];
        
    }else{
        [listDeletedTransaction addObject:[listTransaction objectAtIndex:sender.tag]];
    }
    
    [listTblV reloadData];
}

// Called when user click on calling button
-(void)callAction:(UIButton*) sender{
    [appD startSupportCall];

}

// Rebookingbutton action.
-(void)rebookAction:(UIButton*)sender{
    
    NSDictionary *item = listTransaction[sender.tag];
    
    if ([InternetCheck isOnline]){
        
//        [self hudShowWithMessage:@"Loading"];
//        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
//        //[paramDict setObject: [appD getLoggedInUserId] forKey:@"CustomerId"];
//        [paramDict setObject:[ReadData stringValueFromDictionary:item forKey:KEY_ID] forKey:@"AppointmentId"];
//        //[paramDict setObject:pickstr forKey:PARAM_REASON];
//        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_DETAIL_VIEW_ORDER  Param:paramDict];
        
//        NSMutableArray *replacementArray = [NSMutableArray array];
//        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
//        //[paramDict setObject:@"RECORD_ID" forKey:@"key"];
//        [paramDict setObject:[ReadData stringValueFromDictionary:item forKey:KEY_ID] forKey:@"RECORD_ID"];
//        
//        //[replacementArray addObject:paramDict];
//        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_DETAIL_VIEW_ORDER  Param:paramDict];
//
//       // [[[APIHelper alloc] initWithDelegate:self] callAPI:API_DETAIL_VIEW_ORDER Param:nil replacementStrings:replacementArray];
//        
        
        NSMutableArray *replacementArray = [NSMutableArray array];
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:@"RECORD_ID" forKey:@"key"];
        [paramDict setObject:[ReadData stringValueFromDictionary:item forKey:KEY_ID] forKey:@"value"];
        
        [replacementArray addObject:paramDict];
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_DETAIL_VIEW_ORDER Param:nil replacementStrings:replacementArray];
    }
    
}

// called when user clicks on delete button.
// Popup when click on delete button.
-(void) deleteAction:(UIButton *)path{
    
    
    CGPoint buttonPosition = [path convertPoint:CGPointZero toView:listTblV];
    NSIndexPath *indexPath = [listTblV indexPathForRowAtPoint:buttonPosition];
    
   // NSLog(@"%ld", (long)indexPath.row);
    //NSLog(@"%ld", (long)indexPath.section);
    
    NSArray *allkeysCell = [finalListTransactionDict allKeys];
   //NSString *strkeyCell = [allkeysCell objectAtIndex:indexPath.section];
    itemID = [allkeysCell objectAtIndex:indexPath.section];
 
    NSArray  *arraaddedItemCell = [finalListTransactionDict objectForKey:itemID];
    NSDictionary *dict = [arraaddedItemCell objectAtIndex:indexPath.row];
    NSLog(@"%@", dict);
    itemID = [dict valueForKey:@"Id"];
  
    
     /*
    NSLog(@"TAG VAL: %ld", (long)sender.tag);
    NSDictionary *item = listTransaction[sender.tag];
    NSLog(@"TAG VAL: %@", item);*/

   /*
    UIButton *button = (UIButton *)sender;
    UITableViewCell *cell = (UITableViewCell *)button.superview;
    UITableView *tableView = (UITableView *)cell.superview;
    NSIndexPath *indexPath = [tableView indexPathForCell:cell];*/
   
    // Generate content view to present
    UIView* contentView = [[UIView alloc]init];
    contentView.translatesAutoresizingMaskIntoConstraints = NO;
    contentView.backgroundColor = [UIColor colorWithRed:234.0/255.0 green:238.0/255.0 blue:243.0/255.0 alpha:1.0];
    contentView.layer.cornerRadius = 12.0;
    
    // title
    UILabel * titleL = [[UILabel alloc]initWithFrame:CGRectMake(10, 1, 280, 40)];
    titleL.backgroundColor = [UIColor clearColor];
    titleL.layer.cornerRadius = 12.0;
    
    titleL.text = @"Are you sure? Tell us why....";
    [titleL setFont:[UIFont fontWithName:@"Lato-Bold" size:14]];
    [contentView addSubview:titleL];
    
    // line
    UIView * lineV = [[UIView alloc]initWithFrame:CGRectMake(0, 41, 280, 1)];
    lineV.backgroundColor = [UIColor darkGrayColor];
    [contentView addSubview:lineV];
    
    // cont button
    contB = [UIButton buttonWithType:UIButtonTypeCustom];
    contB.backgroundColor = [UIColor clearColor];
    contB.frame = CGRectMake(10, 55, 20, 20);
    contB.tag = 1;
    [contB setImage:[UIImage imageNamed:@"current_popup_untick.png"] forState:UIControlStateNormal];
    [contB addTarget:self action:@selector(pickValueFrom:) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:contB];
    
    // cont lable.
    UILabel * contL = [[UILabel alloc]initWithFrame:CGRectMake(40, 50, 200, 30)];
    contL.backgroundColor = [UIColor clearColor];
    [contL setFont:[UIFont fontWithName:@"Lato-Regular" size:11]];
    contL.text = @"Can't reach on time";
    [contentView addSubview:contL];
    
    
    // cont button
    wrongB = [UIButton buttonWithType:UIButtonTypeCustom];
    wrongB.backgroundColor = [UIColor clearColor];
    wrongB.frame = CGRectMake(10, 85, 20, 20);
    wrongB.tag = 2;
    [wrongB setImage:[UIImage imageNamed:@"current_popup_untick.png"] forState:UIControlStateNormal];
    [wrongB addTarget:self action:@selector(pickValueFrom:) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:wrongB];
    
    // worng service lable.
    UILabel * worngL = [[UILabel alloc]initWithFrame:CGRectMake(40, 80, 200, 30)];
    worngL.backgroundColor = [UIColor clearColor];
    [worngL setFont:[UIFont fontWithName:@"Lato-Regular" size:11]];
    worngL.text = @"Selected wrong service";
    [contentView addSubview:worngL];
    
    // not comforttable button
    notcomfortB = [UIButton buttonWithType:UIButtonTypeCustom];
    notcomfortB.backgroundColor = [UIColor clearColor];
    notcomfortB.frame = CGRectMake(10, 115, 20, 20);
    notcomfortB.tag = 3;
    [notcomfortB setImage:[UIImage imageNamed:@"current_popup_untick.png"] forState:UIControlStateNormal];
    [notcomfortB addTarget:self action:@selector(pickValueFrom:) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:notcomfortB];
    
    // not comfort lable.
    UILabel * notcomforL = [[UILabel alloc]initWithFrame:CGRectMake(40, 110, 200, 30)];
    notcomforL.backgroundColor = [UIColor clearColor];
    [notcomforL setFont:[UIFont fontWithName:@"Lato-Regular" size:11]];
    
    notcomforL.text = @"Not comfortable with service provider";
    [contentView addSubview:notcomforL];
    
    // no reason button
    noreasonB = [UIButton buttonWithType:UIButtonTypeCustom];
    noreasonB.backgroundColor = [UIColor clearColor];
    noreasonB.frame = CGRectMake(10, 145, 20, 20);
    noreasonB.tag = 4;
    [noreasonB setImage:[UIImage imageNamed:@"current_popup_untick.png"] forState:UIControlStateNormal];
    [noreasonB addTarget:self action:@selector(pickValueFrom:) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:noreasonB];
    
    
    // cont lable.
    UILabel * noReasonL = [[UILabel alloc]initWithFrame:CGRectMake(40, 140, 200, 30)];
    noReasonL.backgroundColor = [UIColor clearColor];
    noReasonL.text = @"No Reason";
    [noReasonL setFont:[UIFont fontWithName:@"Lato-Regular" size:11]];
    
    [contentView addSubview:noReasonL];
    
    
    // submit button.
    UIButton* submittonB = [UIButton buttonWithType:UIButtonTypeCustom];
    submittonB.backgroundColor = [UIColor colorWithRed:248.0/255.0 green:184.0/255.0 blue:15.0/255.0 alpha:1.0];
    submittonB.frame = CGRectMake(0, 200, 280, 40);
    
   // submittonB.tag = indexPath.row;
    //NSLog(@"%ld", (long)submittonB.tag);
    [submittonB setTitle:@"SUBMIT" forState:UIControlStateNormal];
    submittonB.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:14.0];
    [submittonB addTarget:self action:@selector(submitBAction:) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:submittonB];
    
    
    
    /*
     UILabel* dismissLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 300, 40)];
     dismissLabel.translatesAutoresizingMaskIntoConstraints = NO;
     dismissLabel.backgroundColor = [UIColor clearColor];
     dismissLabel.textColor = [UIColor whiteColor];
     dismissLabel.font = [UIFont boldSystemFontOfSize:72.0];
     dismissLabel.text = @"Are you sure? Tell us why....";
     
     UIButton* dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
     dismissButton.translatesAutoresizingMaskIntoConstraints = NO;
     dismissButton.contentEdgeInsets = UIEdgeInsetsMake(10, 20, 10, 20);
     dismissButton.backgroundColor = [UIColor greenColor];
     [dismissButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
     [dismissButton setTitleColor:[[dismissButton titleColorForState:UIControlStateNormal] colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
     dismissButton.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
     [dismissButton setTitle:@"Bye" forState:UIControlStateNormal];
     dismissButton.layer.cornerRadius = 6.0;
     //[dismissButton addTarget:self action:@selector(dismissButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
     
     [contentView addSubview:dismissLabel];
     [contentView addSubview:dismissButton];
     
     
     NSDictionary* views = NSDictionaryOfVariableBindings(contentView, dismissButton, dismissLabel);
     
     [contentView addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(16)-[dismissLabel]-(10)-[dismissButton]-(24)-|"
     options:NSLayoutFormatAlignAllCenterX
     metrics:nil
     views:views]];
     
     [contentView addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(36)-[dismissLabel]-(36)-|"
     options:0
     metrics:nil
     views:views]];
     */
    
    // Show in popup
    KLCPopupLayout layout = KLCPopupLayoutMake(3, 3);
    KLCPopup *popup = [KLCPopup popupWithContentView:contentView];
    
    [popup showWithLayout:layout];
}

// click any circle.
-(void)pickValueFrom:(UIButton*)sender{
    pickstr = @"";
    
    if (sender.tag == 1) {
        [contB setImage:[UIImage imageNamed:@"current_popup_tick.png"] forState:UIControlStateNormal];
        [wrongB setImage:[UIImage imageNamed:@"current_popup_untick"] forState:UIControlStateNormal];
        [notcomfortB setImage:[UIImage imageNamed:@"current_popup_untick"] forState:UIControlStateNormal];
        [noreasonB setImage:[UIImage imageNamed:@"current_popup_untick"] forState:UIControlStateNormal];
        pickstr = @"Can't reach on time";
        
    }
    else if (sender.tag == 2){
        [contB setImage:[UIImage imageNamed:@"current_popup_untick"] forState:UIControlStateNormal];
        [wrongB setImage:[UIImage imageNamed:@"current_popup_tick.png"] forState:UIControlStateNormal];
        [notcomfortB setImage:[UIImage imageNamed:@"current_popup_untick"] forState:UIControlStateNormal];
        [noreasonB setImage:[UIImage imageNamed:@"current_popup_untick"] forState:UIControlStateNormal];
        pickstr = @"Selected wrong service";
        
    }
    else if (sender.tag == 3){
        [contB setImage:[UIImage imageNamed:@"current_popup_untick"] forState:UIControlStateNormal];
        [wrongB setImage:[UIImage imageNamed:@"current_popup_untick"] forState:UIControlStateNormal];
        [notcomfortB setImage:[UIImage imageNamed:@"current_popup_tick.png"] forState:UIControlStateNormal];
        [noreasonB setImage:[UIImage imageNamed:@"current_popup_untick"] forState:UIControlStateNormal];
        pickstr = @"Not comfortable with service provider";
        
    }else if (sender.tag == 4){
        [contB setImage:[UIImage imageNamed:@"current_popup_untick"] forState:UIControlStateNormal];
        [wrongB setImage:[UIImage imageNamed:@"current_popup_untick"] forState:UIControlStateNormal];
        [notcomfortB setImage:[UIImage imageNamed:@"current_popup_untick"] forState:UIControlStateNormal];
        [noreasonB setImage:[UIImage imageNamed:@"current_popup_tick.png"] forState:UIControlStateNormal];
        pickstr = @"No Reason";
        
    }
    
  
    // for multiple selection.
    /*
     switch (sender.tag) {
     case 1:
     if ([contB.imageView.image isEqual:[UIImage imageNamed:@"current_popup_untick"]])
     {
     [contB setImage:[UIImage imageNamed:@"current_popup_tick.png"] forState:UIControlStateNormal];
     }
     else{
     [contB setImage:[UIImage imageNamed:@"current_popup_untick"] forState:UIControlStateNormal];
     }
     break;
     
     case 2:
     
     if ([wrongB.imageView.image isEqual:[UIImage imageNamed:@"current_popup_untick"]])
     {
     [wrongB setImage:[UIImage imageNamed:@"current_popup_tick.png"] forState:UIControlStateNormal];
     }
     else{
     [wrongB setImage:[UIImage imageNamed:@"current_popup_untick"] forState:UIControlStateNormal];
     }
     break;
     
     case 3:
     if ([notcomfortB.imageView.image isEqual:[UIImage imageNamed:@"current_popup_untick"]])
     {
     [notcomfortB setImage:[UIImage imageNamed:@"current_popup_tick.png"] forState:UIControlStateNormal];
     }
     else{
     [notcomfortB setImage:[UIImage imageNamed:@"current_popup_untick"] forState:UIControlStateNormal];
     }
     break;
     case 4:
     if ([noreasonB.imageView.image isEqual:[UIImage imageNamed:@"current_popup_untick"]])
     {
     [noreasonB setImage:[UIImage imageNamed:@"current_popup_tick.png"] forState:UIControlStateNormal];
     }
     else{
     [noreasonB setImage:[UIImage imageNamed:@"current_popup_untick"] forState:UIControlStateNormal];
     }
     break;
     
     default:
     
     break;
     }
     
     */
}




// calling api on submit button.
-(void)submitBAction:(UIButton*)sender{
    if ([pickstr isEqualToString:@""] || [pickstr length] == 0) {
        UIAlertView *errorNotice = [[UIAlertView alloc] initWithTitle:ALERT_TITLE_ERROR message:@"Select any reason" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorNotice show];

    }else{
        
    
        //NSDictionary *item = listTransaction[sender.tag];
        //NSLog(@"%@", item);
        
        [KLCPopup dismissAllPopups];
        
        if ([InternetCheck isOnline]){
            
            [self hudShowWithMessage:@"Loading"];
            NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
            [paramDict setObject: [appD getLoggedInUserId] forKey:@"CustomerId"];
            [paramDict setObject:itemID forKey:@"OrderItemId"];
            [paramDict setObject:pickstr forKey:PARAM_REASON];
            [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_CANCELAPPOINTMENTS  Param:paramDict];
        }
        

    }
}

//- (void) loadNearbyStores
//{
//    if ([InternetCheck isOnline]){
//        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
//        [paramDict setObject:selLatitude forKey:PARAM_LATITUDE];
//        [paramDict setObject:selLongitude forKey:PARAM_LONGITUDE];
//        [paramDict setObject: dlvryType forKey:@"Chain"];
//
//        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_NEARBY_STORES Param:paramDict];
//    }
//}

-(void)removeselection:(id)sender{
    [sender setImage:nil forState:UIControlStateNormal];
}
-(void)uncheck: (id)sender{
    [sender setImage:[UIImage imageNamed:@"current_popup_untick.png"] forState:UIControlStateNormal];
    
}
-(void)check: (id) sender{
    [sender setImage:[UIImage imageNamed:@"current_popup_tick.png"] forState:UIControlStateNormal];
    
}


#pragma mark - API

-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
    if(![ReadData isValidResponseData:response])
    {
        {
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
            [self.view setUserInteractionEnabled:YES];
            [HUD hide:YES];
        }
        return;
    }
    
    if ([apiName isEqualToString:API_GET_PASTAPPOINTMENTS]) {
        [self processResponse:response];
    }else if ([apiName isEqualToString:API_GET_CURRENTAPPOINTMENTS]){
        
         [self processResponse:response];
    }
    else if ([apiName isEqualToString:API_GET_CANCELAPPOINTMENTS]){
        
        //[self processResponse:response];
        
        [self hudWasHidden:HUD];
        //[HUD show:YES];
        [self reloadList];
    }else if ([apiName isEqualToString:API_DETAIL_VIEW_ORDER]){
        //[self processResponse:response];
        [self processResponse_AppointmentOrderbyIDDetail:[ReadData dictionaryFromDictionary:response forKey:RESPONSEKEY_DATA]];
    }else if ([apiName isEqualToString:API_GET_STOREID]){
        //[self processResponse:response];
        [self processResponse_StoreIDDetail:[ReadData dictionaryFromDictionary:response forKey:RESPONSEKEY_DATA]];
    }else if ([apiName isEqualToString:API_GET_STORE_RATE_CARD]){
        //[self processResponseStoreRateCard:response];
    }
}

// This is a method to get store id based on appointid and calling web service for store details.
- (void) processResponse_AppointmentOrderbyIDDetail : (NSDictionary *) dict
{
    NSDictionary *orderByIDDict = dict;
    //NSLog(@"%@",orderByIDDict);
    //[self sendDelegateEvent:API_DETAIL_VIEW_ORDER];
    //[HUD hide:YES];
   NSMutableArray *replacementArray = [NSMutableArray array];
    NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject:@"ID" forKey:@"key"];
    [paramDict setObject:[ReadData stringValueFromDictionary:dict forKey:KEY_CTXN_STOREID] forKey:@"value"];
    
    [replacementArray addObject:paramDict];
    
    [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_STOREID Param:nil replacementStrings:replacementArray];
    
    
   // [self hudShowWithMessage:@"Loading"];
       
    /*
    NSMutableArray *replacementArray = [NSMutableArray array];
    NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
    
    [paramDict setObject:@"RECORD_ID" forKey:@"key"];
    [paramDict setObject:[ReadData recordIdFromDictionary:dict forKey:KEY_CTXN_STOREID] forKey:@"value"];
    [replacementArray addObject:paramDict];
    
    paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject:@"GENDER_ID" forKey:@"key"];
    [paramDict setObject:[appD getLoggedInUserGender] forKey:@"value"];
    [replacementArray addObject:paramDict];
    
    [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_STORE_RATE_CARD Param:nil replacementStrings:replacementArray]; */
 }

// This is a method for response details fro store detatil and calling pointing to store rate card.
- (void) processResponse_StoreIDDetail :(NSDictionary *) dict{
    NSDictionary *storeIDDict = dict;
    //NSLog(@"%@",storeIDDict);
    //[self sendDelegateEvent:API_DETAIL_VIEW_ORDER];
    //[HUD hide:YES];
    
    [cartC setLatitude:_selLatitude andLongitude:_selLongitude];
    [cartC updateStoreInformation:dict];
    UIStoryboard *modalStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    StoreRateCardViewController *LDSSVC = [modalStoryboard instantiateViewControllerWithIdentifier:@"StoreRateCardViewController"];
    [LDSSVC loadForSalonStore:dict Gender:_selGenderId andDeliveryType:@"0" :NO];

    [self.navigationController pushViewController:LDSSVC animated:YES];
 
}

/*
- (void) processResponseStoreRateCard : (NSDictionary *) dict
{
    //NSArray *arrViewC = [self.navigationController viewControllers];
    //NSLog(@"%@",arrViewC);
    [cartC updateStoreInformation:dict];
    UIStoryboard *modalStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LocationDateStoreSelectorViewController *LDSSVC = [modalStoryboard instantiateViewControllerWithIdentifier:@"LocationDateStoreSelectorViewController"];
    [self.navigationController pushViewController:LDSSVC animated:YES];
}
*/


@end
