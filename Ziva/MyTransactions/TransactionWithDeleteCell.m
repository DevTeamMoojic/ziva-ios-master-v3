//
//  TransactionWithDeleteCell.m
//  Ziva
//
//  Created by Leena on 20/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "TransactionWithDeleteCell.h"

@implementation TransactionWithDeleteCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void) configureDataForCell : (NSDictionary *) item
{
    
    self.titleL.text = [item valueForKey:KEY_NAME];
    NSString *storePrice = [item valueForKey:KEY_PING_PRICE];
    self.priceL.text = [ReadData getAmountFormatted:[storePrice floatValue]];
    
    /*
    // NSLog(@"%@", [item allKeys]);
    NSArray *arrItem = [[item valueForKey:@"Items"] objectAtIndex:0];
    // NSLog(@"%@", arr);
    NSString *storeName = [arrItem valueForKey:KEY_NAME];
    NSString *storePrice = [arrItem valueForKey:KEY_PING_PRICE];
    //NSDictionary *dict1 = [ReadData dictionaryFromDictionary:item forKey:@"Items"];
    self.titleL.text = storeName;
    
    //self.titleL.text = [ReadData stringValueFromDictionary:item forKey:KEY_CTXN_TITLE];
    self.titleSubL.text = [ReadData stringValueFromDictionary:item forKey:KEY_CTXN_SUB_TITLE];
    
    //Date formate.
    NSString *dateStr = [ReadData stringValueFromDictionary:item forKey:KEY_CTXN_DATE];
    NSString *FNSTR = [dateStr stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    
    //Set formate.
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:FORMAT_DATE_TIME];
    NSDate *date  = [dateFormatter dateFromString:FNSTR];
    
    // Convert to new Date Format.
    [dateFormatter setDateFormat:@"yyyy-MM-dd h:mm a"];
    NSString *newDateStr = [dateFormatter stringFromDate:date];
    //NSLog(@"%@",newDate);
    self.dateL.text = newDateStr;
    
    
    //self.dateL.text = [ReadData stringValueFromDictionary:item forKey:KEY_CTXN_DATE];
    //self.orderIDL.text = [ReadData stringValueFromDictionary:item forKey:KEY_CTXN_ORDERID];
    
    self.countL.text = [ReadData stringValueFromDictionary:item forKey:KEY_CTXN_COUNT];
    self.orderIDL.text =[NSString stringWithFormat:@"Order ID - %@",[ReadData stringValueFromDictionary:item forKey:KEY_CTXN_ORDERID]];
    
    self.countL.text = storeName;
    //NSString *str  = [ReadData getAmountFormatted:[storePrice floatValue]];
    //NSLog(@"%@",str);
    self.priceL.text = [ReadData getAmountFormatted:[storePrice floatValue]];
     */
}

@end
