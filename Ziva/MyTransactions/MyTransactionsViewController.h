//
//  MyTransactionsViewController.h
//  Ziva
//
//  Created by Leena on 20/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "LocationDateStoreSelectorViewController.h"
#import "StoreRateCardViewController.h"

// Minarao Tamada
@interface MyTransactionsViewController : UIViewController
@property(nonatomic,strong)    NSString *selLatitude;
@property(nonatomic,strong)    NSString *selLongitude;
@property(nonatomic,strong)    NSString *selGenderId;

@end
