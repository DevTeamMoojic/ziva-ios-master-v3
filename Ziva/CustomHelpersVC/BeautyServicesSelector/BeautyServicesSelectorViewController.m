//
//  BeautyServicesSelectorViewController.m
//  Ziva
//
//  Created by Minnarao on 05/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "BeautyServicesSelectorViewController.h"

@interface BeautyServicesSelectorViewController ()
@end

@implementation BeautyServicesSelectorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// For pointing to Home service .

-(IBAction)homeServicesBPressed:(UIButton *)sender{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(homeServicesSelected)])
    {
        [self.delegate homeServicesSelected] ;
    }
}
// For pointing to salon service .
-(IBAction)salonServicesBPressed:(UIButton *)sender{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(salonServicesSelected)])
    {
        [self.delegate salonServicesSelected] ;
    }
}

// Called, when user click's on cross button.
-(IBAction)closePressB:(id)sender{
    
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(crossPresseB)])
    {
        [self.delegate crossPresseB] ;
    }
  
}
@end
