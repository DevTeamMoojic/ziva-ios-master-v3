//
//  BeautyServicesSelectorViewController.h
//  Ziva
//
//  Created by Bharat on 05/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BeautyServicesSelectorDelegate <NSObject>

- (void) homeServicesSelected;

- (void) salonServicesSelected;

- (void)  crossPresseB;

@end

@interface BeautyServicesSelectorViewController : UIViewController

@property (nonatomic, weak) id<BeautyServicesSelectorDelegate> delegate;

@end
