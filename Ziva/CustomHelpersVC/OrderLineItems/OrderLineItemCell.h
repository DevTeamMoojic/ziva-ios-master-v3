//
//  OrderLineItemCell.h
//  Ziva
//
//  Created by Bharat on 12/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderLineItemCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *mainV;

@property (weak, nonatomic) IBOutlet UILabel *lineitemDescL;
@property (weak, nonatomic) IBOutlet UILabel *lineitemQuantityL;
@property (weak, nonatomic) IBOutlet UILabel *lineitemAmountL;

- (void) configureDataForCell : (NSDictionary *) item ;

@end
