//
//  GalleryViewController.h
//  Ziva
//
//  Created by Bharat on 27/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GalleryViewController : UIViewController <UIScrollViewDelegate>

//- (void) setImageForFullScreen : (UIImage *) image;
//- (void)setupScrollView:(UIScrollView*)scrMain ;
@property (strong, nonatomic)  UIButton *closeBtn;
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, strong) NSMutableArray *viewControllers;
@property (nonatomic, assign) BOOL pageControlUsed;

- (void)updateFrame:(CGRect)rect;

- (void) setImageForFullScreen : (NSMutableArray *) imagesArray;

@end
