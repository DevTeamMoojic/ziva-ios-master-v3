//
//  GalleryViewController.m
//  Ziva
//
//  Created by Bharat on 27/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "GalleryViewController.h"

@interface GalleryViewController ()
{
   UIScrollView *mainscrollV;
    __weak IBOutlet UIImageView *galleryImgV;
    
    UIImage *fullscreenImage ;
}
@property (nonatomic, strong) NSArray *imageNames;

@end

@implementation GalleryViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
   /* mainscrollV.center = self.view.center;
    mainscrollV.minimumZoomScale=1.0;
    mainscrollV.maximumZoomScale=5.0;
    galleryImgV.image = fullscreenImage;
    */

  }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return galleryImgV;
}

- (IBAction)closeBPressed:(UIButton *)sender
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:Nil];
}

#pragma mark - view Methods
/*
- (void) setImageForFullScreen : (UIImage *) image{
    fullscreenImage = image;
}
*/

// new implemetation for images scrolling.
#pragma mark - Methods

- (void) setImageForFullScreen : (NSMutableArray *) imagesArray
{
    _imageNames = imagesArray;
    
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < [_imageNames count]; i++)
    {
        [controllers addObject:[NSNull null]];
    }
    _viewControllers = controllers;
    
    mainscrollV = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    mainscrollV.contentSize = CGSizeMake(mainscrollV.frame.size.width * [_imageNames count], mainscrollV.frame.size.height);
    [mainscrollV setBackgroundColor:[UIColor blackColor]];
    mainscrollV.pagingEnabled = YES;
    mainscrollV.bounces = NO;
    mainscrollV.showsHorizontalScrollIndicator = NO;
    mainscrollV.showsVerticalScrollIndicator = NO;
    mainscrollV.scrollsToTop = NO;
    mainscrollV.autoresizesSubviews = YES;
    mainscrollV.delegate = self;
    [self.view addSubview:mainscrollV];
    
    _pageControl = [[UIPageControl alloc]initWithFrame: CGRectMake(0, self.view.frame.size.height-10, self.view.frame.size.width, 10)];
    _pageControl.numberOfPages = [_imageNames count];
    [_pageControl setBackgroundColor:[UIColor clearColor]];
    [_pageControl setUserInteractionEnabled:NO];
    [_pageControl setPageIndicatorTintColor:[UIColor lightGrayColor]];
    [_pageControl setCurrentPageIndicatorTintColor:[UIColor whiteColor]];
    if ([_imageNames count] > 1)
        [self.view addSubview:_pageControl];
    
    [self loadScrollViewWithPage:0];
    [self loadScrollViewWithPage:1];
    
    
    _closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _closeBtn.frame = CGRectMake(self.view.frame.size.width-60, 20, 60, 60);
    [_closeBtn setImage:[UIImage imageNamed:@"icon_close"] forState:UIControlStateNormal];
    [_closeBtn addTarget:self action:@selector(closeBPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_closeBtn];
}

- (void)updateFrame:(CGRect)rect
{
    self.view.frame = rect;
    mainscrollV.frame = rect;
    
    float y = self.view.frame.size.height + mainscrollV.frame.origin.y - 10.0f;
    _pageControl.frame = CGRectMake(0.0f, y, self.view.frame.size.width, 10.0f);
}


- (void)loadScrollViewWithPage:(int)page
{
    if (page < 0) return;
    if (page >= [_imageNames count]) return;
    
    // replace the placeholder if necessary
    UIImageView *controller = [_viewControllers objectAtIndex:page];
    
    if ((NSNull *)controller == [NSNull null])
    {
        controller = [[UIImageView alloc] init];
        CGRect frame = mainscrollV.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        controller.frame = frame;
        [controller setContentMode:UIViewContentModeScaleAspectFill];
        controller.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [controller.layer setMasksToBounds:YES];
        [mainscrollV addSubview:controller];
        
        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [spinner setCenter:CGPointMake(controller.center.x, controller.center.y)];
        [spinner startAnimating];
        [controller addSubview:spinner];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            for (int i = 0; i < [_imageNames count]; i++)
            {
                if (page == i)
                { //set up each page
                    [_viewControllers replaceObjectAtIndex:page withObject:controller];
                    
                    dispatch_async(dispatch_get_main_queue(), ^
                                   {
                                       NSURL *imageURL = [NSURL URLWithString:[_imageNames objectAtIndex:i]];
                                       NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageURL];
                                       [ controller setImageWithURLRequest:urlRequest  placeholderImage:Nil  success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
                                        {
                                            [ controller setImage:image];
                                            
                                            [UIView animateWithDuration:IMAGE_VIEW_TOGGLE   delay:0 options:UIViewAnimationOptionCurveLinear
                                                             animations:^(void)
                                             {
                                                 [ controller setAlpha:1 ];
                                             }
                                                             completion:^(BOOL finished)
                                             {
                                             }];
                                        }
                                                                   failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
                                        {
                                        }];
                                       [spinner removeFromSuperview];
                                       return;
                                   });
                }
            }
        });
    }
}

#pragma mark - ScrollView Methods
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (_pageControlUsed)
    {
        return;
    }
    
    CGFloat pageWidth = mainscrollV.frame.size.width;
    int page = floor((mainscrollV.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    _pageControl.currentPage = page;
    
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
    // A possible optimization would be to unload the views+controllers which are no longer visible
}


// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    _pageControlUsed = YES;
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView  withVelocity:(CGPoint)velocity  targetContentOffset:(inout CGPoint *)targetContentOffset
{
    _pageControlUsed = NO;
}


@end
