//
//  LocationDateStoreSelectorViewController.m
//  Ziva
//
//  Created by Bharat on 28/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//
#import "ChooseLocationViewController.h"
#import "ChooseScheduleViewController.h"
#import "PingingStoresViewController.h"
#import "LocationDateStoreSelectorViewController.h"

@interface LocationDateStoreSelectorViewController ()<ChooseScheduleDelegate,PingingStoresDelegate,ChooseLocationDelegate>
{
    __weak IBOutlet UIView *chooseLocationCV;
    __weak IBOutlet UIView *chooseScheduleCV;
    __weak IBOutlet UIView *chooseStoresCV;
    
    __weak ChooseLocationViewController *chooseLocationVC;
    __weak ChooseScheduleViewController *chooseScheduleVC;
    __weak PingingStoresViewController *chooseStoresVC;
    
    AppDelegate *appD;
    MyCartController *cartC;
    
    BOOL isBackFromOtherView;
}
@end

@implementation LocationDateStoreSelectorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;
    
    [self setupLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!isBackFromOtherView){
        
    }
    else{
        isBackFromOtherView = NO;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if(!isBackFromOtherView){
     
        
    }
}

#pragma mark - Methods

- (void) setupLayout{
    if(cartC.isZenotiEnabled){
        chooseScheduleCV.frame = [UpdateFrame setPositionForView:chooseScheduleCV usingPositionX:0];
    }
    else{
        //To Do : Location selection view, for now user current location hard coded
        chooseScheduleCV.frame = [UpdateFrame setPositionForView:chooseScheduleCV usingPositionX:0];
    }
}

- (void) goToShoppingCart
{
    isBackFromOtherView = YES;
    [chooseLocationVC updateBackFromOtherViewStatus:isBackFromOtherView];
    [chooseScheduleVC updateBackFromOtherViewStatus:isBackFromOtherView];
    [chooseStoresVC updateBackFromOtherViewStatus:isBackFromOtherView];
    [self performSegueWithIdentifier:@"showcart" sender:nil];
}

#pragma mark - Translate View

- (void)translateView:(UIView *)view onScreen : (BOOL) show
{
    [self.view bringSubviewToFront:view];
    
    // DONT CHANGE THE ORDER
    
    // 1 - Dismiss keyboard
    [UIResponder dismissKeyboard];
    
    // 2 - Disable User Interaction
    [self.view setUserInteractionEnabled:NO];
    
    // 3 - Animate
    [UIView animateWithDuration:TIME_VIEW_TOGGLE
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         // Slide in
         view.frame = [UpdateFrame setPositionForView:view usingPositionX: show ? 0 : CGRectGetWidth(view.frame) ];
     }
                     completion:^(BOOL finished)
     {
         // Enable User Interaction
         [self.view setUserInteractionEnabled:YES];
     }];
}

#pragma mark - Delegates Method

- (void) oncancel_Location;
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) onsuccess_Location;
{
    [self translateView:chooseScheduleCV onScreen:YES];
}

- (void) oncancel_Schedule
{
    // Navigation track finding.
  
    if([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromIndividualVC"]) {
        NSArray *array = [self.navigationController viewControllers];
        //NSLog(@"%@", array);
        [self.navigationController popToViewController:[array objectAtIndex:2] animated:YES];
    }
//    else if([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"saloonAndHomeSearchByServices"]){
//        NSArray *array = [self.navigationController viewControllers];
//        NSLog(@"%@", array);
//        [self.navigationController popToViewController:[array objectAtIndex:1] animated:YES];
//    }
    else if([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromsalonservicesVC"] || [appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromhomeserviceVC"]){
        NSArray *array = [self.navigationController viewControllers];
        NSLog(@"%@", array);
        [self.navigationController popToViewController:[array objectAtIndex:1] animated:YES];
    }
    else
    {
    
        [self.navigationController popViewControllerAnimated:YES];

    }

//  ToDo : Location Selector
//    if(cartC.isZenotiEnabled){
//        [self.navigationController popViewControllerAnimated:YES];
//    }
//    else{
//        
//        [self translateView:chooseScheduleCV onScreen:NO];
//    }
}

- (void) onsuccess_Schedule
{
    if(cartC.isZenotiEnabled){
        [self goToShoppingCart];
    }
    else{
        [self translateView:chooseStoresCV onScreen:YES];
        [chooseStoresVC activatePinging];
    }
}

- (void) oncancel_Store
{
    [self translateView:chooseStoresCV onScreen:NO];
}

- (void) onsuccess_Store;
{
    [self goToShoppingCart];
}


#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"embed_chooselocation"]){
        chooseLocationVC = (ChooseLocationViewController *)segue.destinationViewController;
    }
    else if([segue.identifier isEqualToString:@"embed_chooseschedule"]){
        chooseScheduleVC = (ChooseScheduleViewController *)segue.destinationViewController;
        chooseScheduleVC.delegate =self;
    }
    else if([segue.identifier isEqualToString:@"embed_choosestore"]){
        chooseStoresVC = (PingingStoresViewController *)segue.destinationViewController;
        chooseStoresVC.delegate = self;
    }
}

@end
