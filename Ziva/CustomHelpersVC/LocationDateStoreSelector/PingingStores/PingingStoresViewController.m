//
//  PingingStoresViewController.m
//  Ziva
//
//  Created by Bharat on 28/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "KAProgressLabel.h"
#import "QuotesViewController.h"

#import "PingStoreCell.h"
#import "PingingStoresViewController.h"


#define TIMER_SCHEDULED_TICK 1


@interface PingingStoresViewController ()<NSStreamDelegate,UICollectionViewDelegateFlowLayout, MyCartDelegate>
{
    NSMutableArray *lstStores;
        
    CGSize layoutsize;
    
    __weak IBOutlet UILabel *countdownL;
    
    __weak IBOutlet KAProgressLabel *progresstimerL;
    
    __weak IBOutlet UICollectionView *storesLstV;
    
    __weak IBOutlet UIView *timerV;
    
    __weak IBOutlet UIView *quotesCV;
    __weak QuotesViewController *quotesVC;
    
    AppDelegate *appD;
    MyCartController *cartC;
    
    NSDateFormatter *formatter;
    
    NSTimer *progressTimer;
    NSInteger timerInSeconds;
    NSInteger timerElapsed;
    
    NSInputStream	*inputStream;
    NSOutputStream	*outputStream;
    
    NSArray *zenotiStores;
    NSInteger storesCompleted;
    
    BOOL isBackFromOtherView;
    
}
@end

@implementation PingingStoresViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    lstStores = [NSMutableArray array];
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;
    [cartC addDelegate:self];
    
    formatter = [[NSDateFormatter alloc] init];
    
    [self setupLayout];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!isBackFromOtherView){
        
    }
    else{
        isBackFromOtherView = NO;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if(!isBackFromOtherView){
        [progresstimerL cleanUp];
        [progressTimer invalidate];
        
        [cartC removeDelegate:self];
    }
}

#pragma mark - Public Methods

- (void) updateBackFromOtherViewStatus : (BOOL) status{
    isBackFromOtherView = status;
}

#pragma mark - Date Helper methods

- (NSString *) get24HrTimeSlotFromIST : (NSString *) timeInIST
{
    timeInIST = [timeInIST stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    if([timeInIST length] == [FORMAT_DATE_TIME length]){
        [formatter setDateFormat:FORMAT_DATE_TIME];
    }
    else{
        [formatter setDateFormat:FORMAT_DATE_TIME_MILLISECONDS];
    }
    NSDate *tmpDate = [formatter dateFromString:timeInIST];
    [formatter setDateFormat:FORMAT_TIME_24];
    return [formatter stringFromDate:tmpDate];
}

- (NSString *) get24HrTimeSlotIn12Hr : (NSString *) slot24Hr
{
    [formatter setDateFormat:FORMAT_TIME_24];
    NSDate *tmpDate = [formatter dateFromString:slot24Hr];
    [formatter setDateFormat:FORMAT_TIME_12];
    return [formatter stringFromDate:tmpDate];
}

#pragma mark - Methods

- (void) addLoaderRecord
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"-1" forKey:KEY_PING_RESPONSEID];
    [dict setObject:@"XXXXXXXXXX" forKey:KEY_PING_STORENAME];
    [dict setObject:@"XXXXXX" forKey:KEY_PING_STOREADDRESS];
    [dict setObject:@"0" forKey:KEY_PING_DISTANCE];
    [dict setObject:DEFAULT_TIME_SLOT forKey:KEY_PING_AVAILABLE_SLOT];
    [dict setObject:[self get24HrTimeSlotIn12Hr:DEFAULT_TIME_SLOT] forKey:KEY_PING_DISPLAY_SLOT];
    [dict setObject:@"0" forKey:KEY_PING_PRICE];
    [dict setObject:@NO forKey:KEY_PING_FROMZENOTI];
    [lstStores addObject:dict];
    
    [storesLstV reloadData];
}

- (void) addStoreRecord : (NSDictionary *) storeDict
{
    NSInteger status = [ReadData integerValueFromDictionary:storeDict forKey:KEY_PING_STATUS];
    if((status == 1) || (status == 3))
    {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        
        NSInteger responseId = [ReadData integerValueFromDictionary :storeDict forKey:KEY_PING_RESPONSEID];
        CGFloat price = [ReadData floatValueFromDictionary:storeDict forKey:KEY_PING_PRICE];
        CGFloat distance = [ReadData floatValueFromDictionary:storeDict forKey:KEY_PING_DISTANCE];
        [dict setObject: [NSNumber numberWithInteger:responseId] forKey:KEY_PING_RESPONSEID];
        [dict setObject: [ReadData stringValueFromDictionary:storeDict forKey:KEY_PING_STORENAME] forKey:KEY_PING_STORENAME];
        [dict setObject:[ReadData stringValueFromDictionary:storeDict forKey:KEY_PING_STOREADDRESS] forKey:KEY_PING_STOREADDRESS];
        [dict setObject:[NSNumber numberWithFloat:distance]forKey:KEY_PING_DISTANCE];
        [dict setObject: [NSNumber numberWithFloat:price] forKey:KEY_PING_PRICE];
        [dict setObject:@NO forKey:KEY_PING_FROMZENOTI];
        
        if(status == 1)
        {
            NSString *indianTimeSlot = [self get24HrTimeSlotFromIST:[ReadData stringValueFromDictionary:storeDict forKey:KEY_PING_BOOKDATE]];
            [dict setObject:indianTimeSlot forKey:KEY_PING_AVAILABLE_SLOT];
            [dict setObject:[self get24HrTimeSlotIn12Hr:indianTimeSlot] forKey:KEY_PING_DISPLAY_SLOT];
            [lstStores insertObject:dict atIndex:0];
        }
        else if(status == 3)
        {
            {
                NSString *indianTimeSlot = [self get24HrTimeSlotFromIST:[ReadData stringValueFromDictionary:storeDict forKey:KEY_PING_RESCHEDULE1]];
                
                if(indianTimeSlot)
                {
                    NSMutableDictionary *tmpSlot = [dict mutableCopy];
                    [tmpSlot setObject:indianTimeSlot forKey:KEY_PING_AVAILABLE_SLOT];
                    [tmpSlot setObject:[self get24HrTimeSlotIn12Hr:indianTimeSlot] forKey:KEY_PING_DISPLAY_SLOT];
                    [lstStores insertObject:tmpSlot atIndex:0];
                }
            }
            {
                NSString *indianTimeSlot = [self get24HrTimeSlotFromIST:[ReadData stringValueFromDictionary:storeDict forKey:KEY_PING_RESCHEDULE2]];
                
                if(indianTimeSlot)
                {
                    NSMutableDictionary *tmpSlot = [dict mutableCopy];
                    [tmpSlot setObject:indianTimeSlot forKey:KEY_PING_AVAILABLE_SLOT];
                    [tmpSlot setObject:[self get24HrTimeSlotIn12Hr:indianTimeSlot] forKey:KEY_PING_DISPLAY_SLOT];
                    [lstStores insertObject:tmpSlot atIndex:0];
                }
            }
            {
                NSString *indianTimeSlot = [self get24HrTimeSlotFromIST:[ReadData stringValueFromDictionary:storeDict forKey:KEY_PING_RESCHEDULE3]];
                
                if(indianTimeSlot)
                {
                    NSMutableDictionary *tmpSlot = [dict mutableCopy];
                    [tmpSlot setObject:indianTimeSlot forKey:KEY_PING_AVAILABLE_SLOT];
                    [tmpSlot setObject:[self get24HrTimeSlotIn12Hr:indianTimeSlot] forKey:KEY_PING_DISPLAY_SLOT];
                    [lstStores insertObject:tmpSlot atIndex:0];
                }
            }
            {
                NSString *indianTimeSlot = [self get24HrTimeSlotFromIST:[ReadData stringValueFromDictionary:storeDict forKey:KEY_PING_RESCHEDULE4]];
                
                if(indianTimeSlot)
                {
                    NSMutableDictionary *tmpSlot = [dict mutableCopy];
                    [tmpSlot setObject:indianTimeSlot forKey:KEY_PING_AVAILABLE_SLOT];
                    [tmpSlot setObject:[self get24HrTimeSlotIn12Hr:indianTimeSlot] forKey:KEY_PING_DISPLAY_SLOT];
                    [lstStores insertObject:tmpSlot atIndex:0];
                }
            }
            
        }
    }
    
    [storesLstV reloadData];
}


- (void) setupLayout
{
    layoutsize = CGSizeMake(150, 150);
    [self setUpTimer];
}

- (void) setUpTimer
{
    progresstimerL.backgroundColor = [UIColor clearColor];
    progresstimerL.roundedCorners = NO;
    progresstimerL.roundedCornersWidth = 10;
    progresstimerL.trackWidth = 10;
    progresstimerL.progressWidth = 10;
    progresstimerL.trackColor = [UIColor colorWithRed:212.0/255.0 green:212.0/255.0 blue:212.0/255.0 alpha:1.0];
    progresstimerL.progressColor = [UIColor colorWithRed:231.0/255.0 green:77.0/255.0 blue:61.0/255.0 alpha:1.0];
    progresstimerL.progress = 0;
    
}

-(void) displaySeconds : (NSInteger) countdownInSeconds
{
    countdownL.text =  [NSString stringWithFormat:@"%03ld", (long)countdownInSeconds];
}

#pragma mark - Pinging Process

- (void) activatePinging
{
    if([lstStores count] > 0) { [lstStores removeAllObjects];}
    [self initNetworkCommunication];
    
    [self addLoaderRecord];
    timerElapsed = 0;
    timerInSeconds = 120;

    zenotiStores = Nil;
    
    [self displaySeconds:timerInSeconds];
    timerV.hidden = NO;
    [progresstimerL setProgress:0];
}

- (void) deactivatePinging
{
    [progressTimer invalidate];
    [self stopNetworkCommunication];
    [self displaySeconds:0];
    timerV.hidden = YES;
    [progresstimerL setProgress:1];
}

- (void) initNetworkCommunication {
    
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;

    //CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)@"192.168.1.111", 12000, &readStream, &writeStream);

    NSDictionary *helperObj = [[PlistHelper new] getDictionaryForPlist:@"APIs"];
    NSString *tcpPingingURL = [ReadData stringValueFromDictionary:helperObj forKey:@"tcpPinging"];
    NSInteger tcpPortNumber = [ReadData integerValueFromDictionary:helperObj forKey:@"tcpPort"];
    
    CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)tcpPingingURL, tcpPortNumber, &readStream, &writeStream);
    
    inputStream = (__bridge NSInputStream *)readStream;
    outputStream = (__bridge NSOutputStream *)writeStream;
    [inputStream setDelegate:self];
    [outputStream setDelegate:self];
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    [inputStream open];
    [outputStream open];
    
    [self performSelector:@selector(sendMessageOverTCP) withObject:nil afterDelay:2];
}

- (void) stopNetworkCommunication
{
    [inputStream close];
    [inputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    inputStream = nil;

    [outputStream close];
    [outputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    outputStream = nil;
}

- (void) sendMessageOverTCP
{
    NSString *response  = [NSString stringWithFormat:@"{\"RequestId\":%@}<EOF>",cartC.requestId];
    NSData *data = [[NSData alloc] initWithData:[response dataUsingEncoding:NSASCIIStringEncoding]];
    [outputStream write:[data bytes] maxLength:[data length]];
    
    [self startTimer];
    
    storesCompleted = 0;
    zenotiStores = [ReadData arrayFromDictionary:[cartC fetchRequestBookingInfo] forKey:@"ZenotiStoreIds"];
    if([zenotiStores count] > 0){
        [self getAvailabilityForZenotiStore];
    }
}

- (void) recevivedMessageOverTCP : (NSString *) message
{
    if(![message isEqualToString:@"BEAT<EOF>"]){
        NSString *jsonString = [message stringByReplacingOccurrencesOfString:@"<EOF>" withString:@""];
        NSData *jsondata = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:jsondata options:NSJSONReadingMutableContainers error:nil];
        
        if ([json isKindOfClass:[NSDictionary class]])
        {
            [self addStoreRecord:json];
        }
    }
    
}

- (void) getAvailabilityForZenotiStore
{
    NSString *storeId =  [[zenotiStores objectAtIndex:storesCompleted] stringValue];
    [cartC getZenotiAvailability:storeId];
}

#pragma mark - Timer Elapsed

-(void) startTimer
{
    [quotesVC startTimer:timerInSeconds];
    
    [self displaySeconds:timerInSeconds];
    
    [self refreshTimer];
}

- (void) refreshTimer
{
    progressTimer = [NSTimer timerWithTimeInterval:TIMER_SCHEDULED_TICK
                                            target:self
                                          selector:@selector(updateTimer)
                                          userInfo:nil
                                           repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:progressTimer forMode:NSDefaultRunLoopMode];
}

- (void) updateTimer
{
    timerElapsed++;
    
    [quotesVC updateTicks];
    NSInteger remaining = timerInSeconds - timerElapsed;
    CGFloat progresspct = timerElapsed*1.0f/timerInSeconds;
    [progresstimerL setProgress:progresspct];
    
    [self displaySeconds:remaining];
 
    if(remaining <=0) { [self deactivatePinging];}
}

#pragma mark - NSStream delegate

- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent {
    switch (streamEvent) {
        case NSStreamEventOpenCompleted:
            break;
        case NSStreamEventHasBytesAvailable:
            
            if (theStream == inputStream) {
                
                uint8_t buffer[1024];
                int len;
                
                while ([inputStream hasBytesAvailable]) {
                    len = [inputStream read:buffer maxLength:sizeof(buffer)];
                    if (len > 0) {
                        NSString *output = [[NSString alloc] initWithBytes:buffer length:len encoding:NSASCIIStringEncoding];
                        NSLog(@"%@", output);
                        if(nil != output){
                            [self recevivedMessageOverTCP:output];
                        }
                    }
                }
            }
            break;
            
            
        case NSStreamEventErrorOccurred:
            NSLog(@"Can not connect to the host!");
            break;
            
        case NSStreamEventEndEncountered:
            NSLog(@"Event End encountered!");
            break;
        default:
            NSLog(@"Unknown event");
    }    
}

#pragma mark - Cart Delegate methods

- (void) onResponseError{
    //[HUD hide:YES];
}

- (void) onAvailabilityGenerated
{
    NSDictionary *selectedSlot = Nil;
    NSArray *arrSlots = [cartC fetchAvailibilitySlots];
    for(NSDictionary *item in arrSlots){
        NSString *slotId = [ReadData stringValueFromDictionary:item forKey:KEY_SLOT_ID];
        if([slotId compare:cartC.bookTimeSlot] != NSOrderedAscending){
            selectedSlot = item;
            break;
        }
    }
    // new implemntation.
    if (zenotiStores.count >0) {
        if(selectedSlot){
            NSString *storeId = [[zenotiStores objectAtIndex:storesCompleted] stringValue];
            NSDictionary *storeDict = [cartC fetchStoreInfo:storeId];
            if(storeDict){
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                
                [dict setObject: cartC.responseId forKey:KEY_PING_RESPONSEID];
                [dict setObject: [ReadData stringValueFromDictionary:storeDict forKey:KEY_NAME] forKey:KEY_PING_STORENAME];
                NSDictionary *locationDict = [ReadData dictionaryFromDictionary:storeDict forKey:KEY_STORE_LOCATION];
                [dict setObject:[ReadData stringValueFromDictionary:locationDict forKey:KEY_NAME] forKey:KEY_PING_STOREADDRESS];
                CGFloat distance = [ReadData floatValueFromDictionary:storeDict forKey:KEY_DISTANCE];
                CGFloat price = [cartC totalValue];
                [dict setObject:[NSNumber numberWithFloat:distance]forKey:KEY_PING_DISTANCE];
                [dict setObject: [NSNumber numberWithFloat:price] forKey:KEY_PING_PRICE];
                [dict setObject:@YES forKey:KEY_PING_FROMZENOTI];
                
                NSString *zenotiSlot = [ReadData stringValueFromDictionary:selectedSlot forKey:KEY_SLOT_ID];
                [dict setObject: zenotiSlot forKey:KEY_PING_AVAILABLE_SLOT];
                [dict setObject:[self get24HrTimeSlotIn12Hr:zenotiSlot] forKey:KEY_PING_DISPLAY_SLOT];
                [lstStores insertObject:dict atIndex:0];
                
                [storesLstV reloadData];
            }
        }
        storesCompleted ++;
    }
    
    // Previous implementation.
    /*
    if(selectedSlot){
        NSString *storeId = [[zenotiStores objectAtIndex:storesCompleted] stringValue];
        NSDictionary *storeDict = [cartC fetchStoreInfo:storeId];
        if(storeDict){
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            
            [dict setObject: cartC.responseId forKey:KEY_PING_RESPONSEID];
            [dict setObject: [ReadData stringValueFromDictionary:storeDict forKey:KEY_NAME] forKey:KEY_PING_STORENAME];
            NSDictionary *locationDict = [ReadData dictionaryFromDictionary:storeDict forKey:KEY_STORE_LOCATION];
            [dict setObject:[ReadData stringValueFromDictionary:locationDict forKey:KEY_NAME] forKey:KEY_PING_STOREADDRESS];
            CGFloat distance = [ReadData floatValueFromDictionary:storeDict forKey:KEY_DISTANCE];
            CGFloat price = [cartC totalValue];
            [dict setObject:[NSNumber numberWithFloat:distance]forKey:KEY_PING_DISTANCE];
            [dict setObject: [NSNumber numberWithFloat:price] forKey:KEY_PING_PRICE];
            [dict setObject:@YES forKey:KEY_PING_FROMZENOTI];
            
            NSString *zenotiSlot = [ReadData stringValueFromDictionary:selectedSlot forKey:KEY_SLOT_ID];
            [dict setObject: zenotiSlot forKey:KEY_PING_AVAILABLE_SLOT];
            [dict setObject:[self get24HrTimeSlotIn12Hr:zenotiSlot] forKey:KEY_PING_DISPLAY_SLOT];
            [lstStores insertObject:dict atIndex:0];
            
            [storesLstV reloadData];
        }
    }
    storesCompleted ++;
    */
    if(storesCompleted < [zenotiStores count]){
        [self getAvailabilityForZenotiStore];
    }
}

- (void) onUpdateSlot{
    if ([cartC fetchAvailibilityStatus]) {
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(onsuccess_Store)])
        {
            [self.delegate onsuccess_Store] ;
        }
    }
}


#pragma mark - Collection View

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return  1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [lstStores count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
        return layoutsize;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PingStoreCell *cell = (PingStoreCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"pingstorecell" forIndexPath:indexPath];

    cell.loadingAIV.hidden = !(indexPath.row + 1 == [lstStores count]);
    cell.priceL.hidden = !cell.loadingAIV.hidden;
    
    //Reposition elements
    {
        cell.mainV.frame = [UpdateFrame setSizeForView:cell.mainV usingSize:layoutsize];
    }
    
    NSDictionary *item = lstStores[indexPath.row];
    [cell configureDataForCell:item];
    
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    NSDictionary *item = lstStores[indexPath.row];
    if(!([ReadData integerValueFromDictionary:item forKey:KEY_PING_RESPONSEID] == -1))
    {
        cartC.responseId = [NSString stringWithFormat:@"%ld",(long) [ReadData integerValueFromDictionary:item forKey:KEY_PING_RESPONSEID]];
        cartC.bookTimeSlot = [ReadData stringValueFromDictionary:item forKey:KEY_PING_AVAILABLE_SLOT];
        cartC.storeName = [ReadData stringValueFromDictionary:item forKey:KEY_PING_STORENAME];
        cartC.storeAddress = [ReadData stringValueFromDictionary:item forKey:KEY_PING_STOREADDRESS];
        [cartC updateSlot: [ReadData boolValueFromDictionary:item forKey:KEY_PING_FROMZENOTI]];
    }
}

#pragma mark - Events

- (IBAction)backBPressed:(UIButton *)sender{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(oncancel_Store)])
    {
        [self deactivatePinging];
        [self.delegate oncancel_Store] ;
    }
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"embed_quotes"]){
        quotesVC = (QuotesViewController *)segue.destinationViewController;
    }
}


@end
