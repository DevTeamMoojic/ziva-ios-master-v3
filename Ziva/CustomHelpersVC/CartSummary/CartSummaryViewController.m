//
//  CartSummaryViewController.m
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "CartSummaryViewController.h"
#import "UserAddAddressViewController.h"

@interface CartSummaryViewController (){
 
    __weak IBOutlet UILabel *totalL;
    __weak IBOutlet UILabel *minimumL;
    __weak IBOutlet UILabel *actionbuttonL;
    
    AppDelegate *appD;
    MyCartController *cartC;
}
@end

@implementation CartSummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods

- (void) setActionOnCart : (NSString *) keyName{
    if([keyName isEqualToString:CART_ACTION_PROCEED]){
        actionbuttonL.text = @"PROCEED";
    }
    else if([keyName isEqualToString:CART_ACTION_PURCHASE]){
        actionbuttonL.text = @"PURCHASE";
    }
}

- (void) updateSummary{
    totalL.text = [cartC display_SummaryInfo];
}

#pragma mark - Events

- (IBAction)actionBPressed:(UIButton *)sender
{
    // From home service.
    if ([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromhomeserviceVC"]) {
        
        // If User has address.
        if ([appD isHomeAddresOrNot]) {
            if([cartC.cartItems count] > 0){
                if (self.delegate != nil && [self.delegate respondsToSelector:@selector(takeActionOnCart)])
                {
                    [self.delegate takeActionOnCart] ;
                }
            }
        }else{ // If User dont has address.
            UserAddAddressViewController *userAddAddressViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserAddAddressViewController"];
           // userAddAddressViewController.isCommingFrom = @"HomeServiceFromCartSummary";
            [self.navigationController pushViewController:userAddAddressViewController animated:YES];
        }

    }
    // From Product page.
    else if([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromproductVC"]){
        UserAddAddressViewController *userAddAddressViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserAddAddressViewController"];
        // userAddAddressViewController.isCommingFrom = @"HomeServiceFromCartSummary";
        [self.navigationController pushViewController:userAddAddressViewController animated:YES];

    }else if([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"homeServiceaddaddress"]){ // Back from Address page when user going to home service.
        UserAddAddressViewController *userAddAddressViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserAddAddressViewController"];
        // userAddAddressViewController.isCommingFrom = @"HomeServiceFromCartSummary";
        [self.navigationController pushViewController:userAddAddressViewController animated:YES];
    }// From Package View controller page.
    else if([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"frompackageVC"]){
        UserAddAddressViewController *userAddAddressViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserAddAddressViewController"];
        // userAddAddressViewController.isCommingFrom = @"HomeServiceFromCartSummary";
        [self.navigationController pushViewController:userAddAddressViewController animated:YES];
        
    }
    else// common for rest of pages.
    {
        if([cartC.cartItems count] > 0){
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(takeActionOnCart)])
            {
                [self.delegate takeActionOnCart] ;
            }
        }
    }
    
    // old code.
    /*
    
    if([cartC.cartItems count] > 0){
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(takeActionOnCart)])
        {
            [self.delegate takeActionOnCart] ;
        }
    }
  */
}

@end
