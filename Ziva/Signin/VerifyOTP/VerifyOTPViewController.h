//
//  VerifyOTPViewController.h
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VerifyOTPDelegate <NSObject>

- (void) dismissOTPScreen ;


@end

@interface VerifyOTPViewController : UIViewController

@property (nonatomic, weak) id<VerifyOTPDelegate> delegate;

- (void) resetLayout;

@end
