//
//  VerifyOTPViewController.m
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "VerifyOTPViewController.h"

#define PARAM_OTP @"OTP"

@interface VerifyOTPViewController ()<MBProgressHUDDelegate,APIDelegate>
{
    MBProgressHUD *HUD;
    __weak IBOutlet UIScrollView *mainscrollV;
    
    __weak IBOutlet UIView *enterOTPV;
    __weak IBOutlet UITextField *enterOTPTxtF;
    
    __weak IBOutlet UIButton *resendOTPB;
    
    AppDelegate *appD;
    
    UIToolbar* numberToolbar;
    
}
@end

@implementation VerifyOTPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self setupLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout

- (void) setupLayout
{
    [mainscrollV setContentSize: self.view.frame.size];
    [self setupToolBar];
    
    [enterOTPTxtF setValue:[UIColor whiteColor]forKeyPath:@"_placeholderLabel.textColor"] ;
}

- (void) setupToolBar
{
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth( self.view.frame), 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    numberToolbar.barTintColor = [UIColor lightGrayColor];
    //numberToolbar.tintColor = [UIColor officialMaroonColor];
    [numberToolbar sizeToFit];
    
    enterOTPTxtF.inputAccessoryView = numberToolbar;
}


#pragma mark - Public Methods

- (void) resetLayout
{
    enterOTPTxtF.text = @"";
}

#pragma mark - Methods

- (void) dismissKeyboard
{
    [UIResponder dismissKeyboard];
    [mainscrollV setContentOffset:CGPointZero animated:YES];
}

- (void) showErrorMessage : (NSString *)message
{
    UIAlertView *errorNotice = [[UIAlertView alloc] initWithTitle:ALERT_TITLE_ERROR message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorNotice show];
}

- (void) callAPI
{
    if ([InternetCheck isOnline]) {
        [self hudShowWithMessage:@"Loading"];
        
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject: [appD getLoggedInUserId]forKey:PARAM_REGISTRATIONID];
        [paramDict setObject: enterOTPTxtF.text forKey:PARAM_OTP];
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_VERIFY_OTP Param:paramDict];
    }
    else
        [self showErrorMessage:NETWORKERROR];
}

- (BOOL) isGenderDetailUpdated : (NSString *) genderId
{
    return ([genderId isEqualToString:GENDER_MALE] || [genderId isEqualToString:GENDER_FEMALE] );
}

- (void) processResponse : (NSDictionary *) dict
{
    NSMutableDictionary *resultsDict = [dict mutableCopy];
    
    if([self isGenderDetailUpdated:[ReadData genderIdFromDictionary:resultsDict forKey:KEY_USER_GENDER]] )
    {
        NSUserDefaults *standardDef = [NSUserDefaults standardUserDefaults];
        [standardDef setValue: [ReadData stringValueFromDictionary:resultsDict forKey:KEY_USER_MOBILENUMBER] forKey:USER_MOBILE];
        [standardDef setValue: [ReadData stringValueFromDictionary:resultsDict forKey:KEY_USER_EMAIL] forKey:USER_EMAIL];
        [standardDef setValue: [ReadData stringValueFromDictionary:resultsDict forKey:KEY_USER_LASTNAME] forKey:USER_LASTNAME];
        [standardDef setValue: [ReadData stringValueFromDictionary:resultsDict forKey:KEY_USER_FIRSTNAME] forKey:USER_FIRSTNAME];
        [standardDef setValue: [ReadData genderIdFromDictionary:resultsDict forKey:KEY_USER_GENDER] forKey:USER_GENDER];
        [standardDef setValue: [ReadData stringValueFromDictionary:resultsDict forKey:KEY_USER_IMAGE_URL] forKey:USER_PROFILE_URL];
        [standardDef setValue: [ReadData stringValueFromDictionary:resultsDict forKey:KEY_USER_DATEOFBIRTH] forKey:USER_BIRTHDATE];
        
        [standardDef synchronize];
        
        [appD loginUser];
    }
    else{
        [appD showChooseGenderVC];
    }
}

#pragma mark - HUD & Delegate methods

- (void)hudShowWithMessage:(NSString *)message
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    HUD.dimBackground = YES;
    HUD.labelText = message;
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    [HUD show:YES];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}

#pragma mark - ToolBar Events

-(void)cancelNumberPad
{
    [self dismissKeyboard];
}

-(void)doneWithNumberPad
{
    [self continueBPressed:Nil];
}


#pragma mark - Events

- (IBAction)closeBPressed: (UIButton *)sender
{
    [self dismissKeyboard];
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(dismissOTPScreen)])
    {
        [self.delegate dismissOTPScreen] ;
    }
}

- (IBAction)continueBPressed: (UIButton *)sender
{
    if (![appD isEmptyString:enterOTPTxtF.text]) {
        [self dismissKeyboard];
        [self callAPI];
    }
    else{
        [self showErrorMessage:@"Please enter OTP"];
    }
}

- (IBAction)resendOTPPressed: (UIButton *)sender
{
    if ([InternetCheck isOnline]) {
        [self hudShowWithMessage:@"Loading"];
        
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject: [appD getLoggedInUserId]forKey:PARAM_REGISTRATIONID];
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_RESEND_OTP Param:paramDict];
    }
    else
        [self showErrorMessage:NETWORKERROR];
}

#pragma mark - Text Responder

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [self continueBPressed:Nil];
    return NO;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == enterOTPTxtF) {
        if(IS_IPHONE4){
            [mainscrollV setContentOffset:CGPointMake(0,  100) animated:YES];
        }
    }
    else{
        [mainscrollV setContentOffset:CGPointZero animated:YES];
    }
    return YES;
}

#pragma mark - API

-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
    [HUD hide:YES];
    if(![ReadData isValidResponseData:response])
    {
        {
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
            [self.view setUserInteractionEnabled:YES];
        }
        return;
    }
    
    if ([apiName isEqualToString:API_VERIFY_OTP]) {
        [self processResponse:[ReadData dictionaryFromDictionary:response forKey:RESPONSEKEY_DATA]];
    }
    else if ([apiName isEqualToString:API_RESEND_OTP]) {
    }
}

@end
