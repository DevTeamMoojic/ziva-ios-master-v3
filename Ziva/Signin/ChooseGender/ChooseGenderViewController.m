//
//  ChooseGenderViewController.m
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "ChooseGenderViewController.h"

#define PARAM_GENDER @"Gender"

@interface ChooseGenderViewController ()<MBProgressHUDDelegate,APIDelegate>
{
    MBProgressHUD *HUD;
    __weak IBOutlet UIScrollView *mainscrollV;
    
    __weak IBOutlet UIButton *maleB;
    __weak IBOutlet UIButton *femaleB;
    
    AppDelegate *appD;
}
@end

@implementation ChooseGenderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self setupLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout

- (void) setupLayout
{
    maleB.selected = femaleB.selected = NO;
}

- (void) showErrorMessage : (NSString *)message
{
    UIAlertView *errorNotice = [[UIAlertView alloc] initWithTitle:ALERT_TITLE_ERROR message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorNotice show];
}

- (NSString *) getGenderOption{
    
    NSString *value;
    if(maleB.selected) { value = GENDER_MALE; }
    if(femaleB.selected) { value = GENDER_FEMALE; }
    return value;
}

- (void) callAPI
{
    if ([InternetCheck isOnline]) {
        [self hudShowWithMessage:@"Loading"];
        
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject: [appD getLoggedInUserId] forKey:PARAM_ID];
        [paramDict setObject:[self getGenderOption] forKey:PARAM_GENDER];
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_SET_GENDER Param:paramDict];
    }
    else
        [self showErrorMessage:NETWORKERROR];
}

- (void) processResponse : (NSDictionary *) resultsDict
{    
    if([ReadData boolValueFromDictionary:resultsDict forKey:KEY_USER_ISVERIFIED])
    {
        NSUserDefaults *standardDef = [NSUserDefaults standardUserDefaults];
        [standardDef setValue: [ReadData stringValueFromDictionary:resultsDict forKey:KEY_USER_MOBILENUMBER] forKey:USER_MOBILE];        
        [standardDef setValue: [ReadData stringValueFromDictionary:resultsDict forKey:KEY_USER_EMAIL] forKey:USER_EMAIL];
        [standardDef setValue: [ReadData stringValueFromDictionary:resultsDict forKey:KEY_USER_LASTNAME] forKey:USER_LASTNAME];
        [standardDef setValue: [ReadData stringValueFromDictionary:resultsDict forKey:KEY_USER_FIRSTNAME] forKey:USER_FIRSTNAME];
        [standardDef setValue: [ReadData stringValueFromDictionary:resultsDict forKey:KEY_USER_GENDER] forKey:USER_GENDER];
        
        [standardDef synchronize];
        
        [appD loginUser];
    }
}

#pragma mark - HUD & Delegate methods

- (void)hudShowWithMessage:(NSString *)message
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    HUD.dimBackground = YES;
    HUD.labelText = message;
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    [HUD show:YES];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}

#pragma mark - Events

- (IBAction)continueBPressed: (UIButton *)sender
{
    if(!maleB.selected && !femaleB.selected){
        [self showErrorMessage:@"Choose Gender"];
    }
    else{
        [self callAPI];
    }
}

- (IBAction)genderBPressed: (UIButton *)sender
{
    if(sender.tag == 1){
        maleB.selected = YES;
        femaleB.selected = NO;
    }
    else{
        maleB.selected = NO;
        femaleB.selected = YES;
    }
    
}

#pragma mark - API

-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
    [HUD hide:YES];
    if(![ReadData isValidResponseData:response])    
    {
        {
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
            [self.view setUserInteractionEnabled:YES];
        }
        return;
    }
    
    if ([apiName isEqualToString:API_SET_GENDER]) {
        [self processResponse:[ReadData dictionaryFromDictionary:response forKey:RESPONSEKEY_DATA]];
    }
}
   
@end
