//
//  WalletListCell.h
//  Ziva
//
//  Created by Leena on 23/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalletListCell : UITableViewCell



@property (weak, nonatomic) IBOutlet UIImageView *mainImgL;
@property (weak, nonatomic) IBOutlet UIImageView *placeholderImgL;
@property (weak, nonatomic) IBOutlet UILabel *imageL;

@property (weak, nonatomic) IBOutlet UILabel *pricingL;
@property (weak, nonatomic) IBOutlet UILabel *cashL;

@property (weak, nonatomic) IBOutlet UILabel *expDateL;


// new implementation.
//@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
//@property (weak, nonatomic) IBOutlet UIButton *btnExpand;

- (void) configureDataForCell : (NSDictionary *) item;

@end
