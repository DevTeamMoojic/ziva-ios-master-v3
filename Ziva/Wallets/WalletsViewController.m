//
//  WalletsViewController.m
//  Ziva
//
//  Created by Leena on 20/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "WalletsViewController.h"
#import "WalletsListViewController.h"
#import "WalletsHistoryViewController.h"
#import "WalletListCell.h"
#import "WalletListHistoryCell.h"

@interface WalletsViewController ()<MBProgressHUDDelegate,APIDelegate,UITableViewDelegate,UITableViewDataSource>{
    MBProgressHUD *HUD;
    
    // for storing wallets and history.
    NSArray *listWalletsArry;
    NSMutableArray *listWalletsHistoryArray;
    
    CGSize layoutsize;
    
    
    // For Current and Past Transation button.
    __weak IBOutlet UIView *toggleCV;
    __weak IBOutlet UIButton *walletB;
    __weak IBOutlet UIButton *walletHistoryB;
    
    
    __weak IBOutlet UIView *optionsV;
    __weak IBOutlet UIView *currentPastV;
    __weak IBOutlet UILabel *screenTitleL;
    
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIView *topImageV;
    __weak IBOutlet UIImageView *mainImgV;
    
    __weak IBOutlet UIScrollView *scrollviewOnTop;
    
    __weak IBOutlet UIScrollView *tabcontentscrollView;
    
    __weak IBOutlet UITableView *listTblV;
    __weak IBOutlet UILabel *noresultsL;
    
    CGRect cachedImageViewSize;
    CGFloat yscrollOffset;
    
    BOOL isBackFromOtherView;
    BOOL isContentSizeUpdated;
    
    NSInteger pageWidth;
    
    CGFloat contentHeight;
    
    // for button selection.
    BOOL isWlletListOrHistory;
    
    // set variable for container.
    __weak IBOutlet UIView *walletsView;
    __weak WalletsListViewController *walletsVC;
    __weak IBOutlet UIView *walletsHistoryView;
    __weak WalletsHistoryViewController *walletsHistoryVC;
    
    // for paytm
    __weak IBOutlet UIButton *walletConnectsB;
    
    AppDelegate *appD;
    
    // new implementation
    NSArray *items;
    NSMutableArray *itemsInTable;
    __weak IBOutlet UITableView *menuTableView;
    
    //
    
    NSInteger sectionCount;
    NSInteger rowCount;
    NSMutableDictionary *walletFinalDict;
    NSMutableArray *arrayForBool;

}

@end

@implementation WalletsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    walletFinalDict = [[NSMutableDictionary alloc]init];
    arrayForBool = [[NSMutableArray alloc]init];
    [self setupLayout];
}

- (void) setupLayout
{
    
    // button enable and disabled.
    isWlletListOrHistory = YES;
    walletB.selected = YES;
    walletHistoryB.selected = NO;
    
    // displaying view individualky
    walletsView.hidden = NO;
    walletsHistoryView.hidden = YES;
    
    //init arrays.
    listWalletsArry = [NSArray array];
    listWalletsHistoryArray = [NSMutableArray array];
    
    // For Current and Past button rounded rect.
    toggleCV.layer.borderColor = [UIColor grayColor].CGColor;
    
    walletB.backgroundColor = walletB.selected?[UIColor colorWithRed:230.0/255.0 green:76.0/255.0 blue:60.0/255.0 alpha:1.0] : [UIColor whiteColor];
    walletHistoryB.backgroundColor = walletHistoryB.selected?[UIColor colorWithRed:230.0/255.0 green:76.0/255.0 blue:60.0/255.0 alpha:1.0] : [UIColor whiteColor];
    
    [walletB setTitleColor:walletB.selected?[UIColor whiteColor] : [UIColor grayColor] forState:UIControlStateNormal];
    [walletHistoryB setTitleColor:walletHistoryB.selected?[UIColor whiteColor] : [UIColor grayColor] forState:UIControlStateNormal];
    
    //pageWidth = CGRectGetWidth(self.view.bounds);
    
    //[self.view addGestureRecognizer:scrollviewOnTop.panGestureRecognizer];
    // [listTblV removeGestureRecognizer:listTblV.panGestureRecognizer];
    
    /*
     
     //Position elements
     {
     topImageV.frame = [UpdateFrame setSizeForView:topImageV usingHeight:[ResizeToFitContent getHeightForParallaxImage:self.view.frame]];
     
     NSLog(@"x=%f,y=%f,h=%f,w=%f",topImageV.frame.origin.x,topImageV.frame.origin.y, topImageV.frame.size.height, topImageV.frame.size.width);
     
     yscrollOffset = CGRectGetHeight(topImageV.frame) - IMAGE_SCROLL_OFFSET_Y - CGRectGetHeight(optionsV.frame);
     mainImgV.frame = topImageV.frame ;
     
     containerView.frame = [UpdateFrame setSizeForView:containerView usingHeight:(CGRectGetHeight(self.view.bounds) + yscrollOffset)];
     
     //  tabcontentscrollView.frame= [UpdateFrame setPositionForView:tabcontentscrollView usingPositionY: CGRectGetMaxY(topImageV.frame)];
     
     tabcontentscrollView.frame = [UpdateFrame setSizeForView:tabcontentscrollView usingHeight: CGRectGetHeight(containerView.frame) - CGRectGetMaxY(topImageV.frame)];
     
     NSLog(@"x=%f,y=%f,h=%f,w=%f",tabcontentscrollView.frame.origin.x,tabcontentscrollView.frame.origin.y, tabcontentscrollView.frame.size.height, tabcontentscrollView.frame.size.width);
     
     }
     */
    
    CGPoint scrollviewOrigin = scrollviewOnTop.frame.origin;
    scrollviewOnTop.scrollIndicatorInsets = UIEdgeInsetsMake(-scrollviewOrigin.y, 0, scrollviewOrigin.y, scrollviewOrigin.x);
    
    [self reloadList];
}

#pragma mark - Events.

- (IBAction)backBPressed:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)walletsBPressed:(UIButton *)sender{
    if(!sender.selected){
        
        walletB.selected = YES;
        walletHistoryB.selected = NO;
        
        [self toggleListing];
        
        // calling service
        isWlletListOrHistory = YES;
        [self reloadList];
    }
}

- (IBAction) walletsHistoryBPressed:(UIButton *)sender{
    if(!sender.selected){
        walletB.selected = NO;
        walletHistoryB.selected = YES;
        [self toggleListing];
        
        
        isWlletListOrHistory = NO;
        // clling servisce for past.
        [self reloadList];
        
    }
    
 }
- (void) toggleListing
{
    if(walletB.selected){
        //noresultsL.hidden = ([storesArray count] > 0);
        // storesListCV.hidden = ([storesArray count] == 0);
        walletsView.hidden = NO;
        walletsHistoryView.hidden = YES;
        
    }
    else{
        walletsView.hidden = YES;
        walletsHistoryView.hidden = NO;
        //noresultsL.hidden = ([keywordsArray count] > 0);
        // servicesListCV.hidden = ([keywordsArray count] == 0);
    }
    
    walletB.backgroundColor = walletB.selected?[UIColor colorWithRed:230.0/255.0 green:76.0/255.0 blue:60.0/255.0 alpha:1.0] : [UIColor whiteColor];
    walletHistoryB.backgroundColor = walletHistoryB.selected?[UIColor colorWithRed:230.0/255.0 green:76.0/255.0 blue:60.0/255.0 alpha:1.0] : [UIColor whiteColor];
    
    [walletB setTitleColor:walletB.selected?[UIColor whiteColor] : [UIColor grayColor] forState:UIControlStateNormal];
    [walletHistoryB setTitleColor:walletHistoryB.selected?[UIColor whiteColor] : [UIColor grayColor] forState:UIControlStateNormal];
    
}

#pragma mark - Methods


- (void) reloadList
{
    if ([InternetCheck isOnline]){
        
        if (isWlletListOrHistory) { // wallets list.
            
            [self loadWalletsList];
            //[self loadWalletsHistory];
            
            //[loadingAIV startAnimating];
            walletsView.hidden = NO;
            walletsHistoryView.hidden = YES;
            //noresultsL.hidden = YES;
            
            
        }else{ //wallest history.
            
            [self loadWalletsHistoryList];
            
            //[loadingAIV startAnimating];
            walletsView.hidden = NO;
            walletsHistoryView.hidden = YES;
            //noresultsL.hidden = YES;
            
        }
    }else{
         [self showErrorMessage:NETWORKERROR];
    }
}
- (void) showErrorMessage : (NSString *)message
{
    UIAlertView *errorNotice = [[UIAlertView alloc] initWithTitle:ALERT_TITLE_ERROR message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorNotice show];
}


// Called this method for wallet list and calling service.
-(void) loadWalletsList{
    
    if ([InternetCheck isOnline]){
    [self hudShowWithMessage:@"Loading"];
    NSMutableArray *replacementArray = [NSMutableArray array];
    NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject:@"CUSTOMER_ID" forKey:@"key"];
    // [paramDict setObject:@"5620" forKey:@"value"];
    [paramDict setObject: [appD getLoggedInUserId] forKey:@"value"];
    [replacementArray addObject:paramDict];
    [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_WALLETS_LIST Param:nil replacementStrings:replacementArray];
     }
}

// Called this method for wallet list Response.
- (void)processResponsewalletsList : (NSDictionary *) dict

{
    
    [walletFinalDict removeAllObjects];
    [ arrayForBool removeAllObjects];
    [listWalletsHistoryArray removeAllObjects];

    
    NSDictionary *maindDict = [dict valueForKey:@"d"];
    NSArray *filterArray = [[maindDict valueForKey:@"Salon"] valueForKeyPath:@"Id"];
   // NSLog(@"%@", filterArray);
    
    //    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:filterArray];
    //    NSArray *arrayWithoutDuplicates = [orderedSet array];
    //    NSLog(@"%@", arrayWithoutDuplicates);
    
    NSSet *mySet = [NSSet setWithArray:filterArray];
    NSArray *duplicationPrevent = [mySet allObjects];
    
    //NSMutableArray *walletFinalArray = [NSMutableArray new];
    // spliting array.
    for (int i = 0 ; i < duplicationPrevent.count; i++) {
        
        //NSString *salonID = [duplicationPrevent objectAtIndex:i];
        
        NSInteger salonID = [[duplicationPrevent objectAtIndex:i] integerValue];
        NSMutableArray *subArray = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dict in maindDict) {
            //NSLog(@"%d", [[[dict valueForKey:@"Salon"] valueForKey:@"Id"] integerValue]);
            
            if (salonID ==  [[[dict valueForKey:@"Salon"] valueForKey:@"Id"] integerValue]) {
                
                [subArray addObject:dict];
            }
        }
        
        //NSLog(@"%@subarray is",subArray );
        [walletFinalDict setObject:subArray forKey:[NSString stringWithFormat:@"%ld",(long)salonID]];
        
        
        //walletFinalArray = subArray;
    }
    //[subArray removeAllObjects];
    
    //NSLog(@"%@",walletFinalDict);
    
    sectionCount = walletFinalDict.count;
    //NSLog(@" count is = %ld",(long)sectionCount);
    
    noresultsL.hidden = ([walletFinalDict count] > 0);
    listTblV.hidden = ([walletFinalDict count] == 0);
    
    for (int  i = 0; i< walletFinalDict.count; i ++) {
        [arrayForBool addObject:[NSNumber numberWithBool:NO]];
    }
    
    if (walletFinalDict.count > 0) {
        listTblV.delegate = self;
        listTblV.dataSource = self;
        
        [listTblV reloadData];
    }
    
    [HUD hide:YES];
    // NSInteger rowcout;
    
    /*
     //NSArray *newarray = [[dict valueForKey:@"Salon"] valueForKeyPath:@"Id"];
     
     
     //NSLog(@"%@",[dict allKeys]);
     NSDictionary *dicts = [dict valueForKey:@"d"];
     NSLog(@"%@", dicts);
     NSArray *newarray = [[dicts valueForKey:@"Salon"] valueForKeyPath:@"Id"];
     NSLog(@"%@", newarray);
     
     
     NSArray *it = [dict valueForKey:@"d"];
     
     items=[dicts valueForKey:@"Salon"];
     itemsInTable=[[NSMutableArray alloc] init];
     [itemsInTable addObjectsFromArray:it];
     [menuTableView reloadData];*/
    
}


// Called this method for wallest History list and calling service.
-(void) loadWalletsHistoryList{
    
    if ([InternetCheck isOnline]){
        [self hudShowWithMessage:@"Loading"];
        NSMutableArray *replacementArray = [NSMutableArray array];
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:@"CUSTOMER_ID" forKey:@"key"];
        // [paramDict setObject:@"5620" forKey:@"value"];
        [paramDict setObject: [appD getLoggedInUserId] forKey:@"value"];
        [replacementArray addObject:paramDict];
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_WALLETSHISTORY_LIST Param:nil replacementStrings:replacementArray];
    }
    else{
        
    }
}
// Called this method for wallet history Response.
-(void) processResponsewalletsListHistory:(NSDictionary*)dict{
    //NSDictionary *maindDict = [dict valueForKey:@"d"];
    //NSLog(@"%@",maindDict);

    [walletFinalDict removeAllObjects];
    [ arrayForBool removeAllObjects];
    [listWalletsHistoryArray removeAllObjects];
    
    listWalletsHistoryArray = [[ReadData arrayFromDictionary:dict forKey:RESPONSEKEY_DATA] mutableCopy];
    
    if (listWalletsHistoryArray.count > 0) {
        listTblV.hidden = NO;
        noresultsL.hidden = YES;
        [listTblV reloadData];
      
        
    }else{
        listTblV.hidden = YES;
        noresultsL.hidden = NO;
        [listTblV reloadData];
       
    }
    
     [HUD hide:YES];
   
}



#pragma mark - Table view data source

-(CGFloat) calculateheightForRow:(NSInteger)row{
    return 60;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (isWlletListOrHistory) {
        return sectionCount;
    }else
        return 1;
    
    //return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isWlletListOrHistory) {
        if ([[arrayForBool objectAtIndex:section] boolValue])
        {
            NSArray *arr = [walletFinalDict allKeys];
            NSString *strrrr = [arr objectAtIndex:section];
            NSArray *addada = [walletFinalDict objectForKey:strrrr];
            
            return addada.count;
        }
        else
            return 0;
            //return listWalletsHistoryArray.count;

    }else
        return listWalletsHistoryArray.count;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (isWlletListOrHistory) {
         return 60.0f;
    }else
        return 0.0f;
   
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    NSArray *arrkeys = [walletFinalDict allKeys];
    NSString *sectionStr = [arrkeys objectAtIndex:section];
    NSArray *arr = [walletFinalDict objectForKey:sectionStr];
    
    // Individual Section price Caliculation;
    CGFloat price = 0.0f;
    CGFloat sectionPrice = 0.0f;
    for (NSDictionary *priceDict  in arr) {
        if ([priceDict valueForKey:@"Amount"]) {
            price = [[priceDict valueForKey:@"Amount"] floatValue];
        }
        sectionPrice = price + sectionPrice;
    }
    
    //NSLog(@"%ld",(long)sectionPrice);
    
    
    // For showing headder images.
    NSDictionary *dict = [[arr firstObject] valueForKey:@"Salon"];
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,CGRectGetWidth(tableView.frame), 60)];
   //headerView.backgroundColor = [UIColor colorWithRed:174.0/255.0 green:176.0/255.0 blue:183.0/255.0 alpha:1.0];
    //headerView.backgroundColor = [UIColor redColor];

    //headerView.backgroundColor = [UIColor whiteColor];
    // Add image
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.frame = CGRectMake(10, 10, 50, 40);
    
    
    
    
    // ImageView
    NSString *url = [ReadData stringValueFromDictionary:dict forKey:KEY_LOGO_URL];
    if (![url isEqualToString:@""])
    {
        NSURL *imageURL = [NSURL URLWithString:url];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageURL];
        [imageView setImageWithURLRequest:urlRequest
                         placeholderImage:Nil
                                  success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             [imageView setImage:image];
             [UIView animateWithDuration:IMAGE_VIEW_TOGGLE
                                   delay:0 options:UIViewAnimationOptionCurveLinear
                              animations:^(void)
              {
                  
                  //[self.placeholderImgL setAlpha:0 ];
                  [imageView setAlpha:1 ];
              }
                              completion:^(BOOL finished)
              {
              }];
             
             
             
         }
                                  failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
         {
             
         }];
    }
    
    // Add headder title.
    UILabel *headderTitleL = [[UILabel alloc]initWithFrame:CGRectMake(70, 20, 150, 20)];
    //headderTitleL.backgroundColor = [UIColor greenColor];
    headderTitleL.font = [UIFont semiboldFontOfSize:12];
    headderTitleL.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"Name"]];
    
    // Add headder pricingL.
    UILabel *headderPricingL = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetWidth(tableView.frame)-140, 20, 100, 20)];
    //headderPricingL.backgroundColor = [UIColor lightGrayColor];
    headderPricingL.font = [UIFont semiboldFontOfSize:12];
    headderPricingL.textAlignment = NSTextAlignmentRight;
    headderPricingL.textColor = [UIColor redColor];
    headderPricingL.text = [ReadData getAmountFormatted:sectionPrice];
    //headderPricingL.text = [dict valueForKey:@"Name"];
    
    
    //Down arrow images.
    UIImageView *headderDownArrow = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetWidth(tableView.frame)-35, 20, 20, 20)];
    headderDownArrow.image = [UIImage imageNamed:@"icon_wallet_downarrow"];
    
    if ([[arrayForBool objectAtIndex:section] boolValue])
    {
        headderDownArrow.image = [UIImage imageNamed:@"icon_walletuparraow"];
        
    }
    headderDownArrow.tag = section;
     

   
    // Tap Gesture
    UITapGestureRecognizer *headerTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sectionHeaderTapped:)];
    headerTap.view.tag = section;
    [headerView addGestureRecognizer:headerTap];
    headerView.tag = section;
    
     /*
    // expandButton adding.
    UIButton *expandB = [[UIButton alloc]init];
    [expandB setFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 40)];
    //[expandB setTitle:@"click" forState:UIControlStateNormal];
    [expandB setTintColor:[UIColor clearColor]];
    [expandB addTarget:self action:@selector(clickaction:) forControlEvents:UIControlEventTouchUpInside];
    [expandB setTag:section];*/
    
    // controller added to headerView.
    [headerView addSubview:headderPricingL];
    [headerView addSubview:headderTitleL];
    [headerView addSubview:imageView];
    [headerView addSubview:headderDownArrow];
    //[headerView addSubview:expandB];
    return headerView;
}
-(void)clickaction:(UIButton*) sender{
    
    //NSLog(@"%lu",(long)sender.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    //NSLog(@"%lu", (long) indexPath);
    
    if (indexPath.row == 0)
    {
        BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        for (int i=0; i<[walletFinalDict count]; i++)
        {
            if (indexPath.section==i)
            {
                [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!collapsed]];
            }
        }
        
        [listTblV reloadSections:[NSIndexSet indexSetWithIndex:sender.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
        
    }
    
    
}
-(void) sectionHeaderTapped:(UITapGestureRecognizer*)gestureRecognizer{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    //NSLog(@"%lu", (long) indexPath);
    
    if (indexPath.row == 0)
    {
        BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        for (int i=0; i<[walletFinalDict count]; i++)
        {
            if (indexPath.section==i)
            {
                [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!collapsed]];
            }
        }
        
        [listTblV reloadSections:[NSIndexSet indexSetWithIndex:gestureRecognizer.view.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
        
    }

    
}

//- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
//
//    // NSLog(@"%@", [walletFinalArray objectAtIndex:sectionCount]);
//    //return [[walletFinalArray objectAtIndex:sectionCount] valueForKey:@"Salon"];
//    
//    NSArray *arr = [walletFinalArray objectAtIndex:section];
//    NSDictionary *dict = [[arr firstObject] valueForKey:@"Salon"];
//    return [dict valueForKey:@"Name"];
//    
//    //return [[[walletFinalArray objectAtIndex:section] firstObject] valueForKey:@"Salon"][@"Name"];
//}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isWlletListOrHistory) {
        if ([[arrayForBool objectAtIndex:indexPath.section] boolValue]) {
            return [self calculateheightForRow:indexPath.row];
            
        }else
            return 0;

    }else{
        return 80;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    WalletListCell *cell;
    WalletListHistoryCell *celllHistory;
    
    
    if (isWlletListOrHistory) {
       cell = (WalletListCell *)[tableView dequeueReusableCellWithIdentifier:@"walletListCell" forIndexPath:indexPath];
        
        NSArray *allkeysCell = [walletFinalDict allKeys];
        NSString *strkeyCell = [allkeysCell objectAtIndex:indexPath.section];
        NSArray  *arraaddedItemCell = [walletFinalDict objectForKey:strkeyCell];
        NSDictionary *dict = [arraaddedItemCell objectAtIndex:indexPath.row];
        
        // NSArray *arr = [walletFinalArray objectAtIndex:section];
        /*
         // Individual Section price Caliculation;
         CGFloat price = 0.0f;
         CGFloat sectionPrice = 0.0f;
         for (NSDictionary *priceDict  in arr) {
         if ([priceDict valueForKey:@"Amount"]) {
         price = [[priceDict valueForKey:@"Amount"] floatValue];
         }
         sectionPrice = price + sectionPrice;
         }
         
         NSLog(@"%ld",(long)sectionPrice);
         cell.pricingL.text = [ReadData getAmountFormatted:sectionPrice];
         */
        [cell configureDataForCell :dict];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else{
         celllHistory = (WalletListHistoryCell *)[tableView dequeueReusableCellWithIdentifier:@"walletListHistoryCell" forIndexPath:indexPath];
        NSDictionary *historyDict = listWalletsHistoryArray[indexPath.row];
        
        [celllHistory configureDataForCell :historyDict];
         celllHistory.selectionStyle = UITableViewCellSelectionStyleNone;
        return celllHistory;
    
    }
   
    
    //NSString *Title= [[items objectAtIndex:indexPath.row] valueForKey:@"Name"];
    
    
}
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    [arrayForBool replaceObjectAtIndex:indexPath.section withObject:[NSNumber numberWithBool:NO]];
//    
//    [listTblV reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
//}


#pragma mark - API
-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
    if(![ReadData isValidResponseData:response])
    {
        {
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
            [self.view setUserInteractionEnabled:YES];
             [HUD hide:YES];
        }
        return;
    }
    
    if ([apiName isEqualToString:API_GET_WALLETS_LIST]) {
        [self processResponsewalletsList:response];
    }
    if ([apiName isEqualToString:API_GET_WALLETSHISTORY_LIST]){
        [self processResponsewalletsListHistory:response];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - HUD & Delegate methods

- (void)hudShowWithMessage:(NSString *)message
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    HUD.dimBackground = YES;
    HUD.labelText = message;
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    [HUD show:YES];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}





/*
 // new implementation
 
 /////////////////////*****************start***********************
 #pragma mark - Table view data source
 
 - (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
 {
 return 1;
 }
 
 - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
 {
 return [itemsInTable count];
 }
 
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
 {
 NSString *Title= [[items objectAtIndex:indexPath.row] valueForKey:@"Name"];
 
 return [self createCellWithTitle:Title image:[[itemsInTable objectAtIndex:indexPath.row] valueForKey:@"Image name"] indexPath:indexPath];
 }
 /*
 -(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
 {
 NSDictionary *dic=[self.itemsInTable objectAtIndex:indexPath.row];
 if([dic valueForKey:@"SubItems"])
 {
 NSArray *arr=[dic valueForKey:@"SubItems"];
 BOOL isTableExpanded=NO;
 
 for(NSDictionary *subitems in arr )
 {
 NSInteger index=[self.itemsInTable indexOfObjectIdenticalTo:subitems];
 isTableExpanded=(index>0 && index!=NSIntegerMax);
 if(isTableExpanded) break;
 }
 
 if(isTableExpanded)
 {
 [self CollapseRows:arr];
 }
 else
 {
 NSUInteger count=indexPath.row+1;
 NSMutableArray *arrCells=[NSMutableArray array];
 for(NSDictionary *dInner in arr )
 {
 [arrCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
 [self.itemsInTable insertObject:dInner atIndex:count++];
 }
 [self.menuTableView insertRowsAtIndexPaths:arrCells withRowAnimation:UITableViewRowAnimationLeft];
 }
 }
 }
 
 -(void)CollapseRows:(NSArray*)ar
 {
 for(NSDictionary *dInner in ar )
 {
 NSUInteger indexToRemove=[self.itemsInTable indexOfObjectIdenticalTo:dInner];
 NSArray *arInner=[dInner valueForKey:@"SubItems"];
 if(arInner && [arInner count]>0)
 {
 [self CollapseRows:arInner];
 }
 
 if([self.itemsInTable indexOfObjectIdenticalTo:dInner]!=NSNotFound)
 {
 [self.itemsInTable removeObjectIdenticalTo:dInner];
 [self.menuTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:
 [NSIndexPath indexPathForRow:indexToRemove inSection:0]
 ]
 withRowAnimation:UITableViewRowAnimationLeft];
 }
 }
 }
 
 - (UITableViewCell*)createCellWithTitle:(NSString *)title image:(UIImage *)image  indexPath:(NSIndexPath*)indexPath
 {
 NSString *CellIdentifier = @"Cell";
 WalletListCell* cell = [menuTableView dequeueReusableCellWithIdentifier:CellIdentifier];
 UIView *bgView = [[UIView alloc] init];
 bgView.backgroundColor = [UIColor grayColor];
 cell.selectedBackgroundView = bgView;
 cell.lblTitle.text = title;
 cell.lblTitle.textColor = [UIColor blackColor];
 
 [cell setIndentationLevel:[[[itemsInTable objectAtIndex:indexPath.row] valueForKey:@"level"] intValue]];
 cell.indentationWidth = 25;
 
 float indentPoints = cell.indentationLevel * cell.indentationWidth;
 
 cell.contentView.frame = CGRectMake(indentPoints,cell.contentView.frame.origin.y,cell.contentView.frame.size.width - indentPoints,cell.contentView.frame.size.height);
 
 NSDictionary *d1=[itemsInTable objectAtIndex:indexPath.row] ;
 
 
 
 
 if([d1 valueForKey:@"CashbackType"])
 {
 cell.btnExpand.alpha = 1.0;
 [cell.btnExpand addTarget:self action:@selector(showSubItems:) forControlEvents:UIControlEventTouchUpInside];
 }
 else
 {
 cell.btnExpand.alpha = 0.0;
 }
 return cell;
 }
 
 -(void)showSubItems :(id) sender
 {
 UIButton *btn = (UIButton*)sender;
 CGRect buttonFrameInTableView = [btn convertRect:btn.bounds toView:menuTableView];
 NSIndexPath *indexPath = [menuTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
 
 if(btn.alpha==1.0)
 {
 if ([[btn imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"down-arrow.png"]])
 {
 [btn setImage:[UIImage imageNamed:@"up-arrow.png"] forState:UIControlStateNormal];
 }
 else
 {
 [btn setImage:[UIImage imageNamed:@"down-arrow.png"] forState:UIControlStateNormal];
 }
 
 }
 
 NSDictionary *d=[itemsInTable objectAtIndex:indexPath.row] ;
 NSArray *arr=[d valueForKey:@"SubItems"];
 if([d valueForKey:@"SubItems"])
 {
 BOOL isTableExpanded=NO;
 for(NSDictionary *subitems in arr )
 {
 NSInteger index=[itemsInTable indexOfObjectIdenticalTo:subitems];
 isTableExpanded=(index>0 && index!=NSIntegerMax);
 if(isTableExpanded) break;
 }
 
 if(isTableExpanded)
 {
 //[self CollapseRows:arr];
 }
 else
 {
 NSUInteger count=indexPath.row+1;
 NSMutableArray *arrCells=[NSMutableArray array];
 for(NSDictionary *dInner in arr )
 {
 [arrCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
 [itemsInTable insertObject:dInner atIndex:count++];
 }
 [menuTableView insertRowsAtIndexPaths:arrCells withRowAnimation:UITableViewRowAnimationLeft];
 }
 }
 
 
 }
 */

/////////////////////**************************end*****************


/*
 // Response for wallet list.
 - (void) processResponsewalletsList : (NSDictionary *) dict
 {
 NSLog(@"%@",[dict allKeys]);
 NSDictionary *dicts = [dict valueForKey:@"d"];
 NSLog(@"%@", dicts);
 
 items=[dicts valueForKey:@"CashbackType"];
 itemsInTable=[[NSMutableArray alloc] init];
 [itemsInTable addObjectsFromArray:items];
 [menuTableView reloadData];
 [HUD hide:YES];
 
 
 listWalletsArry = [ReadData arrayFromDictionary:dict forKey:RESPONSEKEY_DATA];
 [listTblV reloadData];
 listTblV.hidden = (10 == 0);
 noresultsL.hidden = !listTblV.hidden ;
 
 //[self tabChanged];
 [HUD hide:YES];*/

/*
 //NSDictionary *listDict = [ReadData dictionaryFromDictionary:dict forKey:RESPONSEKEY_DATA];
 // NSLog(@"----- Response is %@", listDict);
 // NSLog(@"----- Response is %@", [listDict allKeys]);
 
 NSArray *storeArray;
 
 //listTblV.scrollEnabled = YES;
 
 //    NSDictionary *dataDict  = [ReadData dictionaryFromDictionary:dict forKey:RESPONSEKEY_DATA];
 //    NSArray *tmpArray = [ReadData arrayFromDictionary:dataDict forKey:@"Services"];
 NSMutableArray *storeName = [[NSMutableArray alloc]init];
 for(NSDictionary *item in listWallets){
 
 NSMutableDictionary *obj = [item objectForKey:@"Salon"];
 [storeName addObject:obj];
 }
 
 NSLog(@"storeName---------%@", storeName);
 
 // split arrray
 
 NSMutableArray *enrichArray = [[NSMutableArray alloc]init];
 NSMutableArray *lemonhArray = [[NSMutableArray alloc]init];
 //NSMutableArray *lemonhArray = [[NSMutableArray alloc]init];
 
 for (NSDictionary *sm in storeName) {
 
 
 if ([[sm valueForKey:@"Name"] isEqualToString:@"Enrich"]) {
 NSMutableDictionary *objs = [sm objectForKey:@"Name"];
 [enrichArray addObject:objs];
 }
 if ([[sm valueForKey:@"Name"] isEqualToString:@"Lemon"]) {
 NSMutableDictionary *objs = [sm objectForKey:@"Name"];
 [lemonhArray addObject:objs];
 }
 }
 
 
 NSLog(@"enrichArray--------- %@", enrichArray);
 NSLog(@" lemonhArray---------- %@", lemonhArray);
 
 
 //[self tabChanged];
 [HUD hide:YES];*/
//}
//// Response for wallets history.
//- (void) processResponsewalletsHistoryList : (NSDictionary *) dict{
//    NSDictionary *dataDict  = [ReadData dictionaryFromDictionary:dict forKey:RESPONSEKEY_DATA];
//    NSArray *tmpArray = [ReadData arrayFromDictionary:dataDict forKey:@"Services"];
//
//    for(NSDictionary *item in tmpArray){
//        NSMutableDictionary *obj = [item mutableCopy];
//        [listWalletsArry addObject:obj];
//    }
//
//    NSLog(@"----- Response is %@", listWalletsArry);
//    NSLog(@"----- countis %lu", (unsigned long)listWalletsArry.count);
//
//    [walletsHistoryVC loadDataWalletsHistoryList:listWalletsArry];
//}




/*
 #pragma mark - HUD & Delegate methods
 
 - (void)hudShowWithMessage:(NSString *)message
 {
 HUD = [[MBProgressHUD alloc] initWithView:self.view];
 [self.view addSubview:HUD];
 
 HUD.dimBackground = YES;
 HUD.labelText = message;
 
 // Regiser for HUD callbacks so we can remove it from the window at the right time
 HUD.delegate = self;
 
 [HUD show:YES];
 }
 
 - (void)hudWasHidden:(MBProgressHUD *)hud {
 // Remove HUD from screen when the HUD was hidded
 [HUD removeFromSuperview];
 HUD = nil;
 }
 */

////////////////////////////////wallet integration/////////////////////////////////////////
/*
 +(NSString*)generateOrderIDWithPrefix:(NSString *)prefix
 {
 srand ( (unsigned)time(NULL) );
 int randomNo = rand(); //just randomizing the number
 NSString *orderID = [NSString stringWithFormat:@"%@%d", prefix, randomNo];
 return orderID;
 }
 -(void)showController:(PGTransactionViewController *)controller
 {
 if (self.navigationController != nil)
 [self.navigationController pushViewController:controller animated:YES];
 else
 [self presentViewController:controller animated:YES
 completion:^{
 
 }];
 }
 
 -(void)removeController:(PGTransactionViewController *)controller
 {
 if (self.navigationController != nil)
 [self.navigationController popViewControllerAnimated:YES];
 else
 [controller dismissViewControllerAnimated:YES
 completion:^{
 }];
 }
 // called, when user clicks on wallets button and connecto to payment wallets page.
 - (IBAction) walletsConnectBPressed:(UIButton *)sender{
 /*{"EMAIL":"varun_barve@ziva.in","MID":"Zivali96954676920199","THEME":"merchant","TXN_AMOUNT":"1.15","ORDER_ID":"54","WEBSITE":"Zivawap","MOBILE_NO":"9730363383","INDUSTRY_TYPE_ID":"Retail120","CHANNEL_ID":"WAP","CUST_ID":"6"}*/
/* }  //Step 1: Create a default merchant config object
 PGMerchantConfiguration *mc = [PGMerchantConfiguration defaultConfiguration];
 
 //Step 2: If you have your own checksum generation and validation url set this here. Otherwise use the default Paytm urls
 mc.checksumGenerationURL = @"https://api.ziva.in/payment/v2/PayTM/BillGenerator";
 mc.checksumValidationURL = @"https://api.ziva.in/payment/v2/PayTM/BillResult";
 
 //Step 3: Create the order with whatever params you want to add. But make sure that you include the merchant mandatory params
 NSMutableDictionary *orderDict = [NSMutableDictionary new];
 //Merchant configuration in the order object
 orderDict[@"MID"] = @"Zivali96954676920199";
 orderDict[@"CHANNEL_ID"] = @"WAP";
 orderDict[@"INDUSTRY_TYPE_ID"] = @"Retail120";
 orderDict[@"WEBSITE"] = @"Zivawap";
 //Order configuration in the order object
 orderDict[@"TXN_AMOUNT"] = @"1.15";
 orderDict[@"ORDER_ID"] = [WalletsViewController generateOrderIDWithPrefix:@""];
 orderDict[@"REQUEST_TYPE"] = @"DEFAULT";
 orderDict[@"CUST_ID"] = @"6";
 
 PGOrder *order = [PGOrder orderWithParams:orderDict];
 
 //Step 4: Choose the PG server. In your production build dont call selectServerDialog. Just create a instance of the
 //PGTransactionViewController and set the serverType to eServerTypeProduction
 [PGServerEnvironment selectServerDialog:self.view completionHandler:^(ServerType type)
 {
 PGTransactionViewController *txnController = [[PGTransactionViewController alloc] initTransactionForOrder:order];
 if (type != eServerTypeNone) {
 txnController.serverType = type;
 txnController.merchant = mc;
 txnController.delegate = self;
 [self showController:txnController];
 }
 }];
 
 }
 
 
 #pragma mark PGTransactionViewController delegate
 
 - (void)didSucceedTransaction:(PGTransactionViewController *)controller
 response:(NSDictionary *)response
 {
 DEBUGLOG(@"ViewController::didSucceedTransactionresponse= %@", response);
 NSString *title = [NSString stringWithFormat:@"Your order  was completed successfully. \n %@", response[@"ORDERID"]];
 [[[UIAlertView alloc] initWithTitle:title message:[response description] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
 
 [self removeController:controller];
 }
 
 - (void)didFailTransaction:(PGTransactionViewController *)controller error:(NSError *)error response:(NSDictionary *)response
 {
 DEBUGLOG(@"ViewController::didFailTransaction error = %@ response= %@", error, response);
 if (response)
 {
 [[[UIAlertView alloc] initWithTitle:error.localizedDescription message:[response description] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
 }
 else if (error)
 {
 [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
 }
 [self removeController:controller];
 }
 
 - (void)didCancelTransaction:(PGTransactionViewController *)controller error:(NSError*)error response:(NSDictionary *)response
 {
 DEBUGLOG(@"ViewController::didCancelTransaction error = %@ response= %@", error, response);
 NSString *msg = nil;
 if (!error) msg = [NSString stringWithFormat:@"Successful"];
 else msg = [NSString stringWithFormat:@"UnSuccessful"];
 
 [[[UIAlertView alloc] initWithTitle:@"Transaction Cancel" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
 [self removeController:controller];
 }
 
 - (void)didFinishCASTransaction:(PGTransactionViewController *)controller response:(NSDictionary *)response
 {
 DEBUGLOG(@"ViewController::didFinishCASTransaction:response = %@", response);
 }
 
 // history.
 
 
 //// Called, this method for wllets history and calling service.
 //-(void) loadWalletsHistory{
 //    //[self hudShowWithMessage:@"Loading"];
 //    NSMutableArray *replacementArray = [NSMutableArray array];
 //    NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
 //    [paramDict setObject:@"CUSTOMER_ID" forKey:@"key"];
 //    [paramDict setObject:@"5620" forKey:@"value"];
 //    [replacementArray addObject:paramDict];
 //
 //    [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_WALLETSHISTORY_LIST Param:nil replacementStrings:replacementArray];
 //
 //   }
 //
 
 */
@end
