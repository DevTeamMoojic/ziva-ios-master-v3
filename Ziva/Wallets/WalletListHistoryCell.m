//
//  WalletListHistoryCell.m
//  Ziva
//
//  Created by Minnarao on 18/2/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "WalletListHistoryCell.h"

@implementation WalletListHistoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void) configureDataForCell : (NSDictionary *) item{

    
    NSDictionary *salonDict = [item valueForKey:@"Salon"];
    
    // imges setting.
    NSString *productType = [ReadData stringValueFromDictionary:item forKey:@"CashbackType"];
    if ([productType isEqualToString:@"Looks"]) {
        [self.mainImgL setImage:[UIImage imageNamed:@"wallet_expanded_looks"]];
        
    } else if ([productType isEqualToString:@"Products"]){
        [self.mainImgL setImage:[UIImage imageNamed:@"wallet_expanded_products"]];
        
    }else if ([productType isEqualToString:@"Cashback"]){
        [self.mainImgL setImage:[UIImage imageNamed:@"wallet_expanded_cashback"]];
        
    }else if ([productType isEqualToString:@"Services"]){
        [self.mainImgL setImage:[UIImage imageNamed:@"wallet_expanded_services"]];
        
    }else if ([productType isEqualToString:@"Packages"]){
        [self.mainImgL setImage:[UIImage imageNamed:@"wallet_expanded_packages"]];
        
    }
    else if ([productType isEqualToString:@"Grooming Points"]){
        [self.mainImgL setImage:[UIImage imageNamed:@"wallet_grooming_points"]];
        
    }

     self.serviceTitleL.text =[ReadData stringValueFromDictionary:salonDict forKey:KEY_NAME];
    self.serviceName.text = [ReadData stringValueFromDictionary:salonDict forKey:KEY_NAME];
   
    //NSString *str  = [ReadData getAmountFormatted:[storePrice floatValue]];
    //NSLog(@"%@",str);
    
    NSString *storePrice = [ReadData stringValueFromDictionary:item forKey:@"AppliedAmount"];
    self.pricingL.text = [ReadData getAmountFormatted:[storePrice floatValue]];
    self.OrderTitleL.text = [ReadData stringValueFromDictionary:item forKey:@"OrderTitle"];
    
    //for Price.
    //Date formate.
    NSString *dateStr = [ReadData stringValueFromDictionary:item forKey:@"OrderDate"];
    NSString *FNSTR = [dateStr stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    //Set formate.
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:FORMAT_DATE_TIME_MILLISECONDS];
    NSDate *date  = [dateFormatter dateFromString:FNSTR];
    // Convert to new Date Format.
    [dateFormatter setDateFormat:@"yyyy-MM-dd h:mm a"];
    NSString *newDateStr = [dateFormatter stringFromDate:date];
    //NSLog(@"%@",newDate);
     self.OrderDateL.text = newDateStr;
   

    
}

@end
