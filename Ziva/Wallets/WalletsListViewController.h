//
//  WalletsListViewController.h
//  Ziva
//
//  Created by Leena on 22/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalletsListViewController : UIViewController

- (void) loadDataWalletList : (NSArray *)list;

@end
