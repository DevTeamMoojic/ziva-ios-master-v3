//
//  WalletListHistoryCell.h
//  Ziva
//
//  Created by Minnarao on 18/2/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalletListHistoryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *mainImgL;
@property (weak, nonatomic) IBOutlet UIImageView *placeholderImgL;
@property (weak, nonatomic) IBOutlet UILabel *serviceTitleL;
@property (weak, nonatomic) IBOutlet UILabel *serviceName;
@property (weak, nonatomic) IBOutlet UILabel *pricingL;
@property (weak, nonatomic) IBOutlet UILabel *OrderTitleL;
@property (weak, nonatomic) IBOutlet UILabel *OrderDateL;

- (void) configureDataForCell : (NSDictionary *) item;

@end
