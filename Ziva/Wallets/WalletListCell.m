//
//  WalletListCell.m
//  Ziva
//
//  Created by Leena on 23/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "WalletListCell.h"
#define FORMAT_DATE_MYTRANSACTION @"dd-MM-yyyy"


@implementation WalletListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

//- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
//{
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//    if (self) {
//        self.lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(15, 10, 320, 20)];
//        self.lblTitle.backgroundColor = [UIColor clearColor];
//        [self.contentView addSubview:self.lblTitle];
//        // Initialization code
//    }
//    return self;
//}


- (void) configureDataForCell : (NSDictionary *) item
{
    //    self.titleL.text = [ReadData stringValueFromDictionary:item forKey:KEY_WTXT_TITLE];
    //    self.pricingL.text = [ReadData stringValueFromDictionary:item forKey:KEY_WTXT_SUB_PRICING];
    
    
    
    // price.
    NSString *strPrice = [ReadData stringValueFromDictionary:item forKey:@"Amount"];
    self.pricingL.text = [ReadData getAmountFormatted:[strPrice floatValue]];
    
    //product type.
    self.cashL.text = [ReadData stringValueFromDictionary:item forKey:@"CashbackType"];
    NSString *productType = [ReadData stringValueFromDictionary:item forKey:@"CashbackType"];
    
    
    NSString *dateStr = [ReadData stringValueFromDictionary:item forKey:@"ValidTill"];
  //Set formate.
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:FORMAT_DATE_TIME_MILLISECONDS];
    NSDate *date = [dateFormatter dateFromString:dateStr];
    [dateFormatter setDateFormat:@"yyyy-MM-dd h:mm a"];
    NSString *newDateStr = [dateFormatter stringFromDate:date];
    //NSLog(@"%@",newDate);
    self.expDateL.text = [NSString stringWithFormat:@"Expires on %@",newDateStr];
    
    // self.expDateL.text = [NSString stringWithFormat:@"Expires on %@",dateStr];
    //self.expDateL.text = dateStr;
    
    // Get immage view.
    // NSDictionary *imgaDict = [item valueForKey:@"Salon"];
    
    if ([productType isEqualToString:@"Looks"]) {
        [self.mainImgL setImage:[UIImage imageNamed:@"wallet_expanded_looks"]];
        
    } else if ([productType isEqualToString:@"Products"]){
        [self.mainImgL setImage:[UIImage imageNamed:@"wallet_expanded_products"]];
        
    }else if ([productType isEqualToString:@"Cashback"]){
        [self.mainImgL setImage:[UIImage imageNamed:@"wallet_expanded_cashback"]];
        
    }else if ([productType isEqualToString:@"Services"]){
        [self.mainImgL setImage:[UIImage imageNamed:@"wallet_expanded_services"]];
        
    }else if ([productType isEqualToString:@"Packages"]){
        [self.mainImgL setImage:[UIImage imageNamed:@"wallet_expanded_packages"]];
        
    }else if ([productType isEqualToString:@"Grooming Points"]){
        [self.mainImgL setImage:[UIImage imageNamed:@"wallet_grooming_points"]];
        
    }
    /*
     // ImageView
     NSString *url = [ReadData stringValueFromDictionary:imgaDict forKey:KEY_LOGO_URL];
     if (![url isEqualToString:@""])
     {
     NSURL *imageURL = [NSURL URLWithString:url];
     NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageURL];
     [self.mainImgL setImageWithURLRequest:urlRequest
     placeholderImage:Nil
     success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
     [self.mainImgL setImage:image];
     [UIView animateWithDuration:IMAGE_VIEW_TOGGLE
     delay:0 options:UIViewAnimationOptionCurveLinear
     animations:^(void)
     {
     
     [self.placeholderImgL setAlpha:0 ];
     [self.mainImgL setAlpha:1 ];
     }
     completion:^(BOOL finished)
     {
     }];
     
     
     
     }
     failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
     {
     
     }];
     }
     */
}


@end
