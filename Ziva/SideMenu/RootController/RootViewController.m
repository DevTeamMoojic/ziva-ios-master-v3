//  RootViewController.m
//  Ziva
//
//  Created by  Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.

#import "RootViewController.h"
#import "LeftMenuViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (void)awakeFromNib
{
    self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentController"];
    self.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"leftMenuController"];
    self.delegate = self;
}

#pragma mark - Handle update notifications for Profile & Location information

- (void) locationUpdated{
    LeftMenuViewController *leftVC = (LeftMenuViewController *) self.leftMenuViewController;
    [leftVC locationUpdated];
}

- (void) profileUpdated
{
    LeftMenuViewController *leftVC = (LeftMenuViewController *) self.leftMenuViewController;
    [leftVC profileUpdated];
}

- (void) backToHome {
    LeftMenuViewController *leftVC = (LeftMenuViewController *) self.leftMenuViewController;
    [leftVC backToHome];
}

// Called this method when payment successfull completed and redirect to Mytrasaction Page.
- (void) backToMyTransaction{
    LeftMenuViewController *leftVC = (LeftMenuViewController *) self.leftMenuViewController;
    [leftVC backToMyTransaction];
}

#pragma mark RESideMenu Delegate
    
- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController
{
}

- (void)sideMenu:(RESideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController
{
    //NSLog(@"didShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController
{
    //NSLog(@"willHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController
{
   // NSLog(@"didHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}


@end
