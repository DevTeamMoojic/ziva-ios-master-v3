//
//  LeftMenuCell.m
//  Ziva
//
//  Created by  Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.

#import "LeftMenuCell.h"

@implementation LeftMenuCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    // Fix : Autoresizing issues for iOS 7 using Xcode 6
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) toogleSelectMenuOption : (BOOL) selected
{
    self.selectRowB.selected = selected;
    self.menuB.selected = selected;
    if(selected){
        self.backV.backgroundColor = [UIColor blackColor];
        self.titleL.textColor = [UIColor colorWithRed:255.0/255 green:181.0/255 blue:16.0/255 alpha:1];
    }
    else{
        self.backV.backgroundColor = [UIColor clearColor];
        self.titleL.textColor = [UIColor whiteColor];
    }
}

- (NSString *) getMenuDisplayTitle : (NSString *) menuOption
{
    NSString *retVal = @"";
    if([menuOption isEqualToString: MENU_MY_PACKAGES])
    {
        retVal = @"My Packages";
    }
    else if([menuOption isEqualToString: MENU_MY_TRANSACTIONS])
    {
        retVal = @"My Transactions";
    }
    else if([menuOption isEqualToString: MENU_WALLETS])
    {
        retVal = @"Wallets";
    }
    else if([menuOption isEqualToString: MENU_COUPONS])
    {
        retVal = @"Coupons";
    }
    else if([menuOption isEqualToString: MENU_PRODUCTS])
    {
        retVal = @"Products";
    }
//    else if([menuOption isEqualToString: MENU_NOTIFICATIONS])
//    {
//        retVal = @"Notification";
//    }
    else if([menuOption isEqualToString: MENU_CONTACT_US])
    {
        retVal = @"Contact us";
    }
    else if([menuOption isEqualToString: MENU_RATE_US])
    {
        retVal = @"Rate us";
    }
    
    return retVal;
}

- (NSString *) getMenuIconTitle : (NSString *) menuOption
{
    NSString *retVal = @"";
    if([menuOption isEqualToString: MENU_MY_PACKAGES])
    {
        retVal = @"icon_mypackages";
    }
    else if([menuOption isEqualToString: MENU_MY_TRANSACTIONS])
    {
        retVal = @"icon_mytrans";
    }
    else if([menuOption isEqualToString: MENU_WALLETS])
    {
        retVal = @"icon_wallets";
    }
    else if([menuOption isEqualToString: MENU_COUPONS])
    {
        retVal = @"icon_coupons";
    }
    else if([menuOption isEqualToString: MENU_PRODUCTS])
    {
        retVal = @"icon_products";
    }
//    else if([menuOption isEqualToString: MENU_NOTIFICATIONS])
//    {
//        retVal = @"icon_mynotification";
//    }
    else if([menuOption isEqualToString: MENU_CONTACT_US])
    {
        retVal = @"icon_contactus";
    }
    else if([menuOption isEqualToString: MENU_RATE_US])
    {
        retVal = @"icon_rateus";
    }
    
    return retVal;
}

- (void) configureDataForCell : (NSString *) menuoption
{
    self.titleL.text = [self getMenuDisplayTitle:menuoption];
    [self.menuB setImage:[UIImage imageNamed: [NSString stringWithFormat:@"%@_unselected",[self getMenuIconTitle:menuoption]]] forState:UIControlStateNormal];
    [self.menuB setImage:[UIImage imageNamed: [NSString stringWithFormat:@"%@_selected",[self getMenuIconTitle:menuoption]]] forState:UIControlStateSelected];
}

@end
