//
//  LeftMenuViewController.m
//  Ziva
//
//  Created by  Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.


#import "LeftMenuCell.h"
#import "LeftMenuViewController.h"
#import "MyProfileViewController.h"
#import "MyPackagesViewController.h"
#import "MyTransactionsViewController.h"
#import "WalletsViewController.h"
#import "CouponsViewController.h"
#import "ProductsViewController.h"
#import "NotificationViewController.h"
#import "ContactUsViewController.h"
#import "HomeViewController.h"
#import "MyPackagesViewController.h"

@interface LeftMenuViewController ()
{
    AppDelegate *appD;
    
    __weak IBOutlet UIView *myContainerV;
    __weak IBOutlet UIImageView *profileImgV;
    __weak IBOutlet UITableView *menuOptionsTblV;
    
    __weak IBOutlet UILabel *myLocationL;
    __weak IBOutlet UILabel *profileNameL;
    __weak IBOutlet UILabel *subtitleL;
    __weak IBOutlet UILabel *versionNoL;
    
    NSInteger selectedRow;
}

@property (strong, readwrite, nonatomic) UITableView *tableView;

@end

@implementation LeftMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    menuTitles = [[NSArray alloc]init];
    
    menuTitles = @[
                   MENU_MY_PACKAGES,
                   MENU_MY_TRANSACTIONS,
                   MENU_WALLETS,
                   MENU_COUPONS,
                   MENU_PRODUCTS,
                  // MENU_NOTIFICATIONS,
                   MENU_CONTACT_US,
                   MENU_RATE_US
                   ];
    
    selectedRow = -1;
    
    myContainerV.frame = [UpdateFrame setSizeForView:myContainerV usingWidth:self.view.center.x + 100]; //contentViewInPortraitOffsetCenterX - ReSideMenu
    

    NSString *isDevBuild = @"";
    if([[appD getAppEnvironment] isEqualToString:ENVIRONMENT_DEV]){
        isDevBuild = @"(DEV)";
    }
    
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *buildNo = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];

    versionNoL.text = [NSString stringWithFormat:@"Version : %@    Build : %@ %@",version, buildNo, isDevBuild];
    
    myLocationL.text = @"Detecting Location ..";
    [self profileUpdated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

#pragma mark - Handle update notifications for Profile & Location information

- (void) locationUpdated{
    myLocationL.text = [appD getUserCurrentLocationAddress];
}

- (void) profileUpdated{
    profileNameL.text = [appD getLoggedInUserDisplayName];
    subtitleL.text = [appD getLoggedInUserMobileNumber];
    
    NSString *url = [appD getLoggedInUserProfileURL];
    // URL Check
    if (![url isEqualToString:@""])
    {
        NSURL *imageURL = [NSURL URLWithString:url];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageURL cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:60 ];
        
        [profileImgV setImageWithURLRequest:urlRequest
                           placeholderImage:nil
                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             if(image)
             {
                 [profileImgV setImage:image];
                 [UIView animateWithDuration:IMAGE_VIEW_TOGGLE
                                       delay:0 options:UIViewAnimationOptionCurveLinear
                                  animations:^(void)
                  {
                      
                      [profileImgV setAlpha:1 ];
                  }
                                  completion:^(BOOL finished)
                  {
                  }];
             }
             
             
             
         }
                                    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
         {
             
         }];
    }
    
}

- (void) backToHome{
    selectedRow = -1;
    [menuOptionsTblV reloadData];
    
    UINavigationController *navigationController = (UINavigationController *)self.sideMenuViewController.contentViewController;
    [navigationController popToRootViewControllerAnimated:YES];
    UIViewController *vc = [[navigationController viewControllers] firstObject];
    if([[vc class] isEqual:[HomeViewController class]])
    {
        HomeViewController *homeVC = (HomeViewController *)vc;
        [homeVC resetLayout];
    }
}

// Called this method when payment successfull completed and redirect to Mytrasaction Page.
- (void) backToMyTransaction{
    UINavigationController *navigationController  = (UINavigationController *)self.sideMenuViewController.contentViewController;
   // NSArray *arr1 = [navigationController viewControllers];
   // NSLog(@"%@", arr1);

    UIViewController *Vc = [[navigationController viewControllers] firstObject];
    if([[Vc class] isEqual:[HomeViewController class]])
    {
        MyTransactionsViewController *mt =[self.storyboard instantiateViewControllerWithIdentifier:@"transactionsVC"];
        [navigationController pushViewController:mt animated:YES];
        //[self presentViewController:mt animated:YES completion:nil];
    }

   
}


#pragma mark - Methods

- (void)menuBPressed: (NSString *) menuDescription
{
    UINavigationController *navigationController = (UINavigationController *)self.sideMenuViewController.contentViewController;
    
    
    if([menuDescription isEqualToString:MENU_SIGNOUT])
    {
        [appD logoutUser];
    }
    else if([menuDescription isEqualToString:MENU_MY_PROFILE])
    {
        MyProfileViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"showmyprofileVC"];
        
        [navigationController pushViewController:profileVC animated:YES];
        
    }
    else if([menuDescription isEqualToString:MENU_COUPONS])
    {
        CouponsViewController *couponsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"showcouponsVC"];
        
        [navigationController pushViewController:couponsVC animated:YES];
    }
    else if([menuDescription isEqualToString:MENU_PRODUCTS])
    {
        ProductsViewController *productsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"showproductsVC"];
        
        [navigationController pushViewController:productsVC animated:YES];
    }
    else if([menuDescription isEqualToString:MENU_CONTACT_US])
    {
        ContactUsViewController *contactusVC = [self.storyboard instantiateViewControllerWithIdentifier:@"showcontactusVC"];
        
        [navigationController pushViewController:contactusVC animated:YES];
        
    }else if([menuDescription isEqualToString:MENU_MY_PACKAGES])  // My Package implementation.

    {
        MyPackagesViewController *packageVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MyPackagesViewController"];
        [navigationController pushViewController:packageVC animated:YES];
    }

    // new changes minna
    else if([menuDescription isEqualToString:MENU_MY_TRANSACTIONS])
    {
    MyTransactionsViewController *transactionsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"transactionsVC"];
    
    [navigationController pushViewController:transactionsVC animated:YES];
    }

    else if([menuDescription isEqualToString:MENU_WALLETS])
    {
        WalletsViewController *couponsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"showWalletsVC"];
        
        [navigationController pushViewController:couponsVC animated:YES];
        
    }
//    else if([menuDescription isEqualToString:MENU_PRODUCTS])
//    {
//        ProductsViewController *productsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"showproductsVC"];
//        
//        [navigationController pushViewController:productsVC animated:YES];
//    }
//    
    else if([menuDescription isEqualToString:MENU_CONTACT_US])
    {
        ContactUsViewController *contactusVC = [self.storyboard instantiateViewControllerWithIdentifier:@"showcontactusVC"];
        
        [navigationController pushViewController:contactusVC animated:YES];
        
    }
    
    else if([menuDescription isEqualToString:MENU_RATE_US])
    {
        NSString *iTunesLink = @"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=1031579277&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8";
        //NSString *iTunesLink = @"itms://itunes.apple.com/app/id1031579277";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
    }
    [self.sideMenuViewController hideMenuViewController];
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [menuTitles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    
    LeftMenuCell *cell = (LeftMenuCell *) [tableView dequeueReusableCellWithIdentifier:@"leftmenucell" forIndexPath:indexPath];
    
    [cell configureDataForCell:menuTitles[row]];
    [cell toogleSelectMenuOption:(indexPath.row == selectedRow)];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    selectedRow = indexPath.row;
    LeftMenuCell *cell = (LeftMenuCell *)[tableView cellForRowAtIndexPath:indexPath];
    if(!cell.selectRowB.selected)
    {
        [cell toogleSelectMenuOption:YES];
        [self menuBPressed:menuTitles[indexPath.row]];
    }
    [cell toogleSelectMenuOption:NO];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LeftMenuCell *cell = (LeftMenuCell *)[tableView cellForRowAtIndexPath:indexPath];
    [cell toogleSelectMenuOption:NO];
}

#pragma mark - Events

- (IBAction)logoutBPressed:(UIButton *)sender
{
    [self menuBPressed:MENU_SIGNOUT];
}


- (IBAction)editProfileBPressed:(UIButton *)sender
{
    [self menuBPressed:MENU_MY_PROFILE];
}


@end
