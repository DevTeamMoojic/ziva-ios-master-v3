//
//  FeaturedSalonCell.m
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "FeaturedSalonCell.h"

@implementation FeaturedSalonCell

- (void)awakeFromNib {
    // Initialization code
    
    [super awakeFromNib];
    
    // Fix : Autoresizing issues for iOS 7 using Xcode 6
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //    Fix : For right aligned view with autoresizing
    UIView *backV = [[self.contentView subviews] objectAtIndex:0];
    backV.frame = [UpdateFrame setSizeForView:backV usingSize:self.bounds.size];
}

- (NSString *) getAddress : (NSDictionary *) item{
    NSString *str = @"";
    str = [ReadData stringValueFromDictionary:item forKey:KEY_SALON_ADDRESS];
    return str;
}

- (void) configureDataForCell : (NSDictionary *) item
{
    self.mainImgV.image = Nil;
    self.mainImgV.alpha = 0;
    self.placeholderImgV.alpha=1;
    
    
    self.titleL.text = [ReadData stringValueFromDictionary:item forKey:KEY_NAME];
    self.ratingsL.text = [ReadData ratingsInPointsFromDictionary:item forKey:KEY_RATING];
    self.distanceL.text = [ReadData distanceInKmFromDictionary:item forKey:KEY_DISTANCE];
    self.addressL.text = [self getAddress:item];
    
    CGFloat tmpHeight = [ResizeToFitContent getHeightForText:self.addressL.text usingFontType:FONT_SEMIBOLD FontOfSize:11 andMaxWidth:self.lblMaxWidth];
    self.addressL.frame = [UpdateFrame setSizeForView:self.addressL usingHeight:tmpHeight];
    
    self.distanceL.frame = [UpdateFrame setPositionForView:self.distanceL usingPositionY: (CGRectGetMaxY(self.addressL.frame) + 2)];
    self.ratingsV.frame = [UpdateFrame setPositionForView:self.distanceL usingPositionY: CGRectGetMaxY(self.distanceL.frame) ];

    // ImageView
    NSDictionary *salonDict =  [ReadData dictionaryFromDictionary: item forKey:KEY_SALON];
    NSString *url = [ReadData stringValueFromDictionary:salonDict forKey:KEY_SALON_LOGOURL];
    if (![url isEqualToString:@""])
    {
        NSURL *imageURL = [NSURL URLWithString:url];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageURL];
        [self.mainImgV setImageWithURLRequest:urlRequest
                             placeholderImage:Nil
                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             [self.mainImgV setImage:image];
             [UIView animateWithDuration:IMAGE_VIEW_TOGGLE
                                   delay:0 options:UIViewAnimationOptionCurveLinear
                              animations:^(void)
              {
                  
                  [self.placeholderImgV setAlpha:0 ];
                  [self.mainImgV setAlpha:1 ];
              }
                              completion:^(BOOL finished)
              {
              }];
             
             
             
         }
                                      failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
         {
             
         }];
    }
}

@end
