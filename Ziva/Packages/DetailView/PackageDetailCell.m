//
//  PackageDetailCell.m
//  Ziva
//
//  Created by Minnarao on 22/2/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "PackageDetailCell.h"

@implementation PackageDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    /*
    // Fix : Autoresizing issues for iOS 7 using Xcode 6
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //    Fix : For right aligned view with autoresizing
    UIView *backV = [[self.contentView subviews] objectAtIndex:0];
    backV.frame = [UpdateFrame setSizeForView:backV usingSize:self.bounds.size];*/
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void) configureDataForCell : (NSDictionary *) item{
    
   // NSLog(@"%@",[item allKeys]);
    self.pkgBNameL.text = [ReadData stringValueFromDictionary:item forKey:KEY_NAME];
    self.pkgBPriceL.text = [ReadData getAmountFormatted:[[ReadData stringValueFromDictionary:item forKey:KEY_SERVICE_PRICE] floatValue]];
    self.pkgBSubTitleL.text = [ReadData stringValueFromDictionary:item forKey:@"SubTitle"];
    

}

// For My package Detatil.
- (void) configureMyPackageDetailDataForCell : (NSDictionary *) item{
    // NSLog(@"%@",[item allKeys]);
    self.pkgBNameL.text = [ReadData stringValueFromDictionary:item forKey:KEY_NAME];
    self.pkgBPriceL.text = [ReadData getAmountFormatted:[[ReadData stringValueFromDictionary:item forKey:KEY_SERVICE_PRICE] floatValue]];
    self.pkgBSubTitleL.text = [ReadData stringValueFromDictionary:item forKey:@"SubTitle"];
    
    NSArray *arrService = [ReadData arrayFromDictionary:item forKey:CART_TYPE_SERVICES];
    NSArray *arrProduct = [ReadData arrayFromDictionary:item forKey:CART_TYPE_PRODUCTS];

    
    if (arrService.count>0) {
        
        self.serviceAndProductL.text = [NSString stringWithFormat:@"Total no. of Services: %lu",(long)arrService.count];
    }else{
       self.serviceAndProductL.text = [NSString stringWithFormat:@"Total no. of Products: %lu",(long)arrProduct.count];
    }
    
    self.remaintingQuantityL.text = [NSString stringWithFormat:@"%@/%@",[ReadData stringValueFromDictionary:item forKey:@"RemainingQuantity"],[ReadData stringValueFromDictionary:item forKey:@"IntialQuantity"]];

}

@end
