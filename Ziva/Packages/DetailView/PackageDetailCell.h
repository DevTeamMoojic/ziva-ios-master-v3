//
//  PackageDetailCell.h
//  Ziva
//
//  Created by Minnarao on 22/2/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PackageDetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *pkgBNameL;
@property (weak, nonatomic) IBOutlet UILabel *pkgBPriceL;
@property (weak, nonatomic) IBOutlet UILabel *pkgBSubTitleL;
@property (weak, nonatomic) IBOutlet UIButton *minusB;
@property (weak, nonatomic) IBOutlet UIButton *plusB;
@property (weak, nonatomic) IBOutlet UILabel *countL;

    
    // For My Pacakge Details.
@property (weak, nonatomic) IBOutlet UILabel *remaintingQuantityL;
@property (weak, nonatomic) IBOutlet UILabel *serviceAndProductL;
    
    
- (void) configureDataForCell : (NSDictionary *) item;
- (void) configureMyPackageDetailDataForCell : (NSDictionary *) item;

@end
