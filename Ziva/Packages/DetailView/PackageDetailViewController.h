//
//  PackageViewController.h
//  Ziva
//
//  Created by Minnarao on 22/2/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PackageDetailViewController : UIViewController

- (void) loadDetailView : (NSDictionary *) dict;

@end
