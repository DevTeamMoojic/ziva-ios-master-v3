//
//  PackageDetailViewController.m
//  Ziva
//
//  Created by Bharat on 24/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "PackageDetailViewController.h"
#import "PackageDetailCell.h"
#import "CartSummaryViewController.h"
#import "CartDisplayViewController.h"

@interface PackageDetailViewController ()<UITableViewDelegate,UITableViewDataSource,CartSummaryDelegate>
{
    __weak IBOutlet UIView *optionsV;    
    __weak IBOutlet UILabel *screenTitleL;
    
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIView *topImageV;
    __weak IBOutlet UIImageView *mainImgV;
    
    __weak IBOutlet UIScrollView *scrollviewOnTop;
    
    __weak IBOutlet UIScrollView *tabcontentscrollView;
    
    __weak IBOutlet UIScrollView *mainscrollView;
    
    __weak IBOutlet UITableView *listTblV;
    __weak IBOutlet UILabel *noresultsL;
    
    
    CGRect cachedImageViewSize;
    CGFloat yscrollOffset;
    
    BOOL isBackFromOtherView;
    BOOL isContentSizeUpdated;
    BOOL isUpdating;
    
    NSUInteger pageIndex;
    NSInteger pageWidth;
    NSInteger pageHeight;
    
    CGFloat contentHeight;
    
    NSDictionary *dataDict;
    
    AppDelegate *appD;
    MyCartController *cartC;
    
    //

    NSArray * packageBundelArr;
    UILabel *prodcutCountL;
    
    
    __weak CartDisplayViewController *cartdisplayVC;
    
    // For cart summary.
    __weak IBOutlet UIView *cartsummaryCV;
    __weak CartSummaryViewController *cartsummaryVC;
     NSInteger quantity;
    
    NSMutableArray *arrayL;
}
@end

@implementation PackageDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;
     [cartC initForPackages];
    listTblV.delegate = self;
    listTblV.dataSource = self;
   
    arrayL = [[NSMutableArray alloc]init];
    
    [self initZeroLabel];
    
    if(packageBundelArr.count > 0){
        listTblV.hidden = NO;
        noresultsL.hidden = YES;
        [listTblV reloadData];
    }else{
        listTblV.hidden = YES;
        noresultsL.hidden = NO;
    }
    quantity = 0;
    //[self setupLayout];
    
    //This condition for to pointing to Address page if the it is "P"
    if ([[dataDict valueForKey:@"PackageFor"] isEqualToString:@"P"]) {
        appD.sessionDelegate.isCommingFromviewController = @"frompackageVC";
    }else{
        appD.sessionDelegate.isCommingFromviewController = @"";
        
    }
    // For Package always Yes to User can user both cash operation.
    cartC.isCashPaymentAllowed = YES;

}

// Making lables according to customeCell.
-(void) initZeroLabel{
    for (int i= 0 ; i < packageBundelArr.count; i++) {
        [arrayL addObject:[NSNumber numberWithInteger:0]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!isBackFromOtherView)
        cachedImageViewSize = mainImgV.frame;
    else
        isBackFromOtherView = NO;
    
    //quantity = [cartC getQuantityInCart:record_Id];
    [self updateQuantityHeading];
    [cartsummaryVC updateSummary];
}

- (void) updateQuantityHeading
{
    cartsummaryCV.hidden = ([cartC.cartItems count] == 0);
    //quantityL.text = [NSString stringWithFormat:@"%ld",(long) quantity];
}

#pragma mark - Public Methods

- (void) loadDetailView : (NSDictionary *) dict
{
    dataDict = dict;
    
       //NSLog(@"%@", [dataDict allKeys]);
    
    NSArray * bundelArr = [dict valueForKey:@"PackageBundles"];
    packageBundelArr = bundelArr;
  //NSLog(@"%@", bundelArr);
    
}




#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 150.0f;
    
}


// custom view for header. will be adjusted to default or specified header height
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,CGRectGetWidth(tableView.frame), 150)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    
    // condition Apply
    // Package bundel title
    UILabel *conditionL = [[UILabel alloc]initWithFrame:CGRectMake(tableView.frame.size.width-120, 8, 100, 20)];
    conditionL.text = @"Conditions Apply";
    conditionL.textColor = [ UIColor lightGrayColor];
    //conditionL.backgroundColor = [UIColor redColor];
    conditionL.font = [UIFont regularFontOfSize:12.0f];

   // Package bundel title
    UILabel *packageNameL = [[UILabel alloc]initWithFrame:CGRectMake(10, 28, 250, 20)];
    packageNameL.text = [NSString stringWithFormat:@"%@",[ReadData stringValueFromDictionary:dataDict forKey:KEY_CTXN_TITLE]];
   // packageNameL.backgroundColor = [UIColor redColor];
    packageNameL.font = [UIFont boldFontOfSize:14.0f];
    
    /*
    // Package cash back
    UILabel *packageCashBackL = [[UILabel alloc]initWithFrame:CGRectMake(10, 48, 250, 20)];
    packageCashBackL.backgroundColor = [UIColor redColor];
    packageCashBackL.font = [UIFont regularFontOfSize:12.0f];*/
    
    // Package StartingPrice.
    UILabel *packageStartingPriceL = [[UILabel alloc]initWithFrame:CGRectMake(10, 50, 250, 20)];
    //packageStartingPriceL.backgroundColor = [UIColor redColor];
    
    //packageStartingPriceL.text =[NSString stringWithFormat:@"Starting @ %@", [ReadData getAmountFormatted:[[ReadData stringValueFromDictionary:dataDict forKey:KEY_PACKAGE_STARTINGPRCE] floatValue]]];
    packageStartingPriceL.text =[ReadData getAmountFormatted:[[ReadData stringValueFromDictionary:dataDict forKey:KEY_PACKAGE_STARTINGPRCE] floatValue]];
       packageStartingPriceL.font = [UIFont regularFontOfSize:12.0f];
    packageStartingPriceL.textColor = [UIColor redColor];
    
   // [ReadData stringValueFromDictionary:dataDict forKey:KEY_PACKAGE_STARTINGPRCE];
    
    // line.
    UIView *LineV = [[UIView alloc] initWithFrame:CGRectMake(10,80,CGRectGetWidth(tableView.frame)-20, 1)];
    LineV.backgroundColor = [UIColor lightGrayColor];

    // Package DescriptionTitleL.
    UILabel *packageDescriptionTitleL = [[UILabel alloc]initWithFrame:CGRectMake(10, 90, 250, 20)];
    packageDescriptionTitleL.text = @"Description";
    packageDescriptionTitleL.textColor = [UIColor redColor];
    packageDescriptionTitleL.font = [UIFont boldFontOfSize:14.0f];
    
    UIView *underline = [[UIView alloc] initWithFrame:CGRectMake(10,110,70, 2)];
    underline.backgroundColor = [UIColor redColor];
    
    
//    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
//    packageDescriptionTitleL.attributedText = [[NSAttributedString alloc] initWithString:packageDescriptionTitleL.text attributes:underlineAttribute];
    
    /*
    NSAttributedString * title =   [[NSAttributedString alloc] initWithString:@"Desctiption"
                                                                   attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle)}];
    [packageDescriptionTitleL setAttributedText:title];
    */

    
//    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:@"Description"];
//    [attributeString addAttribute:NSUnderlineStyleAttributeName
//                            value:[NSNumber numberWithInt:1]
//                            range:(NSRange){0,[attributeString length]}];
//    [packageDescriptionTitleL setAttributedText:attributeString];
    //packageDescriptionTitleL.attributedText = attributeString;
//
    // Package Description downe titleL.
    UILabel *packageDescriptionL = [[UILabel alloc]initWithFrame:CGRectMake(10, 110, 250, 30)];
    packageDescriptionL.textColor = [UIColor lightGrayColor];
    packageDescriptionL.font = [UIFont regularFontOfSize:12.0f];
    packageDescriptionL.text = [ReadData stringValueFromDictionary:dataDict forKey:KEY_CTXN_TITLE];
    

    /*
    // cash back
    NSArray *cashbacks = [ReadData arrayFromDictionary:dataDict forKey:KEY_CASHBACKS];
    //CGFloat positionY = (CGRectGetHeight(self.priceInfoV.frame) - CGRectGetHeight(self.offerpriceL.frame)) *0.5;
    
    // if the looks is cash back else to discount values.
    if([cashbacks count] > 0){
        
        // diffrenciate the amount and percent.
        NSArray *cashbackArray = [[ReadData arrayFromDictionary:dataDict forKey:KEY_CASHBACKS] firstObject];
        //NSLog(@"%@", cashbackArray);
        NSInteger priceMode = [[cashbackArray valueForKey:@"Mode"] integerValue];
        // NSLog(@"%ld", (long)priceMode);
        // NSString *priceMode1 = [cashbackArray valueForKey:@"Cost"];
        //NSLog(@"%@", priceMode1);
        
        // if the it 0 the values is cash eles the percentage.
        if (priceMode == 0) {
            packageCashBackL.text = [NSString stringWithFormat:@"Get %@ Cash Back.",[ReadData getAmountFormatted:[[cashbackArray valueForKey:@"Cost"] floatValue]]];
        }else{
            NSString *precent = @"%";
            packageCashBackL.text = [NSString stringWithFormat:@"Get %@%@ Cash Back",[cashbackArray valueForKey:@"Cost"],precent];
        }
        
    }
    else{ // cash back
        
        
    }
   
    
    
    
    // Diffrencitate the cash.
    if ([cashbacks count] > 0 && ![ReadData stringValueFromDictionary:dataDict forKey:KEY_PACKAGE_STARTINGPRCE]) {
        packageCashBackL.hidden = NO;
        packageStartingPriceL.hidden = NO;
        //self.startPriceL.frame = self.startPriceL.frame;
        
    }else if ([ReadData stringValueFromDictionary:dataDict forKey:KEY_PACKAGE_STARTINGPRCE]){
        packageStartingPriceL.hidden = NO;
        packageCashBackL.hidden = YES;
        //self.startPriceL.frame = self.cashBackPriceL.frame;
        //CGRectMake(self.cashBackPriceL.frame.origin.x, self.cashBackPriceL.frame.origin.y, <#CGFloat width#>, <#CGFloat height#>)
    }
        
//    }else if ([ReadData stringValueFromDictionary:dataDict forKey:KEY_PACKAGE_STARTINGPRCE]){
//        self.startPriceL.hidden = YES;
//        self.cashBackPriceL.hidden = NO;
//    }
     */
    
    [headerView addSubview: conditionL];
    [headerView addSubview:packageNameL];
    [headerView addSubview:underline];
    [headerView addSubview:packageStartingPriceL];
    [headerView addSubview:LineV];
    [headerView addSubview:packageDescriptionTitleL];
    [headerView addSubview:packageDescriptionL];
    return headerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   return packageBundelArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PackageDetailCell *cell = (PackageDetailCell *)[tableView dequeueReusableCellWithIdentifier:@"packageDetailCell" forIndexPath:indexPath];
    NSDictionary *item = packageBundelArr[indexPath.row];
    
    // Minaus Button
    cell.minusB.tag = indexPath.row;
    [cell.minusB addTarget:self action:@selector(minusBAction:) forControlEvents:UIControlEventTouchUpInside];
    
    // Plus Button
    cell.plusB.tag = indexPath.row;
    [cell.plusB addTarget:self action:@selector(plusBAction:) forControlEvents:UIControlEventTouchUpInside];
    
    NSLog(@"%@",prodcutCountL.text);
    
    //cell.countL.text = prodcutCountL.text;
   cell.countL.text = [NSString stringWithFormat:@"%i",[[arrayL objectAtIndex:indexPath.row] intValue]];
    [cell configureDataForCell:item];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
 }


// Called, when user click on Plus button on table cell.
-(void) plusBAction:(UIButton*)sender{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:listTblV];
    NSIndexPath *indexPath = [listTblV indexPathForRowAtPoint:buttonPosition];
    
    //NSLog(@"%d",[[arrayL objectAtIndex:indexPath.row] intValue]);
   // NSLog(@"quantity = %ld",(long)quantity);
   
    
    // For the UIlable count will not go down.
    if ([[arrayL objectAtIndex:indexPath.row] intValue] >= 0) {
        
        if ([[arrayL objectAtIndex:indexPath.row] intValue] < 9) {
            [arrayL replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithInteger:[[arrayL objectAtIndex:indexPath.row] intValue] + 1]];
            [listTblV reloadData];
            
            //NSLog(@"quantity = %ld",(long)quantity);
            
            NSDictionary *PkgDictP = packageBundelArr[indexPath.row];
            
            if(quantity < MAX_REPEAT_QUANTITY){
                [cartC addPackages:PkgDictP];
                [cartsummaryVC updateSummary];
                quantity++;
                [self updateQuantityHeading];
            }

        }
        
        
        //NSLog(@"%@", PkgDictP);
        
        // Reset the table size when the Product count is increase by one.
        if (quantity == 1) {
            listTblV.frame = CGRectMake(listTblV.frame.origin.x,listTblV.frame.origin.y, listTblV.frame.size.width, listTblV.frame.size.height -80);
        }
   }
    
    
}
// Called, when user clicks on the Minaus buttom from table view cell.
-(void) minusBAction:(UIButton*)sender{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:listTblV];
    NSIndexPath *indexPath = [listTblV indexPathForRowAtPoint:buttonPosition];
    //NSLog(@"%d",[[arrayL objectAtIndex:indexPath.row] intValue]);
    
    // For the UIlable count will not go down.
    if ([[arrayL objectAtIndex:indexPath.row] intValue] > 0) {
        [arrayL replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithInteger:[[arrayL objectAtIndex:indexPath.row] intValue] - 1]];
        [listTblV reloadData];
        
        
        //NSLog(@"%@", PkgDictM);
        //NSLog(@"quantity = %ld",(long)quantity);
        NSDictionary *PkgDictM = packageBundelArr[indexPath.row];
        // decrease the package count.
        if(quantity > 0){
            
            [cartC lessPackages:PkgDictM];
            [cartsummaryVC updateSummary];
            quantity--;
            [self updateQuantityHeading];
        }
        /*
        // Reset the table size when the Product count is zero.
        if ([[arrayL objectAtIndex:indexPath.row] intValue] == 0) {
            listTblV.frame = CGRectMake(listTblV.frame.origin.x,listTblV.frame.origin.y, listTblV.frame.size.width, height);
        }*/
        if (quantity == 0) {
            listTblV.frame = CGRectMake(listTblV.frame.origin.x,listTblV.frame.origin.y, listTblV.frame.size.width, listTblV.frame.size.height +80);
        }

    }else{
        
        
    }
}




/*
// Proceeding button
-(IBAction)proceedAction:(id)sender{
    
    UIStoryboard *modalStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CartDisplayViewController *LDSSVC = [modalStoryboard instantiateViewControllerWithIdentifier:@"CartDisplayViewController"];
    [self.navigationController pushViewController:LDSSVC animated:YES];
   
}*/

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"showcartsummary_packages"]){
        cartsummaryVC = (CartSummaryViewController *)segue.destinationViewController;
        cartsummaryVC.delegate = self;
    }
    
}

#pragma mark - Cart Summary Delegate

- (void) takeActionOnCart{
        isBackFromOtherView = YES;
        [self performSegueWithIdentifier:@"showcart_packages" sender:Nil];
}

#pragma mark - Events

// Called, when user tap on Back button.
- (IBAction)backBPressed:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}



//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
//{
//    isBackFromOtherView = YES;
//    NSDictionary *item = packageArray[indexPath.row];
//    [self showPackageBudle_DetailView:item];
//}

//- (void) showPackageBudle_DetailView : (NSDictionary *) item{
//    [self performSegueWithIdentifier:@"show_packageDetatils" sender:item];
//}

/*
#pragma mark - Layout

-(void) resetLayout;
{
    mainscrollView.contentOffset = CGPointZero;
    contentHeight = CGRectGetHeight(self.view.bounds);
    
}
- (void) setupLayout
{
     pageWidth = CGRectGetWidth(self.view.bounds);
    
    //    addToCartV.layer.borderWidth = 0.5f;
    //    addToCartV.layer.borderColor= [UIColor colorWithRed:137.0/255 green:147.0/255 blue:151.0/255 alpha:0.6].CGColor;
    //
    [self.view addGestureRecognizer:scrollviewOnTop.panGestureRecognizer];
    [mainscrollView removeGestureRecognizer:mainscrollView.panGestureRecognizer];
    
    //Position elements
    {
        topImageV.frame = [UpdateFrame setSizeForView:topImageV usingHeight:[ResizeToFitContent getHeightForParallaxImage:self.view.frame]];
        
        yscrollOffset = CGRectGetHeight(topImageV.frame) - IMAGE_SCROLL_OFFSET_Y - CGRectGetHeight(optionsV.frame);
        mainImgV.frame = topImageV.frame ;
        
        containerView.frame = [UpdateFrame setSizeForView:containerView usingHeight:(CGRectGetHeight(self.view.bounds) + yscrollOffset)];
        
        tabcontentscrollView.frame= [UpdateFrame setPositionForView:tabcontentscrollView usingPositionY: CGRectGetMaxY(topImageV.frame)];
        tabcontentscrollView.frame = [UpdateFrame setSizeForView:tabcontentscrollView usingHeight: CGRectGetHeight(containerView.frame) - CGRectGetMaxY(topImageV.frame)];
    }
    
    
    CGPoint scrollviewOrigin = scrollviewOnTop.frame.origin;
    scrollviewOnTop.scrollIndicatorInsets = UIEdgeInsetsMake(-scrollviewOrigin.y, 0, scrollviewOrigin.y, scrollviewOrigin.x);
}

#pragma mark - Methods

- (void) setData
{
    contentHeight = 0;
    if(dataDict)
    {
        
        // ImageView
        NSString *url = [ReadData stringValueFromDictionary:dataDict forKey:KEY_IMAGE_URL];
        if (![url isEqualToString:@""])
        {
            NSURL *imageURL = [NSURL URLWithString:url];
            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageURL];
            [ mainImgV setImageWithURLRequest:urlRequest
                             placeholderImage:Nil
                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
             {
                 [ mainImgV setImage:image];
                 [UIView animateWithDuration:IMAGE_VIEW_TOGGLE
                                       delay:0 options:UIViewAnimationOptionCurveLinear
                                  animations:^(void)
                  {
                      
                      [ mainImgV setAlpha:1 ];
                  }
                                  completion:^(BOOL finished)
                  {
                  }];
                 
                 
                 
             }
                                      failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
             {
                 
             }];
        }
        
        contentHeight = 800;
    }
    
    if(contentHeight <= CGRectGetHeight(mainscrollView.frame)){
        contentHeight = CGRectGetHeight(mainscrollView.bounds);
        mainscrollView.scrollEnabled = NO;
    }
    
    [self tabChanged];
}

- (void) tabChanged
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        CGPoint offset = [self getScrollContentOffset];
        offset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:offset animated:NO];
    }
    isContentSizeUpdated = NO;
}


#pragma mark - Events

- (IBAction)backBPressed:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Expand on Scroll animation

- (CGSize) setContentSize
{
    CGFloat height = CGRectGetHeight(mainImgV.frame) + contentHeight;
    return CGSizeMake(pageWidth, height);
}

- (void) setScrollContentOffset: (CGPoint) offset
{
    mainscrollView.contentOffset = offset;
}

- (CGPoint) getScrollContentOffset
{
    return [mainscrollView contentOffset];
}

- (void) resetOffsetAllTabs
{
    CGPoint offset = CGPointZero;
    [self setScrollContentOffset : offset];
}

-(void) updateContentSize : (CGPoint) contentOffset;
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        contentOffset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    }
    [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    
    isContentSizeUpdated = NO;
}

#pragma mark - Scroll Events

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == scrollviewOnTop)
    {
        {
            if (isContentSizeUpdated)  return;
            
            CGRect scrolledBoundsForContainerView = containerView.bounds;
            if (scrollView.contentOffset.y <= yscrollOffset)
            {
                
                scrolledBoundsForContainerView.origin.y = scrollView.contentOffset.y ;
                containerView.bounds = scrolledBoundsForContainerView;
                
                //Reset offset for all tabs
                if (scrollView.contentOffset.y <= 0) [self resetOffsetAllTabs];
                
                
                CGFloat y = -scrollView.contentOffset.y;
//                CGFloat alphaLevel = 1;
//                CGFloat BLUR_MAX_Y = IMAGE_SCROLL_OFFSET_Y + CGRectGetHeight(topImageV.frame) - CGRectGetMinY(optionsV.frame);
//                if (fabs(y) < BLUR_MAX_Y)
//                {
//                    alphaLevel = (BLUR_MAX_Y - fabs(y))/(BLUR_MAX_Y);
//                }
//                else
//                {
//                    alphaLevel = 0;
//                }
//                
                //[screenTitleL setAlpha:alphaLevel];
                if (y > 0)
                {
                    mainImgV.frame = CGRectInset(cachedImageViewSize, 0, -y/2);
                    mainImgV.center = CGPointMake(mainImgV.center.x, mainImgV.center.y + y/2);
                }
                else
                {
                    mainImgV.frame = [UpdateFrame setPositionForView:mainImgV usingPositionY:y];
                }
                return;
            }
            
            scrolledBoundsForContainerView.origin.y = yscrollOffset ;
            containerView.bounds = scrolledBoundsForContainerView;
            
            [self setScrollContentOffset:CGPointMake(0, scrollView.contentOffset.y - yscrollOffset)  ];
        }
    }
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}
*/

@end
