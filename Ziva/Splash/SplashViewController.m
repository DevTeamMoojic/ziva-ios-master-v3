//
//  SplashViewController.m
//  Ziva
//
//  Created by Bharat on 30/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "SplashViewController.h"
#import "ServiceRatingViewController.h"

@interface SplashViewController ()<ServiceRatingDelegate,MBProgressHUDDelegate,APIDelegate>
{
    AppDelegate *appD;
    MBProgressHUD *HUD;
    int rating;
    NSDictionary *serviceRatingDict;
}
@end

//typedef void(^myCompletion)(BOOL);

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //appD = (AppDelegate *)[UIApplication sharedApplication].delegate;
     appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     
//    [self myMethod:^(BOOL finished) {
//        if (finished) {
//        [self performSelector:@selector(showNextView) withObject:nil afterDelay:0.3];
//           rating = 0;
//        }
//    }];
    
    //[self performSelector:@selector(showNextView) withObject:nil afterDelay:0.3];
    //rating = 0;
   
    
    if ([appD isLoggedIn]) {
         [self callingGetServiceRating];
    
    }else{
         [self performSelector:@selector(showNextView) withObject:nil afterDelay:0.3];
        
    }
    
    //[self performSelector:@selector(showNextView) withObject:nil afterDelay:0.3];
}


//-(void)myMethod:(myCompletion) compblock{
//    [self callingServiceRating];
//    compblock(YES);
//}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Methods

- (void)showNextView
{
    
    {
        BOOL isLoggedIn =  [appD isLoggedIn];
        
        if (isLoggedIn)
        {
            if (rating == 0) {
                ServiceRatingViewController *serviceRating = [self.storyboard instantiateViewControllerWithIdentifier:@"ServiceRatingViewController"];
                serviceRating.delegate = self;
                //NSLog(@"adadsfa %@", serviceRatingDict);
                [serviceRating loadServiceRatingDetails:serviceRatingDict];
                [self presentViewController:serviceRating animated:YES completion:nil];
                
            }else{
                
                [self showLandingView];

            }
             //[self showLandingView];
        }
        else
        {
            [self showLogin];
        }
    }
}

- (void) forceUserToUpdateApp
{
    [appD forceAppUpdate];
}

- (void)showLandingView
{
    [appD registerForRemoteNotification];
    if ([[appD getLoggedInUserGender] isEqualToString:@""]) {
        [appD showChooseGenderVC];
    }
    else
    {
        [appD showHomeVC:NO];
    }
}

- (void)showLogin
{
    [appD showSlideshowVC];
}

- (void)showOfflineAlert
{
    UIAlertView *networkAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE_ERROR message:NETWORKERROR delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [networkAlert show];
}

// new implementation for service Rating functionality on 13/ 04/2017.
#pragma mark service rating Delegate methods.

-(void)serviceRatingSubmit{
    [self showLandingView];
    
}
-(void) serviceRatingCancel{
    [self showLandingView];
    
}


#pragma mark  Calling GET Rating service.
-(void) callingGetServiceRating{
    if ([InternetCheck isOnline])
        {
           // [self hudShowWithMessage:@"Loading"];
            
            NSMutableArray *replacementArray = [NSMutableArray array];
            NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
            paramDict = [NSMutableDictionary dictionary];
            
            [paramDict setObject:@"CUSTOMER_ID" forKey:@"key"];
            
            [paramDict setObject: [appD getLoggedInUserId] forKey:@"value"];
            [replacementArray addObject:paramDict];
            
            [[[APIHelper alloc] initWithDelegate:self] callAPI:API_SERVICE_RATING Param:nil replacementStrings:replacementArray];
            
        }
        else{
            [self showErrorMessage:NETWORKERROR];
        }
  
}

- (void) processResponse_serviceRating : (NSDictionary *)dict{
    NSLog(@"service Dict is = %@", dict.allKeys);
    
    // null checking.
    if (dict == NULL|| dict == nil || dict.count == 0) {
        [self showLandingView];
        
    }else{
        
//        ServiceRatingViewController *SRV = [[ServiceRatingViewController alloc]init];
//        [SRV loadServiceRatingDetails:dict];
        
        serviceRatingDict = dict;
        NSString *str = [dict valueForKey:@"Rating"];
        //NSLog(@"%@",str);

        rating = [str intValue];
       // NSLog(@"%d",rating);
        [self performSelector:@selector(showNextView) withObject:nil afterDelay:0.3];
        
    }
    
   
}

# pragma mark API Delegate methods.
- (void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName{
    
    if(![ReadData isValidResponseData:response])
    {
        {
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
            [self.view setUserInteractionEnabled:YES];
            [HUD hide:YES];
        }
        return;
    }
    
    if ([apiName isEqualToString:API_SERVICE_RATING]) {
        [self processResponse_serviceRating:[ReadData dictionaryFromDictionary:response forKey:RESPONSEKEY_DATA]];
    }

}

#pragma mark - HUD & Delegate methods

- (void)hudShowWithMessage:(NSString *)message
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    HUD.dimBackground = YES;
    HUD.labelText = message;
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    [HUD show:YES];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}

#pragma mark alert message delegate methods.
- (void) showErrorMessage : (NSString *)message
{
    UIAlertView *errorNotice = [[UIAlertView alloc] initWithTitle:ALERT_TITLE_ERROR message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorNotice show];
}


@end
