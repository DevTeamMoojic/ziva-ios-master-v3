//  NSDictionary+JSON.h
//  JSON
//
//  Created by  Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.

#import <Foundation/Foundation.h>

@interface NSDictionary (JSON)

+ (NSDictionary* )dictionaryWithContentsOfJSONURLString: (NSString* )urlAddress;
- (BOOL)isConvertibleToJSON;
- (NSData* )toJSON;

@end
