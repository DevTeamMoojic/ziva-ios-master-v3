//
//  ResizeToFitContent.m
//  WowTables
//
//  Created by Bharat on 03/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Ltd. All rights reserved.
//

#import "ResizeToFitContent.h"

@implementation ResizeToFitContent

+ (CGFloat)getWidthForText:(NSString *) content usingFontType:(NSString *) fontType FontOfSize:(CGFloat) fontSize andMaxWidth:(CGFloat) maxWidth;
{
    CGFloat rowWidth = maxWidth;
    UILabel *displayTextLbl = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, FLT_MAX, fontSize + 2)];
    [displayTextLbl setFont:[UIFont getFontType:fontType andSize:fontSize]];
    displayTextLbl.numberOfLines = 0;
    displayTextLbl.text = content;
    [displayTextLbl sizeToFit];
    if (CGRectGetWidth(displayTextLbl.frame) < rowWidth){
        rowWidth = CGRectGetWidth(displayTextLbl.frame);
    }
    return rowWidth;
}


+ (CGFloat)getHeightForText:(NSString *) content usingFontType:(NSString *) fontType FontOfSize:(CGFloat) fontSize andMaxWidth:(CGFloat) maxWidth
{
    CGFloat rowHeight = fontSize + 3;
    UILabel *displayTextLbl = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, maxWidth , rowHeight)];
    [displayTextLbl setFont:[UIFont getFontType:fontType andSize:fontSize]];
    displayTextLbl.lineBreakMode = NSLineBreakByWordWrapping;
    displayTextLbl.numberOfLines = 0;
    displayTextLbl.text = content;
    [displayTextLbl sizeToFit];
    if (CGRectGetHeight(displayTextLbl.frame) > rowHeight)
    {
        rowHeight = CGRectGetHeight(displayTextLbl.frame);
    }
    return rowHeight;
}


+ (CGFloat)getHeightForNLines : (NSInteger) noOfLines ForText:(NSString *) content usingFontType:(NSString *) fontType FontOfSize:(CGFloat) fontSize andMaxWidth:(CGFloat) maxWidth;
{
    CGFloat rowHeight = fontSize+3;
    UILabel *displayTextLbl = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, maxWidth , rowHeight)];
    [displayTextLbl setFont:[UIFont getFontType:fontType andSize:fontSize]];
    displayTextLbl.lineBreakMode = NSLineBreakByWordWrapping;
    displayTextLbl.numberOfLines = noOfLines;
    displayTextLbl.text = content;
    [displayTextLbl sizeToFit];
    if (CGRectGetHeight(displayTextLbl.frame) > rowHeight)
    {
        rowHeight = CGRectGetHeight(displayTextLbl.frame);
    }
    return rowHeight;
}


+ (CGFloat) getHeightForParallaxImage : (CGRect ) viewBounds{
    
//    CGFloat designScreenHeight = 667;
//    CGFloat designHeight = 230;
//    
//    CGFloat translatedHeight = ceil(designHeight * CGRectGetHeight(viewBounds)/designScreenHeight);
//    return (int)translatedHeight;
    return 130;
}

+ (CGFloat) getHeightForDetailViewImage : (CGRect ) viewBounds{
    
    CGFloat translatedHeight = ceil(0.6 * CGRectGetHeight(viewBounds));
    
    return (int)translatedHeight;
    
}

@end
