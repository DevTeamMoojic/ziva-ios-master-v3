//  InternetCheck.m
//  Ziva
//
//  Created by  Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.

#import "InternetCheck.h"

@implementation InternetCheck

+ (BOOL)isOnline
{
    ReachabilityV *internetReach = [ReachabilityV reachabilityWithHostName:@"www.apple.com"];
    NetworkStatus internetStatus = [internetReach currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}

@end
