//
//  APIHelper.m
//   
//
//  Created by Bharat on 01/06/2016.
//  Copyright (c) 2013 Bharat. All rights reserved.
//

#import "APIHelper.h"

#import "NSMutableDictionary+JSON.h"

#import "PlistHelper.h"

@interface APIHelper ()
{
    NSString *apiName;
    NSString *apiSuffix;
    // For Whoami
    BOOL isWhoamiCall;
    
    NSDictionary *plist;
    NSDictionary *api;
    
    NSString    *apiDomain;
    NSString    *apiString;
    NSURL       *apiURL;
//    NSURLSession       *apiURL;
    NSString    *apiMethod;
    BOOL        isFullURLPath;
    BOOL        hasParameter;
    BOOL        isSecured;
    NSString    *apiParam;
    NSMutableDictionary *apiPostParam;

    NSString    *apiKey;
    
    NSArray     *replacementStrings;
    
    NSMutableData           *responseData;
    NSMutableDictionary     *response;
    NSString                *message;
    NSString                *apiTokenKey;
    
    AppDelegate *appD;
    

}
@end

@implementation APIHelper
@synthesize apiDelegate;

- (id)initWithDelegate:(id<APIDelegate>)delegateObject
{
    if (self = [super init])
    {
        self.apiDelegate = delegateObject;
        [self initData];
    }
    
    return self;
    //checking for.nfghffjhfjj123
}

- (void)initData
{
    response = [NSMutableDictionary new];
    apiSuffix = @"";
    apiTokenKey = @"";
    
    isSecured = NO;
    
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (void)callAPI:(NSString *)apiName2 WithKey:(NSString *)keyId Param:(id)apiParam2
{
    apiSuffix = keyId;
    [self callAPI:apiName2 Param:apiParam2];
}

- (void)callAPI:(NSString *)apiName2 Param:(id)apiParam2 replacementStrings: (NSArray *) replaceKeyValues
{
    replacementStrings = replaceKeyValues;
    [self callAPI:apiName2 Param:apiParam2];
}

- (void)callAPI:(NSString *)apiName2 Param:(id)apiParam2 replacementStrings: (NSArray *) replaceKeyValues apiTokenKey:(NSString *)token;
{
    replacementStrings = replaceKeyValues;
    apiTokenKey = token;
    [self callAPI:apiName2 Param:apiParam2];
}

- (void)callAPI:(NSString *)apiName2 Param:(id)apiParam2
{
    apiName = apiName2;
    
    api = [[PlistHelper new] getAPI:apiName];
    apiDomain = [[[PlistHelper new] getDictionaryForPlist:@"APIs"] objectForKey:@"apiDomain"];
    
    if([api objectForKey:@"isSecured"])
        isSecured = [[api objectForKey:@"isSecured"] boolValue];
    
    isFullURLPath = [[api objectForKey:@"isFullURLPath"] boolValue];
    if (isFullURLPath)
        apiString   = [api objectForKey:@"apiURL"];
    else
        apiString   = [apiDomain stringByAppendingString:[api objectForKey:@"apiURL"]];
    
    if (replacementStrings) {
        for (NSDictionary *item in replacementStrings)
        {
            apiString =  [apiString stringByReplacingOccurrencesOfString:item[@"key"] withString:item[@"value"]];
        }
    }
    
    apiMethod   = [api objectForKey:@"apiMethod"];
    hasParameter= [[api objectForKey:@"hasParameter"] boolValue];
    
    // METHOD
    // POST
    if ([apiMethod isEqualToString:@"POST"] || [apiMethod isEqualToString:@"PUT"] )
    {
        apiParam = nil;
        apiPostParam = apiParam2;
    }
    // GET or DELETE
    else
    {
        if (hasParameter)
        {
            apiParam = [apiParam2 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        }
    }
    
    // GET or DELETE
    if (([apiMethod isEqualToString:@"GET"] || [apiMethod isEqualToString:@"DELETE"]) && apiParam != nil)
        apiString = [NSString stringWithFormat:@"%@?%@", apiString, apiParam];
    
    NSLog(@"---the url is %@",apiString);
    apiURL = [NSURL URLWithString:apiString];

    [self callAPI];
}

// Called when Whoami Successful
- (void)callAPI
{
    // Mark as not Whoami Call
    // isWhoamiCall = NO;
    
    // REQUEST
    // Create Request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:apiURL cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:120];

    
    // Method
    [request setHTTPMethod:apiMethod];
    
    //accessToken
    if (isSecured)
    {
//        [request setValue:[appD getDeviceIdentifier] forHTTPHeaderField:@"X-WOW-DEVICE"];
//        [request setValue:[appD getUserAccessToken] forHTTPHeaderField:@"X-WOW-TOKEN"];
//        [request setValue:[appD preferredCityId] forHTTPHeaderField:@"X-WOW-CITY"];
    }
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // Body
    if ([apiMethod isEqualToString:@"POST"] || [apiMethod isEqualToString:@"PUT"])
    {
        if ([apiPostParam isConvertibleToJSON])
        {
            NSData *postData = [apiPostParam toJSON];
            [request setHTTPBody:postData];
            
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            //[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        }
    }
    
    // Clear response
    responseData = nil;
    
    // Send Request
    [NSURLConnection connectionWithRequest:request delegate:self];
    
    if (YES)
    {
//        if(conn)
//            NSLog(@"Connection Success - %@", apiName);
//        else
//            NSLog(@"Connection FAIL - %@", apiName);
    }
}


#pragma mark - Connection Delegate Fx
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self gotResponse:error];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (responseData == nil)
        responseData = [[NSMutableData alloc] initWithData:data];
    else
        [responseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [connection cancel];
    [self gotResponse:nil];
}


#pragma mark - Fx

-(NSMutableDictionary *) raiseErrorMessage : (NSString *) msg
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:[NSNumber numberWithInt:1] forKey:RESPONSEKEY_STATUS];
    [dict setValue:msg forKey:RESPONSEKEY_ERRORMESSAGE];
    
    return dict;
}

- (void)gotResponse:(NSError *)error
{
    if(error){
        response =  [self raiseErrorMessage: [NSString stringWithFormat:@"%@ If you haven't, please enable cellular data for this app or connect with a wifi connection.", error.localizedDescription]];
    }
    else{
        // Check result
        
        if (responseData)
        {
            response = [NSJSONSerialization JSONObjectWithData:responseData
                                                       options:NSJSONReadingMutableContainers
                                                         error:&error];
            if (error) {
                response = [self raiseErrorMessage: [NSString stringWithFormat:@"%@", error.localizedDescription]];
                NSLog(@"%@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
            }
        }
        else
        {
            response = [self raiseErrorMessage:[NSString stringWithFormat:@"An error occurred while processing the request:%@ ", apiName]];
        }
        
    }
    
    if (apiDelegate != nil && [apiDelegate respondsToSelector:@selector(response:forAPI:)])
    {
        [[self apiDelegate] response:response forAPI:apiName];
    }
}

//////////////////////////////////****Location service start ****///////////
// For User location.
-(void)GetUserLocationUpdatesbasedOnSearchString:(NSString*)SearchString{
    
    apiName = USERSEARCH_LOCATION;
    apiString = USERSEARCH_LOCATION;
    
    apiMethod   = @"GET";//[api objectForKey:@"apiMethod"];
    hasParameter= [[api objectForKey:@"hasParameter"] boolValue];
    
    // GET or DELETE
    if ([apiMethod isEqualToString:@"GET"])
        apiString = [NSString stringWithFormat:@"%@%@", apiString, SearchString];
    
    NSString *localString = [apiString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    
    NSURL *locaURL = [NSURL URLWithString:localString];
    NSLog(@"LOC URL: %@",locaURL);
    
    NSLog(@"---the url is %@",localString);
    apiURL = [NSURL URLWithString:localString];
    
    [self callAPI];

}

// For User location latitude and longitude.
-(void)GetUserLocationLatitudeandLongitude:(NSString*)placeID{
    
    apiName = API_GETUSERSEARCHLOCATION_GOOGPLACEURL;
    apiString = API_USERSEARCHLOCATION_GOOGLEPLACEAPIKEY;
    
    apiMethod   = @"GET";//[api objectForKey:@"apiMethod"];
    hasParameter= [[api objectForKey:@"hasParameter"] boolValue];
    
    // GET or DELETE
    if ([apiMethod isEqualToString:@"GET"])
        apiString = [NSString stringWithFormat:@"%@%@%@", apiName,placeID,apiString];
    
    NSLog(@"---the url is %@",apiString);
    apiURL = [NSURL URLWithString:apiString];
    
    [self callAPI];
    
   
//#define USERSEARCHLOCATION_GOOGPLACEURL @"https://maps.googleapis.com/maps/api/place/details/json?placeid="
//#define USERSEARCHLOCATION_GOOGLEPLACEAPIKEY @"&key=AIzaSyB9EGTVrfa3O8Hb7_DuH8fzPCKuZSPhizE"
//https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJx9Lr6tqZyzsRwvu6koO3k64&key=AIzaSyB9EGTVrfa3O8Hb7_DuH8fzPCKuZSPhizE
}

//////////////////////////////////**** End Location service ****///////////


@end
