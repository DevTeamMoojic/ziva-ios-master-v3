//
//  APIHelper.h
//   
//
//  Created by  Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APIDelegate.h"

// for testing..
@interface APIHelper : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate>

@property (nonatomic, weak) id<APIDelegate> apiDelegate;


- (id)initWithDelegate:(id<APIDelegate>)delegateObject;
- (void)callAPI:(NSString *)apiName Param:(id)apiParam ;
- (void)callAPI:(NSString *)apiName WithKey:(NSString *)keyId Param:(id)apiParam;
- (void)callAPI:(NSString *)apiName2 Param:(id)apiParam2 replacementStrings: (NSArray *) replaceKeyValues;

// For User location.
-(void)GetUserLocationUpdatesbasedOnSearchString:(NSString*)SearchString;
// For User location latitude and longitude.
-(void)GetUserLocationLatitudeandLongitude:(NSString*)placeID;

@end
