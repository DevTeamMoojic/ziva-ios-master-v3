//
//  PlistHelper.h
//
//
//  Created by  Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.

#import <UIKit/UIKit.h>

@interface PlistHelper : NSObject

- (NSMutableDictionary *)getMutableDictionaryForPlist:(NSString *)plistName;

- (NSDictionary *)getDictionaryForPlist:(NSString *)plistName;
- (void)saveDictionary:(NSMutableDictionary *)plistDict toPlist:(NSString *)plistName;

- (NSDictionary *)getAPI:(NSString *)apiName;
- (NSMutableDictionary *)getData:(NSString *)plistName;
- (void)saveData:(NSMutableDictionary *)data toPlist:(NSString *)plistName;

- (UIImage *)getImage:(NSString *)imageName;
- (void)saveImage:(UIImage *)image WithName:(NSString *)imageName;

- (void)deleteAllData;

- (void)deletePlist:(NSString *)plistName;

- (NSMutableDictionary *)getDataFromFileNameWithExtension:(NSString *)fileName;
- (NSMutableArray *)getFileList:(NSString *)filePattern;

@end
