//
//  LookDetailViewController.h
//  Ziva
//
//  Created by Bharat on 24/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LookDetailViewController : UIViewController

- (void) loadDetailView : (NSDictionary *) dict;

@end
