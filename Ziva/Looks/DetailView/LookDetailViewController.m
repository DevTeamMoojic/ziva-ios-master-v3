//
//  LookDetailViewController.m
//  Ziva
//
//  Created by Bharat on 24/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "BeautyServicesSelectorViewController.h"
#import "GalleryViewController.h"
#import "LookDetailViewController.h"
#import "MBProgressHUD.h"
#import "UserAddAddressViewController.h"
#import "ChooseUserLocationViewController.h"

@interface LookDetailViewController ()<BeautyServicesSelectorDelegate,MBProgressHUDDelegate>
{
    __weak IBOutlet UIView *optionsV;
    __weak IBOutlet UILabel *screenTitleL;
    
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIView *topImageV;
    __weak IBOutlet UIImageView *mainImgV;
    
    __weak IBOutlet UIScrollView *scrollviewOnTop;
    
    __weak IBOutlet UIScrollView *tabcontentscrollView;
    
    __weak IBOutlet UIScrollView *mainscrollView;
    
    __weak IBOutlet UILabel *titleL;
    __weak IBOutlet UILabel *otherInfoL;
    __weak IBOutlet UILabel *startingPriceL;
    
    
    __weak IBOutlet UIView *descriptionCV;
    __weak IBOutlet UILabel *descriptionL;
    
    __weak IBOutlet UIView *footerV;
    
    __weak IBOutlet UIView *backV;
    
    __weak IBOutlet UIView *beautyservicesCV;
    __weak BeautyServicesSelectorViewController *beautyservicesVC;
    
    NSArray *containerViews;
    
    CGRect cachedImageViewSize;
    CGFloat yscrollOffset;
    
    BOOL isBackFromOtherView;
    BOOL isContentSizeUpdated;
    BOOL isUpdating;
    
    NSUInteger pageIndex;
    NSInteger pageWidth;
    NSInteger pageHeight;
    
    CGFloat contentHeight;
    
    NSDictionary *dataDict;
    
    AppDelegate *appD;
    MyCartController *cartC;
    MBProgressHUD *HUD;

    
    __weak IBOutlet UIImageView *storeLogoImgV;
    __weak IBOutlet UILabel *storetitleL;
    __weak IBOutlet UILabel *ExclusiveORNewL;
    __weak IBOutlet UIView *ExclusiveORNewV;

    __weak IBOutlet UILabel *ExclusiveLogoL;
    
    // For Scrolling images
    NSMutableArray *looksIMGArray;
    
    // For checking saloon value.
    BOOL IssaloonValues;
    // The looks which type is(i.e for Saloonservice type and home service).
    NSString *saloonType;
    

    //
    __weak IBOutlet UILabel *silverPriceL;
    __weak IBOutlet UILabel *silverPriceStrikeL;
    __weak IBOutlet UILabel *goldPriceL;
    __weak IBOutlet UILabel *DiscountCouponL;


}
@end

@implementation LookDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;
    [ExclusiveLogoL sizeToFit];
    [self setupLayout];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!isBackFromOtherView)
        cachedImageViewSize = mainImgV.frame;
    else
        isBackFromOtherView = NO;
}

#pragma mark - Public Methods

- (void) loadDetailView : (NSDictionary *) dict
{
    dataDict = dict;
    IssaloonValues = NO;
    saloonType = @"";
}

#pragma mark - Layout

-(void) resetLayout;
{
    mainscrollView.contentOffset = CGPointZero;
    contentHeight = CGRectGetHeight(self.view.bounds);
    
}


- (void) setupLayout
{
    containerViews = @[beautyservicesCV];
    
    pageWidth = CGRectGetWidth(self.view.bounds);
    
    [self.view addGestureRecognizer:scrollviewOnTop.panGestureRecognizer];
    [mainscrollView removeGestureRecognizer:mainscrollView.panGestureRecognizer];
    
    //Position elements
    {
        //[ResizeToFitContent getHeightForDetailViewImage:self.view.frame]
        topImageV.frame = [UpdateFrame setSizeForView:topImageV usingHeight:topImageV.frame.size.width/0.8];
        
        yscrollOffset = ceil(CGRectGetHeight(topImageV.frame)* 0.5);
        mainImgV.frame = topImageV.frame ;
        /*
         CGFloat width = image.size.width;
         CGFloat height = image.size.height;
         NSLog(@"%f     %f     %f",width,height,width/height);
         NSLog(@"%f     %f     %f",mainImgV.frame.size.width,mainImgV.frame.size.height,mainImgV.frame.size.width/mainImgV.frame.size.height);
         540.000000     960.000000     0.562500
         375.000000     401.000000     0.935162
         */
        containerView.frame = [UpdateFrame setSizeForView:containerView usingHeight:(CGRectGetHeight(self.view.bounds) + yscrollOffset)];
        
        tabcontentscrollView.frame= [UpdateFrame setPositionForView:tabcontentscrollView usingPositionY: CGRectGetMaxY(topImageV.frame)];
        tabcontentscrollView.frame = [UpdateFrame setSizeForView:tabcontentscrollView usingHeight: CGRectGetHeight(containerView.frame) - CGRectGetMaxY(topImageV.frame)];
    }
    
    
    CGPoint scrollviewOrigin = scrollviewOnTop.frame.origin;
    scrollviewOnTop.scrollIndicatorInsets = UIEdgeInsetsMake(-scrollviewOrigin.y, 0, scrollviewOrigin.y, scrollviewOrigin.x);
    
    [self setData];
}

#pragma mark - Methods

- (NSMutableAttributedString *) getStartingPrice : (NSString *) price withExclusiveSalon :(NSString *) salonName
{
    NSMutableAttributedString *aAttrString ;
    NSDictionary *bFontDict = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[UIFont semiboldFontOfSize:14],[UIColor colorWithRed:154.0/255.0 green:154.0/255.0 blue:154.0/255.0 alpha:1],Nil] forKeys:[NSArray arrayWithObjects:NSFontAttributeName,NSForegroundColorAttributeName,Nil]];
    NSDictionary *gFontDict = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[UIFont semiboldFontOfSize:14],[UIColor colorWithRed:86.0/255.0 green:190.0/255.0 blue:159.0/255.0 alpha:1],Nil] forKeys:[NSArray arrayWithObjects:NSFontAttributeName,NSForegroundColorAttributeName,Nil]];
    
    if([salonName length] > 0)
    {
        
        aAttrString = [[NSMutableAttributedString alloc] initWithString:@"Complete look at " attributes: bFontDict];
        [aAttrString appendAttributedString:  [[NSMutableAttributedString alloc] initWithString:salonName   attributes: gFontDict]];
        [aAttrString appendAttributedString:  [[NSMutableAttributedString alloc] initWithString:@". Starting @ "   attributes: bFontDict]];
        [aAttrString appendAttributedString:  [[NSMutableAttributedString alloc] initWithString:price   attributes: bFontDict]];
        
    }
    else
    {
        aAttrString = [[NSMutableAttributedString alloc] initWithString:@"Starting @ " attributes: bFontDict];
        [aAttrString appendAttributedString:  [[NSMutableAttributedString alloc] initWithString:price   attributes: bFontDict]];
        
    }
    return  aAttrString;
}

- (void) setData
{
    contentHeight = 0;
    if(dataDict)
    {
        NSString *recordId = [ReadData recordIdFromDictionary:dataDict forKey:KEY_ID];
        [appD.sessionDelegate.mytrackingC trackLookById:recordId];
        
        titleL.text = [ReadData stringValueFromDictionary:dataDict forKey:KEY_NAME];
        //otherInfoL.text = [ReadData stringValueFromDictionary:dataDict forKey:KEY_LOOK_SUBTITLE];
        otherInfoL.text = [ReadData stringValueFromDictionary:dataDict forKey:@"SubTitle"];

        
        NSString *isNewStr = [ReadData stringValueFromDictionary:dataDict forKey:KEY_LOOK_ISNEW];

        NSString *salonName = @"";
        if(![ReadData IsnulldictionaryFromDictionary:dataDict forKey:KEY_LOOK_SALON]){
            IssaloonValues = YES; // Set yet to saloon have a value.
            NSDictionary *storesDict = [ReadData dictionaryFromDictionary:dataDict forKey:KEY_LOOK_SALON];
            salonName = [ReadData stringValueFromDictionary:storesDict forKey:KEY_NAME];
            saloonType = [ReadData stringValueFromDictionary:storesDict forKey:KEY_TYPE];
            
            // set the store vlaue eithor exclusive or new.
            ExclusiveORNewV.hidden = NO;
            ExclusiveORNewL.text = @"EXCLUSIVE";
            
            // Set store logo imges.
            // ImageView
            NSString *url = [ReadData stringValueFromDictionary:storesDict forKey:KEY_LOGO_URL];
            if (![url isEqualToString:@""])
            {
                NSURL *imageURL = [NSURL URLWithString:url];
                NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageURL];
                [ storeLogoImgV setImageWithURLRequest:urlRequest
                                      placeholderImage:Nil
                                               success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
                 {
                     // mainImgV.contentMode = UIViewContentModeScaleAspectFit;
                     [ storeLogoImgV setImage:image];
                     [UIView animateWithDuration:IMAGE_VIEW_TOGGLE
                                           delay:0 options:UIViewAnimationOptionCurveLinear
                                      animations:^(void)
                      {
                          
                          [ storeLogoImgV setAlpha:1 ];
                      }
                                      completion:^(BOOL finished)
                      {
                      }];
                     
                     
                     
                 }
                                               failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
                 {
                     
                 }];
            }
            
        }
        else // if the store is new.
        {
            IssaloonValues = NO; // Set NO to saloon does not have a value.

            // set the store vlaue  new.
            if ([isNewStr isEqualToString:@"1"]) {
                ExclusiveORNewL.text = @"NEW";
                ExclusiveORNewV.hidden = NO;
            }else{
                ExclusiveORNewV.hidden = YES;
            }
            
        }
       
        //startingPriceL.attributedText = [self getStartingPrice:[ReadData amountInRsFromDictionary:dataDict forKey:KEY_LOOK_SILVERPRICE] withExclusiveSalon:salonName];
        ////
        
       silverPriceStrikeL.text =  [ReadData getAmountFormatted:[[ReadData stringValueFromDictionary:dataDict forKey:KEY_LOOK_SILVERPRICE] floatValue]];
        //NSString *strSilverP =
        
        NSAttributedString * title = [[NSAttributedString alloc] initWithString:silverPriceStrikeL.text  attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle)}];
        [silverPriceStrikeL setAttributedText:title];
        

        
    
//        NSString *strikeLinesilverPrice = [ReadData stringValueFromDictionary:dataDict forKey:KEY_LOOK_SILVERPRICE];
//        
//        NSAttributedString * title = [[NSAttributedString alloc] initWithString:strikeLinesilverPrice  attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle)}];
//        [silverPriceStrikeL setAttributedText:title];
        
        NSInteger golgPrice = [[ReadData stringValueFromDictionary:dataDict forKey:KEY_LOOK_GOLDPRICE] floatValue];
        NSInteger silverPrice = [[ReadData stringValueFromDictionary:dataDict forKey:KEY_LOOK_SILVERPRICE] floatValue];
        
//        NSString *strSCNT = [ReadData getAmountFormatted:[[ReadData stringValueFromDictionary:dataDict forKey:KEY_LOOK_SILVERPRICE] floatValue]];
//          NSString *strGCNT = [ReadData getAmountFormatted:[[ReadData stringValueFromDictionary:dataDict forKey:KEY_LOOK_GOLDPRICE] floatValue]];
        
        silverPriceL.text = [ReadData getAmountFormatted:[[ReadData stringValueFromDictionary:dataDict forKey:KEY_LOOK_SILVERPRICE] floatValue]];
        goldPriceL.text = [ReadData getAmountFormatted:[[ReadData stringValueFromDictionary:dataDict forKey:KEY_LOOK_GOLDPRICE] floatValue]];
        //silverPriceStrikeL.text = [ReadData getAmountFormatted:[[ReadData stringValueFromDictionary:dataDict forKey:KEY_LOOK_SILVERPRICE] floatValue]];
        
        // Hidden all the labels.
        goldPriceL.hidden = YES;
        silverPriceStrikeL.hidden = YES;
        silverPriceL.hidden = YES;
        DiscountCouponL.hidden = YES;
        
        
        // new implementation.
        NSArray *cashbacks = [ReadData arrayFromDictionary:dataDict forKey:KEY_CASHBACKS];
        //CGFloat positionY = (CGRectGetHeight(self.priceInfoV.frame) - CGRectGetHeight(self.offerpriceL.frame)) *0.5;
        
        // if the looks is cash back else to discount values.
        if([cashbacks count] > 0){
           
            goldPriceL.hidden = YES;
            silverPriceStrikeL.hidden = YES;
            silverPriceL.hidden = YES;
            DiscountCouponL.hidden = NO;
            
            //
            // diffrenciate the amount and percent.
            NSArray *cashbackArray = [[ReadData arrayFromDictionary:dataDict forKey:KEY_CASHBACKS] firstObject];
            //NSLog(@"%@", cashbackArray);
            NSInteger priceMode = [[cashbackArray valueForKey:@"Mode"] integerValue];
            // NSLog(@"%ld", (long)priceMode);
            // NSString *priceMode1 = [cashbackArray valueForKey:@"Cost"];
            //NSLog(@"%@", priceMode1);
            
            // if the it 0 the values is cash eles the percentage.
            if (priceMode == 0) {
                DiscountCouponL.text = [NSString stringWithFormat:@"%@ Cash Back",[ReadData getAmountFormatted:[[cashbackArray valueForKey:@"Cost"] floatValue]]];
            }else{
                NSString *precent = @"%";
                DiscountCouponL.text = [NSString stringWithFormat:@"Cash Back %@%@",[cashbackArray valueForKey:@"Cost"],precent];
            }
            
        }
        else{ // cash back
            
            //self.DiscountCouponL.hidden = YES;
            if (silverPrice>golgPrice) {
                goldPriceL.hidden = NO;
                silverPriceStrikeL.hidden = NO;
                silverPriceL.hidden = YES;
                
            }
            else if((silverPrice = golgPrice || golgPrice> silverPrice)){
                silverPriceL.hidden = NO;
                goldPriceL.hidden = YES;
                silverPriceStrikeL.hidden = YES;
            }
            
        }
        
        ////
        
//        
//        NSString * startingStr = @" Starting price from ";
//        startingPriceL.text = [NSString stringWithFormat:@" %@%@ ",startingStr,[ReadData amountInRsFromDictionary:dataDict forKey:KEY_LOOK_SILVERPRICE]];
//        startingPriceL.backgroundColor = [UIColor blackColor];
//        startingPriceL.textColor = [UIColor whiteColor];
//        [startingPriceL sizeToFit];
        
          /*
        //////
        
        NSArray *arr = [dataDict objectForKey:KEY_LOOK_NOTES];
        NSString *noteStr1 = [arr objectAtIndex:0];
        NSString *noteStr2 = [arr objectAtIndex:1];
        NSArray *separteStr = [noteStr2 componentsSeparatedByString:@":"];
        NSString *separteArr1 = [separteStr objectAtIndex:0];
        NSString *separteArr2 = [separteStr objectAtIndex:1];

        NSMutableAttributedString *aAttrString ;
        NSDictionary *bFontDict = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[UIFont semiboldFontOfSize:12],[UIColor colorWithRed:84.0/255.0 green:1649.0/255.0 blue:112.0/255.0 alpha:1],Nil] forKeys:[NSArray arrayWithObjects:NSFontAttributeName,NSForegroundColorAttributeName,Nil]];
        
        
        aAttrString = [[NSMutableAttributedString alloc] initWithString:noteStr1 attributes:bFontDict];
        [aAttrString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:separteArr1   attributes: bFontDict]];
        [aAttrString appendAttributedString:  [[NSMutableAttributedString alloc] initWithString:separteArr2 attributes: nil]];
        descriptionL.attributedText = aAttrString;
        
        descriptionL.text = [[ReadData arrayFromDictionary:dataDict forKey:KEY_LOOK_NOTES] componentsJoinedByString:@"\n\n"];
        CGFloat tmpHeight = [ResizeToFitContent getHeightForText:(NSString*)aAttrString usingFontType:FONT_SEMIBOLD FontOfSize:13 andMaxWidth:CGRectGetWidth(descriptionL.frame)];
        descriptionL.frame = [UpdateFrame setSizeForView:descriptionL usingHeight:tmpHeight];
        descriptionCV.frame =  [UpdateFrame setSizeForView:descriptionCV usingHeight:tmpHeight + 50];

        
        ////
            */
        
        
        descriptionL.text = [[ReadData arrayFromDictionary:dataDict forKey:KEY_LOOK_NOTES] componentsJoinedByString:@"\n\n"];
        CGFloat tmpHeight = [ResizeToFitContent getHeightForText:descriptionL.text usingFontType:FONT_SEMIBOLD FontOfSize:13 andMaxWidth:CGRectGetWidth(descriptionL.frame)];
        descriptionL.frame = [UpdateFrame setSizeForView:descriptionL usingHeight:tmpHeight];
        descriptionCV.frame =  [UpdateFrame setSizeForView:descriptionCV usingHeight:tmpHeight + 50];
        
     
        
        // ImageView
        NSString *url = [ReadData stringValueFromDictionary:dataDict forKey:KEY_IMAGE1_URL];
        if (![url isEqualToString:@""])
        {
             [self hudShowWithMessage:@"Loading"];
            NSURL *imageURL = [NSURL URLWithString:url];
            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageURL];
            [ mainImgV setImageWithURLRequest:urlRequest
                             placeholderImage:Nil
                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
             {
                 mainImgV.contentMode = UIViewContentModeScaleAspectFit;
                 CGFloat width = image.size.width;
                 CGFloat height = image.size.height;
                 //NSLog(@"%f     %f     %f",width,height,width/height);
                // NSLog(@"%f     %f     %f",mainImgV.frame.size.width,mainImgV.frame.size.height,mainImgV.frame.size.width/mainImgV.frame.size.height);
                 mainImgV.frame = [UpdateFrame setSizeForView:mainImgV usingHeight:mainImgV.frame.size.width/(width/height)];
                 [ mainImgV setImage:image];
                // NSLog(@"%f     %f     %f",mainImgV.frame.size.width,mainImgV.frame.size.height,mainImgV.frame.size.width/mainImgV.frame.size.height);
                 cachedImageViewSize = mainImgV.frame;
                 
                 [UIView animateWithDuration:IMAGE_VIEW_TOGGLE
                                       delay:0 options:UIViewAnimationOptionCurveLinear
                                  animations:^(void)
                  {
                      [HUD hide:YES];
                      [ mainImgV setAlpha:1 ];
                  }
                                  completion:^(BOOL finished)
                  {
                  }];
                 
                 
                 
             }
                                      failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
             {
                 
             }];
        }
        
        
        
        contentHeight = CGRectGetMaxY(descriptionCV.frame) + CGRectGetHeight(footerV.frame);
    }
    
    if(contentHeight <= CGRectGetHeight(mainscrollView.frame)){
        contentHeight = CGRectGetHeight(mainscrollView.bounds);
        mainscrollView.scrollEnabled = NO;
    }
    
    [self tabChanged];
}

- (void) tabChanged
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        CGPoint offset = [self getScrollContentOffset];
        offset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:offset animated:NO];
    }
    isContentSizeUpdated = NO;
}

- (void) startBookingWithDeliveryType : (NSString *) type{
    [cartC initForLooksWithDeliveryType:type];
    [cartC setLatitude:[appD getUserCurrentLocationLatitude] andLongitude:[appD getUserCurrentLocationLongitude]];
    [cartC addLooks:dataDict];
    [self performSegueWithIdentifier:@"showlocationdate_looks" sender:Nil];
}

#pragma mark - Showing/Hiding Views - Generic

- (void)showView:(UIView *)view andGreyView:(BOOL)showGreyView
{
    // Shrink WebView
    view.transform = CGAffineTransformMakeScale(0.8, 0.8);
    
    // Ready Grey View
    if (showGreyView)
    {
        backV.alpha = 0;
        [self.view bringSubviewToFront:backV];
    }
    
    // Ready WebView
    view.alpha = 0;
    [self.view bringSubviewToFront:view];
    
    // DONT CHANGE THE ORDER
    
    // 1 - Dismiss keyboard
    [UIResponder dismissKeyboard];
    
    // 2 - Disable User Interaction
    [self.view setUserInteractionEnabled:NO];
    
    // 3 - Animate
    [UIView animateWithDuration:TIME_VIEW_TOGGLE
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         // Animate Grow and Fade In
         view.transform = CGAffineTransformMakeScale(1.0, 1.0);
         view.alpha = 1;
         
         if (showGreyView)
             backV.alpha = 1;
     }
                     completion:^(BOOL finished)
     {
         // Enable User Interaction
         [self.view setUserInteractionEnabled:YES];
     }];
}

- (void)hideView:(UIView *)view andGreyView:(BOOL)hideGreyView
{
    // DONT CHANGE THE ORDER
    
    // 1 - Dismiss keyboard
    [UIResponder dismissKeyboard];
    
    
    // 2 - Disable User Interaction
    [self.view setUserInteractionEnabled:NO];
    
    // 3 - Animate
    [UIView animateWithDuration:TIME_VIEW_TOGGLE
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         // Animate Shrink & Fade Out
         view.transform = CGAffineTransformMakeScale(0.8, 0.8);
         view.alpha = 0;
         
         if(hideGreyView)
             backV.alpha = 0;
     }
                     completion:^(BOOL finished)
     {
         
         // Send Views to Back
         [self.view sendSubviewToBack:view];
         
         if(hideGreyView)
             [self.view sendSubviewToBack:backV];
         
         // Enable User Interaction
         [self.view setUserInteractionEnabled:YES];
     }];
}

- (IBAction)backVClicked:(id)sender
{
    [self hideAllViews];
}

- (void)hideAllViews
{
    // DONT CHANGE THE ORDER
    
    // 1 - Dismiss keyboard
    [UIResponder dismissKeyboard];
    
    
    // 2 - Disable User Interaction
    [self.view setUserInteractionEnabled:NO];
    
    
    // 3 - Animate
    [UIView animateWithDuration:TIME_VIEW_TOGGLE
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         for (UIView *view in containerViews)
         {
             view.transform = CGAffineTransformMakeScale(0.8, 0.8);
             view.alpha = 0;
         }
         
         backV.alpha = 0;
         
     }
                     completion:^(BOOL finished)
     {
         
         for (UIView *view in containerViews)
             [self.view sendSubviewToBack:view];
         
         [self.view sendSubviewToBack:backV];
         
         // Enable User Interaction
         [self.view setUserInteractionEnabled:YES];
         
     }];
}

#pragma mark - Delegates

- (void) homeServicesSelected
{
    //[self hideView:beautyservicesCV andGreyView:YES];
    //[self startBookingWithDeliveryType:DELIVERY_TYPE_HOME];
    [cartC initForLooksWithDeliveryType:DELIVERY_TYPE_HOME];
    [cartC setLatitude:[appD getUserCurrentLocationLatitude] andLongitude:[appD getUserCurrentLocationLongitude]];
    [cartC addLooks:dataDict];
    
    UserAddAddressViewController *userAddAddressViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UserAddAddressViewController"];
    appD.sessionDelegate.isCommingFromviewController = @"fromlookdetailhomeserviceVC";
    [self.navigationController pushViewController:userAddAddressViewController animated:YES];
}

- (void) salonServicesSelected
{
    //[self hideView:beautyservicesCV andGreyView:YES];
    //[self startBookingWithDeliveryType:DELIVERY_TYPE_SALON];
    [cartC initForLooksWithDeliveryType:DELIVERY_TYPE_SALON];
    [cartC setLatitude:[appD getUserCurrentLocationLatitude] andLongitude:[appD getUserCurrentLocationLongitude]];
    [cartC addLooks:dataDict];
    
    ChooseUserLocationViewController *chooseUserLocationViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChooseUserLocationViewController"];
    appD.sessionDelegate.isCommingFromviewController = @"fromlookdetailsalonserviceVC";
    [self.navigationController pushViewController:chooseUserLocationViewController animated:YES];

}

-(void) crossPresseB{
    [self hideView:beautyservicesCV andGreyView:YES];
}

#pragma mark - Events

- (IBAction)backBPressed:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)galleryBPressed:(UIButton *)sender{
    [self performSegueWithIdentifier:@"showgallery_look" sender:Nil];
}

- (IBAction)bookBPressed:(UIButton *)sender{
    //[self showView:beautyservicesCV andGreyView:YES];
    //[self startBookingWithDeliveryType:DELIVERY_TYPE_SALON];

    if (IssaloonValues) {
        
        // if the saloone has value and the value is 0 then pointing to location  and 1 is for address page.
        if ([saloonType isEqualToString:@"0"]){
            [self  salonServicesSelected];
        }else{
            [self homeServicesSelected];
        }
            
       // [self startBookingWithDeliveryType:DELIVERY_TYPE_SALON];

    }else{
        [self showView:beautyservicesCV andGreyView:YES];
    }
}


#pragma mark - Expand on Scroll animation

- (CGSize) setContentSize
{
    CGFloat height = CGRectGetHeight(mainImgV.frame) + contentHeight;
    return CGSizeMake(pageWidth, height);
}

- (void) setScrollContentOffset: (CGPoint) offset
{
    mainscrollView.contentOffset = offset;
}

- (CGPoint) getScrollContentOffset
{
    return [mainscrollView contentOffset];
}

- (void) resetOffsetAllTabs
{
    CGPoint offset = CGPointZero;
    [self setScrollContentOffset : offset];
}

-(void) updateContentSize : (CGPoint) contentOffset;
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        contentOffset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    }
    [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    
    isContentSizeUpdated = NO;
}

#pragma mark - Scroll Events

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == scrollviewOnTop)
    {
        {
            if (isContentSizeUpdated)  return;
            
            CGRect scrolledBoundsForContainerView = containerView.bounds;
            if (scrollView.contentOffset.y <= yscrollOffset)
            {
                
                scrolledBoundsForContainerView.origin.y = scrollView.contentOffset.y ;
                containerView.bounds = scrolledBoundsForContainerView;
                
                //Reset offset for all tabs
                if (scrollView.contentOffset.y <= 0) [self resetOffsetAllTabs];
                
                
                CGFloat y = -scrollView.contentOffset.y;
                //                CGFloat alphaLevel = 1;
                //                CGFloat BLUR_MAX_Y = IMAGE_SCROLL_OFFSET_Y + CGRectGetHeight(topImageV.frame) - CGRectGetMinY(optionsV.frame);
                //                if (fabs(y) < BLUR_MAX_Y)
                //                {
                //                    alphaLevel = (BLUR_MAX_Y - fabs(y))/(BLUR_MAX_Y);
                //                }
                //                else
                //                {
                //                    alphaLevel = 0;
                //                }
                //
                //[screenTitleL setAlpha:alphaLevel];
                if (y > 0)
                {
                    mainImgV.frame = CGRectInset(cachedImageViewSize, 0, -y/2);
                    mainImgV.center = CGPointMake(mainImgV.center.x, mainImgV.center.y + y/2);
                }
                else
                {
                    mainImgV.frame = [UpdateFrame setPositionForView:mainImgV usingPositionY:y];
                }
                return;
            }
            
            scrolledBoundsForContainerView.origin.y = yscrollOffset ;
            containerView.bounds = scrolledBoundsForContainerView;
            
            [self setScrollContentOffset:CGPointMake(0, scrollView.contentOffset.y - yscrollOffset)  ];
        }
    }
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"showgallery_look"]){
        GalleryViewController *galleryVC = (GalleryViewController *)segue.destinationViewController;
        
        looksIMGArray = [[NSMutableArray alloc]init];
        NSMutableArray *arrayImageKeys = [self GetLooksImageUrlFromDictionary:dataDict];
       // NSLog(@"The iamge urls -- %@", arrayImageKeys);
        
        // split the actual images from hole images.
        for (NSString *URL in arrayImageKeys) {
            
            NSString *url = [ReadData stringValueFromDictionary:dataDict forKey:URL];
            if (![url isEqualToString:@""] && [url containsString:@"https://"]) {
                [looksIMGArray addObject:url];
            }
        }
    
        [galleryVC setImageForFullScreen:looksIMGArray];
        //[galleryVC setImageForFullScreen:mainImgV.image];
    }
    else if([segue.identifier isEqualToString:@"showlocationdate_looks"]){
    }
    else if([segue.identifier isEqualToString:@"showservicesselector_look"]){
        beautyservicesVC = (BeautyServicesSelectorViewController *)segue.destinationViewController;
        beautyservicesVC.delegate = self;
    }
}

#pragma mark - HUD & Delegate methods

- (void)hudShowWithMessage:(NSString *)message
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    HUD.dimBackground = YES;
    HUD.labelText = message;
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    [HUD show:YES];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}

// This is a method to split the images URL from Main dictonary object.
-(NSMutableArray*)GetLooksImageUrlFromDictionary:(NSMutableDictionary*)dictionary
{
    NSMutableArray* result = [NSMutableArray array];
    for (NSString* key in dictionary)
    {
        if ([key hasPrefix:@"Image"])
        {
            [result addObject:key];
        }
    }
    return result;
}


@end
