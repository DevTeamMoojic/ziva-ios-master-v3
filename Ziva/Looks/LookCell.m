//
//  LookCell.m
//  Ziva
//
//  Created by Bharat on 23/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "LookCell.h"

@implementation LookCell

- (void)awakeFromNib {
    // Initialization code
    
    [super awakeFromNib];
    
    // Fix : Autoresizing issues for iOS 7 using Xcode 6
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //    Fix : For right aligned view with autoresizing
    UIView *backV = [[self.contentView subviews] objectAtIndex:0];
    backV.frame = [UpdateFrame setSizeForView:backV usingSize:self.bounds.size];
}

- (NSMutableAttributedString *) getStartingPrice : (NSString *) price withExclusiveSalon :(NSString *) salonName
{
    price = @"";
    NSMutableAttributedString *aAttrString ;
    NSDictionary *bFontDict = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[UIFont semiboldFontOfSize:14],[UIColor colorWithRed:154.0/255.0 green:154.0/255.0 blue:154.0/255.0 alpha:1],Nil] forKeys:[NSArray arrayWithObjects:NSFontAttributeName,NSForegroundColorAttributeName,Nil]];
    NSDictionary *gFontDict = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[UIFont semiboldFontOfSize:14],[UIColor colorWithRed:86.0/255.0 green:190.0/255.0 blue:159.0/255.0 alpha:1],Nil] forKeys:[NSArray arrayWithObjects:NSFontAttributeName,NSForegroundColorAttributeName,Nil]];
    
    if([salonName length] > 0){
    
        aAttrString = [[NSMutableAttributedString alloc] initWithString:@"Complete look at " attributes: bFontDict];
        [aAttrString appendAttributedString:  [[NSMutableAttributedString alloc] initWithString:salonName   attributes: gFontDict]];
        [aAttrString appendAttributedString:  [[NSMutableAttributedString alloc] initWithString:@"."   attributes: bFontDict]];
        [aAttrString appendAttributedString:  [[NSMutableAttributedString alloc] initWithString:price   attributes: bFontDict]];
        
    }
    else{
        aAttrString = [[NSMutableAttributedString alloc] initWithString:@"Complete look at nearby salon" attributes: bFontDict];
        [aAttrString appendAttributedString:  [[NSMutableAttributedString alloc] initWithString:price   attributes: bFontDict]];
        
    }
    return  aAttrString;
}

- (void) configureDataForCell : (NSDictionary *) item
{
    self.mainImgV.image = Nil;
    self.placeholderImgV.alpha=1;
    
    self.titleL.text = [ReadData stringValueFromDictionary:item forKey:KEY_NAME];
    self.otherInfoL.text = [ReadData stringValueFromDictionary:item forKey:KEY_LOOK_SUBTITLE];
    
    NSString *salonName = @"";    
    if(![ReadData IsnulldictionaryFromDictionary:item forKey:KEY_LOOK_SALON]){
        NSDictionary *storesDict = [ReadData dictionaryFromDictionary:item forKey:KEY_LOOK_SALON];
        salonName = [ReadData stringValueFromDictionary:storesDict forKey:KEY_NAME];
    }
    
    self.startingPriceL.attributedText = [self getStartingPrice:[ReadData amountInRsFromDictionary:item forKey:KEY_LOOK_SILVERPRICE] withExclusiveSalon:salonName];
    
    
    self.startingPriceL.frame = [UpdateFrame setSizeForView:self.startingPriceL usingWidth:self.startingPriceL.frame.size.width];
    
   //CGFloat startingPriceXPO = self.startingPriceL.frame.size.width + 15;
    //CGFloat startingPriceStrikeXPO = self.startingPriceL.frame.size.width ;

    
     //self.silverPriceL.frame = [UpdateFrame setPositionForView:self.silverPriceL usingPositionX:startingPriceXPO];
    //self.silverPriceStrikeL.frame = [UpdateFrame setPositionForView:self.silverPriceStrikeL usingPositionX:startingPriceXPO];

    
   // CGFloat silverPricAndStartingPriceXPO = self.silverPriceL.frame.size.width +25;
    
    //self.goldPriceL.backgroundColor = [UIColor yellowColor];
    //self.goldPriceL.frame = [UpdateFrame setPositionForView:self.goldPriceL usingPositionX:silverPricAndStartingPriceXPO];
    
    
    self.goldPriceL.text = [ReadData getAmountFormatted:[[ReadData stringValueFromDictionary:item forKey:KEY_LOOK_GOLDPRICE] floatValue]];
    self.silverPriceL.text = [ReadData getAmountFormatted:[[ReadData stringValueFromDictionary:item forKey:KEY_LOOK_SILVERPRICE] floatValue]];
    self.silverPriceStrikeL.text =  [ReadData getAmountFormatted:[[ReadData stringValueFromDictionary:item forKey:KEY_LOOK_SILVERPRICE] floatValue]];
    //NSString *strSilverP =
    
    NSAttributedString * title = [[NSAttributedString alloc] initWithString:self.silverPriceStrikeL.text  attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle)}];
    [self.silverPriceStrikeL setAttributedText:title];
    
    NSInteger golgPrice = [[ReadData stringValueFromDictionary:item forKey:KEY_LOOK_GOLDPRICE] floatValue];
    NSInteger silverPrice = [[ReadData stringValueFromDictionary:item forKey:KEY_LOOK_SILVERPRICE] floatValue];
    
    self.goldPriceL.hidden = YES;
    self.silverPriceStrikeL.hidden = YES;
    self.silverPriceL.hidden = YES;
    self.DiscountCouponL.hidden = YES;
    
    //self.goldPriceL.backgroundColor = [UIColor greenColor];
    //self.silverPriceStrikeL.backgroundColor = [UIColor yellowColor];
    //self.silverPriceL.backgroundColor = [UIColor lightGrayColor];
    
    
    // new implementation.
    NSArray *cashbacks = [ReadData arrayFromDictionary:item forKey:KEY_CASHBACKS];
    //CGFloat positionY = (CGRectGetHeight(self.priceInfoV.frame) - CGRectGetHeight(self.offerpriceL.frame)) *0.5;
    
    // if the looks is cash back else to discount values.
    if([cashbacks count] > 0){
        
        self.goldPriceL.hidden = YES;
        self.silverPriceStrikeL.hidden = YES;
        self.silverPriceL.hidden = YES;
        self.DiscountCouponL.hidden = NO;
        
        //
        // diffrenciate the amount and percent.
        NSArray *cashbackArray = [[ReadData arrayFromDictionary:item forKey:KEY_CASHBACKS] firstObject];
        //NSLog(@"%@", cashbackArray);
        NSInteger priceMode = [[cashbackArray valueForKey:@"Mode"] integerValue];
       // NSLog(@"%ld", (long)priceMode);
       // NSString *priceMode1 = [cashbackArray valueForKey:@"Cost"];
        //NSLog(@"%@", priceMode1);
        
        // if the it 0 the values is cash eles the percentage.
        if (priceMode == 0) {
            self.DiscountCouponL.text = [NSString stringWithFormat:@"%@ Cash Back",[ReadData getAmountFormatted:[[cashbackArray valueForKey:@"Cost"] floatValue]]];
        }else{
            NSString *precent = @"%";
            self.DiscountCouponL.text = [NSString stringWithFormat:@"Cash Back %@%@",[cashbackArray valueForKey:@"Cost"],precent];
        }
        
    }
    else{ // cash back
      
        self.DiscountCouponL.hidden = YES;
        if (silverPrice>golgPrice) {
            self.goldPriceL.hidden = NO;
            self.silverPriceStrikeL.hidden = NO;
            self.silverPriceL.hidden = YES;
            
        }
        else if((silverPrice = golgPrice || golgPrice> silverPrice)){
            self.silverPriceL.hidden = NO;
            self.goldPriceL.hidden = YES;
            self.silverPriceStrikeL.hidden = YES;
        }
        
    }
    
   // ImageView
    NSString *url = [ReadData stringValueFromDictionary:item forKey:KEY_BANNER_URL];
    if (![url isEqualToString:@""])
    {
        NSURL *imageURL = [NSURL URLWithString:url];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageURL];
        [self.mainImgV setImageWithURLRequest:urlRequest
                             placeholderImage:Nil
                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {             
             [self.mainImgV setImage:image];
             
             [UIView animateWithDuration:IMAGE_VIEW_TOGGLE
                                   delay:0 options:UIViewAnimationOptionCurveLinear
                              animations:^(void)
              {
                  
                  [self.placeholderImgV setAlpha:0 ];
                  [self.mainImgV setAlpha:1 ];
              }
                              completion:^(BOOL finished)
              {
              }];
             
         }
                                      failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
         {
             
         }];
    }
}

@end
