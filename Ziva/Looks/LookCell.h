//
//  LookCell.h
//  Ziva
//
//  Created by Bharat on 23/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LookCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *mainV;

@property (weak, nonatomic) IBOutlet UIView *imageCV;
@property (weak, nonatomic) IBOutlet UIImageView *mainImgV;
@property (weak, nonatomic) IBOutlet UIImageView *placeholderImgV;

@property (weak, nonatomic) IBOutlet UIView *infoCV;
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *otherInfoL;
@property (weak, nonatomic) IBOutlet UILabel *startingPriceL;
@property (weak, nonatomic) IBOutlet UILabel *silverPriceL;
@property (weak, nonatomic) IBOutlet UILabel *silverPriceStrikeL;
@property (weak, nonatomic) IBOutlet UILabel *goldPriceL;
@property (weak, nonatomic) IBOutlet UILabel *DiscountCouponL;




- (void) configureDataForCell : (NSDictionary *) item;

@end
