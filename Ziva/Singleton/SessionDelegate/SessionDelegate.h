//
//  SessionDelegate.h
//  GuestList
//
//  Created by Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

@class MyCartController;
@class MyTrackingController;

#import <Foundation/Foundation.h>

@interface SessionDelegate : NSObject

@property (nonatomic, strong) MyCartController *mycartC;
@property (nonatomic, strong) MyTrackingController *mytrackingC;
@property (nonatomic, strong) NSMutableArray *pastHistoryLocationArray; // Set to store the user location places.
@property (nonatomic, strong) NSString *isCommingFromviewController;
@property (nonatomic, strong) NSMutableArray *cardDetailsArray; // Set to store the card details.

- (void) loadDataForLoggedInUser ;

@end
