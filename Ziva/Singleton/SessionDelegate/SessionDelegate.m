//
//  SessionDelegate.m
//  GuestList
//
//  Created by Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "SessionDelegate.h"

@implementation SessionDelegate
{
    
}

- (id)init
{
    if (self = [super init])
    {
        self.mycartC = [MyCartController new];
        self.mytrackingC = [MyTrackingController new];
        
        self.pastHistoryLocationArray = [NSMutableArray new]; // Store the past location place.
        self.isCommingFromviewController = @"";
        self.cardDetailsArray = [NSMutableArray new]; // Store the card details.
    }
    return self;
}

- (void) loadDataForLoggedInUser {
    [self.mytrackingC initTrackingData];
}



@end
