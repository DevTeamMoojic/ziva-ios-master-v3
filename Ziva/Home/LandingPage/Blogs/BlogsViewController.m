//
//  BlogsViewController.m
//  Ziva
//
//  Created by Bharat on 30/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "BlogCell.h"
#import "BlogsViewController.h"
#import "InappBrowserViewController.h"
#import "LandingPageViewController.h"

@interface BlogsViewController ()<APIDelegate,UICollectionViewDelegateFlowLayout>
{
    NSArray *list;
    CGSize layoutsize;
    
    __weak IBOutlet UICollectionView *collectionlistV;
    __weak IBOutlet UIActivityIndicatorView *sectionloaderAIV;
    
    AppDelegate *appD;
}
@end

@implementation BlogsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    list = [NSMutableArray array];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -  Public Methods

- (void) setCellContentSize : (CGSize) contentsize
{
    layoutsize = contentsize;
    collectionlistV.frame= [UpdateFrame setSizeForView:collectionlistV usingHeight:layoutsize.height];
}

- (void) reloadList
{
    if ([InternetCheck isOnline]){
        collectionlistV.hidden = YES;
        [sectionloaderAIV startAnimating];
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_BLOGS Param:NULL];
    }
}

#pragma mark - Methods

- (void) processResponse : (NSDictionary *) dict
{
    list = [ReadData arrayFromDictionary:dict forKey:RESPONSEKEY_DATA];
    [self setupLayout];
}

- (void) setupLayout
{
    [collectionlistV reloadData];
    [collectionlistV setContentOffset:CGPointZero animated:NO];
    
    collectionlistV.hidden = NO;
    [sectionloaderAIV stopAnimating];
}


#pragma mark - Collection View

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [list count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return layoutsize;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // set cell
    BlogCell *cell = (BlogCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"blogcell" forIndexPath:indexPath];
    
    //Reposition elements
    {
        cell.mainV.frame = [UpdateFrame setSizeForView:cell.mainV usingSize:layoutsize];
        CGSize newSz = CGSizeMake(layoutsize.width, layoutsize.height - 17);
        cell.imageCV.frame = [UpdateFrame setSizeForView:cell.imageCV usingSize:newSz];
        cell.mainImgV.frame = [UpdateFrame setSizeForView:cell.mainImgV usingSize:newSz];
    }
    NSDictionary *item = list[indexPath.row];
    [cell configureDataForCell:item];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    LandingPageViewController *parentVC = (LandingPageViewController *)self.parentViewController;
    [parentVC launchDetailView];
    
    NSDictionary *item = list[indexPath.row];
    NSString *urlString = [ReadData stringValueFromDictionary:item forKey:KEY_CONTENT_URL];
    [self performSegueWithIdentifier:@"showdetail_blog" sender:urlString];
}


#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showdetail_blog"])
    {
        InappBrowserViewController *browserVC = (InappBrowserViewController *) segue.destinationViewController;
        [browserVC loadURL: [NSURL URLWithString:  (NSString *)sender ]];
    }
    
}


#pragma mark - API

-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
    if(![ReadData isValidResponseData:response])
    {
        {
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
            [self.view setUserInteractionEnabled:YES];
            [sectionloaderAIV stopAnimating];
        }
        return;
    }
    
    if ([apiName isEqualToString:API_GET_BLOGS]) {
        [self processResponse:response];
    }
}

@end
