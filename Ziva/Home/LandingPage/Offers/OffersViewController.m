//
//  OffersViewController.m
//  Ziva
//
//  Created by Bharat on 30/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "OfferCell.h"
#import "OffersViewController.h"
#import "LooksViewController.h"
#import "LookDetailViewController.h"
#import "StoreRateCardViewController.h"
#import "ProductsViewController.h"
#import "ProductDetailViewController.h"
#import "StylistsViewController.h"
#import "PackageViewController.h"
#import "PackageDetailViewController.h"

@interface OffersViewController ()<APIDelegate,UICollectionViewDelegateFlowLayout,MBProgressHUDDelegate>
{
    NSMutableArray *list;
    CGSize layoutsize;
    
    __weak IBOutlet UILabel *noOffersL;
    __weak IBOutlet UICollectionView *collectionlistV;
    __weak IBOutlet UIPageControl *pagerControl;
    
    __weak IBOutlet UIActivityIndicatorView *sectionloaderAIV;
    
    AppDelegate *appD;
    MyCartController *cartC;

    
    NSInteger pageIndex;

    NSInteger banner_position;
    NSString *cellIdentifier;
     MBProgressHUD *HUD;
    

}
@end

@implementation OffersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;
    
    list = [NSMutableArray array];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -  Public Methods

- (void) setCellContentSize : (CGSize) contentsize andPosition : (NSInteger) position
{
    layoutsize = contentsize;
    banner_position = position;
    switch(banner_position){
       case BANNER_POSITION_TOP:
            cellIdentifier = @"offercell_top";
            break;
        case BANNER_POSITION_MIDDLE:
            cellIdentifier = @"offercell_middle";
            break;
        case BANNER_POSITION_BOTTOM:
            cellIdentifier = @"offercell_bottom";
            break;
    }
    
    if(banner_position == BANNER_POSITION_BOTTOM ){
        collectionlistV.frame= [UpdateFrame setSizeForView:collectionlistV usingHeight:layoutsize.height + 20];
    }
    else{
        collectionlistV.frame= [UpdateFrame setSizeForView:collectionlistV usingSize:layoutsize];
    }
}

- (void) reloadList
{
    if ([InternetCheck isOnline]){
        
        [sectionloaderAIV startAnimating];
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_OFFERS Param:NULL];
    }
}

#pragma mark - Methods

- (void) processResponse : (NSDictionary *) dict
{
    NSArray *tmpArray = [ReadData arrayFromDictionary:dict forKey:RESPONSEKEY_DATA];
    if([list count] > 0 ) [list removeAllObjects];
    for(NSDictionary *item in tmpArray){
        
        if([ReadData integerValueFromDictionary:item forKey:KEY_POSITION] == banner_position)
        {
            [list addObject:item];
        }
    }
    if(([list count] == 0) && (banner_position != BANNER_POSITION_TOP)){
        for(NSDictionary *item in tmpArray){
            if([ReadData integerValueFromDictionary:item forKey:KEY_POSITION] == BANNER_POSITION_TOP)
            {
                [list addObject:item];
            }
        }
    }
    
    
    [self setupLayout];
}

- (void) setupLayout
{
    pageIndex = 0;
    pagerControl.currentPage = pageIndex;
    pagerControl.numberOfPages = [list count];
    pagerControl.hidden = ([list count] < 2);
    
//    if(!pagerControl.hidden){
//        id firstItem = list[0];
//        id lastItem = [list lastObject];
//        
//        [list insertObject:lastItem atIndex:0];
//        
//        // Add the copy of the first item to the end
//        [list addObject:firstItem];
//    }
    
    [collectionlistV reloadData];
    [collectionlistV setContentOffset:CGPointZero animated:NO];
    
    [sectionloaderAIV stopAnimating];
    noOffersL.hidden = !([list count] == 0);
    collectionlistV.hidden = !noOffersL.hidden;
    
    noOffersL.text = noOffersL.hidden ? @"Currently, we are not running any offers" : @"";
}


#pragma mark - Collection View

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [list count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return layoutsize;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // set cell
    OfferCell *cell = (OfferCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    //Reposition elements
    {
        
        cell.mainV.frame = [UpdateFrame setSizeForView:cell.mainV usingSize:layoutsize];
        cell.imageCV.frame = [UpdateFrame setSizeForView:cell.imageCV usingSize:layoutsize];
        cell.mainImgV.frame = [UpdateFrame setSizeForView:cell.mainImgV usingSize:layoutsize];
        cell.placeholderImgV.frame = [UpdateFrame setSizeForView:cell.placeholderImgV usingSize:layoutsize];
    }
    NSDictionary *item = list[indexPath.row];
    [cell configureDataForCell:item];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    NSDictionary *item = list[indexPath.row];
    
    switch ([[item valueForKey:@"CallToAction"] integerValue]) {
        case 0:// Look listing page.
        {
            LooksViewController *LookVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LooksViewController"];
            [self.navigationController pushViewController:LookVC animated:YES];
        }
            break;
        case 1:// Particular Look
             [self loadLookByID:item];
            break;
        case 2: // Exclusive Look.
             [self loadLookByID:item];
           break;
        case 3: // Store Rate Card.
            [self loadStoreRateCard:item];

//        {
////            ProductDetailViewController *LDV = [self.storyboard instantiateViewControllerWithIdentifier:@"productdetailVC"];
////            [LDV loadDetailView:[ReadData stringValueFromDictionary:item forKey:@"StoreId"]];
////            [self.navigationController pushViewController:LDV animated:YES];
//
//        }
            break;
        case 4: // Products listing page.
        {
            ProductsViewController *ProductLVC = [self.storyboard instantiateViewControllerWithIdentifier:@"showproductsVC"];
            [self.navigationController pushViewController:ProductLVC animated:YES];
        }
            break;
        case 5: // Products of Salon.
            break;
        case 6: // Particular Product.
        {
            ProductDetailViewController *LDV = [self.storyboard instantiateViewControllerWithIdentifier:@"productdetailVC"];
            [LDV loadDetailView:[ReadData stringValueFromDictionary:item forKey:@"ProductId"]];
            [self.navigationController pushViewController:LDV animated:YES];

        }
       
            break;
        case 7: // Packages.
        {
            PackageViewController *PackageVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PackageViewController"];
            [self.navigationController pushViewController:PackageVC animated:YES];

        }
            break;
        case 8: // Particular Packages.
        {
            [self loadPackageByID:item];
           
        }
            break;
        case 9: // Stylist.
        {
            //StylistsViewController *FeaturedVC = [self.storyboard instantiateViewControllerWithIdentifier:@"stylistsVC"];

           // [self.navigationController pushViewController:FeaturedVC animated:YES];
        }
            
            break;
        case 10: // Particular Stylist.
            
            break;
        case 11:// NO Action.
            
            break;
            
        default:
            break;
    }
    

}

#pragma mark - Public Methods


// Calling web service for lookDeatils through lookByID.
- (void) loadLookByID :(NSDictionary*)dict{
    if ([InternetCheck isOnline])
    {
        NSMutableArray *replacementArray = [NSMutableArray array];
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:@"ID" forKey:@"key"];
        [paramDict setObject:[ReadData stringValueFromDictionary:dict forKey:@"LookId"] forKey:@"value"];
        
        [replacementArray addObject:paramDict];
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_LOOKBYID Param:nil replacementStrings:replacementArray];

    }
    else{
        [self showErrorMessage:NETWORKERROR];
    }
}



// Response for lookByID web service for look details and navigate to page.
- (void) processResponse_LookIDDetails : (NSDictionary *)dict
{
   // NSLog(@"%@", dict);
    
        LookDetailViewController *LDV = [self.storyboard instantiateViewControllerWithIdentifier:@"LookDetailViewController"];
            [LDV loadDetailView:dict];
          [self.navigationController pushViewController:LDV animated:YES];
}


// calling webservice for store rate card through get nearby searvice.
-(void) loadStoreRateCard :(NSDictionary *)dict{
  //https://api.ziva.in/v2/GetStoreById/1101
    //[HUD hide:YES];
    if ([InternetCheck isOnline])
    {
    NSMutableArray *replacementArray = [NSMutableArray array];
    NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
    [paramDict setObject:@"ID" forKey:@"key"];
    [paramDict setObject:[ReadData stringValueFromDictionary:dict forKey:KEY_CTXN_STOREID] forKey:@"value"];
    [replacementArray addObject:paramDict];
    [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_STOREID Param:nil replacementStrings:replacementArray];
    }else{
        [self showErrorMessage:NETWORKERROR];

    }
 }

// Response for storerateCard web service for store rate card list and navigate to page.
- (void) processResponse_StoreRateCardDetails : (NSDictionary *)dict
{
    //NSLog(@"%@", dict);
   NSDictionary *storeIDDict = dict;
    //NSLog(@"%@",storeIDDict);
    //[self sendDelegateEvent:API_DETAIL_VIEW_ORDER];
    //[HUD hide:YES];
    
    [cartC updateStoreInformation:dict];
    [cartC setLatitude:[appD getUserCurrentLocationLatitude] andLongitude:[appD getUserCurrentLocationLongitude]];
    
    UIStoryboard *modalStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    StoreRateCardViewController *LDSSVC = [modalStoryboard instantiateViewControllerWithIdentifier:@"StoreRateCardViewController"];
    [LDSSVC loadForSalonStore:dict Gender:[appD getLoggedInUserGender] andDeliveryType:@"0" :NO];
    
    [self.navigationController pushViewController:LDSSVC animated:YES];
}

// Calling web service for package and redirect the package detatils view controller.
-(void) loadPackageByID :(NSDictionary *)dict{
  
    if ([InternetCheck isOnline]) {
        [HUD hide:YES];

        NSMutableArray *replacementArray = [NSMutableArray array];
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:@"ID" forKey:@"key"];
        [paramDict setObject:[ReadData stringValueFromDictionary:dict forKey:KEY_PACKAGE_CTXN_STOREID] forKey:@"value"];
        [replacementArray addObject:paramDict];
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_PACKAGEBYID Param:nil replacementStrings:replacementArray];

        
    }else{
        [self showErrorMessage:NETWORKERROR];
    }
   }

// Response for Package By id Details.
-(void) processResponse_PackageByIDDetails:(NSDictionary *)dict{
    PackageDetailViewController *PackageDVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PackageDetailViewController"];
    
    [PackageDVC loadDetailView:dict];
    [self.navigationController pushViewController:PackageDVC animated:YES];
    
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

}

#pragma mark - Scroll

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(banner_position != BANNER_POSITION_BOTTOM)
    {
        NSUInteger page = floor((scrollView.contentOffset.x - layoutsize.width / 2) / layoutsize.width) + 1;
        if (pageIndex != page)
        {
            pageIndex = page ;
            pagerControl.currentPage = pageIndex;
        }
    }
    
}


#pragma mark - API

-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
    if(![ReadData isValidResponseData:response])
    {
        {
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
            [self.view setUserInteractionEnabled:YES];
            [sectionloaderAIV stopAnimating];
        }
        return;
    }
    
    if ([apiName isEqualToString:API_GET_OFFERS]) {
        [self processResponse:response];
    }
    else if ([apiName isEqualToString:API_GET_LOOKBYID]) {
        [self processResponse_LookIDDetails:[ReadData dictionaryFromDictionary:response forKey:RESPONSEKEY_DATA]];
    }
    else if ([apiName isEqualToString:API_GET_STOREID]) {
        [self processResponse_StoreRateCardDetails:[ReadData dictionaryFromDictionary:response forKey:RESPONSEKEY_DATA]];
    }
    else if ([apiName isEqualToString:API_GET_PACKAGEBYID]) {
        [self processResponse_PackageByIDDetails:[ReadData dictionaryFromDictionary:response forKey:RESPONSEKEY_DATA]];
    }

}

#pragma mark - HUD & Delegate methods

- (void)hudShowWithMessage:(NSString *)message
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    HUD.dimBackground = YES;
    HUD.labelText = message;
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    [HUD show:YES];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}

- (void) showErrorMessage : (NSString *)message
{
    UIAlertView *errorNotice = [[UIAlertView alloc] initWithTitle:ALERT_TITLE_ERROR message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorNotice show];
}



@end
