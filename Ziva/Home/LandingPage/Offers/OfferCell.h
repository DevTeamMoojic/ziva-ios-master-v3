//
//  OfferCell.h
//  Ziva
//
//  Created by Bharat on 30/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OfferCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *mainV;
@property (weak, nonatomic) IBOutlet UIView *imageCV;

@property (weak, nonatomic) IBOutlet UIImageView *mainImgV;
@property (weak, nonatomic) IBOutlet UIImageView *placeholderImgV;

- (void) configureDataForCell : (NSDictionary *) item;

@end
