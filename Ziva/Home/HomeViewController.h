//
//  HomeViewController.h
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChooseServicesViewController.h"

@interface HomeViewController : UIViewController

@property (nonatomic, assign) BOOL isback;
@property (nonatomic,strong) ChooseServicesViewController *homeservicesVC;
@property (nonatomic,strong) ChooseServicesViewController *salonservicesVC;


- (void) launchDetailView;

- (void) resetLayout;
@end
