//
//  ChooseServicesViewController.h
//  Ziva
//
//  Created by Bharat on 30/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseServicesViewController : UIViewController

- (CGFloat) getContentHeight;
- (void) scrollContent : (CGPoint) offset;
- (CGPoint) getScrollContent ;

@property(nonatomic,strong)    NSString *selLatitude;
@property(nonatomic,strong)    NSString *selLongitude;

- (void) loadStoresForSalonServices ;

- (void) loadStoresForHomeServices ;

- (void) displayStoreRateCard : (NSDictionary *) storesDict;

- (void) displayStoreRateCardUsingService : (NSDictionary *) serviceDict;

- (void) getServicesByKeyword : (NSString *) queryText;

// Called, this method after user select location from choose user location viewcontrolller.
- (void) loadNearbyStoresaAfterUserSelectLocation : (NSString *)latitude : (NSString *)Longitude : (NSString *)dlvType;

/// Update location  on top.
- (void) locationUpdatedOnTop;


/// For segment controller Action, when user clicks on the segment controller then this method will fire.
// New functionality.
-(IBAction)CallingServiceForNearbyHomeWorkSegmentedAction:(UISegmentedControl *)segIndex;


@end
