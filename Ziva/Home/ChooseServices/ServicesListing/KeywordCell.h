//
//  KeywordCell.h
//  Ziva
//
//  Created by Bharat on 16/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KeywordCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *mainV;
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *subtitleL;
@property (weak, nonatomic) IBOutlet UILabel *priceL;

@property (weak, nonatomic) IBOutlet UILabel *strikePriceL;
@property (weak, nonatomic) IBOutlet UILabel *zivaPriceL;
@property (weak, nonatomic) IBOutlet UILabel *cashBackPriceL;
@property (weak, nonatomic) IBOutlet UIButton *redirectB;

- (void) configureDataForCell : (NSDictionary *) item;

@end
