//
//  ServiceCell.m
//  Ziva
//
//  Created by Bharat on 15/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "ServiceCell.h"

@implementation ServiceCell

- (void)awakeFromNib {
    // Initialization code
    
    [super awakeFromNib];
    
    // Fix : Autoresizing issues for iOS 7 using Xcode 6
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //    Fix : For right aligned view with autoresizing
    UIView *backV = [[self.contentView subviews] objectAtIndex:0];
    backV.frame = [UpdateFrame setSizeForView:backV usingSize:self.bounds.size];
}

- (void) configureDataForCell : (NSDictionary *) item
{    
    self.titleL.text =  [ReadData stringValueFromDictionary:item forKey:@"ServiceName"];
    self.subtitleL.text = [ReadData stringValueFromDictionary:item forKey:@"StoreName"];
    self.priceL.text = [ReadData amountInRsFromDictionary:item forKey:@"Price"];

}

@end
