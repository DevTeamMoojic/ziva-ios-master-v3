
//
//  KeywordCell.m
//  Ziva
//
//  Created by Bharat on 16/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "KeywordCell.h"

@implementation KeywordCell

- (void)awakeFromNib {
    // Initialization code
    
    [super awakeFromNib];
    
    // Fix : Autoresizing issues for iOS 7 using Xcode 6
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //    Fix : For right aligned view with autoresizing
    UIView *backV = [[self.contentView subviews] objectAtIndex:0];
    backV.frame = [UpdateFrame setSizeForView:backV usingSize:self.bounds.size];
}


- (void) configureDataForCell : (NSDictionary *) item
{
    self.titleL.text = [ReadData stringValueFromDictionary:item forKey:@"ServiceName"];
    //[self.subtitleL sizeToFit];
    self.subtitleL.text = [ReadData stringValueFromDictionary:item forKey:@"StoreName"];
    self.priceL.text = [ReadData amountInRsFromDictionary:item forKey:KEY_SERVICE_ZIVAPRICE];
    
    // VARUN
    NSInteger actualCMP = [[ReadData stringValueFromDictionary:item forKey:KEY_SERVICE_PRICE] floatValue];
    NSInteger zivaCMP = [[ReadData stringValueFromDictionary:item forKey:KEY_SERVICE_ZIVAPRICE] floatValue];
    
    NSString *str = [ReadData amountInRsFromDictionary:item forKey:KEY_SERVICE_PRICE];
    
    NSAttributedString * strikePriceAtt =   [[NSAttributedString alloc] initWithString:str
                                                                   attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle)}];
    
    self.strikePriceL.text = [ReadData amountInRsFromDictionary:item forKey:KEY_SERVICE_PRICE];
    self.zivaPriceL.text = [ReadData amountInRsFromDictionary:item forKey:KEY_SERVICE_ZIVAPRICE];

    
    [self.strikePriceL setAttributedText:strikePriceAtt];
    
//    // For strikeline in cernter on text .
//    NSAttributedString * title =
//    [[NSAttributedString alloc] initWithString:[ReadData amountInRsFromDictionary:self.strikePriceL.text forKey:KEY_SERVICE_PRICE]
//                                    attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle)}];
//    [self.strikePriceL setAttributedText:title];
    
    // discount price.
    //self.priceDiscountL.text = [ReadData amountInRsFromDictionary:item forKey:KEY_SERVICE_DISCOUNT_PRICE];
    

    // new implementation.
    // NSArray *cashbacks = [ReadData arrayFromDictionary:item forKey:KEY_CASHBACKS];
    //CGFloat positionY = (CGRectGetHeight(self.priceInfoV.frame) - CGRectGetHeight(self.offerpriceL.frame)) *0.5;
    
    

    NSArray *cashbacks = [ReadData arrayFromDictionary:item forKey:KEY_CASHBACKS];

    self.strikePriceL.hidden = YES;
    self.zivaPriceL.hidden = YES;
    self.cashBackPriceL.hidden = YES;

    // cash back and discount caliculation.
    //NSLog(@"actual--- %ld",(long)actualCMP);
    //NSLog(@"ziva --- %ld",(long)zivaCMP);
    // if the looks is cash back else to discount values.
    if([cashbacks count] > 0){
       
        self.strikePriceL.hidden = YES;
        self.zivaPriceL.hidden = YES;
        self.cashBackPriceL.hidden = NO;
        
        //
        // diffrenciate the amount and percent.
        NSArray *cashbackArray = [[ReadData arrayFromDictionary:item forKey:KEY_CASHBACKS] firstObject];
        //NSLog(@"%@", cashbackArray);
        NSInteger priceMode = [[cashbackArray valueForKey:@"Mode"] integerValue];
        // NSLog(@"%ld", (long)priceMode);
        // NSString *priceMode1 = [cashbackArray valueForKey:@"Cost"];
        //NSLog(@"%@", priceMode1);
        
        // if the it 0 the values is cash eles the percentage.
        if (priceMode == 0) {
            self.cashBackPriceL.text = [NSString stringWithFormat:@"%@ Cash Back",[ReadData getAmountFormatted:[[cashbackArray valueForKey:@"Cost"] floatValue]]];
        }else{
            NSString *precent = @"%";
            self.cashBackPriceL.text = [NSString stringWithFormat:@"Cash Back %@%@",[cashbackArray valueForKey:@"Cost"],precent];
        }
        
        
    }
    else if(actualCMP > zivaCMP){ //disctount
        self.strikePriceL.hidden = NO;
        self.zivaPriceL.hidden = NO;
        self.cashBackPriceL.hidden = YES;
    }else{
        self.strikePriceL.hidden = YES;
        self.zivaPriceL.hidden = YES;
        self.cashBackPriceL.hidden = YES;
    }
 }

@end
