//
//  ServiceCell.h
//  Ziva
//
//  Created by Bharat on 15/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *mainV;

@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *subtitleL;
@property (weak, nonatomic) IBOutlet UILabel *priceL;
@property (weak, nonatomic) IBOutlet UILabel *distanceL;


- (void) configureDataForCell : (NSDictionary *) item;

@end
