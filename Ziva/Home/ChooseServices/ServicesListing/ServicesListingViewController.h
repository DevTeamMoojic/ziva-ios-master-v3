//
//  ServicesListingViewController.h
//  Ziva
//
//  Created by Bharat on 15/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServicesListingViewController : UIViewController

-(void) loadKeywords : (NSArray *) filteredDataList;

- (void) loadServices : (NSArray *) filteredDataList;

- (CGFloat) getContentHeight;

@end
