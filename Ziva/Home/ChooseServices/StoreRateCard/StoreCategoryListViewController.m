//
//  StoreCategoryListViewController.m
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "ServicesCell.h"
#import "StoreRateCardViewController.h"
#import "StoreCategoryListViewController.h"

@interface StoreCategoryListViewController (){
    CGFloat contentHeight;
    NSMutableArray *list;
    
    __weak IBOutlet UIScrollView *mainscrollView;
    __weak IBOutlet UITableView *listTblV;
    __weak IBOutlet UILabel *noresultsL;
    
    BOOL isStoresRateCard;
    AppDelegate *appD;
    
    BOOL isExpandingCollapsing;
}
@end

@implementation StoreCategoryListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [mainscrollView removeGestureRecognizer:mainscrollView.panGestureRecognizer];
    [self resetLayout];
}
-(void) viewWillAppear:(BOOL)animated{
    
    NSLog(@"viewwill appear");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods

- (void) loadData: (NSArray *) listArray andIsRateCard : (BOOL) isRateCard{
    
    NSLog(@"%@", listArray);
    
    
    [listTblV setContentOffset:CGPointZero animated:YES];
    
    //NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:KEY_SERVICE_TITLE ascending:YES];
    //NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    //list = [listArray sortedArrayUsingDescriptors:sortDescriptors];
    
    list = [listArray mutableCopy];
    isStoresRateCard = isRateCard;
    [listTblV reloadData];
    
    listTblV.frame = [UpdateFrame setSizeForView:listTblV usingHeight:[self calculateHeightForTable]];
    
    contentHeight = CGRectGetMaxY(listTblV.frame);
    
    if (contentHeight <= CGRectGetHeight(mainscrollView.bounds))
    {
        contentHeight = CGRectGetHeight(mainscrollView.bounds);
        mainscrollView.scrollEnabled = NO;
    }
    
    noresultsL.hidden = ([list count] > 0);
    listTblV.hidden = ([list count] == 0);
    
    CGPoint scrollToPoint = CGPointZero;
    StoreRateCardViewController *parentVC = (StoreRateCardViewController *)self.parentViewController;
    [mainscrollView setContentOffset:scrollToPoint animated:YES];
    [parentVC updateContentSize : scrollToPoint];
    

    
    
    /*
    // *************************************** Start old code****************************************

    // This code for just display topaly with list using array
    //
    [listTblV setContentOffset:CGPointZero animated:NO];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:KEY_SERVICE_TITLE ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    list = [listArray sortedArrayUsingDescriptors:sortDescriptors];
    
    isStoresRateCard = isRateCard;
    [listTblV reloadData];
    
    listTblV.frame = [UpdateFrame setSizeForView:listTblV usingHeight:[self calculateHeightForTable]];
    
    contentHeight = CGRectGetMaxY(listTblV.frame);
    
    if (contentHeight <= CGRectGetHeight(mainscrollView.bounds))
    {
        contentHeight = CGRectGetHeight(mainscrollView.bounds);
        mainscrollView.scrollEnabled = NO;
    }
    
    noresultsL.hidden = ([list count] > 0);
    listTblV.hidden = ([list count] == 0);
 
    CGPoint scrollToPoint = CGPointZero;
    StoreRateCardViewController *parentVC = (StoreRateCardViewController *)self.parentViewController;
    [mainscrollView setContentOffset:scrollToPoint animated:YES];
    [parentVC updateContentSize : scrollToPoint];
    */
    
    // *************************************** End old code****************************************
    
    
    
    
    
}

- (void) reloadData
{
    [listTblV reloadData];
}

#pragma mark - Methods

-(void) resetLayout
{
    mainscrollView.contentOffset = CGPointZero;
    contentHeight = CGRectGetHeight(self.view.bounds);
}

- (CGFloat) calculateHeightForTable{
    CGFloat height = 0;
    for(NSInteger ctr =0; ctr < [list count]; ctr++){
        height += [self calculateheightForRow:ctr];
    }
   // height +=60;
    
    //Sub service row count.
    NSInteger subCount = 0;
    // row count.
    for (NSDictionary *dict in list) {
        
        NSArray *subservicecount = [ dict valueForKey:@"SubCategoryServicesList"];
       // NSLog(@"%ld", (long) subservicecount.count);
        subCount = subCount + subservicecount.count;
    }
    
    //NSLog(@" final count %ld",(long)a * 60);
    
    height += 60 * subCount ;
    
   return height;
}

#pragma mark - Scroll content

- (CGFloat) getContentHeight;
{
    return contentHeight;
}

- (void) scrollContent : (CGPoint) offset
{
    mainscrollView.contentOffset = offset;
}

- (CGPoint) getScrollContent
{
    return mainscrollView.contentOffset;
}

#pragma mark - Table view data source

-(CGFloat) calculateheightForRow:(NSInteger)row{
    
    CGFloat height = 00.0f;
    if (isExpandingCollapsing) {
        height = 60.0f;
        
    }else{
        height = 60.0f;
    }
    return height;
    /*
    CGFloat height = 60.0f;
    //NSDictionary *item = list[indexPath.row];
    return height;
     */
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [list count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self calculateheightForRow:indexPath.row];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (isExpandingCollapsing) {
        
        ServicesCell *cell = (ServicesCell *)[tableView dequeueReusableCellWithIdentifier:@"servicescell" forIndexPath:indexPath];
        NSDictionary *item = list[indexPath.row];
        cell.isStoreRateCard = isStoresRateCard;
        [cell configureDataForCell :item];
        
        cell.showOffersB.tag = cell.selectServicesB.tag = cell.actionB.tag = indexPath.row;
        cell.selectServicesB.selected = [appD.sessionDelegate.mycartC isAddedToCart:[ReadData recordIdFromDictionary:item forKey:KEY_ID]];
        
        if(!cell.showOffersB.hidden){
            [cell.actionB addTarget:self action:@selector(expandSectionBPressed:) forControlEvents:UIControlEventTouchUpInside];
        }
        else if (!cell.selectServicesB.hidden){
            [cell.actionB addTarget:self action:@selector(addToCartBPressed:) forControlEvents:UIControlEventTouchUpInside];
        }

        
        return cell;
    }else{
        
        NSString *Title= [[list objectAtIndex:indexPath.row] valueForKey:@"ServiceSubCategoryName"];
        return [self createCellWithTitle:Title image:[[list objectAtIndex:indexPath.row] valueForKey:@"Image name"] indexPath:indexPath];
        
    }
    

    
    /*
    ServicesCell *cell = (ServicesCell *)[tableView dequeueReusableCellWithIdentifier:@"servicescell" forIndexPath:indexPath];
    NSDictionary *item = list[indexPath.row];
    cell.isStoreRateCard = isStoresRateCard;
    [cell configureDataForCell :item];
    
    cell.showOffersB.tag = cell.selectServicesB.tag = cell.actionB.tag = indexPath.row;
    cell.selectServicesB.selected = [appD.sessionDelegate.mycartC isAddedToCart:[ReadData recordIdFromDictionary:item forKey:KEY_ID]];
    
    if(!cell.showOffersB.hidden){
        [cell.actionB addTarget:self action:@selector(expandSectionBPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if (!cell.selectServicesB.hidden){
        [cell.actionB addTarget:self action:@selector(addToCartBPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;*/
}

-(void)expandSectionBPressed:(id)sender
{
//    isExpandingCollapsing = YES;
//    
//    UIButton *btn = (UIButton *)sender;
//    NSInteger selectedRow = btn.tag;
//    NSMutableDictionary *item = lstReservations[selectedRow];
//    
//    btn.selected = !btn.selected;
//    [item setObject: btn.selected ? @"1" : @"0" forKey:KEY_SECTION_DISPLAYSTATE];
//    
//    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:selectedRow inSection:0];
//    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
//    [listingTblV reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
}

-(void)addToCartBPressed:(UIButton *)sender{
    NSInteger selectedRow = sender.tag;
    NSDictionary *item = list[selectedRow];
    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:selectedRow inSection:0];
    ServicesCell *cell = (ServicesCell *)[listTblV cellForRowAtIndexPath:rowToReload];
    cell.selectServicesB.selected = !cell.selectServicesB.selected;
    
    StoreRateCardViewController *parentVC = (StoreRateCardViewController *)self.parentViewController;
    if(cell.selectServicesB.selected){
        [parentVC addItemToCart:item];
    }
    else{
        [parentVC removeItemFromCart:item];
    }
}



/// new implementtion

- (UITableViewCell*)createCellWithTitle:(NSString *)title image:(UIImage *)image  indexPath:(NSIndexPath*)indexPath
{
    NSString *CellIdentifier = @"servicesHeaderCell";
    ServicesCell* cell = [listTblV dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.lblTitle.text = title;
    cell.lblTitle.textColor = [UIColor blackColor];
    [cell setIndentationLevel:[[[list objectAtIndex:indexPath.row] valueForKey:@"ServiceSubCategoryId"] intValue]];
    cell.indentationWidth = 0;
    float indentPoints = cell.indentationLevel * cell.indentationWidth;
    cell.contentView.frame = CGRectMake(indentPoints,cell.contentView.frame.origin.y,cell.contentView.frame.size.width - indentPoints,cell.contentView.frame.size.height);
 
    NSDictionary *dict = [list objectAtIndex:indexPath.row] ;
    
    // For view shadow.
    [appD shadowView:cell.mainV];
    [cell.mainV.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [cell.mainV.layer setBorderWidth:0.2f];
   
    
    // drop shadow
    [cell.mainV.layer setShadowColor:[UIColor darkGrayColor].CGColor];
    [cell.mainV.layer setShadowOpacity:0.3];
    [cell.mainV.layer setShadowRadius:4.0];
    [cell.mainV.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
    
    

    if([dict valueForKey:@"SubCategoryServicesList"])
    {
        cell.btnExpand.alpha = 1.0;
        [cell.btnExpand addTarget:self action:@selector(showSubItems:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    else
    {
        cell.btnExpand.alpha = 0.0;
        
    }
    return cell;
    
    
}

-(void)showSubItems :(id) sender
{
    
    isExpandingCollapsing = YES;
    UIButton *btn = (UIButton*)sender;
    CGRect buttonFrameInTableView = [btn convertRect:btn.bounds toView:listTblV];
    NSIndexPath *indexPath = [listTblV indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
//    CustomCell *cell = (CustomCell*)[tblView1 cellForRowAtIndexPath:myIP];
//    UIButton *btn = cell.btn;
    
    ServicesCell *cell = (ServicesCell*)[listTblV cellForRowAtIndexPath:indexPath];

   //icon_sectionheader_collapsed_white
    
    if(btn.alpha==1.0)
    {
        if ([[btn imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"icon_sectionheader_collapsed_white"]])
        {
            [btn setImage:[UIImage imageNamed:@"icon_sectionheader_expanded_white"] forState:UIControlStateNormal];
            //[cell.btnExpand setImage:[UIImage imageNamed:@"icon_section_expanded"] forState:UIControlStateNormal];
            
           // [cell.btnImg setImage:[UIImage imageNamed:@"icon_section_collapsed"] forState:UIControlStateNormal];
            
             [cell.btnImg setImage:[UIImage imageNamed:@"icon_section_expanded"] forState:UIControlStateNormal];
            
        }
        else
        {
           // [cell.btnExpand setImage:[UIImage imageNamed:@"icon_section_collapsed"] forState:UIControlStateNormal];

            [btn setImage:[UIImage imageNamed:@"icon_sectionheader_collapsed_white"] forState:UIControlStateNormal];
            [cell.btnImg setImage:[UIImage imageNamed:@"icon_section_collapsed"] forState:UIControlStateNormal];
           // [cell.btnImg setImage:[UIImage imageNamed:@"icon_section_expanded"] forState:UIControlStateNormal];
        }
        
    }
    
    NSDictionary *dict=[list objectAtIndex:indexPath.row] ;
    NSArray *arr=[dict valueForKey:@"SubCategoryServicesList"];
    if([dict valueForKey:@"SubCategoryServicesList"])
    {
        BOOL isTableExpanded=NO;
        for(NSDictionary *subitems in arr )
        {
            NSInteger index=[list indexOfObjectIdenticalTo:subitems];
            isTableExpanded=(index>0 && index!=NSIntegerMax);
            if(isTableExpanded) break;
        }
        
        if(isTableExpanded)
        {
            [self CollapseRows:arr];
        }
        else
        {
            NSUInteger count=indexPath.row+1;
            NSMutableArray *arrCells=[NSMutableArray array];
            for(NSDictionary *dInner in arr )
            {
                [arrCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
                [list insertObject:dInner atIndex:count++];
            }
            
            listTblV.frame = [UpdateFrame setSizeForView:listTblV usingHeight:[self calculateHeightForTable]];
            
            contentHeight = CGRectGetMaxY(listTblV.frame);
            
            if (contentHeight <= CGRectGetHeight(mainscrollView.bounds))
            {
                contentHeight = CGRectGetHeight(mainscrollView.bounds);
                mainscrollView.scrollEnabled = NO;
            }

            [listTblV insertRowsAtIndexPaths:arrCells withRowAnimation:UITableViewRowAnimationFade];
        }
    }
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    /*
     isExpandingCollapsing = YES;
     NSDictionary *dic=[list objectAtIndex:indexPath.row];
     if([dic valueForKey:@"SubCategoryServicesList"])
     {
     NSArray *arr=[dic valueForKey:@"SubCategoryServicesList"];
     BOOL isTableExpanded=NO;
     
     for(NSDictionary *subitems in arr )
     {
     NSInteger index=[list indexOfObjectIdenticalTo:subitems];
     isTableExpanded=(index>0 && index!=NSIntegerMax);
     if(isTableExpanded) break;
     }
     
     if(isTableExpanded)
     {
     [self CollapseRows:arr];
     }
     else
     {
     NSUInteger count=indexPath.row+1;
     NSMutableArray *arrCells=[NSMutableArray array];
     for(NSDictionary *dInner in arr )
     {
     [arrCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
     [list insertObject:dInner atIndex:count++];
     }
     [listTblV insertRowsAtIndexPaths:arrCells withRowAnimation:UITableViewRowAnimationLeft];
     }
     }*/
    
}

-(void)CollapseRows:(NSArray*)array
{
    for(NSDictionary *dInner in array )
    {
        NSUInteger indexToRemove=[list indexOfObjectIdenticalTo:dInner];
        NSArray *arInner=[dInner valueForKey:@"SubCategoryServicesList"];
        if(arInner && [arInner count]>0)
        {
            [self CollapseRows:arInner];
        }
        
        if([list indexOfObjectIdenticalTo:dInner]!=NSNotFound)
        {
            [list removeObjectIdenticalTo:dInner];
            [listTblV deleteRowsAtIndexPaths:[NSArray arrayWithObject:
                                              [NSIndexPath indexPathForRow:indexToRemove inSection:0]
                                              ]
                            withRowAnimation:UITableViewRowAnimationFade];
        }
    }
}


@end
