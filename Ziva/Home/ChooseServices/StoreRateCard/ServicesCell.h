//
//  ServicesCell.h
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServicesCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *mainV;

@property (weak, nonatomic) IBOutlet UIView *serviceHeadingV;
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *priceL;

@property (weak, nonatomic) IBOutlet UIButton *actionB;
@property (weak, nonatomic) IBOutlet UIButton *showOffersB;
@property (weak, nonatomic) IBOutlet UIButton *selectServicesB;

@property (weak, nonatomic) IBOutlet UIView *offersV;

@property (nonatomic) BOOL isStoreRateCard;

// minnarao new code.
@property (weak, nonatomic) IBOutlet UILabel *priceActualL;
@property (weak, nonatomic) IBOutlet UILabel *priceDiscountL;
@property (weak, nonatomic) IBOutlet UILabel *DiscountCouponL;
@property (weak, nonatomic) IBOutlet UILabel *DiscountCouponCashBackL;


// header cell out lets.

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnExpand;
@property (weak, nonatomic) IBOutlet UIImageView *btnExpandImg;
@property (weak, nonatomic) IBOutlet UIButton *btnImg;




- (void) configureDataForCell : (NSDictionary *) item ;

@end
