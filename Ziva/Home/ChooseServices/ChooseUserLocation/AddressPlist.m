//
//  AddressPlist.m
//  Ziva
//
//  Created by Minna Rao on 05/01/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "AddressPlist.h"

@implementation AddressPlist

- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self)
    {
        self.flate = dictionary[kNoteCategory];
        self.landmark = dictionary[kNoteName];
        self.location = dictionary[kNoteEvent];
        self.type = dictionary[kNoteType];
        self.slocation = dictionary[kSLocation];

    }
    
    return self;
}

+ (NSString *)userNotesDocumentPath
{
    NSString *documentsPath  = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:@"UserAddAddress.plist"];
    
    return documentsPath;
    
}

+ (NSArray *)savedAddress
{
    NSString *documentsPath = [self userNotesDocumentPath];
    NSArray *savedNotes = [NSArray arrayWithContentsOfFile:documentsPath];
    NSMutableArray *savedUserNotes = [@[] mutableCopy];
    for (NSDictionary *dict in savedNotes) {
        AddressPlist *addressPlist = [[AddressPlist alloc]initWithDictionary:dict];
        [savedUserNotes addObject:addressPlist];
    }
    
    return savedUserNotes;
    
}

- (NSDictionary *)userNoteDictionary
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    if (self.flate) {
        dict[kNoteCategory] = self.flate;
    }
    if (self.landmark) {
        dict[kNoteName] = self.landmark;
    }
    if (self.location) {
        dict[kNoteEvent] = self.location;
    }
    if (self.type) {
        dict[kNoteType] = self.type;
    }
    if (self.slocation) {
        dict[kSLocation] = self.slocation;
    }

    return dict;
}

- (void)saveUserNotesToPlist:(NSArray *)userNotes
{
    NSMutableArray *mutableUserNotes = [@[] mutableCopy];
    for (AddressPlist *addressPlist in userNotes) {
        NSDictionary *dict = [addressPlist userNoteDictionary];
        [mutableUserNotes addObject:dict];
    }
    NSString *documentsPath  = [AddressPlist userNotesDocumentPath];
    [mutableUserNotes writeToFile:documentsPath atomically:YES];
}

#pragma mark - Introspection

- (BOOL)isEqualToNote:(AddressPlist *)note{
    if (![note.flate isEqualToString:self.flate]) {
        return FALSE;
    }else if (![note.landmark isEqualToString:self.landmark]) {
        return FALSE;
    }else if(![note.location isEqualToString:self.location]){
        return FALSE;
    }else if(![note.type isEqualToString:self.type]){
        return FALSE;
    }else if(![note.slocation isEqualToString:self.slocation]){
        return FALSE;
    }else{
        return TRUE;
    }
}

- (BOOL)isEqual:(id)object{
    if (object == self) {
        return TRUE;
    }else if([object isKindOfClass:[self class]]){
        return [self isEqualToNote:object];
    }else{
        return FALSE;
    }
}

#pragma mark - Save

- (void)save
{
    NSMutableArray *savedNotes = [[AddressPlist savedAddress] mutableCopy];
    [savedNotes addObject:self];
    [self saveUserNotesToPlist:savedNotes];
}

- (void)remove
{
    NSMutableArray *savedNotes = [[AddressPlist savedAddress] mutableCopy];
    
    if ([savedNotes containsObject:self]) {
        [savedNotes removeObject:self];
        [self saveUserNotesToPlist:savedNotes];
    }
    
}

@end
