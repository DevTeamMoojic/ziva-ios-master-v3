//
//  AddEditAddressViewController.h
//  Ziva
//
//  Created by Minnarao on 3/1/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddEditAddressViewController : UIViewController

//@property (nonatomic, strong) NSString *flatStr;
//@property (nonatomic, strong) NSString *locationSubStr;
@property (nonatomic, strong) NSString *flatStr;
@property (nonatomic, strong) NSString *landmarStr;

@property (nonatomic, strong) NSString *locationStr;
@property (nonatomic, strong) NSString *locationSubStr;
@property (nonatomic,assign) int btntag;

@property (nonatomic,assign) BOOL isAddressEdit;

@property (nonatomic, strong) NSString *LatStr;
@property (nonatomic, strong) NSString *LongStr;

@end
