//
//  UserAddAddressCell.h
//  Ziva
//
//  Created by Minnarao on 3/1/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserAddAddressCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *locationL;
@property (weak, nonatomic) IBOutlet UILabel *subLocationL;
@property (weak, nonatomic) IBOutlet UIImageView *mainImgV;

- (void) configureDataForCell : (NSDictionary *) item;

@end
