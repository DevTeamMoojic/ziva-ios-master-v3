//
//  UserAddAddressViewController.m
//  Ziva
//
//  Created by Minnarao on 3/1/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "UserAddAddressViewController.h"
#import "UserAddAddressCell.h"
#define ROW_HEIGHT 110
#import "ChooseUserLocationViewController.h"
#import "AddressPlist.h"
#import "AddEditAddressViewController.h"
#import "CartDisplayViewController.h"

@interface UserAddAddressViewController ()<APIDelegate,UIActionSheetDelegate,CartSummaryDelegate>{
    AppDelegate *appD;
    __weak IBOutlet UIImageView *mainImgV;
    
    NSArray *list;


    CGRect cachedImageViewSize;
    CGFloat yscrollOffset;
    
    BOOL isBackFromOtherView;
    BOOL isContentSizeUpdated;
    
    __weak IBOutlet UITableView *listTblV;
    __weak IBOutlet UILabel *noresultsL;
    __weak IBOutlet UIButton *addressB;

    // for cell outlets
 
    __weak IBOutlet UIImage *homeandaddressIMG;
    __weak IBOutlet UIImage *editaddressIMG;

    
    __weak CartSummaryViewController *cartsummaryVC;
    MyCartController *cartC;
    
    __weak CartDisplayViewController *cartDisplayViewController;
    NSString *deliveryType;
}

@end

@implementation UserAddAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
   // NSDictionary *dict = [[NSDictionary alloc]init];
    //[dict setValue:@"chennai" forKey:@"UserAddress"];
    

   // list = [[NSArray alloc]initWithObjects:@"mumba",@"chennai", nil];
    
     cartC = appD.sessionDelegate.mycartC;
   // [self getAddress];

}

-(void) getAddress{
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"Usersavedaddress.plist"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath])
    {
        plistPath = [[NSBundle mainBundle] pathForResource:@"Usersavedaddress" ofType:@"plist"];
    }
    
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
   // NSLog(@"%@",dict);
   
    
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!isBackFromOtherView)
        cachedImageViewSize = mainImgV.frame;
    else
        isBackFromOtherView = NO;
    
      //self.address = [[AddressPlist savedAddress]mutableCopy];
    //NSLog(@"%@",self.address.firstObject);
    //NSLog(@"%@", self.address)
    /*
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"addres.plist"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath])
    {
        plistPath = [[NSBundle mainBundle] pathForResource:@"addres" ofType:@"plist"];
    }
    
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    
    list = [dict valueForKey:@"New item"]; */
    
    
    //NSDictionary *retrievedDictionary = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HOMEDDRESS"];
   // NSLog(@"%@",retrievedDictionary.allKeys);
    
    NSMutableArray *arrary = [[NSMutableArray alloc]init];
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"HOMEADDRESS"]) {
        [arrary addObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"HOMEADDRESS"]];
    }
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"WORKADDRESS"]){
        [arrary addObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"WORKADDRESS"]];

    }
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"OTHERADDRESS"]){
        [arrary addObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"OTHERADDRESS"]];
    }
    
    
    //NSLog(@"%@",arrary);
    //NSLog(@"%lu",(unsigned long)arrary.count);

    
    list = arrary;
    
    listTblV.separatorColor = [UIColor clearColor];
    listTblV.hidden = ([list count] == 0);
    noresultsL.hidden = !listTblV.hidden ;
    
    [listTblV reloadData];

   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

-(CGFloat) calculateheightForRow:(NSInteger)row
{
    return ROW_HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 40.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
    //footerView.backgroundColor = [UIColor yellowColor];
    UIButton *addressbtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [addressbtn setImage:[UIImage imageNamed:@"add_new_address"] forState:UIControlStateNormal];
    //[addressbtn setTitle:@"Add Address" forState:UIControlStateNormal];
    [addressbtn addTarget:self action:@selector(addAddressAction:) forControlEvents:UIControlEventTouchUpInside];
    [addressbtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];//set the color this is may be different for iOS 7
    
    addressbtn.frame=CGRectMake(20, 5, self.view.frame.size.width-40, 40); //set some large width to ur title
    addressbtn.layer.cornerRadius = 5; // this value vary as per your desire
    addressbtn.clipsToBounds = YES;
    
    addressbtn.backgroundColor = [UIColor colorWithRed:250.0/255.0 green:183.0/255.0 blue:63.0/255.0 alpha:1.0];    [footerView addSubview:addressbtn];
    
    /*
     [footerView addConstraints:
     @[[NSLayoutConstraint constraintWithItem:addressbtn
     attribute:NSLayoutAttributeCenterX
     relatedBy:NSLayoutRelationEqual
     toItem:footerView
     attribute:NSLayoutAttributeCenterX
     multiplier:1 constant:0],
     [NSLayoutConstraint constraintWithItem:addressbtn
     attribute:NSLayoutAttributeCenterY
     relatedBy:NSLayoutRelationEqual
     toItem:footerView
     attribute:NSLayoutAttributeCenterY
     multiplier:1 constant:0]]];*/
 
    return footerView;
 
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [list count] ;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self calculateheightForRow:indexPath.row];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    static NSString *MyIdentifier = @"MyIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:MyIdentifier];
    }
    
    /*
    AddressPlist *addressPlist = self.address[indexPath.row];
  
    UILabel *addresssL = (UILabel*)[cell viewWithTag:1111];
    addresssL.text = [NSString stringWithFormat:@"%@%@",addressPlist.flate,addressPlist.landmark];
    UILabel *addresssLocation = (UILabel*)[cell viewWithTag:2222];
    addresssLocation.text = addressPlist.location;
    
    //button
    UIButton *editButton = (UIButton*)[cell viewWithTag:4444];
    editButton.tag = indexPath.row;
    [editButton addTarget:self action:@selector(editButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    // delete button Action.
    UIButton *deleteButton = (UIButton*)[cell viewWithTag:3333];
    deleteButton.tag = indexPath.row;
    [deleteButton addTarget:self action:@selector(deleteAddressAction:) forControlEvents:UIControlEventTouchUpInside];


    cell.selectionStyle = UITableViewCellAccessoryNone;*/
    
    
    NSDictionary *dict = list[indexPath.row];
    
    UILabel *addresssL = (UILabel*)[cell viewWithTag:1111];
    //addresssL.text = [dict valueForKey:@"FLATADDRESS"];
    addresssL.text = [NSString stringWithFormat:@"%@ %@",[dict valueForKey:@"FLATADDRESS"], [dict valueForKey:@"LANDMARKADDRESS"]];
    
    UILabel *addresssLocation = (UILabel*)[cell viewWithTag:2222];
    addresssLocation.text = [dict valueForKey:@"LOCATIONSTR"];

    /*
    //button
    UIButton *editButton = (UIButton*)[cell viewWithTag:4444];
    editButton.tag = indexPath.row;
    [editButton addTarget:self action:@selector(editButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    // delete button Action.
    UIButton *deleteButton = (UIButton*)[cell viewWithTag:3333];
    deleteButton.tag = indexPath.row;
    [deleteButton addTarget:self action:@selector(deleteAddressAction:) forControlEvents:UIControlEventTouchUpInside];
    */
    
    // More button Acdtion and implementation.
        UIButton *moreButton = (UIButton*)[cell viewWithTag:3333];
    moreButton.tag = indexPath.row;
    [moreButton addTarget:self action:@selector(moreBAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    // header title.
    UILabel *headertitle = (UILabel*)[cell viewWithTag:5555];
    headertitle.text = @"";
    // set images view.
    UIImageView *img = (UIImageView*)[cell viewWithTag:6666];
    
  
    // view rounded rect.
    UIView *view = (UIView*)[cell viewWithTag:7777];
    view.layer.cornerRadius = 10;
    view.layer.masksToBounds = YES;

    
 
    // image and header title set
    if([[dict valueForKey:@"Type"] isEqualToString:@"1"]){
         headertitle.text = @"HOME ADDRESS";
          [img setImage:[UIImage imageNamed:@"address_home_selected"]];
    }
    if ([[dict valueForKey:@"Type"] isEqualToString:@"2"]){
        headertitle.text = @"WORK ADDRESS";
          [img setImage:[UIImage imageNamed:@"address_work_selected"]];

    }
    if ([[dict valueForKey:@"Type"] isEqualToString:@"3"]){
        headertitle.text = @"OTHER ADDRESS";
          [img setImage:[UIImage imageNamed:@"address_others_selected"]];
        
    }
    
    /*
    if (indexPath.row == 0) {
        headertitle.text = @"HOME ADDRESS";
    } else if (indexPath.row == 1){
        headertitle.text = @"WORK ADDRESS";
    } else if (indexPath.row == 2){
        headertitle.text = @"OTHER ADDRESS";
    }
    */
    
    
    
    cell.selectionStyle = UITableViewCellAccessoryNone;
    return cell;

}

// Called after the user changes the selection.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    isBackFromOtherView = YES;
   
    // Navigation track finding.
    NSArray *array = [self.navigationController viewControllers];
   // NSLog(@"%@", array);
    
    cartC.userAddadress = list[indexPath.row];
    
    if ([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromhomeserviceVC"]||[appD.sessionDelegate.isCommingFromviewController isEqualToString:@"homeServiceaddaddress"]) {
        UIStoryboard *modalStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LocationDateStoreSelectorViewController *LDSSVC = [modalStoryboard instantiateViewControllerWithIdentifier:@"LocationDateStoreSelectorViewController"];
        //    UIStoryboardSegue *uu = [[UIStoryboardSegue alloc] initWithIdentifier:@"showlocationdate_services" source:self destination:LDSSVC];
        //
        //    [self performSegueWithIdentifier:@"showlocationdate_services" sender:Nil];
        
        [self.navigationController pushViewController:LDSSVC animated:YES];
    } else if ([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromproductVC"]||[appD.sessionDelegate.isCommingFromviewController isEqualToString:@"frompackageVC"]){
        UIStoryboard *modalStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        CartDisplayViewController *LDSSVC = [modalStoryboard instantiateViewControllerWithIdentifier:@"CartDisplayViewController"];
        [self.navigationController pushViewController:LDSSVC animated:YES];

    }
    else if ([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromlookdetailhomeserviceVC"]){
        //[cartC initForLooksWithDeliveryType:DELIVERY_TYPE_HOME];
//        NSLog(@"%@%@",[list valueForKey:@"LAT"],[list valueForKey:@"LONG"]);
//        NSString *lat = [[NSUserDefaults standardUserDefaults] valueForKey:@"LAT"];
       
        NSDictionary *dt = list[indexPath.row];
        NSString *sL = [dt valueForKey:@"LAT"];
        NSString *sLo = [dt valueForKey:@"LONG"];

        
        
        [cartC setLatitude:sL andLongitude:sLo];
        //[cartC addLooks:dataDict];
        UIStoryboard *modalStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LocationDateStoreSelectorViewController *LDSSVC = [modalStoryboard instantiateViewControllerWithIdentifier:@"LocationDateStoreSelectorViewController"];
        [self.navigationController pushViewController:LDSSVC animated:YES];
    }
 
     /*
    UIStoryboard *modalStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CartDisplayViewController *LDSSVC = [modalStoryboard instantiateViewControllerWithIdentifier:@"CartDisplayViewController"];
    
    //    UIStoryboardSegue *uu = [[UIStoryboardSegue alloc] initWithIdentifier:@"showlocationdate_services" source:self destination:LDSSVC];
    //
    //    [self performSegueWithIdentifier:@"showlocationdate_services" sender:Nil];
    
    
    
    [self.navigationController pushViewController:LDSSVC animated:YES];
    */
}

#pragma mark action sheet delegate methods.
-(void)moreBAction:(UIButton*)sender{
    UIActionSheet *moreActoneSheet = [[UIActionSheet alloc]initWithTitle:@"Please select any option" delegate:self cancelButtonTitle:@"Cancle" destructiveButtonTitle:nil otherButtonTitles:@"Edit",@"Delete", nil];
    
    moreActoneSheet.tag = sender.tag;
    [moreActoneSheet showInView:self.view];

}
// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Edit"]) {
        [self editButtonAction:actionSheet.tag];
        
    }else if([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Delete"]){
        [self deleteAddressAction:actionSheet.tag];
    }
    
}

// Edit Button Action.
-(void)editButtonAction:(NSInteger)sender{
    
    //CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:listTblV];
    //NSIndexPath *tappedIP = [listTblV indexPathForRowAtPoint:buttonPosition];
    //NSLog(@"%ld",(long)tappedIP.row);
  
    //NSDictionary *dict = list[tappedIP.row];
    //NSLog(@"%@", dict);
    
    NSDictionary *dict = list[sender];
    
    AddEditAddressViewController *addEditAddressViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddEditAddressViewController"];
    addEditAddressViewController.isAddressEdit = YES;
    addEditAddressViewController.flatStr = [dict valueForKey:@"FLATADDRESS"];
    addEditAddressViewController.landmarStr =  [dict valueForKey:@"LANDMARKADDRESS"];
    addEditAddressViewController.locationStr = [dict valueForKey:@"LOCATIONSTR"];
    addEditAddressViewController.locationSubStr = [dict valueForKey:@"LOCATIONSUBSTR"];
    addEditAddressViewController.LatStr = [dict valueForKey:@"LAT"];
    addEditAddressViewController.LongStr = [dict valueForKey:@"LONG"];

    NSString *str = [dict valueForKey:@"Type"];
    addEditAddressViewController.btntag = [str intValue];
   [self.navigationController pushViewController:addEditAddressViewController animated:YES];
    
    /*
    UIButton *button = (UIButton *)sender;
    int row = button.tag;
    
    UITableViewCell *clickedCell = (UITableViewCell *)[[sender superview] superview];
    NSIndexPath *clickedButtonPath = [listTblV indexPathForCell:clickedCell];
    AddressPlist *addressPlist = self.address[clickedButtonPath.row];
    NSLog(@"%@",addressPlist.flate);

    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"What do you want to do with the file?"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Edit", @"Delete", nil];
    
    [actionSheet showInView:self.view];*/
}

-(void)deleteAddressAction:(NSInteger)sender{
    
    /*
    CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                           toView:listTblV];
    NSIndexPath *tappedIP = [listTblV indexPathForRowAtPoint:buttonPosition];
    NSLog(@"%ld",(long)tappedIP.row);
    
    NSDictionary *dict = list[tappedIP.row];
    NSLog(@"%@", dict);*/
    
    NSDictionary *dict = list[sender];
   // NSLog(@"%@", dict);

//    //NSIndexPath *indexPath = [listTblV indexPathForCell:sender.tag];
//    NSIndexPath *indexPath = [listTblV indexPathForCell:(UITableViewCell *)sender.superview.superview];
//    
//   NSDictionary *dict = list[indexPath.row ];
    
    if([[dict valueForKey:@"Type"] isEqualToString:@"1"]) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"HOMEADDRESS"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }else if ([[dict valueForKey:@"Type"] isEqualToString:@"2"]){
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"WORKADDRESS"];
        [[NSUserDefaults standardUserDefaults]synchronize];

    }else if ([[dict valueForKey:@"Type"] isEqualToString:@"3"]){
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"OTHERADDRESS"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }
    
    //Reload table.
    NSMutableArray *ar = [[NSMutableArray alloc]init];
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"HOMEADDRESS"]) {
        [ar addObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"HOMEADDRESS"]];
    }
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"WORKADDRESS"]){
        [ar addObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"WORKADDRESS"]];
        
    }
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"OTHERADDRESS"]){
        [ar addObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"OTHERADDRESS"]];
    }
    
    
   // NSLog(@"%@",ar);
    //NSLog(@"%lu",(unsigned long)ar.count);
    
    
    list = ar;
    [listTblV reloadData];
    


    //UITableViewCell *clickedCell = (UITableViewCell *)[[sender superview] superview];
    //NSIndexPath *clickedButtonPath = [listTblV indexPathForCell:clickedCell];
    //AddressPlist *addressPlist = self.address[indexPath.row];
    
    //[addressPlist remove];
    //[list remove]
    //[list removeObjectAtIndex:indexPath.row];
    //[listTblV reloadData];
    
   // [listTblV setEditing:YES];
    
    //listTblV.allowsMultipleSelectionDuringEditing = YES;
    //[super setEditing:listTblV animated:YES];
}

// Add button action.
-(IBAction)addaddress:(UIButton*)sender{
    [self addAddressAction:sender];
}

// Called, when user click on add button.
-(void) addAddressAction:(UIButton*)sender{
    ChooseUserLocationViewController *chooseUserLocationViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChooseUserLocationViewController"];
    // userAddAddressViewController.isCommingFrom = @"HomeServiceFromCartSummary";
   // appD.sessionDelegate.isCommingFromviewController = @"homeServiceaddaddress";
    
    if ([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromhomeserviceVC"]) { // from homeservice
        appD.sessionDelegate.isCommingFromviewController = @"homeServiceaddaddress";

    } else if ([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromproductVC"]){ // from product.
        appD.sessionDelegate.isCommingFromviewController = @"fromproductVC";

    } else if ([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromlookdetailhomeserviceVC"]){ // from lookdetails home service.
        appD.sessionDelegate.isCommingFromviewController = @"fromlookdetailhomeserviceVC";
        
    } else if ([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"frompackageVC"]){ // from Packages.
        appD.sessionDelegate.isCommingFromviewController = @"frompackageVC";
        
    }

    [self.navigationController pushViewController:chooseUserLocationViewController animated:YES];
    
}


#pragma mark - Events
// Back button action.
- (IBAction)backBPressed:(UIButton *)sender
{
    NSArray *array = [self.navigationController viewControllers];
    NSLog(@"%@", array);

    if ([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromhomeserviceVC"] ||[appD.sessionDelegate.isCommingFromviewController isEqualToString:@"homeServiceaddaddress"]  ) {
        [self.navigationController popToViewController:[array objectAtIndex:1] animated:YES];

    } else if ([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"homeServiceaddaddress"]) {
        [self.navigationController popToViewController:[array objectAtIndex:1] animated:YES];
        
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
 
    }
    
}

//
//////////////////////// exesis///////
//
//- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
//    //Get the name of the current pressed button
//    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
//    
//    // NSIndexPath *indexPath = [listTblV indexPathForCell:(UITableViewCell *)actionSheet.superview];
//    
//    //    UITableViewCell *clickedCell = (UITableViewCell *)[[buttonIndex superview] superview];
//    //    NSIndexPath *clickedButtonPath = [listTblV indexPathForCell:clickedCell];
//    //    AddressPlist *addressPlist = self.address[clickedButtonPath.row];
//    //    NSLog(@"%@",addressPlist.flate);
//    //
//    if ([buttonTitle isEqualToString:@"Add"]) {
//        
//        AddEditAddressViewController *addEditAddressViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddEditAddressViewController"];
//        addEditAddressViewController.isAddressEdit = @"0";
//        [self.navigationController pushViewController:addEditAddressViewController animated:YES];
//        
//        
//    }else if ([buttonTitle isEqualToString:@"Edit"]) {
//        
//        AddEditAddressViewController *addEditAddressViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddEditAddressViewController"];
//        addEditAddressViewController.isAddressEdit = @"1";
//        [self.navigationController pushViewController:addEditAddressViewController animated:YES];
//        
//    }else if ([buttonTitle isEqualToString:@"Delete"]) {
//        NSLog(@"Other 3 pressed");
//    }else if ([buttonTitle isEqualToString:@"Cancel Button"]) {
//        NSLog(@"Cancel pressed --> Cancel ActionSheet");
//    }
//    
//    
//}
//

@end
