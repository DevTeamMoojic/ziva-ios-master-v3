//
//  ChooseUserLocationViewController.m
//  Ziva
//
//  Created by Leena on 21/12/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "ChooseUserLocationViewController.h"
#define ROW_HEIGHT 45
#import "ChooseUserLocationCell.h"
#import "ChooseServicesViewController.h"
#import "AddEditAddressViewController.h"

@interface ChooseUserLocationViewController ()<APIDelegate,MBProgressHUDDelegate,UISearchBarDelegate,UISearchDisplayDelegate>
{
    MBProgressHUD *HUD;
    
    NSArray *list;
    CGSize layoutsize;
    
    __weak IBOutlet UIView *optionsV;
    __weak IBOutlet UILabel *screenTitleL;
    
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIView *topImageV;
    __weak IBOutlet UIImageView *mainImgV;
    
    __weak IBOutlet UIScrollView *scrollviewOnTop;
    
    __weak IBOutlet UIScrollView *tabcontentscrollView;
    
    __weak IBOutlet UITableView *listTblV;
    __weak IBOutlet UILabel *noresultsL;
    
    __weak IBOutlet UIView *UserLocationsearchBarV;
    __weak IBOutlet UISearchBar  *userLocationsearchBar;
    
    CGRect cachedImageViewSize;
    CGFloat yscrollOffset;
    
    BOOL isBackFromOtherView;
    BOOL isContentSizeUpdated;
    
    NSInteger pageWidth;
    
    AppDelegate *appD;
    
    CGFloat contentHeight;
    NSMutableDictionary *pastHistoryDict;
    NSMutableArray *pastHistoryArray;
    
    __weak IBOutlet UIView *AddAddressView;
    __weak IBOutlet UIButton *AddAddressBtn;
    
    __weak IBOutlet UIView *EditAddressView;
    __weak IBOutlet UIButton *SaveAddressBtn;


    // new
    NSString * locationString;
    NSString * locationSubString;
      MyCartController *cartC;

    
}
@end

@implementation ChooseUserLocationViewController
@synthesize  isCommingFrom;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;
    pastHistoryDict = [[NSMutableDictionary alloc] init];
    
    // initial load the table with past history location placess.
    pastHistoryArray = appD.sessionDelegate.pastHistoryLocationArray;
    
    list = pastHistoryArray;
    //listTblV.userInteractionEnabled = NO;
    
    listTblV.hidden = ([list count] == 0);
    noresultsL.hidden = !listTblV.hidden ;
    
    [listTblV reloadData];
    userLocationsearchBar.delegate = self;
    
    
    
   /*
    // add guster for keyboard hide.
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:gestureRecognizer];*/
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!isBackFromOtherView)
        cachedImageViewSize = mainImgV.frame;
    else
        isBackFromOtherView = NO;
}
/*
#pragma mark - Layout

- (void) setupLayout
{
    pageWidth = CGRectGetWidth(self.view.bounds);
    
    [self.view addGestureRecognizer:scrollviewOnTop.panGestureRecognizer];
    [listTblV removeGestureRecognizer:listTblV.panGestureRecognizer];
    
    //Position elements
    {
        topImageV.frame = [UpdateFrame setSizeForView:topImageV usingHeight:[ResizeToFitContent getHeightForParallaxImage:self.view.frame]];
        //NSLog(@"%f, %f", topImageV.frame.origin.x,topImageV.frame.origin.y);
        
        
        yscrollOffset = CGRectGetHeight(topImageV.frame) - IMAGE_SCROLL_OFFSET_Y - CGRectGetHeight(optionsV.frame);
        mainImgV.frame = topImageV.frame ;
        
        //NSLog(@"%f, %f,%f, %f", mainImgV.frame.origin.x,mainImgV.frame.origin.y,mainImgV.frame.size.width,mainImgV.frame.size.height);
        
        
        containerView.frame = [UpdateFrame setSizeForView:containerView usingHeight:(CGRectGetHeight(self.view.bounds) + yscrollOffset)];
        
        // NSLog(@"%f, %f,%f, %f", tabcontentscrollView.frame.origin.x,tabcontentscrollView.frame.origin.y,tabcontentscrollView.frame.size.width,tabcontentscrollView.frame.size.height);
        
        tabcontentscrollView.frame= [UpdateFrame setPositionForView:tabcontentscrollView usingPositionY: CGRectGetMaxY(topImageV.frame)];
        
        //NSLog(@"%f, %f,%f, %f", tabcontentscrollView.frame.origin.x,tabcontentscrollView.frame.origin.y,tabcontentscrollView.frame.size.width,tabcontentscrollView.frame.size.height);
        
        tabcontentscrollView.frame = [UpdateFrame setSizeForView:tabcontentscrollView usingHeight: CGRectGetHeight(containerView.frame) - CGRectGetMaxY(topImageV.frame)];
        
        //NSLog(@"%f, %f,%f, %f", tabcontentscrollView.frame.origin.x,tabcontentscrollView.frame.origin.y,tabcontentscrollView.frame.size.width,tabcontentscrollView.frame.size.height);
        
    }
    
    
    CGPoint scrollviewOrigin = scrollviewOnTop.frame.origin;
    scrollviewOnTop.scrollIndicatorInsets = UIEdgeInsetsMake(-scrollviewOrigin.y, 0, scrollviewOrigin.y, scrollviewOrigin.x);
    
    //[self reloadList];
}
*/

-(CGFloat) calculateHeightOfList
{
    return ([list count]  * ROW_HEIGHT);
}

/*
 - (void) processResponse : (NSDictionary *) dict
 {
 list = [ReadData arrayFromDictionary:dict forKey:RESPONSEKEY_DATA];
 [listTblV reloadData];
 [listTblV setContentOffset:CGPointZero animated:NO];
 
 contentHeight = [self calculateHeightOfList];
 if(contentHeight <= CGRectGetHeight(tabcontentscrollView.bounds)){
 contentHeight = CGRectGetHeight(tabcontentscrollView.bounds);
 listTblV.scrollEnabled = NO;
 }
 
 listTblV.frame = [UpdateFrame setSizeForView:listTblV usingHeight:[self calculateHeightOfList]];
 
 listTblV.hidden = ([list count] == 0);
 noresultsL.hidden = !listTblV.hidden ;
 
 [self tabChanged];
 [HUD hide:YES];
 }
 
- (void) tabChanged
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        CGPoint offset = [self getScrollContentOffset];
        offset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:offset animated:NO];
    }
    isContentSizeUpdated = NO;
}
*/

#pragma mark - HUD & Delegate methods

- (void)hudShowWithMessage:(NSString *)message
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    HUD.dimBackground = YES;
    HUD.labelText = message;
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    [HUD show:YES];
}

- (void)hudWasHidden:(MBProgressHUD *)hud
{
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}

#pragma mark - Events

- (IBAction)backBPressed:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Expand on Scroll animation

- (CGSize) setContentSize
{
    CGFloat height = CGRectGetHeight(mainImgV.frame) + contentHeight;
    return CGSizeMake(pageWidth, height);
}

- (void) setScrollContentOffset: (CGPoint) offset
{
    listTblV.contentOffset = offset;
}

- (CGPoint) getScrollContentOffset
{
    return [listTblV contentOffset];
}

- (void) resetOffsetAllTabs
{
    CGPoint offset = CGPointZero;
    [self setScrollContentOffset : offset];
}

-(void) updateContentSize : (CGPoint) contentOffset;
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        contentOffset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    }
    [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    
    isContentSizeUpdated = NO;
}

*/
#pragma mark - Table view data source

-(CGFloat) calculateheightForRow:(NSInteger)row
{
    return ROW_HEIGHT;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [list count] ;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self calculateheightForRow:indexPath.row];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChooseUserLocationCell *cell = (ChooseUserLocationCell *)[tableView dequeueReusableCellWithIdentifier:@"ShowUserLocationCell" forIndexPath:indexPath];
    NSDictionary *item = list[indexPath.row];
    //NSDictionary *str = [ item valueForKey:@"structured_formatting"];
    //NSLog(@"strkey %@", [str allKeys]);
    
    [cell configureDataForCell:item];
    return cell;
}

// Called after the user changes the selection.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *item = list[indexPath.row];

    if ([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"homeServiceaddaddress"] || [appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromproductVC"]||[appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromlookdetailhomeserviceVC"] ||  [appD.sessionDelegate.isCommingFromviewController isEqualToString:@"frompackageVC"]){
    //NSMutableDictionary *item = list[indexPath.row];
    [appD.sessionDelegate.pastHistoryLocationArray addObject:item]; // stored the past location places.
    
    // calling service for lat and long
    NSString *placeIDstr = [item valueForKey:@"place_id"];
    [self callingServiceGetLatLongofuserselectplace:placeIDstr];
     NSDictionary *secondaryItem = [ item valueForKey:@"structured_formatting"];
    
 
   locationString  = [ReadData stringValueFromDictionary:secondaryItem forKey:KEY_SECONDARYTEXT];
    locationSubString = [ReadData stringValueFromDictionary:secondaryItem forKey:@"main_text"];
    
    /*
        NSLog(@"Done");
        AddEditAddressViewController *addEditAddressViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddEditAddressViewController"];
        
        //passing values
        addEditAddressViewController.locationSubStr = [ReadData stringValueFromDictionary:item forKey:KEY_DESCRIPTION_NAME];
        NSDictionary *secondaryItem = [ item valueForKey:@"structured_formatting"];
        addEditAddressViewController.locationStr  = [ReadData stringValueFromDictionary:secondaryItem forKey:KEY_SECONDARYTEXT];
    
        locationString = addEditAddressViewController.locationStr;
    
        [self.navigationController pushViewController:addEditAddressViewController animated:YES];
     */
        
    }else if ([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromsalonservicesVC"] ||[appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromhomeserviceVC"]){
        
        //NSMutableDictionary *item = list[indexPath.row];
        [appD.sessionDelegate.pastHistoryLocationArray addObject:item]; // stored the past location places.
   
        // calling service for lat and long
        NSString *placeIDstr = [item valueForKey:@"place_id"];
        [self callingServiceGetLatLongofuserselectplace:placeIDstr];
        
    } else if([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromsalonhomeBtnService"]){ // From Saloon Service Home button clicks
        [appD.sessionDelegate.pastHistoryLocationArray addObject:item]; // stored the past location places.
        
        // calling service for lat and long
        NSString *placeIDstr = [item valueForKey:@"place_id"];
        [self callingServiceGetLatLongofuserselectplace:placeIDstr];
        NSDictionary *secondaryItem = [ item valueForKey:@"structured_formatting"];
        
        
        locationString  = [ReadData stringValueFromDictionary:secondaryItem forKey:KEY_SECONDARYTEXT];
        locationSubString = [ReadData stringValueFromDictionary:secondaryItem forKey:@"main_text"];
        


    } else if([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromsalonWorkBtnService"]){ // From Saloon Service Work button clicks
        [appD.sessionDelegate.pastHistoryLocationArray addObject:item]; // stored the past location places.
        
        // calling service for lat and long
        NSString *placeIDstr = [item valueForKey:@"place_id"];
        [self callingServiceGetLatLongofuserselectplace:placeIDstr];
        NSDictionary *secondaryItem = [ item valueForKey:@"structured_formatting"];
        
        
        locationString  = [ReadData stringValueFromDictionary:secondaryItem forKey:KEY_SECONDARYTEXT];
        locationSubString = [ReadData stringValueFromDictionary:secondaryItem forKey:@"main_text"];
        
    }

    
    else if ([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromlookdetailsalonserviceVC"]){
        
        
        //NSMutableDictionary *item = list[indexPath.row];
        [appD.sessionDelegate.pastHistoryLocationArray addObject:item]; // stored the past location places.
        
        // calling service for lat and long
        NSString *placeIDstr = [item valueForKey:@"place_id"];
        [self callingServiceGetLatLongofuserselectplace:placeIDstr];
        

        /*
        //[cartC initForLooksWithDeliveryType:DELIVERY_TYPE_HOME];
        //[cartC setLatitude:[appD getUserCurrentLocationLatitude] andLongitude:[appD getUserCurrentLocationLongitude]];
        //[cartC addLooks:dataDict];
        UIStoryboard *modalStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LocationDateStoreSelectorViewController *LDSSVC = [modalStoryboard instantiateViewControllerWithIdentifier:@"LocationDateStoreSelectorViewController"];
        [self.navigationController pushViewController:LDSSVC animated:YES];*/
    }
    
    /*else if ([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromhomeservice"]){
        //NSMutableDictionary *item = list[indexPath.row];
        [appD.sessionDelegate.pastHistoryLocationArray addObject:item]; // stored the past location places.
        
        
        
        // calling service for lat and long
        NSString *placeIDstr = [item valueForKey:@"place_id"];
        [self callingServiceGetLatLongofuserselectplace:placeIDstr];

    }
   */
    
    
}
/*
#pragma mark - Scroll Events

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == scrollviewOnTop)
    {
        {
            if (isContentSizeUpdated)  return;
            
            CGRect scrolledBoundsForContainerView = containerView.bounds;
            if (scrollView.contentOffset.y <= yscrollOffset)
            {
                
                scrolledBoundsForContainerView.origin.y = scrollView.contentOffset.y ;
                containerView.bounds = scrolledBoundsForContainerView;
                
                //Reset offset for all tabs
                if (scrollView.contentOffset.y <= 0) [self resetOffsetAllTabs];
                
                
                CGFloat y = -scrollView.contentOffset.y;
                CGFloat alphaLevel = 1;
                CGFloat BLUR_MAX_Y = IMAGE_SCROLL_OFFSET_Y + CGRectGetHeight(topImageV.frame) - CGRectGetMinY(optionsV.frame);
                if (fabs(y) < BLUR_MAX_Y)
                {
                    alphaLevel = (BLUR_MAX_Y - fabs(y))/(BLUR_MAX_Y);
                }
                else
                {
                    alphaLevel = 0;
                }
                
                //[screenTitleL setAlpha:alphaLevel];
                if (y > 0)
                {
                    mainImgV.frame = CGRectInset(cachedImageViewSize, 0, -y/2);
                    mainImgV.center = CGPointMake(mainImgV.center.x, mainImgV.center.y + y/2);
                }
                else
                {
                    mainImgV.frame = [UpdateFrame setPositionForView:mainImgV usingPositionY:y];
                }
                return;
            }
            
            scrolledBoundsForContainerView.origin.y = yscrollOffset ;
            containerView.bounds = scrolledBoundsForContainerView;
            
            [self setScrollContentOffset:CGPointMake(0, scrollView.contentOffset.y - yscrollOffset)  ];
        }
    }
}
*/

#pragma mark - API

-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
    if ([apiName isEqualToString:USERSEARCH_LOCATION])
    {
        [self processLocationResponse:response];
    }
    else if ([apiName isEqualToString:API_GETUSERSEARCHLOCATION_GOOGPLACEURL])
    {
        [self processLocationLatitudeAndLongitudeResponse:response];
    }
}

// Called, this method ater getting respose form user search place locatin webservice.
- (void) processLocationResponse : (NSDictionary *) dict
{
    list = [ReadData arrayFromDictionary:dict forKey:@"predictions"];
    [listTblV reloadData];
    /*
    [listTblV setContentOffset:CGPointZero animated:NO];
    
    contentHeight = [self calculateHeightOfList];
    if(contentHeight <= CGRectGetHeight(tabcontentscrollView.bounds))
    {
        contentHeight = CGRectGetHeight(tabcontentscrollView.bounds);
        listTblV.scrollEnabled = NO;
    }
    listTblV.frame = [UpdateFrame setSizeForView:listTblV usingHeight:[self calculateHeightOfList]];
    */
    listTblV.hidden = ([list count] == 0);
    noresultsL.hidden = !listTblV.hidden ;
   // listTblV.userInteractionEnabled = YES;
    
    //[self tabChanged];
    [HUD hide:YES];
}

// Called, this method after the getting response form userselectlocation lititude and longitude web service.
- (void) processLocationLatitudeAndLongitudeResponse : (NSDictionary *) dict
{
    
    //NSLog(@"%@", [dict allKeys]);
    
    NSDictionary *resst = [dict valueForKey:@"result"];
   // NSLog(@"%@", [resst allKeys]);
    
    NSDictionary *geometry = [resst valueForKey:@"geometry"];
   // NSLog(@"%@", [geometry allKeys]);
    
    NSDictionary *location = [geometry valueForKey:@"location"];
   // NSLog(@"%@", [location allKeys]);
    
    NSString *strLat = [ReadData stringValueFromDictionary:location forKey:@"lat"];
    NSString *strLong = [ReadData stringValueFromDictionary:location forKey:@"lng"];
    
    //NSLog(@"%@%@", strLat,strLat);
    
    
    /*
     for (NSDictionary *dt in dict) {
     if ([dt valueForKey:@"location"]) {
     
     NSLog(@"%@",[dt allKeys]);
     }
     } */
    
    if ([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"homeServiceaddaddress"] || [appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromproductVC"]||[appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromlookdetailhomeserviceVC"]||[appD.sessionDelegate.isCommingFromviewController isEqualToString:@"frompackageVC"])
    {
       // NSLog(@"Done");
        AddEditAddressViewController *addEditAddressViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddEditAddressViewController"];
       
        addEditAddressViewController.locationStr = locationSubString;//locationString;
        addEditAddressViewController.locationSubStr = locationString; //locationSubString; ;
        
        
        addEditAddressViewController.LatStr =strLat;
        addEditAddressViewController.LongStr =strLong;

        //[cartC setLatitude:strLat andLongitude:strLat];
        //locationString = addEditAddressViewController.locationStr;
        
        [self.navigationController pushViewController:addEditAddressViewController animated:YES];
        
    }else if ([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromlookdetailsalonserviceVC"]){
        //[cartC initForLooksWithDeliveryType:DELIVERY_TYPE_HOME];
        //[cartC setLatitude:[appD getUserCurrentLocationLatitude] andLongitude:[appD getUserCurrentLocationLongitude]];
        //[cartC addLooks:dataDict];
        [cartC setLatitude:strLat andLongitude:strLong];
        UIStoryboard *modalStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LocationDateStoreSelectorViewController *LDSSVC = [modalStoryboard instantiateViewControllerWithIdentifier:@"LocationDateStoreSelectorViewController"];
        [self.navigationController pushViewController:LDSSVC animated:YES];
    }else if ([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromsalonhomeBtnService"]){ // Saloon service home button action.
        // NSLog(@"Done");
        AddEditAddressViewController *addEditAddressViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddEditAddressViewController"];
        
        addEditAddressViewController.locationStr = locationSubString;//locationString;
        addEditAddressViewController.locationSubStr = locationString; //locationSubString; ;
        
        
        addEditAddressViewController.LatStr =strLat;
        addEditAddressViewController.LongStr =strLong;
        
        //[cartC setLatitude:strLat andLongitude:strLat];
        //locationString = addEditAddressViewController.locationStr;
        
        [self.navigationController pushViewController:addEditAddressViewController animated:YES];
    }else if ([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromsalonWorkBtnService"]){ // Saloon service work button action.
        // NSLog(@"Done");
        AddEditAddressViewController *addEditAddressViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddEditAddressViewController"];
        
        addEditAddressViewController.locationStr = locationSubString;//locationString;
        addEditAddressViewController.locationSubStr = locationString; //locationSubString; ;
        
        
        addEditAddressViewController.LatStr =strLat;
        addEditAddressViewController.LongStr =strLong;
        
        //[cartC setLatitude:strLat andLongitude:strLat];
        //locationString = addEditAddressViewController.locationStr;
        
        [self.navigationController pushViewController:addEditAddressViewController animated:YES];
    }

    
    
    else{
        ChooseServicesViewController *chooseServiceViewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
        
        chooseServiceViewController.selLatitude = strLat;
        chooseServiceViewController.selLongitude = strLong;
        //    [chooseServiceViewController loadNearbyStoresaAfterUserSelectLocation:strLat :strLong :@"0"];
        [self.navigationController popViewControllerAnimated:YES];

    
    }
    
    
    
}


#pragma mark - Searchbar delegate methods
- (void) searchBarSearchButtonClicked:(UISearchBar *)asearchBar
{
    [userLocationsearchBar resignFirstResponder];
    //[self searchListing:asearchBar.text];
   // NSLog(@"%@",asearchBar.text);
    [self callingGetLocationService:asearchBar.text];
    
    // [[[APIHelper alloc]initWithDelegate:self] GetUserLocationUpdatesbasedOnSearchString:asearchBar.text];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)asearchBar
{
    userLocationsearchBar.showsCancelButton = NO;
    [userLocationsearchBar resignFirstResponder];
    userLocationsearchBar.text = @"";
    [self callingGetLocationService:asearchBar.text];
     }

- (void)searchBarTextDidBeginEditing:(UISearchBar *)asearchBar
{
    [userLocationsearchBar becomeFirstResponder];
    userLocationsearchBar.showsCancelButton = YES;
    //NSLog(@"%@",asearchBar.text);
}

- (void)searchBar:(UISearchBar *)asearchBar textDidChange:(NSString *)searchText
{
    // [self searchListing:asearchBar.text];
    //NSLog(@"%@",asearchBar.text);
    
    // calling service for user locatin .
    // [[[APIHelper alloc]initWithDelegate:self] GetUserLocationUpdatesbasedOnSearchString:searchText];
    
    [self callingGetLocationService:searchText];
 }

// Called, when user enter the text in search view controller and calling services.
-(void) callingGetLocationService:(NSString*)searchText
{
    if ([InternetCheck isOnline])
    {
        // [self hudShowWithMessage:@"Loading"];
        // calling service for user location .
        [[[APIHelper alloc]initWithDelegate:self] GetUserLocationUpdatesbasedOnSearchString:searchText];
    }
}

// Called, when user select the any place from table and calling service for lat and longutude.
-(void) callingServiceGetLatLongofuserselectplace:(NSString *) storeID
{
    [[[APIHelper alloc]initWithDelegate:self] GetUserLocationLatitudeandLongitude:storeID];
}

// Called, when user clicks on the add button.
-(IBAction)addUserAddressAction:(id)sender{
    containerView.hidden = YES;
    EditAddressView.hidden = NO;
    AddAddressView.hidden = YES;
}
// Called, when user click on save button and save the values in the profile view.
-(IBAction)saveAddressAction:(id)sender{
    
    containerView.hidden = NO;
    EditAddressView.hidden = YES;
}

-(void)hideKeyboard{
    [userLocationsearchBar resignFirstResponder];
    //listTblV.userInteractionEnabled = NO;
    //[self.view endEditing:YES];

}

// 7893319701.


@end
