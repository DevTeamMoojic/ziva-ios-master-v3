//
//  ChooseUserLocationCell.h
//  Ziva
//
//  Created by Leena on 26/12/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseUserLocationCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *locationL;
@property (weak, nonatomic) IBOutlet UILabel *subLocationL;
@property (weak, nonatomic) IBOutlet UIImageView *mainImgV;

- (void) configureDataForCell : (NSDictionary *) item;

@end
