//
//  ChooseUserLocationViewController.h
//  Ziva
//
//  Created by Leena on 21/12/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationDateStoreSelectorViewController.h"

@interface ChooseUserLocationViewController : UIViewController

@property (nonatomic, retain) NSString *isCommingFrom;


@end
