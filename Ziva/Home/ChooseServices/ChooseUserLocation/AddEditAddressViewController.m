//
//  AddEditAddressViewController.m
//  Ziva
//
//  Created by Minnarao on 3/1/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "AddEditAddressViewController.h"
#import "UserAddAddressViewController.h"
#import "AddressPlist.h"

@interface AddEditAddressViewController ()<UITextFieldDelegate>{
    
   __weak IBOutlet UITextField *flatAddressTxtF;
    __weak IBOutlet UITextField *landMarkTxtF;
    __weak IBOutlet UILabel *locationL;
    __weak IBOutlet UILabel *sublocationL;
    AppDelegate *appD;
    
    
    __weak IBOutlet UIButton *homeB;
    __weak IBOutlet UIButton *WorkB;
    __weak IBOutlet UIButton *otherB;
    __weak IBOutlet NSString *picktype;
    __weak IBOutlet UIView *homeImgsV;


}

@end

@implementation AddEditAddressViewController
@synthesize locationStr = _locationStr;
@synthesize locationSubStr = _locationSubStr;
@synthesize isAddressEdit = _isAddressEdit;
@synthesize btntag = _btntag;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
   // NSLog(@"%@",_LatStr);
    //NSLog(@"%@",_LongStr);
    
     [self setupLayout];
    
    // For enable and disabled button from Home and Work.
    if([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromsalonhomeBtnService"]) {
        homeB.enabled = YES;
        WorkB.enabled = NO;
        otherB.enabled = NO;
    }else if([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromsalonWorkBtnService"]) {
        homeB.enabled = NO;
        WorkB.enabled = YES;
        otherB.enabled = NO;
    }
//    else if([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromproductVC"]){
//        homeB.userInteractionEnabled =YES;
//        WorkB.userInteractionEnabled = YES;
//        otherB.userInteractionEnabled = YES;
//
//    }
    else{
        homeB.userInteractionEnabled =YES;
        WorkB.userInteractionEnabled = YES;
        otherB.userInteractionEnabled = YES;
    }
    
    // default select button.
    if (_btntag == 1) {
        [homeB setImage:[UIImage imageNamed:@"address_home_selected"] forState:UIControlStateNormal];
        [WorkB setImage:[UIImage imageNamed:@"address_work_not_selected"] forState:UIControlStateNormal];
        [otherB setImage:[UIImage imageNamed:@"address_others_not_selected"] forState:UIControlStateNormal];
         picktype = @"1";
        
    } else if (_btntag == 2) {
        [homeB setImage:[UIImage imageNamed:@"location_home_notselected"] forState:UIControlStateNormal];
        [WorkB setImage:[UIImage imageNamed:@"address_work_selected"] forState:UIControlStateNormal];
        [otherB setImage:[UIImage imageNamed:@"address_others_not_selected"] forState:UIControlStateNormal];
          picktype = @"2";
        
    }else if(_btntag == 3){
        [homeB setImage:[UIImage imageNamed:@"location_home_notselected"] forState:UIControlStateNormal];
        [WorkB setImage:[UIImage imageNamed:@"address_work_not_selected"] forState:UIControlStateNormal];
        [otherB setImage:[UIImage imageNamed:@"address_others_selected"] forState:UIControlStateNormal];
         picktype = @"3";

    }

}
- (void) setupLayout
{
    // Set delegate.
    flatAddressTxtF.delegate = self;
    landMarkTxtF.delegate = self;
    
    if(_isAddressEdit){
        flatAddressTxtF.text = self.flatStr;
        landMarkTxtF.text = self.landmarStr;
        locationL.text = self.locationSubStr;
        sublocationL.text = self.locationStr;
    }else{
        
        flatAddressTxtF.text = @"";
        landMarkTxtF.text = @"";
        locationL.text = self.locationSubStr;
        sublocationL.text = self.locationStr;
        
    }
    
    //locationL.text = self.locationStr;
    //sublocationL.text = self.locationSubStr;
    
    //flatAddressTxtF.text = [appD getLoggedInUserFirstName];
    // landMarkTxtF.text = [appD getLoggedInUserLastName];
    
    
    // view rounded rect.
    homeImgsV.layer.cornerRadius = 7;
    homeImgsV.layer.masksToBounds = YES;
    

    
}
#pragma mark - Events

- (IBAction)backBPressed:(UIButton *)sender
{
    // Navigation track finding.
    NSArray *array = [self.navigationController viewControllers];
    NSLog(@"%@", array);
    if([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromsalonhomeBtnService"] ||[appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromsalonWorkBtnService"]) {
        [self.navigationController popToViewController:[array objectAtIndex:1] animated:YES];

    }else if([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromhomeserviceVC"] || [appD.sessionDelegate.isCommingFromviewController isEqualToString:@"homeServiceaddaddress"]) {
        [self.navigationController popToViewController:[array objectAtIndex:1] animated:YES];
    }
    else{
        
        [self.navigationController popToViewController:[array objectAtIndex:3] animated:YES];
    }
//    // Navigation track finding.
//    NSArray *array = [self.navigationController viewControllers];
//    NSLog(@"%@", array);
//    [self.navigationController popToViewController:[array objectAtIndex:3] animated:YES];
}



// Called, when user clicks on save button and go back to userAddaddressviewcontroler class and add item to table.
-(IBAction)saveUserAddresAction:(id)sender{
    //[self dismissKeyboard];
    
    if([self isValid]){
            [self updateAddAddress];
        }
    
}

// click any circle.
-(IBAction)pickType:(UIButton*)sender{
   picktype = @"";
    
    if (sender.tag == 1) {
        [homeB setImage:[UIImage imageNamed:@"address_home_selected"] forState:UIControlStateNormal];
        [WorkB setImage:[UIImage imageNamed:@"address_work_not_selected"] forState:UIControlStateNormal];
        [otherB setImage:[UIImage imageNamed:@"address_others_not_selected"] forState:UIControlStateNormal];
        picktype = @"1";
        
    }
    else if (sender.tag == 2){
        [homeB setImage:[UIImage imageNamed:@"location_home_notselected"] forState:UIControlStateNormal];
        [WorkB setImage:[UIImage imageNamed:@"address_work_selected"] forState:UIControlStateNormal];
        [otherB setImage:[UIImage imageNamed:@"address_others_not_selected"] forState:UIControlStateNormal];
        picktype = @"2";
        
    }
    else if (sender.tag == 3){
        [homeB setImage:[UIImage imageNamed:@"location_home_notselected"] forState:UIControlStateNormal];
        [WorkB setImage:[UIImage imageNamed:@"address_work_not_selected"] forState:UIControlStateNormal];
        [otherB setImage:[UIImage imageNamed:@"address_others_selected"] forState:UIControlStateNormal];
        picktype = @"3";
        
    }
}


//http://stackoverflow.com/questions/23867337/how-can-i-save-retrieve-delete-update-my-data-in-plist-file-in-ios
-(void) updateAddAddress{
     /*
    if (_isAddressEdit) {
        [self addressedit];
    }else{
        AddressPlist *addressPlist = [[AddressPlist alloc]init];
        addressPlist.flate = flatAddressTxtF.text;
        addressPlist.landmark = landMarkTxtF.text;
        addressPlist.location = self.locationStr;
        addressPlist.slocation = self.locationSubStr;
        addressPlist.type = @"111";
        
        [addressPlist save];

        
    }*/
  
    if([picktype isEqualToString:@"1"]){
        // Add values to dictonary.
        NSMutableDictionary * addDict = [[NSMutableDictionary alloc]init];
        [addDict setObject:flatAddressTxtF.text forKey:@"FLATADDRESS"];
        [addDict setObject:landMarkTxtF.text forKey:@"LANDMARKADDRESS"];
        [addDict setObject:self.locationStr forKey:@"LOCATIONSTR"];
        [addDict setObject:self.locationSubStr forKey:@"LOCATIONSUBSTR"];
        [addDict setObject:_LatStr forKey:@"LAT"];
        [addDict setObject:_LongStr forKey:@"LONG"];

        [addDict setObject:picktype forKey:@"Type"];
        
        [[NSUserDefaults standardUserDefaults] setObject:addDict forKey:@"HOMEADDRESS"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        NSLog(@"%@", addDict);
        // set home address values.
        [[NSUserDefaults standardUserDefaults] setObject:addDict forKey:USER_HOMEADDRESS];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [appD getLoggedInHomeAddress];
        
       // [appD isHomeAddresOrNot];
//        NSDictionary *retrievedDictionary = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HOMEDDRESS"];
//        NSLog(@"%@",retrievedDictionary);

        
    } else if ([picktype isEqualToString:@"2"]){
        // Add values to dictonary.
        NSMutableDictionary * addDict = [[NSMutableDictionary alloc]init];
        [addDict setObject:flatAddressTxtF.text forKey:@"FLATADDRESS"];
        [addDict setObject:landMarkTxtF.text forKey:@"LANDMARKADDRESS"];
        [addDict setObject:self.locationStr forKey:@"LOCATIONSTR"];
        [addDict setObject:self.locationSubStr forKey:@"LOCATIONSUBSTR"];
        [addDict setObject:_LatStr forKey:@"LAT"];
        [addDict setObject:_LongStr forKey:@"LONG"];

        [addDict setObject:picktype forKey:@"Type"];
        
        [[NSUserDefaults standardUserDefaults] setObject:addDict forKey:@"WORKADDRESS"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
//        NSDictionary *retrievedDictionary = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HOMEDDRESS"];
//        NSLog(@"%@",retrievedDictionary);
        
        NSLog(@"%@", addDict);
        // set home address values.
        [[NSUserDefaults standardUserDefaults] setObject:addDict forKey:USER_WORKADDRESS];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [appD getLoggedInWorkAddress];

        
    }else if ([picktype isEqualToString:@"3"]){
        // Add values to dictonary.
        NSMutableDictionary * addDict = [[NSMutableDictionary alloc]init];
        [addDict setObject:flatAddressTxtF.text forKey:@"FLATADDRESS"];
        [addDict setObject:landMarkTxtF.text forKey:@"LANDMARKADDRESS"];
        [addDict setObject:self.locationStr forKey:@"LOCATIONSTR"];
        [addDict setObject:self.locationSubStr forKey:@"LOCATIONSUBSTR"];
        [addDict setObject:_LatStr forKey:@"LAT"];
        [addDict setObject:_LongStr forKey:@"LONG"];

        [addDict setObject:picktype forKey:@"Type"];
        
        [[NSUserDefaults standardUserDefaults] setObject:addDict forKey:@"OTHERADDRESS"];
        [[NSUserDefaults standardUserDefaults]synchronize];
//        
//        NSDictionary *retrievedDictionary = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HOMEDDRESS"];
//        NSLog(@"%@",retrievedDictionary);
//
        
    }
    
    
     /*
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"Usersavedaddress.plist"];
    
    NSMutableDictionary * addDict = [[NSMutableDictionary alloc]init];
    [addDict setObject:flatAddressTxtF.text forKey:@"FLATADDRESS"];
    [addDict setObject:landMarkTxtF.text forKey:@"LANDMARKADDRESS"];
    [addDict setObject:self.locationStr forKey:@"LOCATIONSTR"];
    [addDict setObject:self.locationSubStr forKey:@"LOCATIONSUBSTR"];
    
    //NSDictionary *plistDict = [[NSDictionary alloc] initWithObjects: [NSArray arrayWithObjects: self.nameArr, self.countryArr, self.imageArr, nil] forKeys:[NSArray arrayWithObjects: @"Name", @"Country",@"Image", nil]];
    
    NSString *error = nil;
    //NSData *plistData = [NSPropertyListSerialization dataWithPropertyList:addDict format:NSPropertyListXMLFormat_v1_0 options:0 error:&error];
NSData *plistData = [NSPropertyListSerialization dataFromPropertyList:addDict format:NSPropertyListXMLFormat_v1_0 errorDescription:&error];
    
    
    if(plistData)
    {
        [plistData writeToFile:plistPath atomically:YES];
        NSLog(@"Data saved sucessfully") ;
    }
    else
    {
         NSLog(@"Data not saved") ;
    }
   
    */
    
    // Navigation track finding.
    NSArray *array = [self.navigationController viewControllers];
    //NSLog(@"%@", array);
    if ([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromsalonhomeBtnService"] ||[appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromsalonWorkBtnService"]) {
        [self.navigationController popToViewController:[array objectAtIndex:1] animated:YES];

    }else if([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"homeServiceaddaddress"]){
        [self.navigationController popToViewController:[array objectAtIndex:4] animated:YES];
    }else if([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromhomeserviceVC"]){
        [self.navigationController popToViewController:[array objectAtIndex:4] animated:YES];
    }
    
    else{
        [self.navigationController popToViewController:[array objectAtIndex:3] animated:YES];

    }
   
    
}

-(void) addressedit{
    
    // get paths from root direcory
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    
    // get documents path
    
    
    NSString *documentsPath = [paths objectAtIndex:0];
    // get the path to plist file
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"UserAddAddress.plist"];
    // check to see if Data.plist exists in documents
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath])
    {
        // if not in documents, get property list from main bundle
        plistPath = [[NSBundle mainBundle] pathForResource:@"UserAddAddress" ofType:@"plist"];
    }
    /*
    
    // read property list into memory as an NSData object
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
    NSString *errorDesc = nil;
    NSPropertyListFormat format;
    // convert static property list into dictionary object
    NSDictionary *temp = (NSDictionary *)[NSPropertyListSerialization propertyListFromData:plistXML mutabilityOption:NSPropertyListMutableContainersAndLeaves format:&format errorDescription:&errorDesc];
   
    if (!temp)
    {
                NSLog(@"Error reading plist: %@, format: %d", errorDesc, format);
    }*/
    
    // checking if element exists, if yes overwriting
    // if element not exists adding new element
    NSMutableArray *t = [[[NSMutableArray alloc]initWithContentsOfFile:plistPath]mutableCopy];
    // new implementation.
    NSMutableArray *finalarray = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dict in t) {
        if ([[dict valueForKey:@"Type"] isEqualToString:@"33"]) {
            [finalarray insertObject:dict atIndex:0];
        }
    }
    
    NSMutableDictionary *edidict = [finalarray objectAtIndex:0];
    [edidict setObject:flatAddressTxtF.text forKey:@"Flate"];
    [edidict setObject:landMarkTxtF.text forKey:@"Landmark"];
    [edidict setObject: self.locationSubStr forKey:@"Location"];
    [t replaceObjectAtIndex:0 withObject:edidict];
    
    NSString *documentsPath1= [paths objectAtIndex:0];
    // get the path to plist file
    NSString *plistPath1 = [documentsPath1 stringByAppendingPathComponent:@"UserAddAddress.plist"];
    [t writeToFile:plistPath1 atomically:YES];
}


- (BOOL) isValid
{
    //[UIResponder dismissKeyboard];
    
    if ([appD isEmptyString:flatAddressTxtF.text])
    {
        [self showErrorMessage:FIRSTNAMEEMPTY];
        return  NO;
    }
    /*if ([appD isEmptyString:landMarkTxtF.text])
    {
        [self showErrorMessage:LASTNAMEEMPTY];
        return  NO;
    }*/
    if ([appD isEmptyString:picktype])
    {
        [self showErrorMessage:8888];
        return  NO;
    }
    return YES;
}


- (void) showErrorMessage : (int)errorCode
{
    NSString *message = @"";
    switch (errorCode) {
    
        case FIRSTNAMEEMPTY: {
            message = @"Address cannot be blank.";
        }
            break;
       /* case LASTNAMEEMPTY: {
            message = @"Land mark cannot be blank.";
        }
            break;*/
        case 8888: {
            message = @"Type of the address cannot be blank.";
        }
            break;

}
    
    UIAlertView *errorNotice = [[UIAlertView alloc] initWithTitle:ALERT_TITLE_ERROR message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorNotice show];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [flatAddressTxtF resignFirstResponder];
    [landMarkTxtF resignFirstResponder];

    return YES;
}



- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan) {
        [flatAddressTxtF resignFirstResponder];
        [landMarkTxtF resignFirstResponder];

    }
}


@end
