//
//  UserAddAddressCell.m
//  Ziva
//
//  Created by Minnarao on 3/1/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "UserAddAddressCell.h"

@implementation UserAddAddressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) configureDataForCell : (NSDictionary *) item{
    
    self.locationL.text = [ReadData stringValueFromDictionary:item forKey:KEY_DESCRIPTION_NAME];
    NSDictionary *secondaryItem = [ item valueForKey:@"structured_formatting"];
    self.subLocationL.text = [ReadData stringValueFromDictionary:secondaryItem forKey:KEY_SECONDARYTEXT];
    // self.subLocationL.text = [ReadData stringValueFromDictionary:item forKey:KEY_SECONDARYTEXT];
}

@end
