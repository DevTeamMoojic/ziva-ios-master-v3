//
//  AddressPlist.h
//  Ziva
//
//  Created by Minna Rao on 05/01/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>


#define kNoteCategory  @"Flate"
#define kNoteName      @"Landmark"
#define kNoteEvent     @"Location"
#define kSLocation     @"SLocation"
#define kNoteType      @"Type"


@interface AddressPlist : NSObject

@property (nonatomic, copy) NSString *flate;
@property (nonatomic, copy) NSString *landmark;
@property (nonatomic, copy) NSString *location;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *slocation;


- (id)initWithDictionary:(NSDictionary *)dictionary;

/*Find all saved address*/
+ (NSArray *)savedAddress;

/*Saved current address*/
- (void)save;

/*Removes address from plist*/
- (void)remove;

@end
