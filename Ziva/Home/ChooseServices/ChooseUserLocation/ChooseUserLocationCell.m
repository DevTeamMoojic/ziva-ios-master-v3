//
//  ChooseUserLocationCell.m
//  Ziva
//
//  Created by Leena on 26/12/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "ChooseUserLocationCell.h"

@implementation ChooseUserLocationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) configureDataForCell : (NSDictionary *) item{
    
//    self.locationL.text = [ReadData stringValueFromDictionary:item forKey:KEY_DESCRIPTION_NAME];
//   NSDictionary *secondaryItem = [ item valueForKey:@"structured_formatting"];
//    self.subLocationL.text = [ReadData stringValueFromDictionary:secondaryItem forKey:KEY_SECONDARYTEXT];
//
    NSDictionary *secondaryItem = [ item valueForKey:@"structured_formatting"];
    self.locationL.text = [ReadData stringValueFromDictionary:secondaryItem forKey:KEY_SECONDARYTEXT];
    
    //NSDictionary *secondaryItem = [ item valueForKey:@"structured_formatting"];
    self.subLocationL.text = [ReadData stringValueFromDictionary:item forKey:KEY_DESCRIPTION_NAME];

   // self.subLocationL.text = [ReadData stringValueFromDictionary:item forKey:KEY_SECONDARYTEXT];
}

@end
