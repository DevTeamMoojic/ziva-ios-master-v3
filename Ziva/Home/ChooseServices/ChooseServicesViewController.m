
//
//  ChooseServicesViewController.m
//  Ziva
//
//  Created by Bharat on 30/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "GenderSelectorViewController.h"
#import "ServicesListingViewController.h"
#import "StoresListingViewController.h"
#import "StoreRateCardViewController.h"
#import "ChooseServicesViewController.h"
#import "HomeViewController.h"
#import "ChooseUserLocationViewController.h"

@interface ChooseServicesViewController ()<APIDelegate,UICollectionViewDelegateFlowLayout,UISearchBarDelegate,GenderSelectorDelegate,UIAlertViewDelegate>
{
    NSMutableArray *storesArray;
    NSMutableArray *keywordsArray;
    NSMutableArray *servicesArray;
    
    __weak IBOutlet UIScrollView *mainscrollView;
    
    __weak IBOutlet UIView *optionsV;
    
    __weak IBOutlet UIView *toggleCV;
    __weak IBOutlet UIButton *searchbySalonsB;
    __weak IBOutlet UIButton *searchbyServicesB;
    
    __weak IBOutlet UIView *searchBarCV;
    __weak IBOutlet UISearchBar  *searchBar;
    
    __weak IBOutlet UIView *listingCV;
    
    __weak IBOutlet UIView *storesListCV;
    __weak StoresListingViewController *storesListVC;
    
    __weak IBOutlet UIView *servicesListCV;
    __weak ServicesListingViewController *servicesListVC;
    
    __weak IBOutlet UIView *genderselectorCV;
    __weak GenderSelectorViewController *genderselectorVC;
    
    __weak IBOutlet UIActivityIndicatorView *loadingAIV;
    __weak IBOutlet UILabel *noresultsL;
    
    
    CGFloat contentHeight;
    
    AppDelegate *appD;
    MyCartController *cartC;
    
    NSString *dlvryType;
    
    NSString *selGenderId;
    
    BOOL isBackFromOtherView;
    BOOL isShowingServiceDetails;
    
    BOOL isFromSearchServices;
    
    NSInteger apiCtr ;
    
    // For User Location button.
    __weak IBOutlet UIButton *userLocationB;
    __weak IBOutlet UILabel *currentAddressL;
    __weak IBOutlet UISegmentedControl *seg;
    
    NSInteger  LastselectedSegmentedIndex;
    
}
@end

@implementation ChooseServicesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [mainscrollView removeGestureRecognizer:mainscrollView.panGestureRecognizer];
    
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;
    
    [self setupLayout];
    //storesListCV.frame = CGRectMake(0, self.view.frame.origin.y - 50, self.view.frame.size.height, self.view.frame.size.width);
    
    // For update current address.
    //[self locationUpdatedOnTop];
    
    
}

-(void) viewWillAppear:(BOOL)animated
{
    genderselectorCV.hidden = YES; // Hide the male female controller when salon button is on.
    
    // Frame changed y and hiight.
    CGRect r = [listingCV frame];
    r.origin.y = 210.0;
    [listingCV setFrame:r];
    CGRect h = [listingCV frame];
    h.size.height = 455.0;
    //    [listingCV setFrame:h];
    
   // selectedSegmentedIndex = 0;
   // seg.selectedSegmentIndex = selectedSegmentedIndex;
   // [self CallingServiceForNearbyHomeWorkSegmentedAction:selectedSegmentedIndex];
    
    if ([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromsalonhomeBtnService"]){
        seg.selectedSegmentIndex = 1;
        
        if (searchbySalonsB.selected) {
            NSDictionary *dictHome = [appD getLoggedInHomeAddress];
            //NSLog(@"dictSel %@", dictHome);
            //[[appD getLoggedInHomeAddress] isEqual: [NSNull null]];
            if (dictHome == NULL || dictHome == nil || dictHome.count == 0) {
               // NSLog(@"null values");
                UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"This address doesn't exist. Do you want to add this address?" message:nil delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"OK", nil];
                alt.tag = 1;
                [alt show];
                
                
            }else{
               // NSLog(@"not null values");
                
                _selLatitude = [dictHome valueForKey:@"LAT"];
                _selLongitude = [dictHome valueForKey:@"LONG"];
               // dlvryType = DELIVERY_TYPE_SALON;
                
                [self loadNearbyStores];
                
            }
            
            
        } else {
            
            NSDictionary *dict = [appD getLoggedInHomeAddress];
            //NSLog(@"dict %@", dict);
            //[[appD getLoggedInHomeAddress] isEqual: [NSNull null]];
            if (dict == NULL || dict == nil || dict.count == 0) {
                //NSLog(@"null values");
                UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"This address doesn't exist. Do you want to add this address?" message:nil delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"OK", nil];
                alt.tag = 1;
                [alt show];
                
                
            }else{
                //NSLog(@"not null values");
                //dlvryType =DELIVERY_TYPE_HOME;
                selGenderId = [appD getLoggedInUserGender];
                _selLatitude = [dict valueForKey:@"LAT"];
                _selLongitude = [dict valueForKey:@"LONG"];
                [self loadSearchKeywords];
                
            }
        }

        
    }else if ([appD.sessionDelegate.isCommingFromviewController isEqualToString:@"fromsalonWorkBtnService"]){
        seg.selectedSegmentIndex = 2;
        if (searchbySalonsB.selected) {
            
            NSDictionary *workDict = [appD getLoggedInWorkAddress];
           // NSLog(@"%@", workDict);
            //[[appD getLoggedInHomeAddress] isEqual: [NSNull null]];
            if (workDict == NULL || workDict == nil || workDict.count == 0) {
               // NSLog(@"null values");
                UIAlertView *altW = [[UIAlertView alloc]initWithTitle:@"This address doesn't exist. Do you want to add this address?" message:nil delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"OK", nil];
                altW.tag = 2;
                [altW show];
                
                
            }else{
               // NSLog(@"not null values");
                
                _selLatitude = [workDict valueForKey:@"LAT"];
                _selLongitude = [workDict valueForKey:@"LONG"];
                //dlvryType = DELIVERY_TYPE_SALON;
                
                [self loadNearbyStores];
            }
        }
        else{
            NSDictionary *dict = [appD getLoggedInWorkAddress];
            //NSLog(@"%@", dict);
            //[[appD getLoggedInHomeAddress] isEqual: [NSNull null]];
            if (dict==nil||[dict isEqual:[NSNull null]]) {
              //  NSLog(@"null values");
                UIAlertView *altW = [[UIAlertView alloc]initWithTitle:@"This address doesn't exist. Do you want to add this address?" message:nil delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"OK", nil];
                altW.tag = 2;
                
                [altW show];
                
                
            }else{
               // NSLog(@"not null values");
                
                //[self sallonORServie:1];
                //selectedSegmentedIndex = 2;
                //[self CallingServiceForNearbyHomeWorkSegmentedAction:selectedSegmentedIndex];
                selGenderId = [appD getLoggedInUserGender];
                _selLatitude = [dict valueForKey:@"LAT"];
                _selLongitude = [dict valueForKey:@"LONG"];
                [self loadSearchKeywords];
                
            }
            
        }

        
    }else{
        seg.selectedSegmentIndex = 0;
        [self reloadList];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods

- (void) loadStoresForSalonServices
{
    dlvryType = DELIVERY_TYPE_SALON;
}

- (void) loadStoresForHomeServices
{
    dlvryType = DELIVERY_TYPE_HOME;
    
}

- (void) displayStoreRateCard : (NSDictionary *) storesDict
{
    isFromSearchServices =NO;
    [cartC updateStoreInformation:storesDict];
    [self performSegueWithIdentifier:@"showstoreratecard" sender:storesDict];
}

// MINNARAO
- (void) displayStoreRateCardUsingService : (NSDictionary *) serviceDict
{
    isFromSearchServices =YES;
    [cartC updateStoreInformationFromSearchByServices:serviceDict];
    [self performSegueWithIdentifier:@"showstoreratecard" sender:serviceDict];
    
}

- (void) getServicesByKeyword : (NSString *) queryText
{
    isShowingServiceDetails = YES;
    [UIResponder dismissKeyboard];
    searchBar.text = queryText;
    [self loadSearchKeywords];
    
}

#pragma mark - Methods

- (void) setupLayout
{
    
    searchbySalonsB.selected = YES;
    searchbyServicesB.selected = NO;
    
    _selLatitude = [appD getUserCurrentLocationLatitude];
    _selLongitude = [appD getUserCurrentLocationLongitude];
    selGenderId = [appD getLoggedInUserGender];
    [genderselectorVC setGender:selGenderId];
    
    storesArray = [NSMutableArray array];
    keywordsArray = [NSMutableArray array];
    servicesArray = [NSMutableArray array];
    
    toggleCV.layer.borderColor = [UIColor whiteColor].CGColor;
    searchBarCV.layer.borderColor = [UIColor whiteColor].CGColor;
    
    // new code minnarao.
    //mainscrollView.contentOffset = CGPointZero;
    //contentHeight = CGRectGetHeight(self.view.bounds);
    
    storesListCV.hidden = servicesListCV.hidden = YES;
    
    [self setupSearchBar];
    
    searchbySalonsB.backgroundColor = searchbySalonsB.selected?[UIColor whiteColor] : [UIColor clearColor];
    searchbyServicesB.backgroundColor = searchbyServicesB.selected?[UIColor whiteColor] : [UIColor clearColor];
    
    UITextField *txfSearchField = [searchBar valueForKey:@"_searchField"];
    txfSearchField.placeholder = searchbySalonsB.selected? @"Search Salons": @"Search Services";
}

- (void) setupSearchBar
{
    UITextField *txfSearchField = [searchBar valueForKey:@"_searchField"];
    
    txfSearchField.clearButtonMode = UITextFieldViewModeWhileEditing;
    txfSearchField.backgroundColor = [UIColor clearColor];
    txfSearchField.font = [UIFont semiboldFontOfSize:13.0];
    txfSearchField.textColor = [UIColor whiteColor];
    txfSearchField.tintColor = txfSearchField.textColor;
    txfSearchField.placeholder = @"Search Services";
    
    //To remove black border around searchbar
    [searchBar setBackgroundImage:[[UIImage alloc]init]];
    for (UIView * view in [[[searchBar subviews] objectAtIndex:0] subviews])
    {
        if (![view isKindOfClass:[UITextField class]] && ![view isKindOfClass:NSClassFromString(@"UISearchBarBackground")])
        {
            view .alpha = 0;
        }
    }
}

- (void) toggleListing
{
    if(searchbySalonsB.selected)
    {
        genderselectorCV.hidden = YES; // Hide the male female controller.
        
        // Fram change intioal.
        CGRect r = [listingCV frame];
        r.origin.y = 210.0;
        [listingCV setFrame:r];
        
        // set height.
        CGRect h = [listingCV frame];
        h.size.height = 455.0;
        //        [listingCV setFrame:h];
        
        noresultsL.hidden = ([storesArray count] > 0);
        storesListCV.hidden = ([storesArray count] == 0);
        
        //[self sallonORServie:1];
       // seg.selectedSegmentIndex = selectedSegmentedIndex;
      //  [self CallingServiceForNearbyHomeWorkSegmentedAction:selectedSegmentedIndex];
        
    }
    else
    {
        genderselectorCV.hidden = NO;
        
        // Fram change int.
        CGRect r = [listingCV frame];
        r.origin.y = 260.0;
        [listingCV setFrame:r];
        
        // set height.
        CGRect h = [listingCV frame];
        h.size.height = 400.0;
        //        [listingCV setFrame:h];
        
        noresultsL.hidden = ([keywordsArray count] > 0);
        servicesListCV.hidden = ([keywordsArray count] == 0);
    }
    searchbySalonsB.backgroundColor = searchbySalonsB.selected?[UIColor whiteColor] : [UIColor clearColor];
    searchbyServicesB.backgroundColor = searchbyServicesB.selected?[UIColor whiteColor] : [UIColor clearColor];
    
    UITextField *txfSearchField = [searchBar valueForKey:@"_searchField"];
    txfSearchField.placeholder = searchbySalonsB.selected? @"Search Salons": @"Search Services";
    
    // For when chage the tab the text will be empty.
    searchBar.text = @"";
    [self searchListing : @""];
    
    // [self sallonORServie:2];
    //seg.selectedSegmentIndex = 0;
    // seg.selectedSegmentIndex = selectedSegmentedIndex;
    //[self CallingServiceForNearbyHomeWorkSegmentedAction:0];
}


- (void) searchListing : (NSString *) queryText
{
    NSArray *tmpArray;
    if(searchbySalonsB.selected)
    {
        if (queryText.length > 0)
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"searchKey CONTAINS[cd] %@",queryText];
            tmpArray =[storesArray filteredArrayUsingPredicate:predicate];
        }
        else
        {
            tmpArray = storesArray;
        }
        [storesListVC loadData: tmpArray];
        storesListCV.hidden = NO;
        servicesListCV.hidden = YES;
    }
    else
    {
        isShowingServiceDetails = NO;
       
        if (queryText.length > 0)
        {
//            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"Name CONTAINS[cd] %@",queryText];

            NSMutableArray *DictDupooo = [[NSMutableArray alloc]init];
            for(NSDictionary *keyword in keywordsArray){
                NSString *servicename = [ReadData stringValueFromDictionary:keyword forKey:@"ServiceName"];
                if([servicename rangeOfString:queryText].location!= NSNotFound){
                    [DictDupooo addObject:keyword];
                }
            }
            
            tmpArray = DictDupooo;
        }
        else
        {
            tmpArray = keywordsArray;
        }
        
        storesListCV.hidden = YES;
        servicesListCV.hidden = NO;
        
        [servicesListVC loadKeywords:tmpArray];
    }
    
    // new code minnarao.
    /*
     contentHeight = CGRectGetHeight(optionsV.frame) + ( searchbySalonsB.selected ? [storesListVC getContentHeight] : [servicesListVC getContentHeight]) ;
     if (contentHeight <= CGRectGetHeight(mainscrollView.bounds))
     {
     contentHeight = CGRectGetHeight(mainscrollView.bounds);
     mainscrollView.scrollEnabled = NO;
     }
     
     [mainscrollView setContentSize:CGSizeMake(CGRectGetWidth(mainscrollView.frame), contentHeight)];
     listingCV.frame = [UpdateFrame setSizeForView:listingCV usingHeight:contentHeight];*/
}

- (void) reloadList
{
    
   // NSLog(@"_selLatitude  %@    ,_selLongitude  %@",_selLatitude,_selLongitude);
    if ([InternetCheck isOnline])
    {
        apiCtr = 2;
        [loadingAIV startAnimating];
        [self loadNearbyStores];
        [self loadSearchKeywords];
        
        
        storesListCV.hidden = YES;
        servicesListCV.hidden = YES;
        noresultsL.hidden = YES;
    }
}

- (void) loadNearbyStores
{
    if ([InternetCheck isOnline])
    {
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:_selLatitude forKey:PARAM_LATITUDE];
        [paramDict setObject:_selLongitude forKey:PARAM_LONGITUDE];
        [paramDict setObject: dlvryType forKey:@"Chain"];
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_NEARBY_STORES Param:paramDict];
    }
}

// GLOBAL SEARCH
- (void) loadSearchKeywords
{
    if ([InternetCheck isOnline])
    {
        
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:selGenderId forKey:KEY_GENDER];
        [paramDict setObject:dlvryType forKey:@"ServiceAvailableAt"];
        [paramDict setObject:@NO forKey:@"IsSearchBlog"];
        [paramDict setObject:@NO forKey:@"IsSearchLook"];
        [paramDict setObject:@NO forKey:@"IsSearchPackage"];
        [paramDict setObject:@NO forKey:@"IsSearchProduct"];
        [paramDict setObject:@NO forKey:@"IsSearchSaloon"];
        [paramDict setObject:@NO forKey:@"IsSearchStylist"];
        [paramDict setObject:@YES forKey:@"IsSearchService"];
        
        [paramDict setObject:_selLatitude forKey:PARAM_LATITUDE];
        [paramDict setObject:_selLongitude forKey:PARAM_LONGITUDE];
        
        [paramDict setObject: @"" forKey:@"Keyword"];
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_SEARCH_KEYWORDS Param:paramDict];
    }
}

- (void) loadServicesByKeyword
{
    apiCtr = 2;
    if ([InternetCheck isOnline])
    {
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:selGenderId forKey:KEY_GENDER];
        //[paramDict setObject:dlvryType forKey:@"ServiceAvailableAt"];
        [paramDict setObject:@NO forKey:@"IsSearchBlog"];
        [paramDict setObject:@NO forKey:@"IsSearchLook"];
        [paramDict setObject:@NO forKey:@"IsSearchPackage"];
        [paramDict setObject:@NO forKey:@"IsSearchProduct"];
        [paramDict setObject:@NO forKey:@"IsSearchSaloon"];
        [paramDict setObject:@NO forKey:@"IsSearchStylist"];
        [paramDict setObject:@YES forKey:@"IsSearchService"];
        [paramDict setObject: searchBar.text forKey:@"Keyword"];
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_SEARCH_SERVICES_BY_KEYWORD Param:paramDict];
    }
}

- (void) processResponse_Stores : (NSDictionary *) dict
{
    if([storesArray count] > 0) [storesArray removeAllObjects];
    NSArray *tmpArray  = [ReadData arrayFromDictionary:dict forKey:RESPONSEKEY_DATA];
    NSDictionary *tmpDict;
    for(NSDictionary *item in tmpArray)
    {
        NSMutableArray *searchKeys = [NSMutableArray array];
        
        tmpDict = [ReadData dictionaryFromDictionary:item forKey:KEY_SALON];
        [searchKeys addObject: [ReadData stringValueFromDictionary:tmpDict forKey:KEY_NAME]];
        tmpDict = [ReadData dictionaryFromDictionary:item forKey: KEY_STORE_LOCATION];
        [searchKeys addObject: [ReadData stringValueFromDictionary:tmpDict forKey:KEY_NAME]];
        
        NSMutableDictionary *obj = [item mutableCopy];
        [obj setObject:[searchKeys componentsJoinedByString: @" "] forKey:@"searchKey"];
        [storesArray addObject:obj];
        
        
        [self toggleListing]; // add by minna
    }
}

- (void) processResponse_Keywords : (NSDictionary *) dict
{
    if([keywordsArray count] > 0) [keywordsArray removeAllObjects];
    
    NSDictionary *dataDict  = [ReadData dictionaryFromDictionary:dict forKey:RESPONSEKEY_DATA];
    NSArray *tmpArray = [ReadData arrayFromDictionary:dataDict forKey:@"Services"];
    [loadingAIV startAnimating];
    keywordsArray = [tmpArray mutableCopy];
    //NSLog(@"keya array is %@ = ", keywordsArray);
    
   // [servicesListVC loadKeywords: keywordsArray];
    //serviceDict = dict;
    //[servicesListVC loadKeywords: tmpArray];
    
    
//    for(NSDictionary *item in tmpArray)
//    {
//        BOOL isFound = NO;
//        NSString *serviceNm = [ReadData stringValueFromDictionary:item forKey:@"ServiceName"];
//        for(NSMutableDictionary *obj in keywordsArray)
//        {
//            if([[ReadData stringValueFromDictionary:obj forKey:KEY_NAME] isEqualToString:serviceNm])
//            {
//                NSInteger newCtr = [ReadData integerValueFromDictionary:obj forKey:KEY_COUNT] + 1;
//                [obj setObject:[NSNumber numberWithInteger:newCtr] forKey:KEY_COUNT];
//                isFound =YES;
//                break;
//            }
//        }
//        if(!isFound)
//        {
//            NSMutableDictionary *obj = [NSMutableDictionary dictionary];
//            [obj setValue:serviceNm forKey:KEY_NAME];
//            [obj setValue:[NSNumber numberWithInteger:1] forKey:KEY_COUNT];
//            [keywordsArray addObject:obj];
//        }
//    }
}

- (void) processResponse_Services : (NSDictionary *) dict
{
    if([servicesArray count] > 0) [servicesArray removeAllObjects];
    NSDictionary *dataDict  = [ReadData dictionaryFromDictionary:dict forKey:RESPONSEKEY_DATA];
    NSArray *tmpArray = [ReadData arrayFromDictionary:dataDict forKey:@"Services"];
    
    for(NSDictionary *item in tmpArray)
    {
        NSMutableDictionary *obj = [item mutableCopy];
        [servicesArray addObject:obj];
    }
    //[servicesListVC loadServices:servicesArray];
}


#pragma mark - Scroll content

- (CGFloat) getContentHeight;
{
    return contentHeight;
}

- (void) scrollContent : (CGPoint) offset
{
    mainscrollView.contentOffset = offset;
}

- (CGPoint) getScrollContent
{
    return mainscrollView.contentOffset;
}

#pragma mark - Gender Selector & Delegate Methods

//- (void) valueChanged
//{
//    selGenderId = [genderselectorVC getGender];
//    [self reloadList];
//}
- (void) valueChanged
{
    if([InternetCheck isOnline]){
     apiCtr = 1;
    [loadingAIV startAnimating];
    selGenderId = [genderselectorVC getGender];
        [self loadSearchKeywords];
        
        storesListCV.hidden = YES;
        servicesListCV.hidden = YES;
        noresultsL.hidden = YES;
}
   // [self reloadList];
}

#pragma mark - Searchbar delegate methods

- (void) searchBarSearchButtonClicked:(UISearchBar *)asearchBar
{
    [searchBar resignFirstResponder];
    [self searchListing:asearchBar.text];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)asearchBar
{
    searchBar.showsCancelButton = NO;
    [searchBar resignFirstResponder];
    searchBar.text = @"";
    [self searchListing:asearchBar.text];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)asearchBar
{
    [searchBar becomeFirstResponder];
    searchBar.showsCancelButton = YES;
}

- (void)searchBar:(UISearchBar *)asearchBar textDidChange:(NSString *)searchText
{
    [self searchListing:asearchBar.text];
}

#pragma mark - Events

- (IBAction) searchbySalonsBPressed:(UIButton *)sender
{
   // NSLog(@"%hhd",sender.selected);

    [searchBar resignFirstResponder];
    
    //userLocationB.hidden = NO;
    //currentAddressL.hidden = NO;
    seg.selectedSegmentIndex = 0;
    LastselectedSegmentedIndex = seg.selectedSegmentIndex;
   // NSLog(@"%lu", (long)LastselectedSegmentedIndex);
    [self CallingServiceForNearbyHomeWorkSegmentedAction:0];
    if(!sender.selected)
    {
        searchbySalonsB.selected = YES;
        searchbyServicesB.selected = NO;
        [self toggleListing];
    }
    
}

- (IBAction) searchbyServicesBPressed:(UIButton *)sender
{
   // NSLog(@"searchbyServicesBPressed %hhd",sender.selected);
    
    [searchBar resignFirstResponder];
    //userLocationB.hidden = NO;
    //currentAddressL.hidden = YES;
    seg.selectedSegmentIndex = 0;
    LastselectedSegmentedIndex = seg.selectedSegmentIndex;
   // NSLog(@"%lu", (long)LastselectedSegmentedIndex);
    //appD.sessionDelegate.isCommingFromviewController = @"saloonAndHomeSearchByServices";
    [self CallingServiceForNearbyHomeWorkSegmentedAction:0];

    if(!sender.selected)
    {
        [loadingAIV startAnimating];
        searchbySalonsB.selected = NO;
        searchbyServicesB.selected = YES;
        noresultsL.hidden = YES;
        [self toggleListing];
    }
    
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"showgenderselector_services_home"])
    {
        genderselectorVC = (GenderSelectorViewController *)segue.destinationViewController;
        genderselectorVC.delegate = self;
    }
    else if([segue.identifier isEqualToString:@"embed_storeslisting"])
    {
        storesListVC = (StoresListingViewController *) segue.destinationViewController;
    }
    else if([segue.identifier isEqualToString:@"embed_serviceslisting"])
    {
        servicesListVC = (ServicesListingViewController *) segue.destinationViewController;
    }
    else if([segue.identifier isEqualToString:@"showstoreratecard"])
    {
        [cartC setLatitude:_selLatitude andLongitude:_selLongitude];
        StoreRateCardViewController *servicesVC = (StoreRateCardViewController *)segue.destinationViewController;
        [servicesVC loadForSalonStore:(NSDictionary *)sender Gender:selGenderId  andDeliveryType:dlvryType :isFromSearchServices];
    }
}


#pragma mark - API

-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
    apiCtr --;
    if(![ReadData isValidResponseData:response])
    {
        {
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
            [self.view setUserInteractionEnabled:YES];
        }
        return;
    }
    
    if ([apiName isEqualToString:API_GET_NEARBY_STORES])
    {
        [self processResponse_Stores:response];
    }
    
    if ([apiName isEqualToString:API_SEARCH_KEYWORDS])
    {
        [self processResponse_Keywords:response];
    }
    
//    if ([apiName isEqualToString:API_SEARCH_SERVICES_BY_KEYWORD])
//    {
//        [self processResponse_Services:response];
//    }
    
    if(apiCtr <= 0)
    {
        [loadingAIV stopAnimating];
        [self toggleListing];
    }
}

//Minnarao New implementaion
#pragma mark - Events

- (IBAction)backBPressed:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


// New changes for userlocation.
-(IBAction)userChooseLocationAction:(id)sender
{
    
    ChooseUserLocationViewController *chooseLocationViewController =[self.storyboard instantiateViewControllerWithIdentifier:@"ChooseUserLocationViewController"];
    [self.navigationController  pushViewController:chooseLocationViewController animated:YES];
}

// Called, this method after user select location from choose user location viewcontrolller.
- (void) loadNearbyStoresaAfterUserSelectLocation : (NSString *)latitude : (NSString *)Longitude : (NSString *)dlvType
{
    
    if ([InternetCheck isOnline])
    {
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:latitude forKey:PARAM_LATITUDE];
        [paramDict setObject:Longitude forKey:PARAM_LONGITUDE];
        [paramDict setObject: dlvType forKey:@"Chain"];
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_NEARBY_STORES Param:paramDict];
    }
}

/// Update location  on top.
- (void) locationUpdatedOnTop{
    //NSString * currentAddress = [ appD getUserCurrentLocationAddress];
    //NSLog(@"%@", currentAddress);
    
    currentAddressL.text = [appD getUserCurrentLocationAddress];
   // NSLog(@"%@", currentAddressL.text);
}




//************************** New functionality ***************



/// New functionality for Segment controller and implemeted on 03 Aprial, 2017.
-(IBAction)CallingServiceForNearbyHomeWorkSegmentedAction:(UISegmentedControl *)segIndex{
    
    //NSLog(@"%@",segIndex);
   // NSLog(@"%ld",(long)segIndex.selectedSegmentIndex);
   // NSLog(@"searchbySalonsB index %d",searchbySalonsB.selected);
   // NSLog(@"searchbyServicesB index %d",searchbyServicesB.selected);
    
    
    if (segIndex.selectedSegmentIndex== 0) { // NearBy service.
        //selectedSegmentedIndex = 0;
        
        userLocationB.hidden = NO;
        currentAddressL.hidden = NO;
        
        //NSLog(@" searchbySalonsB %hhd",searchbySalonsB.selected);
       // NSLog(@"searchbyServicesB %hhd",searchbyServicesB.selected);
        
        if(searchbySalonsB.selected){
            
            // [storesListVC loadData: storesArray];
            _selLatitude = [appD getUserCurrentLocationLatitude];
            _selLongitude = [appD getUserCurrentLocationLongitude];
           // dlvryType = DELIVERY_TYPE_SALON;
            [self loadNearbyStores];
        }else{
            //[servicesListVC loadKeywords:keywordsArray];
           // dlvryType =DELIVERY_TYPE_HOME;
            selGenderId = [appD getLoggedInUserGender];
            _selLatitude = [appD getUserCurrentLocationLatitude];
            _selLongitude = [appD getUserCurrentLocationLongitude];
            [self loadSearchKeywords];
        }
        
        
    }else if (segIndex.selectedSegmentIndex == 1){ // Home service.
        //LastselectedSegmentedIndex = seg.selectedSegmentIndex;
        
        userLocationB.hidden = YES;
        currentAddressL.hidden = YES;
        if (searchbySalonsB.selected) {
            NSDictionary *dictHome = [appD getLoggedInHomeAddress];
           // NSLog(@"dictSel %@", dictHome);
            //[[appD getLoggedInHomeAddress] isEqual: [NSNull null]];
            if (dictHome == NULL || dictHome == nil || dictHome.count == 0) {
               // NSLog(@"null values");
                UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"This address doesn't exist. Do you want to add this address?" message:nil delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"OK", nil];
                alt.tag = 1;
                [alt show];
                
                
            }else{
               // NSLog(@"not null values");
               //
                _selLatitude = [dictHome valueForKey:@"LAT"];
                _selLongitude = [dictHome valueForKey:@"LONG"];
                //dlvryType = DELIVERY_TYPE_SALON;
                
                [self loadNearbyStores];
                
            }
            
            
        } else {
            
            
            NSDictionary *dict = [appD getLoggedInHomeAddress];
           // NSLog(@"dict %@", dict);
            //[[appD getLoggedInHomeAddress] isEqual: [NSNull null]];
            if (dict == NULL || dict == nil || dict.count == 0) {
                //NSLog(@"null values");
                UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"This address doesn't exist. Do you want to add this address?" message:nil delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"OK", nil];
                alt.tag = 1;
                [alt show];
                
                
            }else{
                //NSLog(@"not null values");
                //dlvryType =DELIVERY_TYPE_HOME;
                selGenderId = [appD getLoggedInUserGender];
                _selLatitude = [dict valueForKey:@"LAT"];
                _selLongitude = [dict valueForKey:@"LONG"];
                [self loadSearchKeywords];
                
            }
        }
        
    } else if (segIndex.selectedSegmentIndex == 2){ // Work service.
       // LastselectedSegmentedIndex = seg.selectedSegmentIndex;
        userLocationB.hidden = YES;
        currentAddressL.hidden = YES;
        if (searchbySalonsB.selected) {
            
            NSDictionary *workDict = [appD getLoggedInWorkAddress];
           // NSLog(@"%@", workDict);
            //[[appD getLoggedInHomeAddress] isEqual: [NSNull null]];
            if (workDict == NULL || workDict == nil || workDict.count == 0) {
               // NSLog(@"null values");
                UIAlertView *altW = [[UIAlertView alloc]initWithTitle:@"This address doesn't exist. Do you want to add this address?" message:nil delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"OK", nil];
                altW.tag = 2;
                [altW show];
                
                
            }else{
                //NSLog(@"not null values");
                
                _selLatitude = [workDict valueForKey:@"LAT"];
                _selLongitude = [workDict valueForKey:@"LONG"];
                //dlvryType = DELIVERY_TYPE_SALON;
                
                [self loadNearbyStores];
            }
        }
        else{
            NSDictionary *dict = [appD getLoggedInWorkAddress];
            //NSLog(@"%@", dict);
            //[[appD getLoggedInHomeAddress] isEqual: [NSNull null]];
            
            if (dict == NULL || dict == nil || dict.count == 0) {
               // NSLog(@"null values");
                UIAlertView *altW = [[UIAlertView alloc]initWithTitle:@"This address doesn't exist. Do you want to add this address?" message:nil delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"OK", nil];
                altW.tag = 2;
                
                [altW show];
                
                
            }else{
               // NSLog(@"not null values");
                
                //[self sallonORServie:1];
                //selectedSegmentedIndex = 2;
                //[self CallingServiceForNearbyHomeWorkSegmentedAction:selectedSegmentedIndex];
                selGenderId = [appD getLoggedInUserGender];
                _selLatitude = [dict valueForKey:@"LAT"];
                _selLongitude = [dict valueForKey:@"LONG"];
                [self loadSearchKeywords];
                
            }
            
        }
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1 ) {
        if (buttonIndex == 0) {
            
           // NSLog(@"%lu", (long)LastselectedSegmentedIndex);
            if(searchbySalonsB.selected){
                
                // [storesListVC loadData: storesArray];
                _selLatitude = [appD getUserCurrentLocationLatitude];
                _selLongitude = [appD getUserCurrentLocationLongitude];
                // dlvryType = DELIVERY_TYPE_SALON;
                [self loadNearbyStores];
            }else{
                //[servicesListVC loadKeywords:keywordsArray];
                // dlvryType =DELIVERY_TYPE_HOME;
                selGenderId = [appD getLoggedInUserGender];
                _selLatitude = [appD getUserCurrentLocationLatitude];
                _selLongitude = [appD getUserCurrentLocationLongitude];
                [self loadSearchKeywords];
            }
              seg.selectedSegmentIndex = LastselectedSegmentedIndex;
            userLocationB.hidden = NO;
            currentAddressL.hidden = NO;

            
        }else{
            ChooseUserLocationViewController *chooseUserLocationViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChooseUserLocationViewController"];
            appD.sessionDelegate.isCommingFromviewController = @"fromsalonhomeBtnService";
            [self.navigationController pushViewController:chooseUserLocationViewController animated:YES];
            
            
        }
    }else if(alertView.tag == 2){
        if (buttonIndex == 0) {
            
            if(searchbySalonsB.selected){
                
                // [storesListVC loadData: storesArray];
                _selLatitude = [appD getUserCurrentLocationLatitude];
                _selLongitude = [appD getUserCurrentLocationLongitude];
                // dlvryType = DELIVERY_TYPE_SALON;
                [self loadNearbyStores];
            }else{
                //[servicesListVC loadKeywords:keywordsArray];
                // dlvryType =DELIVERY_TYPE_HOME;
                selGenderId = [appD getLoggedInUserGender];
                _selLatitude = [appD getUserCurrentLocationLatitude];
                _selLongitude = [appD getUserCurrentLocationLongitude];
                [self loadSearchKeywords];
            }
            
            // NSLog(@"%lu", (long)LastselectedSegmentedIndex);
            seg.selectedSegmentIndex = LastselectedSegmentedIndex;
            userLocationB.hidden = NO;
            currentAddressL.hidden = NO;

            
        }else{
            ChooseUserLocationViewController *chooseUserLocationViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChooseUserLocationViewController"];
            appD.sessionDelegate.isCommingFromviewController = @"fromsalonWorkBtnService";
            [self.navigationController pushViewController:chooseUserLocationViewController animated:YES];
            
            
        }
    }
    
    
}


@end
