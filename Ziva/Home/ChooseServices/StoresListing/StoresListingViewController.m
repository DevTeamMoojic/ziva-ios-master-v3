//
//  StoresListingViewController.m
//  Ziva
//
//  Created by Bharat on 15/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "StoreCell.h"
#import "StoresListingViewController.h"
#import "ChooseServicesViewController.h"
#import "IndividualSaloanViewController.h"

@interface StoresListingViewController ()<individualSaloonDelegates>
{
    NSArray *list;
    
    __weak IBOutlet UITableView *listTblV;
    __weak IBOutlet UILabel *noresultsL;
    
    // individual saloan dict.
    NSDictionary * pickDict;
}
@end

@implementation StoresListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Methods

- (void) loadData : (NSArray *) filteredDataList
{
    [listTblV setContentOffset:CGPointZero animated:NO];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:KEY_DISTANCE ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    list = [filteredDataList sortedArrayUsingDescriptors:sortDescriptors];
    [listTblV reloadData];
    noresultsL.hidden = ([list count] > 0);
    listTblV.hidden = ([list count] == 0);
    
}

- (CGFloat) getContentHeight
{
    CGFloat height = 0;
    for(NSInteger ctr = 0; ctr < [list count]; ctr++){
        height += 100;
    }
    return height;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [list count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    StoreCell *cell = (StoreCell *)[tableView dequeueReusableCellWithIdentifier:@"storecell" forIndexPath:indexPath];
    NSDictionary *item = list[indexPath.row];
    [cell configureDataForCell :item];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSDictionary *item = list[indexPath.row];

    pickDict = item;
    /*
    NSDictionary *item = list[indexPath.row];
    ChooseServicesViewController *parentVC = (ChooseServicesViewController *)self.parentViewController;
    [parentVC displayStoreRateCard:item];*/
    
    // Pointing to individual page.
    IndividualSaloanViewController *INDVC = [self.storyboard instantiateViewControllerWithIdentifier:@"IndividualSaloanViewController"];
    INDVC.delegate = self;
    [INDVC loadsaloanData:item];
    [self.navigationController pushViewController:INDVC animated:YES];
}


/// new functionality for individual saloon sotre.
#pragma mark individual saloan delegates.
-(void) individualSaloonDetails{
   //NSDictionary *item = list[indexPath.row];
    ChooseServicesViewController *parentVC = (ChooseServicesViewController *)self.parentViewController;
    [parentVC displayStoreRateCard:pickDict];

}

@end
