//
//  IndividualSaloanCell.m
//  Ziva
//
//  Created by Minnarao on 26/5/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "IndividualSaloanCell.h"

@implementation IndividualSaloanCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

    
    // Fix : Autoresizing issues for iOS 7 using Xcode 6
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //    Fix : For right aligned view with autoresizing
    UIView *backV = [[self.contentView subviews] objectAtIndex:0];
    backV.frame = [UpdateFrame setSizeForView:backV usingSize:self.bounds.size];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void) configureDataForCell : (NSDictionary *) item
{
    //NSLog(@"%@",item);
    self.titleL.text = [ReadData stringValueFromDictionary:item forKey:KEY_SERVICE_TITLE];
    self.priceL.text = [ReadData amountInRsFromDictionary:item forKey:KEY_SERVICE_ZIVAPRICE];
    
    /*
     self.priceActualL.hidden = YES;
     self.priceDiscountL.hidden = YES;
     self.DiscountCouponL.hidden = YES;
     self.DiscountCouponCashBackL.hidden = YES;
     */
    
    // For strikeline in cernter on text .
    NSAttributedString * title =
    [[NSAttributedString alloc] initWithString:[ReadData amountInRsFromDictionary:item forKey:KEY_SERVICE_PRICE]
                                    attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle)}];
    [self.priceActualL setAttributedText:title];
    
    // discount price.
    //self.priceDiscountL.text = [ReadData amountInRsFromDictionary:item forKey:KEY_SERVICE_DISCOUNT_PRICE];
    self.priceDiscountL.text = [ReadData amountInRsFromDictionary:item forKey:KEY_SERVICE_ZIVAPRICE];
    
    // discount coupan.
    //self.DiscountCouponL.text = [ReadData amountInRsFromDictionary:item forKey:KEY_SERVICE_DISCOUNTCOUPAN_PRICE];
    
    
    
    
    /*
     NSLog(@"%@",[ReadData stringValueFromDictionary:item forKey:KEY_SERVICE_PRICE]);
     if ([[ReadData stringValueFromDictionary:item forKey:KEY_SERVICE_PRICE] isEqual:[NSNull null]]) {
     
     self.priceActualL.hidden = NO;
     self.priceDiscountL.hidden = NO;
     self.DiscountCouponL.hidden = NO;
     self.DiscountCouponCashBackL.hidden = NO;
     
     }else{
     //self.priceActualL.hidden = YES;
     //self.priceDiscountL.hidden = YES;
     //self.DiscountCouponL.hidden = YES;
     //self.DiscountCouponCashBackL.hidden = YES;
     
     self.priceActualL.hidden = NO;
     self.priceDiscountL.hidden = NO;
     self.DiscountCouponL.hidden = NO;
     self.DiscountCouponCashBackL.hidden = NO;
     }
     
     */
    
    
    
    NSArray *cashbacks = [ReadData arrayFromDictionary:item forKey:KEY_CASHBACKS];
    
    self.showOffersB.hidden = ([cashbacks count] < 2);
    self.selectServicesB.hidden = !self.showOffersB.hidden;
    
    self.selectServicesB.selected = NO;
    
    
    
    // new implementation.
    // NSArray *cashbacks = [ReadData arrayFromDictionary:item forKey:KEY_CASHBACKS];
    //CGFloat positionY = (CGRectGetHeight(self.priceInfoV.frame) - CGRectGetHeight(self.offerpriceL.frame)) *0.5;
    NSInteger actualCMP = [[ReadData stringValueFromDictionary:item forKey:KEY_SERVICE_PRICE] floatValue];
    NSInteger zivaCMP = [[ReadData stringValueFromDictionary:item forKey:KEY_SERVICE_ZIVAPRICE] floatValue];
    
    
    /*
     if([cashbacks count] > 0){
     
     self.priceActualL.hidden = YES;
     self.priceDiscountL.hidden = YES;
     self.DiscountCouponL.hidden = NO;
     }
     else{
     
     if (actulP == zivaP) {
     self.DiscountCouponL.hidden = YES;
     self.priceActualL.hidden = YES;
     self.priceDiscountL.hidden = YES;
     
     }else{
     if (actulP > zivaP) {
     self.DiscountCouponL.hidden = NO;
     self.priceActualL.hidden = NO;
     self.priceDiscountL.hidden = NO;
     }else{
     self.DiscountCouponL.hidden = NO;
     self.priceActualL.hidden = YES;
     self.priceDiscountL.hidden = YES;
     
     }
     }
     
     }
     */
    
    //NSLog(@"actual--- %ld",(long)actualCMP);
    //NSLog(@"ziva --- %ld",(long)zivaCMP);
    // if the looks is cash back else to discount values.
    if([cashbacks count] > 0){
        
        self.priceActualL.hidden = YES;
        self.priceDiscountL.hidden = YES;
        self.DiscountCouponL.hidden = NO;
        
        //
        // diffrenciate the amount and percent.
        NSArray *cashbackArray = [[ReadData arrayFromDictionary:item forKey:KEY_CASHBACKS] firstObject];
        //NSLog(@"%@", cashbackArray);
        NSInteger priceMode = [[cashbackArray valueForKey:@"Mode"] integerValue];
        // NSLog(@"%ld", (long)priceMode);
        // NSString *priceMode1 = [cashbackArray valueForKey:@"Cost"];
        //NSLog(@"%@", priceMode1);
        
        // if the it 0 the values is cash eles the percentage.
        if (priceMode == 0) {
            self.DiscountCouponL.text = [NSString stringWithFormat:@"%@ Cash Back",[ReadData getAmountFormatted:[[cashbackArray valueForKey:@"Cost"] floatValue]]];
        }else{
            NSString *precent = @"%";
            self.DiscountCouponL.text = [NSString stringWithFormat:@"Cash Back %@%@",[cashbackArray valueForKey:@"Cost"],precent];
        }
        
        
    }
    else if(actualCMP > zivaCMP){ //disctount
        self.priceActualL.hidden = NO;
        self.priceDiscountL.hidden = NO;
        self.DiscountCouponL.hidden = YES;
    }else{
        self.priceActualL.hidden = YES;
        self.priceDiscountL.hidden = YES;
        self.DiscountCouponL.hidden = YES;
    }
    
    
    
    
    //        //self.DiscountCouponL.hidden = YES;
    //        if (actualCMP > zivaCMP) {
    //
    //            //self.silverPriceStrikeL.hidden = NO;
    //            //self.silverPriceL.hidden = YES;
    //
    //        }
    //        else if((silverPrice = golgPrice || golgPrice> silverPrice)){
    //            self.priceActualL.hidden = NO;
    //            self.priceDiscountL.hidden = YES;
    //            //self.silverPriceStrikeL.hidden = YES;
    //        }
    //
    //    }
    
}

@end
