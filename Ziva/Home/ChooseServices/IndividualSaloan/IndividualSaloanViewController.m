//
//  IndividualSaloanViewController.m
//  Ziva
//
//  Created by Minnarao on 21/4/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "IndividualSaloanViewController.h"
#import <MapKit/MapKit.h>
#import "IndividualSaloanCell.h"
#import "StoreRateCardViewController.h"
#import "LocationDateStoreSelectorViewController.h"


@interface IndividualSaloanViewController ()<UIScrollViewDelegate,APIDelegate,UITableViewDelegate,UITableViewDataSource>{

    AppDelegate *appD;
    __weak IBOutlet UIScrollView *scrollV;
    
    __weak IBOutlet UIImageView *topImgV;
    __weak IBOutlet UIView *topNavigationV;

    __weak IBOutlet UIView *saloanDetailsV;
    __weak IBOutlet UITextView *descriptionTxtV;
    __weak IBOutlet UIView *cashBackscrollingV;
    
    __weak IBOutlet UIView *seeAllServiceV;
    __weak IBOutlet UIView  *recomendedServices;
    __weak IBOutlet UITableView  *listV;
    __weak IBOutlet MKMapView *mapV;   //insert the one map view
    
    // saloon outlets.
    __weak IBOutlet UILabel *saloanNameL;
    __weak IBOutlet UILabel *saloanAddrsL;
    __weak IBOutlet UIImageView *logoImgV;
    __weak IBOutlet UIImageView *placeholderImgV;
    
    __weak IBOutlet UILabel *ratingL;
    __weak IBOutlet UITextField *ratingStarTXTF;
    
    // saloon dict
    NSDictionary *saloanMainDcit;
    NSArray *recomendedList;
    
    // Add Cart Item View variables.
    __weak IBOutlet UIView *cartAddV;
    int serviceCountItems;
    MyCartController *cartC;
   
}

@end


@implementation IndividualSaloanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;
    

    
    scrollV.delegate = self;
   // scrollV.contentSize = CGSizeMake(self.view.bounds.size.width, topImgV.frame.size.height + saloanDetailsV.frame.size.height + descriptionTxtV.frame.size.height + cashBackscrollingV.frame.size.height + seeAllServiceV.frame.size.height + listV.frame.size.height + mapV.frame.size.height +  topNavigationV.frame.size.height + recomendedServices.frame.size.height+ 60);
    
    scrollV.contentSize = CGSizeMake(self.view.bounds.size.width, topImgV.frame.size.height + saloanDetailsV.frame.size.height + descriptionTxtV.frame.size.height  + seeAllServiceV.frame.size.height + mapV.frame.size.height +  topNavigationV.frame.size.height );

    
    // For view shadow.
    [appD shadowView:cashBackscrollingV];
    [appD shadowView:descriptionTxtV];
    
    
    [ self loadData:saloanMainDcit];

    // get lat and long.
    NSString * lat = [ReadData stringValueFromDictionary:saloanMainDcit forKey:PARAM_LATITUDE];
    NSString * longitude = [ReadData stringValueFromDictionary:saloanMainDcit forKey:PARAM_LONGITUDE];
  
    /// map view
    MKPointAnnotation *pointAnnotation = [[MKPointAnnotation alloc]init];
    CLLocationCoordinate2D cord= CLLocationCoordinate2DMake([lat doubleValue], [longitude doubleValue]);

    //CLLocationCoordinate2D cord= CLLocationCoordinate2DMake(19.062066,  73.025922);
    pointAnnotation.coordinate=cord;
    // title as saloan name.
    NSDictionary *sDict = [saloanMainDcit valueForKey:@"Saloon"];
    pointAnnotation.title=[ReadData stringValueFromDictionary:sDict forKey:@"Name"];
    
     // saloan Adress.
    NSDictionary *locationDict = [saloanMainDcit valueForKey:@"Location"];
    pointAnnotation.subtitle= [ReadData stringValueFromDictionary:locationDict forKey:@"Name"];
    [mapV addAnnotation:pointAnnotation];
    
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.7;
    span.longitudeDelta = 0.7;
    region.center=cord;
    region.span = span;
 
    //region.span.longitudeDelta  = 0.005;
    //region.span.latitudeDelta  = 0.005;
    
    [mapV setRegion:region animated:YES];
    
    // Calling recommended service
    [self loadStoreRateCardOnIndividualSalon:saloanMainDcit];
}
- (void) loadsaloanData : (NSDictionary *) saloanDcit{
    
    saloanMainDcit = saloanDcit;
}

// This is a method to load the store details and display on the view.
- (void) loadData : (NSDictionary *) saloanDcit{
    
    //saloan name.
    NSDictionary *sDict = [ saloanDcit valueForKey:@"Saloon"];
    saloanNameL.text = [ReadData stringValueFromDictionary:sDict forKey:@"Name"];
    
    // saloan Adress.
    NSDictionary *locationDict = [ saloanDcit valueForKey:@"Location"];
    saloanAddrsL.text = [ReadData stringValueFromDictionary:locationDict forKey:@"Name"];
    
    // salon logo.

    // ImageView
    NSString *url = [ReadData stringValueFromDictionary:sDict forKey:KEY_SALON_LOGOURL];
    if (![url isEqualToString:@""])
    {
        NSURL *imageURL = [NSURL URLWithString:url];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageURL];
        [logoImgV setImageWithURLRequest:urlRequest
                             placeholderImage:Nil
                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             [logoImgV setImage:image];
             [UIView animateWithDuration:IMAGE_VIEW_TOGGLE
                                   delay:0 options:UIViewAnimationOptionCurveLinear
                              animations:^(void)
              {
                  
                  [placeholderImgV setAlpha:0 ];
                  [logoImgV setAlpha:1 ];
              }
                              completion:^(BOOL finished)
              {
              }];
             
             
             
         }
                                      failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
         {
             
         }];
    }
    
    
    // ImageView
    NSString *urltop = [ReadData stringValueFromDictionary:sDict forKey:KEY_SALON_LOGOURL];
    if (![url isEqualToString:@""])
    {
        NSURL *imageURL = [NSURL URLWithString:urltop];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageURL];
        [topImgV setImageWithURLRequest:urlRequest
                        placeholderImage:Nil
                                 success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             [topImgV setImage:image];
             [UIView animateWithDuration:IMAGE_VIEW_TOGGLE
                                   delay:0 options:UIViewAnimationOptionCurveLinear
                              animations:^(void)
              {
                  
                  [placeholderImgV setAlpha:0 ];
                  [topImgV setAlpha:1 ];
              }
                              completion:^(BOOL finished)
              {
              }];
             
             
             
         }
                                 failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
         {
             
         }];
    }
    
    
    // description
    descriptionTxtV.text = [ReadData stringValueFromDictionary:saloanDcit forKey:@"Address"];
}

#pragma mark - Events
// back button action.
- (IBAction)backBPressed:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


// For pointing to rate card with all service .
-(IBAction)individualSaloanServiceBPressed:(UIButton *)sender{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(individualSaloonDetails)])
    {
        [self.delegate individualSaloonDetails] ;
    }
    
   // NSLog(@"%@", saloanMainDcit);
    NSString *chainValue =[ReadData stringValueFromDictionary:saloanMainDcit forKey:@"Chain"];
    
    if ([chainValue isEqualToString:@"0"]) {
        //appD.sessionDelegate.isCommingFromviewController = @"fromhomeserviceVC";
        appD.sessionDelegate.isCommingFromviewController= @"fromsalonservicesVC";
    }else if ([chainValue isEqualToString:@"1"]){
    appD.sessionDelegate.isCommingFromviewController = @"fromhomeserviceVC";
    }else{
        appD.sessionDelegate.isCommingFromviewController = @"saloonAndHomeSearchByServices";
    }
    

   //appD.sessionDelegate.isCommingFromviewController = @"fromIndividualVC";

}

#pragma mark scroll view delegate methods.

- (void)scrollViewDidScroll:(UIScrollView *)scrollView // any offset changes
{
  scrollView.contentOffset = CGPointMake(0, scrollView.contentOffset.y);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Recomended Services Methods.

// Calling web service for Store rate card and get the recomented services.
-(void) loadStoreRateCardOnIndividualSalon :(NSDictionary *)dict{
    
    //[HUD hide:YES];
    if ([InternetCheck isOnline])
    {
        NSMutableArray *replacementArray = [NSMutableArray array];
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:@"RECORD_ID" forKey:@"key"];
        [paramDict setObject:[ReadData stringValueFromDictionary:saloanMainDcit forKey:@"Id"] forKey:@"value"];
        [replacementArray addObject:paramDict];
        
        paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:@"GENDER_ID" forKey:@"key"];
        [paramDict setObject:[appD getLoggedInUserGender] forKey:@"value"];
        [replacementArray addObject:paramDict];
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_STORE_RATE_CARD Param:nil replacementStrings:replacementArray];
    }else{
        [self showErrorMessage:NETWORKERROR];
        
    }
    
    
  
}

// Response for storerateCard web service for store rate card list and navigate to page.
-(void)processResponseIndividualSaloanRateCard:(NSDictionary*)dict{
    
    //NSLog(@"%@", dict);
   // NSDictionary *storeIDDict = dict;
    
   
    
    
    recomendedList = [ReadData arrayFromDictionary:dict forKey:RESPONSEKEY_DATA];
    
    // Sliting Recomended services.
    
    NSMutableArray *recomendedArr = [NSMutableArray new];
    for (NSDictionary *D in recomendedList) {
        
        int recomended = [[D valueForKey:@"IsRecommendedService"] integerValue];
       // [ReadData boolValueFromDictionary:D forKey:@"IsRecommendedService"];
        
        if (recomended == 1) {
            [recomendedArr addObject:D];
        }
    }
    
    NSLog(@"%@", recomendedArr);
    recomendedList = recomendedArr;
    serviceCountItems = 0;
    
    // the count is zero then the cart button is hide else to show
    if (serviceCountItems == 0) {
        cartAddV.hidden = YES;
    }else{
        cartAddV.hidden = NO;
    }
    
    // hide the tableview when the count is zero.
    if (recomendedList.count> 0) {
        listV.hidden = NO;
        recomendedServices.hidden = NO;

        listV.delegate = self;
        listV.dataSource = self;
        [listV reloadData];
    }else{
        listV.hidden = YES;
        recomendedServices.hidden = YES;
    }
    
    
//    //[HUD hide:YES];
//    
//    [cartC updateStoreInformation:dict];
//    [cartC setLatitude:[appD getUserCurrentLocationLatitude] andLongitude:[appD getUserCurrentLocationLongitude]];
//    
//    UIStoryboard *modalStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    StoreRateCardViewController *LDSSVC = [modalStoryboard instantiateViewControllerWithIdentifier:@"StoreRateCardViewController"];
//    [LDSSVC loadForSalonStore:dict Gender:[appD getLoggedInUserGender] andDeliveryType:@"0" :NO];
//    
//    [self.navigationController pushViewController:LDSSVC animated:YES];
}

#pragma mark UITable View Methods.

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return recomendedList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    static NSString *identifire = @"RecommendedCell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifire];
//    
//    if (cell == nil) {
//        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifire];
//    }
//    
//    NSDictionary *item = recomendedList[indexPath.row];
//
//    // service Name.
//    UILabel *serviceNameL = (UILabel*)[cell.contentView viewWithTag:1111];
//    serviceNameL.text = [item valueForKey:@"Service"];
//    
//    // Discount Price.
//    
//    // Actual Price.
//    
//    // Ziva Price.
//    
//    cell.textLabel.text = [item valueForKey:@"Service"];
//    return cell;
    
    
    
    IndividualSaloanCell *cell = (IndividualSaloanCell *)[tableView dequeueReusableCellWithIdentifier:@"individualsaloancell" forIndexPath:indexPath];
    
 
        NSDictionary *item = recomendedList[indexPath.row];

        //cell.isStoreRateCard = isStoresRateCard;
        [cell configureDataForCell :item];
        
        cell.showOffersB.tag = cell.selectServicesB.tag = cell.actionB.tag = indexPath.row;
        cell.selectServicesB.selected = [appD.sessionDelegate.mycartC isAddedToCart:[ReadData recordIdFromDictionary:item forKey:KEY_ID]];
        
  
    return cell;
}


#pragma mark - API

-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
    if(![ReadData isValidResponseData:response])
    {
        {
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
            [self.view setUserInteractionEnabled:YES];
           // [sectionloaderAIV stopAnimating];
        }
        return;
    }
    
    if ([apiName isEqualToString:API_GET_STORE_RATE_CARD]) {
        [self processResponseIndividualSaloanRateCard:response];
    }
    
}

#pragma mark  Error Message Methods.
- (void) showErrorMessage : (NSString *)message
{
    UIAlertView *errorNotice = [[UIAlertView alloc] initWithTitle:ALERT_TITLE_ERROR message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorNotice show];
}


// new functionlity for recomonded service.


@end
