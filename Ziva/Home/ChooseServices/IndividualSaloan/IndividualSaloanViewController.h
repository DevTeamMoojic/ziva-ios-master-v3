//
//  IndividualSaloanViewController.h
//  Ziva
//
//  Created by Minnarao on 21/4/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol individualSaloonDelegates <NSObject>

-(void)individualSaloonDetails;

@end

@interface IndividualSaloanViewController : UIViewController

@property (nonatomic, weak) id <individualSaloonDelegates> delegate;

-(IBAction)individualSaloanServiceBPressed:(UIButton *)sender;

- (void) loadsaloanData : (NSDictionary *) saloanDcit;


@end
