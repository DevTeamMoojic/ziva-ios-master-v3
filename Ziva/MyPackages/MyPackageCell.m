//
//  MyPackageCell.m
//  Ziva
//
//  Created by Minnarao on 2/3/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "MyPackageCell.h"

@implementation MyPackageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
    // Fix : Autoresizing issues for iOS 7 using Xcode 6
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //    Fix : For right aligned view with autoresizing
    UIView *backV = [[self.contentView subviews] objectAtIndex:0];
    backV.frame = [UpdateFrame setSizeForView:backV usingSize:self.bounds.size];
}

- (void) configureDataForCell : (NSDictionary *) item{
    
    //NSLog(@"%@", [item allKeys]);
    
   // NSDictionary *pkgBundelDict = [item valueForKey:@"PackageBundle"];
    self.titleL.text = [ReadData stringValueFromDictionary:item forKey:KEY_PACKAGE_TITLE];
    
       /*
    self.startPriceL.text = [NSString stringWithFormat:@"Starting @ %@", [ReadData getAmountFormatted:[[ReadData stringValueFromDictionary:pkgBundelDict forKey:KEY_PACKAGE_PRICE] floatValue]]];
    //NSDictionary *cashBackDict = [item valueForKey:KEY_CASHBACKS];
    
 
    // cash back
    NSArray *cashbacks = [ReadData arrayFromDictionary:item forKey:KEY_CASHBACKS];
    //CGFloat positionY = (CGRectGetHeight(self.priceInfoV.frame) - CGRectGetHeight(self.offerpriceL.frame)) *0.5;
    
    // if the looks is cash back else to discount values.
    if([cashbacks count] > 0){
        
        // diffrenciate the amount and percent.
        NSArray *cashbackArray = [[ReadData arrayFromDictionary:item forKey:KEY_CASHBACKS] firstObject];
        //NSLog(@"%@", cashbackArray);
        NSInteger priceMode = [[cashbackArray valueForKey:@"Mode"] integerValue];
        // NSLog(@"%ld", (long)priceMode);
        // NSString *priceMode1 = [cashbackArray valueForKey:@"Cost"];
        //NSLog(@"%@", priceMode1);
        
        // if the it 0 the values is cash eles the percentage.
        if (priceMode == 0) {
            self.cashBackPriceL.text = [NSString stringWithFormat:@"Get %@ Cash Back.",[ReadData getAmountFormatted:[[cashbackArray valueForKey:@"Cost"] floatValue]]];
        }else{
            NSString *precent = @"%";
            self.cashBackPriceL.text = [NSString stringWithFormat:@"Get %@%@ Cash Back",[cashbackArray valueForKey:@"Cost"],precent];
        }
        
    }
    else{ // cash back
        
        
    }
    
    // Diffrencitate the cash.
    if ([cashbacks count] > 0 && [ReadData stringValueFromDictionary:item forKey:KEY_PACKAGE_STARTINGPRCE]) {
        self.cashBackPriceL.hidden = NO;
        self.startPriceL.hidden = NO;
        self.startPriceL.frame = self.startPriceL.frame;
        
    }else if ([ReadData stringValueFromDictionary:item forKey:KEY_PACKAGE_STARTINGPRCE]){
        self.startPriceL.hidden = NO;
        self.cashBackPriceL.hidden = YES;
        self.startPriceL.frame = self.cashBackPriceL.frame;
        //CGRectMake(self.cashBackPriceL.frame.origin.x, self.cashBackPriceL.frame.origin.y, <#CGFloat width#>, <#CGFloat height#>)
        
        
    }else if ([ReadData stringValueFromDictionary:item forKey:KEY_PACKAGE_STARTINGPRCE]){
        self.startPriceL.hidden = YES;
        self.cashBackPriceL.hidden = NO;
    }
     */
    
    
   // NSDictionary *imgDict = [pkgBundelDict valueForKey:@"Package"];
    
    // image display.
    // ImageView
    NSString *url = [ReadData stringValueFromDictionary:item forKey:KEY_IMAGE_URL];
    if (![url isEqualToString:@""])
    {
        NSURL *imageURL = [NSURL URLWithString:url];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageURL];
        [self.mainImgV setImageWithURLRequest:urlRequest
                             placeholderImage:Nil
                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             [self.mainImgV setImage:image];
             
             [UIView animateWithDuration:IMAGE_VIEW_TOGGLE
                                   delay:0 options:UIViewAnimationOptionCurveLinear
                              animations:^(void)
              {
                  
                  [self.placeholderImgV setAlpha:0 ];
                  [self.mainImgV setAlpha:1 ];
              }
                              completion:^(BOOL finished)
              {
              }];
             
         }
                                      failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
         {
             
         }];
    }
    
    
    
    
    
}

///  For Details
- (void) configureDetailDataForCell : (NSDictionary *) item{
    
    // NSLog(@"%@",[item allKeys]);
    self.pkgBNameL.text = [ReadData stringValueFromDictionary:item forKey:KEY_NAME];
    self.pkgBPriceL.text = [ReadData getAmountFormatted:[[ReadData stringValueFromDictionary:item forKey:KEY_SERVICE_PRICE] floatValue]];
    self.pkgBSubTitleL.text = [ReadData stringValueFromDictionary:item forKey:@"SubTitle"];
}

@end
