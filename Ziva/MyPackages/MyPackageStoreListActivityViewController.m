//
//  MyPackageStoreListActivityViewController.m
//  Ziva
//
//  Created by Minnarao on 8/3/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "MyPackageStoreListActivityViewController.h"
#import "MyPackageStoreActivityCell.h"
#import "StoreRateCardViewController.h"
#import "LocationDateStoreSelectorViewController.h"
#define ROW_HEIGHT 100


@interface MyPackageStoreListActivityViewController ()<UITableViewDelegate,UITableViewDataSource>
    {
        MBProgressHUD *HUD;
        
        NSArray *list;
        CGSize layoutsize;
        
        __weak IBOutlet UIView *optionsV;
        __weak IBOutlet UILabel *screenTitleL;
        
        __weak IBOutlet UIView *containerView;
        __weak IBOutlet UIView *topImageV;
        __weak IBOutlet UIImageView *mainImgV;
        
        __weak IBOutlet UIScrollView *scrollviewOnTop;
        
        __weak IBOutlet UIScrollView *tabcontentscrollView;
        
        __weak IBOutlet UITableView *listTblV;
        __weak IBOutlet UILabel *noresultsL;
        
        CGRect cachedImageViewSize;
        CGFloat yscrollOffset;
        
        BOOL isBackFromOtherView;
        BOOL isContentSizeUpdated;
        
        NSInteger pageWidth;
        
        AppDelegate *appD;
        
        CGFloat contentHeight;
         MyCartController *cartC;
        BOOL isFromSearchServicesPackage;
        
         NSString *selGenderId;
         NSString *selLatitude;
         NSString *selLongitude;

        
    }
    @end

@implementation MyPackageStoreListActivityViewController
    
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;
    
    
    selLatitude = [appD getUserCurrentLocationLatitude];
    selLongitude = [appD getUserCurrentLocationLongitude];
    selGenderId = [appD getLoggedInUserGender];
  
     [self setupLayout];
    listTblV.delegate = self;
    listTblV.dataSource = self;
    [listTblV reloadData];
    //list = [ReadData arrayFromDictionary:dict forKey:RESPONSEKEY_DATA];
    [listTblV setContentOffset:CGPointZero animated:NO];
    
    contentHeight = [self calculateHeightOfList];
    if(contentHeight <= CGRectGetHeight(tabcontentscrollView.bounds)){
        contentHeight = CGRectGetHeight(tabcontentscrollView.bounds);
        listTblV.scrollEnabled = NO;
    }
    
    listTblV.frame = [UpdateFrame setSizeForView:listTblV usingHeight:[self calculateHeightOfList]];
    listTblV.hidden = ([list count] == 0);
    noresultsL.hidden = !listTblV.hidden ;
    
    [self tabChanged];
    //[HUD hide:YES];


    
}
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
-(void) viewWillAppear:(BOOL)animated
    {
        [super viewWillAppear:animated];
        
        if (!isBackFromOtherView)
        cachedImageViewSize = mainImgV.frame;
        else
        isBackFromOtherView = NO;
    }
    
#pragma mark - Layout
    
- (void) setupLayout
    {
        pageWidth = CGRectGetWidth(self.view.bounds);
        
        [self.view addGestureRecognizer:scrollviewOnTop.panGestureRecognizer];
        [listTblV removeGestureRecognizer:listTblV.panGestureRecognizer];
        
        //Position elements
        {
            topImageV.frame = [UpdateFrame setSizeForView:topImageV usingHeight:[ResizeToFitContent getHeightForParallaxImage:self.view.frame]];
            
            yscrollOffset = CGRectGetHeight(topImageV.frame) - IMAGE_SCROLL_OFFSET_Y - CGRectGetHeight(optionsV.frame);
            mainImgV.frame = topImageV.frame ;
            
            containerView.frame = [UpdateFrame setSizeForView:containerView usingHeight:(CGRectGetHeight(self.view.bounds) + yscrollOffset)];
            
            tabcontentscrollView.frame= [UpdateFrame setPositionForView:tabcontentscrollView usingPositionY: CGRectGetMaxY(topImageV.frame)];
            tabcontentscrollView.frame = [UpdateFrame setSizeForView:tabcontentscrollView usingHeight: CGRectGetHeight(containerView.frame) - CGRectGetMaxY(topImageV.frame)];
        }
        
        
        CGPoint scrollviewOrigin = scrollviewOnTop.frame.origin;
        scrollviewOnTop.scrollIndicatorInsets = UIEdgeInsetsMake(-scrollviewOrigin.y, 0, scrollviewOrigin.y, scrollviewOrigin.x);
        
        //[self reloadList];
    }
    
//- (void) loadDetailView : (NSArray *) arr{
//    list = arr;
//    
//}

- (void) loadDetailViewNSArray : (NSArray *) arr{
    
    // new implementtion.
    
    // prevent Duplicates.
    NSMutableSet *keysSet = [[NSMutableSet alloc] init];
    NSMutableArray *filteredArray = [[NSMutableArray alloc]init];
    for (NSDictionary *msg in arr) {
        NSDictionary *d = [msg valueForKey:@"StoreDetail"];
        NSString *key = [NSString stringWithFormat:@"%@",[d valueForKey:@"Id"]];
        if (![keysSet containsObject:key]) {
            [filteredArray addObject:msg];
            [keysSet addObject:key];
        }
    }
   // NSLog(@"filteredResults %@ keyset%@",filteredArray , keysSet);

    list = filteredArray;

    // old implemetnation.
    /*
    NSLog(@"SERVICE ARRAY SIZE: %lu",(long)arr.count);
 
    NSMutableArray  *dits = [NSMutableArray new];
    for (NSDictionary *dict in arr) {
        
        if([dits valueForKey:@"StoreDetail"] != NULL){
            [dits addObject:dict];
        }
    }
    
    NSLog(@"%@", dits);
    list = dits;
     */
   
}
    
    
#pragma mark - Methods
/*
- (void) reloadList
    {
        if ([InternetCheck isOnline]){
            
            [self hudShowWithMessage:@"Loading"];
            
            NSMutableArray *replacementArray = [NSMutableArray array];
            NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
            [paramDict setObject:@"CUSTOMER_ID" forKey:@"key"];
            [paramDict setObject:[appD getLoggedInUserId] forKey:@"value"];
            [replacementArray addObject:paramDict];
            
            
            [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_COUPONS Param:nil replacementStrings:replacementArray];
        }
    }
    */
-(CGFloat) calculateHeightOfList
    {
        return (list.count  * ROW_HEIGHT);
    }
    
    
- (void) processResponse : (NSDictionary *) dict
    {
        list = [ReadData arrayFromDictionary:dict forKey:RESPONSEKEY_DATA];
        [listTblV reloadData];
        [listTblV setContentOffset:CGPointZero animated:NO];
        
        contentHeight = [self calculateHeightOfList];
        if(contentHeight <= CGRectGetHeight(tabcontentscrollView.bounds)){
            contentHeight = CGRectGetHeight(tabcontentscrollView.bounds);
            listTblV.scrollEnabled = NO;
        }
        listTblV.frame = [UpdateFrame setSizeForView:listTblV usingHeight:[self calculateHeightOfList]];
        
        listTblV.hidden = ([list count] == 0);
        noresultsL.hidden = !listTblV.hidden ;
        
        [self tabChanged];
        [HUD hide:YES];
    }
    
- (void) tabChanged
    {
        CGPoint oldOffset = scrollviewOnTop.contentOffset;
        isContentSizeUpdated = YES;
        CGSize newsize =  [self setContentSize];
        [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
        scrollviewOnTop.contentSize = newsize;
        if (oldOffset.y >= yscrollOffset)
        {
            CGPoint offset = [self getScrollContentOffset];
            offset.y += yscrollOffset;
            [scrollviewOnTop setContentOffset:offset animated:NO];
        }
        isContentSizeUpdated = NO;
    }
    
    
#pragma mark - HUD & Delegate methods
    
- (void)hudShowWithMessage:(NSString *)message
    {
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:HUD];
        
        HUD.dimBackground = YES;
        HUD.labelText = message;
        
        // Regiser for HUD callbacks so we can remove it from the window at the right time
        HUD.delegate = self;
        
        [HUD show:YES];
    }
    
- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}
    
#pragma mark - Events
    
- (IBAction)backBPressed:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
    
    
#pragma mark - Expand on Scroll animation
    
- (CGSize) setContentSize
    {
        CGFloat height = CGRectGetHeight(mainImgV.frame) + contentHeight;
        return CGSizeMake(pageWidth, height);
    }
    
- (void) setScrollContentOffset: (CGPoint) offset
    {
        listTblV.contentOffset = offset;
    }
    
- (CGPoint) getScrollContentOffset
    {
        return [listTblV contentOffset];
    }
    
- (void) resetOffsetAllTabs
    {
        CGPoint offset = CGPointZero;
        [self setScrollContentOffset : offset];
    }
    
-(void) updateContentSize : (CGPoint) contentOffset;
    {
        CGPoint oldOffset = scrollviewOnTop.contentOffset;
        isContentSizeUpdated = YES;
        CGSize newsize =  [self setContentSize];
        [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
        scrollviewOnTop.contentSize = newsize;
        if (oldOffset.y >= yscrollOffset)
        {
            contentOffset.y += yscrollOffset;
            [scrollviewOnTop setContentOffset:contentOffset animated:NO];
        }
        [scrollviewOnTop setContentOffset:contentOffset animated:NO];
        
        isContentSizeUpdated = NO;
    }
    
#pragma mark - Table view data source

-(CGFloat) calculateheightForRow:(NSInteger)row{
    return ROW_HEIGHT;
}
    
    
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
    
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [list count] ;
}
    
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        return [self calculateheightForRow:indexPath.row];
    }
    
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MyPackageStoreActivityCell *cell = (MyPackageStoreActivityCell *)[tableView dequeueReusableCellWithIdentifier:@"myPackageStoreActivityCell" forIndexPath:indexPath];
    NSDictionary *item = list[indexPath.row];
    
    //cell.titleL.text = [ReadData stringValueFromDictionary:item forKey:@"Name"];
    [cell configureDataForCell :item];
    
       return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    /*
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:nil
                                  message:@"Would you like to buy more services?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* SKIP = [UIAlertAction
                         actionWithTitle:@"SKIP"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                            [self skipAction:indexPath]; // calling method for skip.
                             NSLog(@"Resolving UIAlert Action for tapping OK Button");
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    UIAlertAction* OK = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                [self okAction:indexPath]; // calling method for ok.

                                 NSLog(@"Resolving UIAlertActionController for tapping cancel button");
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:SKIP];
    [alert addAction:OK];
    
    [self presentViewController:alert animated:YES completion:nil];
     */
    
    [self skipAction:indexPath];
}

// For Pinging to store.
-(void) skipAction:(NSIndexPath*)indexPath{
    
    NSDictionary *storesDict = list[indexPath.row];

   //[cartC updateStoreInformationFromPackage:storesDict];
    //cartC.latitude = @"0.0";
   // cartC.longitude = @"0.0";
    cartC.deliveryType = @"0";
    [cartC setLatitude:selLatitude andLongitude:selLongitude];

    // [cartC setLatitude:strLat andLongitude:strLong];
    UIStoryboard *modalStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LocationDateStoreSelectorViewController *LDSSVC = [modalStoryboard instantiateViewControllerWithIdentifier:@"LocationDateStoreSelectorViewController"];
    [self.navigationController pushViewController:LDSSVC animated:YES];
    
    
   
    /*
     
     NSDictionary *storesDict = list[indexPath.row];
     //NSLog(@"%@",storeIDDict);
     //[self sendDelegateEvent:API_DETAIL_VIEW_ORDER];
     //[HUD hide:YES];
     
     [cartC setLatitude:selLatitude andLongitude:selLongitude];
     
     [cartC updateStoreInformationFromPackage:storesDict];
     
     UIStoryboard *modalStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     StoreRateCardViewController *LDSSVC = [modalStoryboard instantiateViewControllerWithIdentifier:@"StoreRateCardViewController"];
     [LDSSVC loadForSalonStore:storesDict Gender:selGenderId andDeliveryType:@"0" :NO];
     
     [self.navigationController pushViewController:LDSSVC animated:YES];
     
*/
    
    
   }

-(void) okAction:(NSIndexPath*)indexPath{
     
     
     NSDictionary *storesDict = list[indexPath.row];
     
     ///
     isFromSearchServicesPackage =NO;
     [cartC updateStoreInformationFromPackage:storesDict];
     //cartC.cartType = CART_TYPE_PACKAGES;
     [cartC setLatitude:selLatitude andLongitude:selLongitude];
     
     UIStoryboard *modalStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     StoreRateCardViewController *LDSSVC = [modalStoryboard instantiateViewControllerWithIdentifier:@"StoreRateCardViewController"];
     [LDSSVC loadForSalonStore:storesDict Gender:selGenderId andDeliveryType:@"0" :YES];
     
     [self.navigationController pushViewController:LDSSVC animated:YES];

  
}


#pragma mark - Scroll Events
    
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
    {
        if (scrollView == scrollviewOnTop)
        {
            {
                if (isContentSizeUpdated)  return;
                
                CGRect scrolledBoundsForContainerView = containerView.bounds;
                if (scrollView.contentOffset.y <= yscrollOffset)
                {
                    
                    scrolledBoundsForContainerView.origin.y = scrollView.contentOffset.y ;
                    containerView.bounds = scrolledBoundsForContainerView;
                    
                    //Reset offset for all tabs
                    if (scrollView.contentOffset.y <= 0) [self resetOffsetAllTabs];
                    
                    
                    CGFloat y = -scrollView.contentOffset.y;
                    CGFloat alphaLevel = 1;
                    CGFloat BLUR_MAX_Y = IMAGE_SCROLL_OFFSET_Y + CGRectGetHeight(topImageV.frame) - CGRectGetMinY(optionsV.frame);
                    if (fabs(y) < BLUR_MAX_Y)
                    {
                        alphaLevel = (BLUR_MAX_Y - fabs(y))/(BLUR_MAX_Y);
                    }
                    else
                    {
                        alphaLevel = 0;
                    }
                    
                    //[screenTitleL setAlpha:alphaLevel];
                    if (y > 0)
                    {
                        mainImgV.frame = CGRectInset(cachedImageViewSize, 0, -y/2);
                        mainImgV.center = CGPointMake(mainImgV.center.x, mainImgV.center.y + y/2);
                    }
                    else
                    {
                        mainImgV.frame = [UpdateFrame setPositionForView:mainImgV usingPositionY:y];
                    }
                    return;
                }
                
                scrolledBoundsForContainerView.origin.y = yscrollOffset ;
                containerView.bounds = scrolledBoundsForContainerView;
                
                [self setScrollContentOffset:CGPointMake(0, scrollView.contentOffset.y - yscrollOffset)  ];
            }
        }
    }
    
//#pragma mark - API
//    
//-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
//    {
//        if(![ReadData isValidResponseData:response])
//        {
//            {
//                NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
//                [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
//                [self.view setUserInteractionEnabled:YES];
//                [HUD hide:YES];
//            }
//            return;
//        }
//        
//        if ([apiName isEqualToString:API_GET_COUPONS]) {
//            [self processResponse:response];
//        }
//    }
//    

@end
