//
//  MyPackageStoreListActivityViewController.h
//  Ziva
//
//  Created by Minnarao on 8/3/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPackageStoreListActivityViewController : UIViewController
    
//- (void) loadDetailView : (NSDictionary *) arr;



- (void) loadDetailViewNSArray : (NSArray *) arr;
    
@end
