//
//  MyPackageStoreActivityCell.m
//  Ziva
//
//  Created by Minnarao on 8/3/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "MyPackageStoreActivityCell.h"

@implementation MyPackageStoreActivityCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    // Fix : Autoresizing issues for iOS 7 using Xcode 6
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //    Fix : For right aligned view with autoresizing
    UIView *backV = [[self.contentView subviews] objectAtIndex:0];
    backV.frame = [UpdateFrame setSizeForView:backV usingSize:self.bounds.size];

}
    
- (void) configureDataForCell : (NSDictionary *) item
    {
        
        
        self.mainImgV.image = Nil;
        self.mainImgV.alpha = 0;
        self.placeholderImgV.alpha=1;
        
        NSDictionary *storedict = [ReadData dictionaryFromDictionary:item forKey:@"StoreDetail"];

        NSDictionary *salonDict = [ReadData dictionaryFromDictionary:storedict forKey:KEY_SALON];
        //NSDictionary *locationDict = [ReadData dictionaryFromDictionary:item forKey: KEY_STORE_LOCATION];
        
        self.titleL.text = [NSString stringWithFormat:@"%@ (%@)", [ReadData stringValueFromDictionary:salonDict forKey:KEY_NAME], [ReadData stringValueFromDictionary:item forKey:@"StoreName"]];
        
        self.subtitleL.text = [ReadData stringValueFromDictionary:storedict forKey:@"Address"];
        
        self.ratingL.text = [NSString stringWithFormat:@"Rating : %@", [ReadData ratingsInPointsFromDictionary:storedict forKey:KEY_RATING]];
        self.distanceL.text = [ReadData distanceInKmFromDictionary:storedict forKey:KEY_DISTANCE];
        
        //    CGFloat tmpHeight = [ResizeToFitContent getHeightForNLines:3 ForText:self.titleL.text usingFontType:FONT_LIGHT FontOfSize:10 andMaxWidth:CGRectGetWidth(self.titleL.frame)];
        //    self.titleL.frame = [UpdateFrame setSizeForView:self.titleL usingHeight: tmpHeight];
        
        // ImageView
        NSString *url = [ReadData stringValueFromDictionary:salonDict forKey:KEY_SALON_LOGOURL];
        if (![url isEqualToString:@""])
        {
            NSURL *imageURL = [NSURL URLWithString:url];
            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageURL];
            [self.mainImgV setImageWithURLRequest:urlRequest
                                 placeholderImage:Nil
                                          success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
             {
                 [self.mainImgV setImage:image];
                 [UIView animateWithDuration:IMAGE_VIEW_TOGGLE
                                       delay:0 options:UIViewAnimationOptionCurveLinear
                                  animations:^(void)
                  {
                      
                      [self.placeholderImgV setAlpha:0 ];
                      [self.mainImgV setAlpha:1 ];
                  }
                                  completion:^(BOOL finished)
                  {
                  }];
                 
                 
                 
             }
                                          failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
             {
                 
             }];
        }
}


@end
