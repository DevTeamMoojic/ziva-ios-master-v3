//
//  MyPackageDetailViewController.m
//  Ziva
//
//  Created by Minnarao on 2/3/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "MyPackageDetailViewController.h"
#import "PackageDetailCell.h"
#define ROW_HEIGHT 120
#import "MyPackageStoreListActivityViewController.h"

@interface MyPackageDetailViewController ()<MBProgressHUDDelegate,UITableViewDelegate,UITableViewDataSource>{
    
    MBProgressHUD *HUD;
    NSMutableArray *mypackageDetailsArray;
    
    __weak IBOutlet UIView *optionsV;
    __weak IBOutlet UILabel *screenTitleL;
    
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIView *topImageV;
    __weak IBOutlet UIImageView *mainImgV;
    
    __weak IBOutlet UIScrollView *scrollviewOnTop;
    __weak IBOutlet UIScrollView *tabcontentscrollView;
    
    __weak IBOutlet UITableView *listTblV;
    __weak IBOutlet UILabel *noresultsL;
    
    __weak IBOutlet UIView *backV;
    __weak IBOutlet UIView *headerV;

    
    NSString *selectedGenderId;
    
    CGRect cachedImageViewSize;
    CGFloat yscrollOffset;
    
    BOOL isBackFromOtherView;
    BOOL isContentSizeUpdated;
    
    NSInteger pageWidth;
    
    AppDelegate *appD;
    
    CGFloat contentHeight;
    NSDictionary *datadict;
     __weak IBOutlet UIView *genderselectorCV;
    
    NSMutableArray *arrayL;
    NSMutableArray *storeListActivityArry;
    NSMutableArray *cellCheckArray;

    BOOL ischeck;
    NSIndexPath *lastindex ;
    NSDictionary *storeListDict;
    MyCartController *cartC;
    
    NSMutableDictionary *storeListCopyDict;
    
}

@end

@implementation MyPackageDetailViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;
    [self setupLayout];
    
     arrayL = [[NSMutableArray alloc]init];
    storeListActivityArry = [[NSMutableArray alloc]init];
    cellCheckArray = [[NSMutableArray alloc]init];
    storeListDict = [[NSDictionary alloc]init];
    /*
    ///
    NSMutableArray *pkBArr = [NSMutableArray new];
    NSMutableDictionary *pkDict = [[NSMutableDictionary alloc]init];
    
   
    for (NSDictionary *d in datadict) {
        NSInteger pkBID = [[ReadData stringValueFromDictionary:d forKey:KEY_ID] integerValue];
        
        if (pkBID == [[ReadData stringValueFromDictionary:datadict forKey:KEY_ID] integerValue]) {
            
            [pkBArr addObject:d];
            [pkDict setValue:pkBArr forKeyPath:@"id"];
        }
        NSLog(@"%@", pkDict);
        NSLog(@"%@", pkDict.allKeys);
        
    }
    
  //////*/
    
    
    //[mypackageDetailsArray removeAllObjects];
    NSArray * packagePundelArr = [datadict valueForKey:@"PackageBundles"];
    NSLog(@"%ld",(long) packagePundelArr.count);
    
    
    // By Default Adding check Mark.
    for (int i = 0; i< packagePundelArr.count; i++) {
        NSMutableDictionary *markDict = [packagePundelArr objectAtIndex:i];
        [markDict setObject:@"0" forKey:@"Mark"];
        NSLog(@"%@", markDict);

    }
    
    
  
    mypackageDetailsArray = [packagePundelArr mutableCopy];
    
    if ([mypackageDetailsArray count] > 0) {
        noresultsL.hidden = YES;
        listTblV.hidden = NO;
        contentHeight = [self calculateHeightOfList];
        
        [self initZeroLabel];
        if(contentHeight <= CGRectGetHeight(tabcontentscrollView.bounds)){
            contentHeight = CGRectGetHeight(tabcontentscrollView.bounds);
            listTblV.scrollEnabled = NO;
        }
        
        listTblV.frame = [UpdateFrame setSizeForView:listTblV usingHeight:[self calculateHeightOfList]];
        [self tabChanged];
        
        listTblV.delegate = self;
        listTblV.dataSource = self;
        [listTblV reloadData];
        headerV.hidden = NO;
    }
    else{
        noresultsL.hidden = NO;
        listTblV.hidden = YES;
        headerV.hidden = YES;
    
    }
  

 }

// Making lables according to customeCell.
-(void) initZeroLabel{
    for (int i= 0 ; i < mypackageDetailsArray.count; i++) {
        [arrayL addObject:[NSNumber numberWithInteger:0]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!isBackFromOtherView)
        cachedImageViewSize = mainImgV.frame;
    else{
        isBackFromOtherView = NO;
    }
}
- (void) loadDetailView : (NSDictionary *) dict{
    datadict = dict;
    
    //NSLog(@"%@", [dataDict allKeys]);
//    
//    [mypackageDetailsArray removeAllObjects];
//   
//    
//    NSArray * packagePundelArr = [dict valueForKey:@"PackageBundles"];
//    NSLog(@"%ld",(long) packagePundelArr.count);
//    
//    NSArray * serviceArray = [packagePundelArr valueForKey:@"Services"];
//    NSLog(@"%ld", (long)serviceArray.count);
//    
//    NSArray * productArray = [packagePundelArr valueForKey:@"Products"];
//    NSLog(@"%ld", (long)productArray.count);

    
    
//    if ( [packagePundelArr count] != 0) {
//        
//        NSArray * serviceArray = [packagePundelArr valueForKey:@"Services"];
//        NSLog(@"%ld", (long)serviceArray.count);
//        
//        NSArray * productArray = [packagePundelArr valueForKey:@"Products"];
//        NSLog(@"%ld", (long)productArray.count);
//        
//        
//        NSArray *fSP = [serviceArray arrayByAddingObjectsFromArray:productArray];
//        
//        NSLog(@"%ld", (long)fSP.count);
//        NSLog(@"%@", fSP);
//        
//    }
    
      //NSLog(@"%@", bundelArr);s
    
//    mypackageDetailsArray = [packagePundelArr mutableCopy];
//
//  
//    
//    noresultsL.hidden = ([mypackageDetailsArray count] > 0);
//    listTblV.hidden = ([mypackageDetailsArray count] == 0);
//    
//    contentHeight = [self calculateHeightOfList];
//    
//    if(contentHeight <= CGRectGetHeight(tabcontentscrollView.bounds)){
//        contentHeight = CGRectGetHeight(tabcontentscrollView.bounds);
//        listTblV.scrollEnabled = NO;
//    }
//    listTblV.frame = [UpdateFrame setSizeForView:listTblV usingHeight:[self calculateHeightOfList]];
//    [self tabChanged];
//    
//    listTblV.delegate = self;
//    listTblV.dataSource = self;
//    [listTblV reloadData];
//

}


//- (void) showLook_DetailView : (NSDictionary *) item{
//    [self performSegueWithIdentifier:@"showlookdetail" sender:item];
//}

#pragma mark - Methods

- (CGFloat) calculateImageHeight
{
    CGFloat designScreenHeight = 667;
    CGFloat designHeight = 230;
    
    CGFloat translatedHeight = ceil(designHeight * CGRectGetHeight(self.view.frame)/designScreenHeight);
    
    return (int)translatedHeight;
}


- (void) setupLayout
{
    mypackageDetailsArray = [NSMutableArray array];
    
    //selectedGenderId = [appD getLoggedInUserGender];
    
    //[genderselectorVC setGender:selectedGenderId];
    
    pageWidth = CGRectGetWidth(self.view.bounds);
    
    [self.view addGestureRecognizer:scrollviewOnTop.panGestureRecognizer];
    [listTblV removeGestureRecognizer:listTblV.panGestureRecognizer];
    
    //Position elements
    {
        
        topImageV.frame = [UpdateFrame setSizeForView:topImageV usingHeight:[ResizeToFitContent getHeightForParallaxImage:self.view.frame]];
        
        yscrollOffset = CGRectGetHeight(topImageV.frame) - IMAGE_SCROLL_OFFSET_Y - CGRectGetHeight(optionsV.frame);
        mainImgV.frame = topImageV.frame ;
        
        containerView.frame = [UpdateFrame setSizeForView:containerView usingHeight:(CGRectGetHeight(self.view.bounds) + yscrollOffset)];
        
               genderselectorCV.frame = [UpdateFrame setPositionForView:genderselectorCV usingPositionY:CGRectGetMaxY(topImageV.frame)];
        //
               tabcontentscrollView.frame= [UpdateFrame setPositionForView:tabcontentscrollView usingPositionY: CGRectGetMaxY(genderselectorCV.frame)];
                tabcontentscrollView.frame = [UpdateFrame setSizeForView:tabcontentscrollView usingHeight: CGRectGetHeight(containerView.frame) - CGRectGetMaxY(genderselectorCV.frame)];
    }
    
    CGPoint scrollviewOrigin = scrollviewOnTop.frame.origin;
    scrollviewOnTop.scrollIndicatorInsets = UIEdgeInsetsMake(-scrollviewOrigin.y, 0, scrollviewOrigin.y, scrollviewOrigin.x);
    
  
}

-(CGFloat) calculateHeightOfList
{
    return ([mypackageDetailsArray count]  * ROW_HEIGHT + 150);
}

- (void) tabChanged
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        CGPoint offset = [self getScrollContentOffset];
        offset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:offset animated:NO];
    }
    isContentSizeUpdated = NO;
}


#pragma mark - HUD & Delegate methods

- (void)hudShowWithMessage:(NSString *)message
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    HUD.dimBackground = YES;
    HUD.labelText = message;
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    [HUD show:YES];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}


#pragma mark - Expand on Scroll animation

- (CGSize) setContentSize
{
    CGFloat height = CGRectGetHeight(mainImgV.frame) + CGRectGetHeight(genderselectorCV.frame) + contentHeight +60;
   // CGFloat height = CGRectGetHeight(mainImgV.frame) + contentHeight;
    
    return CGSizeMake(pageWidth, height);
}

- (void) setScrollContentOffset: (CGPoint) offset
{
    listTblV.contentOffset = offset;
}

- (CGPoint) getScrollContentOffset
{
    return [listTblV contentOffset];
}

- (void) resetOffsetAllTabs
{
    CGPoint offset = CGPointZero;
    [self setScrollContentOffset : offset];
}

-(void) updateContentSize : (CGPoint) contentOffset;
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        contentOffset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    }
    [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    
    isContentSizeUpdated = NO;
}



#pragma mark - Table view data source

-(CGFloat) calculateheightForRow:(NSInteger)row{
    return ROW_HEIGHT;
}


#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 150.0f;
    
}


// custom view for header. will be adjusted to default or specified header height
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,CGRectGetWidth(tableView.frame), 150)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    
    // condition Apply
    // Package bundel title
    UILabel *conditionL = [[UILabel alloc]initWithFrame:CGRectMake(tableView.frame.size.width-120, 8, 100, 20)];
    conditionL.text = @"Conditions Apply";
    conditionL.textColor = [ UIColor lightGrayColor];
    //conditionL.backgroundColor = [UIColor redColor];
    conditionL.font = [UIFont regularFontOfSize:12.0f];
    
    // Package bundel title
    UILabel *packageNameL = [[UILabel alloc]initWithFrame:CGRectMake(10, 28, 250, 20)];
    packageNameL.text = [NSString stringWithFormat:@"%@",[ReadData stringValueFromDictionary:datadict forKey:KEY_CTXN_TITLE]];
    // packageNameL.backgroundColor = [UIColor redColor];
    packageNameL.font = [UIFont boldFontOfSize:14.0f];
    
    /*
     // Package cash back
     UILabel *packageCashBackL = [[UILabel alloc]initWithFrame:CGRectMake(10, 48, 250, 20)];
     packageCashBackL.backgroundColor = [UIColor redColor];
     packageCashBackL.font = [UIFont regularFontOfSize:12.0f];*/
    
    // Package StartingPrice.
    UILabel *packageStartingPriceL = [[UILabel alloc]initWithFrame:CGRectMake(10, 50, 250, 20)];
    //packageStartingPriceL.backgroundColor = [UIColor redColor];
    
    //packageStartingPriceL.text =[NSString stringWithFormat:@"Starting @ %@", [ReadData getAmountFormatted:[[ReadData stringValueFromDictionary:dataDict forKey:KEY_PACKAGE_STARTINGPRCE] floatValue]]];
    packageStartingPriceL.text =[ReadData getAmountFormatted:[[ReadData stringValueFromDictionary:datadict forKey:KEY_PACKAGE_STARTINGPRCE] floatValue]];
    packageStartingPriceL.font = [UIFont regularFontOfSize:12.0f];
    packageStartingPriceL.textColor = [UIColor redColor];
    
    // [ReadData stringValueFromDictionary:dataDict forKey:KEY_PACKAGE_STARTINGPRCE];
    
    // line.
    UIView *LineV = [[UIView alloc] initWithFrame:CGRectMake(10,80,CGRectGetWidth(tableView.frame)-20, 1)];
    LineV.backgroundColor = [UIColor lightGrayColor];
    
    // Package DescriptionTitleL.
    UILabel *packageDescriptionTitleL = [[UILabel alloc]initWithFrame:CGRectMake(10, 90, 250, 20)];
    packageDescriptionTitleL.text = @"Description";
    packageDescriptionTitleL.textColor = [UIColor redColor];
    packageDescriptionTitleL.font = [UIFont boldFontOfSize:14.0f];
    
    UIView *underline = [[UIView alloc] initWithFrame:CGRectMake(10,110,70, 2)];
    underline.backgroundColor = [UIColor redColor];
    
    
    //    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    //    packageDescriptionTitleL.attributedText = [[NSAttributedString alloc] initWithString:packageDescriptionTitleL.text attributes:underlineAttribute];
    
    /*
     NSAttributedString * title =   [[NSAttributedString alloc] initWithString:@"Desctiption"
     attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle)}];
     [packageDescriptionTitleL setAttributedText:title];
     */
    
    
    //    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:@"Description"];
    //    [attributeString addAttribute:NSUnderlineStyleAttributeName
    //                            value:[NSNumber numberWithInt:1]
    //                            range:(NSRange){0,[attributeString length]}];
    //    [packageDescriptionTitleL setAttributedText:attributeString];
    //packageDescriptionTitleL.attributedText = attributeString;
    //
    // Package Description downe titleL.
    UILabel *packageDescriptionL = [[UILabel alloc]initWithFrame:CGRectMake(10, 110, 250, 30)];
    packageDescriptionL.textColor = [UIColor lightGrayColor];
    packageDescriptionL.font = [UIFont regularFontOfSize:12.0f];
    packageDescriptionL.text = [ReadData stringValueFromDictionary:datadict forKey:KEY_CTXN_TITLE];
    
    
    /*
     // cash back
     NSArray *cashbacks = [ReadData arrayFromDictionary:dataDict forKey:KEY_CASHBACKS];
     //CGFloat positionY = (CGRectGetHeight(self.priceInfoV.frame) - CGRectGetHeight(self.offerpriceL.frame)) *0.5;
     
     // if the looks is cash back else to discount values.
     if([cashbacks count] > 0){
     
     // diffrenciate the amount and percent.
     NSArray *cashbackArray = [[ReadData arrayFromDictionary:dataDict forKey:KEY_CASHBACKS] firstObject];
     //NSLog(@"%@", cashbackArray);
     NSInteger priceMode = [[cashbackArray valueForKey:@"Mode"] integerValue];
     // NSLog(@"%ld", (long)priceMode);
     // NSString *priceMode1 = [cashbackArray valueForKey:@"Cost"];
     //NSLog(@"%@", priceMode1);
     
     // if the it 0 the values is cash eles the percentage.
     if (priceMode == 0) {
     packageCashBackL.text = [NSString stringWithFormat:@"Get %@ Cash Back.",[ReadData getAmountFormatted:[[cashbackArray valueForKey:@"Cost"] floatValue]]];
     }else{
     NSString *precent = @"%";
     packageCashBackL.text = [NSString stringWithFormat:@"Get %@%@ Cash Back",[cashbackArray valueForKey:@"Cost"],precent];
     }
     
     }
     else{ // cash back
     
     
     }
     
     
     
     
     // Diffrencitate the cash.
     if ([cashbacks count] > 0 && ![ReadData stringValueFromDictionary:dataDict forKey:KEY_PACKAGE_STARTINGPRCE]) {
     packageCashBackL.hidden = NO;
     packageStartingPriceL.hidden = NO;
     //self.startPriceL.frame = self.startPriceL.frame;
     
     }else if ([ReadData stringValueFromDictionary:dataDict forKey:KEY_PACKAGE_STARTINGPRCE]){
     packageStartingPriceL.hidden = NO;
     packageCashBackL.hidden = YES;
     //self.startPriceL.frame = self.cashBackPriceL.frame;
     //CGRectMake(self.cashBackPriceL.frame.origin.x, self.cashBackPriceL.frame.origin.y, <#CGFloat width#>, <#CGFloat height#>)
     }
     
     //    }else if ([ReadData stringValueFromDictionary:dataDict forKey:KEY_PACKAGE_STARTINGPRCE]){
     //        self.startPriceL.hidden = YES;
     //        self.cashBackPriceL.hidden = NO;
     //    }
     */
    
    [headerView addSubview: conditionL];
    [headerView addSubview:packageNameL];
    [headerView addSubview:underline];
    [headerView addSubview:packageStartingPriceL];
    [headerView addSubview:LineV];
    [headerView addSubview:packageDescriptionTitleL];
    [headerView addSubview:packageDescriptionL];
    return headerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [mypackageDetailsArray count] ;
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self calculateheightForRow:indexPath.row];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    PackageDetailCell *cell = (PackageDetailCell *)[tableView dequeueReusableCellWithIdentifier:@"myPackageDetailCell" forIndexPath:indexPath];
    NSDictionary *dict = mypackageDetailsArray[indexPath.row];
    
    
    
    // Minaus Button
    cell.minusB.tag = indexPath.row;
    //[cell.minusB addTarget:self action:@selector(minusBAction:) forControlEvents:UIControlEventTouchUpInside];
    
    // Plus Button
    cell.plusB.tag = indexPath.row;
    //[cell.plusB addTarget:self action:@selector(plusBAction:) forControlEvents:UIControlEventTouchUpInside];

    //cell.countL.text = prodcutCountL.text;
    cell.countL.text = [NSString stringWithFormat:@"%i",[[arrayL objectAtIndex:indexPath.row] intValue]];
    

   [cell configureMyPackageDetailDataForCell:dict];
    
    // This block of code for check mark image hide and unhide.
    if ([[dict valueForKey:@"Mark"] isEqualToString:@"0"])
    {
        [cell.plusB setImage:[UIImage imageNamed:@"tick_notselected"] forState:UIControlStateNormal];
        //cell.plusB.hidden  = YES;
    }
    else
    {
        [cell.plusB setImage:[UIImage imageNamed:@"tick_selected"] forState:UIControlStateNormal];

        //cell.plusB.hidden  = NO;

    }

    
    
    
   // cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
      //if (ischeck) {
     //cell.accessoryType = UITableViewCellAccessoryCheckmark;
     // }else{
   // cell.accessoryType = UITableViewCellAccessoryNone;
    //  }
    //lastindex = indexPath;
    
  
    return cell;

}

    
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSMutableDictionary *selectDict = [mypackageDetailsArray objectAtIndex:[indexPath row]];
    
    if ([[selectDict valueForKey:@"Mark"] isEqualToString:@"0"]) {
        for (int i = 0;  i <  [mypackageDetailsArray count];  i++ )
        {
            NSMutableDictionary *dictTempUsers = [mypackageDetailsArray objectAtIndex:i];
            [dictTempUsers setValue:@"0" forKey:@"Mark"];
        }
        [selectDict setValue:@"1" forKey:@"Mark"];
        
        // dictonary.
        //storeListDict = [[NSDictionary alloc]init];
        storeListDict = mypackageDetailsArray[indexPath.row];
        [cartC addMyPackages:storeListDict];
        storeListCopyDict = [[storeListDict copy] mutableCopy];
       // NSLog(@"%@storeListCopyDict is  ", storeListCopyDict);
        
    }else{
        
        NSLog(@"%@", storeListCopyDict);
        [storeListCopyDict removeAllObjects];
        [selectDict setValue:@"0" forKey:@"Mark"];
    }
    
    [listTblV reloadData];
   
    /*
    //lastindex = indexPath;
    
    [storeListActivityArry removeAllObjects];
    NSInteger newRow = [indexPath row];
    NSInteger oldRow = [lastindex row];
    
    if (newRow != oldRow )
    {
        UITableViewCell *newCell = [tableView cellForRowAtIndexPath:
                                    indexPath];
     
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
        
        UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:
                                    lastindex];
        oldCell.accessoryType = UITableViewCellAccessoryNone;
        
        lastindex = indexPath;
    }else if (newRow == oldRow){
        UITableViewCell *newCell = [tableView cellForRowAtIndexPath:
                                    indexPath];
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
        
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    */
    
    //NSDictionary *dict = mypackageDetailsArray[indexPath.row];
   // [storeListActivityArry addObject:dict];
  
    
        /*
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (cell.accessoryType == UITableViewCellAccessoryNone) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }*/
    //isBackFromOtherView = YES;
    //NSDictionary *item = mypackageDetailsArray[indexPath.row];
    
   // [self showPackageBudle_DetailView:item];
   /*
    UITableViewCell *mySelectedCell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (mySelectedCell.isSelected) {
        ischeck = YES;
        
    }else{
         ischeck = NO;
    }
    
      if (mySelectedCell.accessoryType == UITableViewCellAccessoryNone) {
        mySelectedCell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else
        mySelectedCell.accessoryType = UITableViewCellAccessoryNone;

  */
 
    /*
    // For multiple selection.
    NSLog(@"%ld", (long) indexPath.row);
    
    if ([cellCheckArray containsObject:[mypackageDetailsArray objectAtIndex:indexPath.row]]) {
        [cellCheckArray removeObject:[mypackageDetailsArray objectAtIndex:indexPath.row]];
        
    }else{
        [cellCheckArray addObject:[mypackageDetailsArray objectAtIndex:indexPath.row]];
    }
    
    [listTblV reloadData];*/
}
    /*
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_checkedCell == indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}
    
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _checkedCell = indexPath.row;
    [self.tableView reloadData];
}
*/
    
// Called, when user click on Plus button on table cell.
-(void) plusBAction:(UIButton*)sender{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:listTblV];
    NSIndexPath *indexPath = [listTblV indexPathForRowAtPoint:buttonPosition];
    
    NSInteger newRow = [indexPath row];
    NSInteger oldRow = [lastindex row];
    
    if (newRow != oldRow )
    {
        UITableViewCell *newCell = [listTblV cellForRowAtIndexPath:
                                    indexPath];
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
        
        UITableViewCell *oldCell = [listTblV cellForRowAtIndexPath:
                                    lastindex];
        oldCell.accessoryType = UITableViewCellAccessoryNone;
        
        lastindex = indexPath;
    }else if (newRow == oldRow){
        UITableViewCell *newCell = [listTblV cellForRowAtIndexPath:
                                    indexPath];
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
        
    }
    
    [listTblV deselectRowAtIndexPath:indexPath animated:YES];
    

    
    /*
    
    //new implementation
    NSLog(@"%ld", (long) sender.tag);
    
        if ([cellCheckArray containsObject:[mypackageDetailsArray objectAtIndex:sender.tag]]) {
            [cellCheckArray removeObject:[mypackageDetailsArray objectAtIndex:sender.tag]];
            
        }else{
            [cellCheckArray addObject:[mypackageDetailsArray objectAtIndex:sender.tag]];
        }
        
        [listTblV reloadData];

    
   
    ///////////////////
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:listTblV];
    NSIndexPath *indexPath = [listTblV indexPathForRowAtPoint:buttonPosition];
    
    //NSLog(@"%d",[[arrayL objectAtIndex:indexPath.row] intValue]);
    // NSLog(@"quantity = %ld",(long)quantity);
    
    // Store the array.
    NSDictionary *dict = mypackageDetailsArray[indexPath.row];
    
    [self addStoreDetails:dict];
    //[storeListActivityArry addObject:dict];

    /*
    // For the UIlable count will not go down.
    if ([[arrayL objectAtIndex:indexPath.row] intValue] >= 0) {
        
        if ([[arrayL objectAtIndex:indexPath.row] intValue] < 1) {
            [arrayL replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithInteger:[[arrayL objectAtIndex:indexPath.row] intValue] + 1]];
            [listTblV reloadData];
            
            //NSLog(@"quantity = %ld",(long)quantity);
           /*
           NSDictionary *PkgDictP = mypackageDetailsArray[indexPath.row];
            if(quantity < MAX_REPEAT_QUANTITY){
                [cartC addPackages:PkgDictP];
                [cartsummaryVC updateSummary];
                quantity++;
                [self updateQuantityHeading];
            }
            */
        //}
        
        /*
        //NSLog(@"%@", PkgDictP);
        // Reset the table size when the Product count is increase by one.
        if (quantity == 1) {
            listTblV.frame = CGRectMake(listTblV.frame.origin.x,listTblV.frame.origin.y, listTblV.frame.size.width, listTblV.frame.size.height -80);
        }*/
    //}*/
    
    ///////////////////////////
}
    
    
// Called, when user clicks on the Minaus buttom from table view cell.
-(void) minusBAction:(UIButton*)sender{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:listTblV];
    NSIndexPath *indexPath = [listTblV indexPathForRowAtPoint:buttonPosition];
    //NSLog(@"%d",[[arrayL objectAtIndex:indexPath.row] intValue]);
    
    // Store the array.
    NSDictionary *dict = mypackageDetailsArray[indexPath.row];
    [storeListActivityArry addObject:dict];
    
    // For the UIlable count will not go down.
    if ([[arrayL objectAtIndex:indexPath.row] intValue] > 0) {
        [arrayL replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithInteger:[[arrayL objectAtIndex:indexPath.row] intValue] - 1]];
        [listTblV reloadData];
        
       /*
        //NSLog(@"%@", PkgDictM);
        //NSLog(@"quantity = %ld",(long)quantity);
        NSDictionary *PkgDictM = packageBundelArr[indexPath.row];
        // decrease the package count.
        if(quantity > 0){
            
            [cartC lessPackages:PkgDictM];
            [cartsummaryVC updateSummary];
            quantity--;
            [self updateQuantityHeading];
        }
       
         // Reset the table size when the Product count is zero.
         if ([[arrayL objectAtIndex:indexPath.row] intValue] == 0) {
         listTblV.frame = CGRectMake(listTblV.frame.origin.x,listTblV.frame.origin.y, listTblV.frame.size.width, height);
         }
        if (quantity == 0) {
            listTblV.frame = CGRectMake(listTblV.frame.origin.x,listTblV.frame.origin.y, listTblV.frame.size.width, listTblV.frame.size.height +80);
        }*/
        
    }else{
        
        
    }
}


//- (void) showPackageBudle_DetailView : (NSDictionary *) item{
//    [self performSegueWithIdentifier:@"show_packageDetatils" sender:item];
//}

    // user click on proceed button.
-(IBAction)proceedClick:(id)sender{
    
    /*
    NSLog(@"%@",storeListActivityArry);
    if (storeListActivityArry.count>0) {
        MyPackageStoreListActivityViewController *myPackageStoreActivityVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MyPackageStoreListActivityViewController"];
        //[myPackageStoreActivityVC loadDetailView:storeListActivityArry];
        [self.navigationController pushViewController:myPackageStoreActivityVC animated:YES];
    }else{
        UIAlertView *alert = [[ UIAlertView alloc]initWithTitle:nil message:@"Please select any one package" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
  */
    
    //NSLog(@"%@", storeListDict);
    NSLog(@"%@", storeListCopyDict);
    NSLog(@"%@", [storeListCopyDict allKeys]);

     NSLog(@"%lu", (unsigned long)storeListDict.count);
    if (storeListCopyDict.count > 0) {
        
        NSArray * serviceArr = [ReadData arrayFromDictionary:storeListCopyDict forKey:@"Services"];
        
        if (serviceArr.count>0) {
            MyPackageStoreListActivityViewController *myPackageStoreActivityVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MyPackageStoreListActivityViewController"];
            [myPackageStoreActivityVC loadDetailViewNSArray:serviceArr];
            [self.navigationController pushViewController:myPackageStoreActivityVC animated:YES];
        }else
        {
            NSString * str = [ReadData stringValueFromDictionary:storeListDict forKey:@"BundleDependency"];
            
            if ([str isEqualToString:@"CIT"] || [str isEqualToString:@"LOC"] ) // NEARY BY
            {
            } else if ([str isEqualToString:@"STORE"] )  // REDIRECT TO STORE RATE CARD.
            {
            } else if ([str isEqualToString:@"SAL"]) //  DISPLAY THE SALOON DETAILS.
            {
            }

        }
        
        
       /*MyPackageStoreListActivityViewController *myPackageStoreActivityVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MyPackageStoreListActivityViewController"];
        [self.navigationController pushViewController:myPackageStoreActivityVC animated:YES];*/

        
    }else{
        UIAlertView *alert = [[ UIAlertView alloc]initWithTitle:nil message:@"Please select any one package" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
      
    }
    
}

#pragma mark - Scroll Events

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == scrollviewOnTop)
    {
        {
            if (isContentSizeUpdated)  return;
            
            CGRect scrolledBoundsForContainerView = containerView.bounds;
            if (scrollView.contentOffset.y <= yscrollOffset)
            {
                
                scrolledBoundsForContainerView.origin.y = scrollView.contentOffset.y ;
                containerView.bounds = scrolledBoundsForContainerView;
                
                //Reset offset for all tabs
                if (scrollView.contentOffset.y <= 0) [self resetOffsetAllTabs];
                
                
                CGFloat y = -scrollView.contentOffset.y;
                CGFloat alphaLevel = 1;
                CGFloat BLUR_MAX_Y = IMAGE_SCROLL_OFFSET_Y + CGRectGetHeight(topImageV.frame) - CGRectGetMinY(optionsV.frame);
                if (fabs(y) < BLUR_MAX_Y)
                {
                    alphaLevel = (BLUR_MAX_Y - fabs(y))/(BLUR_MAX_Y);
                }
                else
                {
                    alphaLevel = 0;
                }
                
                //[screenTitleL setAlpha:alphaLevel];
                if (y > 0)
                {
                    mainImgV.frame = CGRectInset(cachedImageViewSize, 0, -y/2);
                    mainImgV.center = CGPointMake(mainImgV.center.x, mainImgV.center.y + y/2);
                }
                else
                {
                    mainImgV.frame = [UpdateFrame setPositionForView:mainImgV usingPositionY:y];
                }
                return;
            }
            
            scrolledBoundsForContainerView.origin.y = yscrollOffset ;
            containerView.bounds = scrolledBoundsForContainerView;
            
            [self setScrollContentOffset:CGPointMake(0, scrollView.contentOffset.y - yscrollOffset)  ];
        }
    }
}

#pragma mark - Events

- (IBAction)backBPressed:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ////    //showgenderselector_services
    ////    if([segue.identifier isEqualToString:@"showgenderselector_packag"]){
    ////        genderselectorVC = (GenderSelectorViewController *)segue.destinationViewController;
    ////        genderselectorVC.delegate = self;
    ////    }
    ////    else
    //    if([segue.identifier isEqualToString:@"show_packageDetatils"]){
    //
    //        isBackFromOtherView = YES;
    //        PackageDetailViewController *detailVC = (PackageDetailViewController *)segue.destinationViewController;
    //        [detailVC loadDetailView:sender];
    //    }
}


@end


