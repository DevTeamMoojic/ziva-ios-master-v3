//
//  MyPackageDetailViewController.h
//  Ziva
//
//  Created by Minnarao on 2/3/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPackageDetailViewController : UIViewController

- (void) loadDetailView : (NSDictionary *) dict;

@end
