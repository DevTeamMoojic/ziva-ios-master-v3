//
//  MyPackageCell.h
//  Ziva
//
//  Created by Minnarao on 2/3/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPackageCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *mainV;

@property (weak, nonatomic) IBOutlet UIView *imageCV;
@property (weak, nonatomic) IBOutlet UIImageView *mainImgV;
@property (weak, nonatomic) IBOutlet UIImageView *placeholderImgV;

@property (weak, nonatomic) IBOutlet UIView *infoCV;

@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *startPriceL;
@property (weak, nonatomic) IBOutlet UILabel *cashBackPriceL;

// Fo details

@property (weak, nonatomic) IBOutlet UILabel *pkgBNameL;
@property (weak, nonatomic) IBOutlet UILabel *pkgBPriceL;
@property (weak, nonatomic) IBOutlet UILabel *pkgBSubTitleL;


- (void) configureDataForCell : (NSDictionary *) item;
- (void) configureDetailDataForCell : (NSDictionary *) item;

@end
