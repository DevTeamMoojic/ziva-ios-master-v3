//
//  ServiceRatingViewController.h
//  Ziva
//
//  Created by Minnarao on 13/4/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>


// Proto call implementation.

@protocol ServiceRatingDelegate <NSObject>
-(void)serviceRatingSubmit;
-(void)serviceRatingCancel;


@end


@interface ServiceRatingViewController : UIViewController
@property (nonatomic, weak) id<ServiceRatingDelegate> delegate;

// action for submit.
-(IBAction)ServiceSubmitAction:(id)sender;

// action for cancel
-(IBAction)ServiceCancelAction:(id)sender;

// load the rating service details.
- (void) loadServiceRatingDetails : (NSDictionary *) dict;

@end
