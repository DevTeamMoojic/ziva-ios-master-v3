//
//  ServiceRatingViewController.m
//  Ziva
//
//  Created by Minnarao on 13/4/17.
//  Copyright © 2017 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "ServiceRatingViewController.h"
#import "SplashViewController.h"

@interface ServiceRatingViewController ()<APIDelegate,MBProgressHUDDelegate,UITextViewDelegate>
{
    int rateCounting;
    AppDelegate *appD;
    MBProgressHUD *HUD;
    NSDictionary *serviceRatingDict;
    
    // Out lets for displaying elements.
    __weak IBOutlet UILabel *payableAmounL;
    __weak IBOutlet UILabel *receiptNOStrL;
    __weak IBOutlet UILabel *dateStL;
    __weak IBOutlet UILabel *dateTimeStL;
    __weak IBOutlet UILabel *serviceStL;
    __weak IBOutlet UITextView *commentTXTV;

    // button outlets
    __weak IBOutlet UIButton *star1B;
    __weak IBOutlet UIButton *star2B;
    __weak IBOutlet UIButton *star3B;
    __weak IBOutlet UIButton *star4B;
    __weak IBOutlet UIButton *star5B;
    
    // view out lets.
    __weak IBOutlet UIView *happyServiceV;
    __weak IBOutlet UIView *ratingV;
  
}


@end

@implementation ServiceRatingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    rateCounting = 0;
    
    //App delegate object passing.
    appD = (AppDelegate*)[[UIApplication sharedApplication] delegate];
     NSLog(@"serviceRatingDict ******** %@", serviceRatingDict);
    [self setUpData];
}

// load the rating service details.
- (void) loadServiceRatingDetails : (NSDictionary *) dict{
    //serviceRatingDict = [[NSDictionary alloc]init];
    serviceRatingDict = dict;
    NSLog(@"serviceRatingDict %@", serviceRatingDict);
 
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Fill the data for service Ratting .
-(void) setUpData{
    //cornours
    [[commentTXTV layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [[commentTXTV layer] setBorderWidth:0.5];
    [[commentTXTV layer] setCornerRadius:10];

    
    
    payableAmounL.text = [ReadData amountInRsFromDictionary:serviceRatingDict forKey:@"PayableAmount"];
    receiptNOStrL.text = [NSString stringWithFormat:@"Receipt No :%@",[ReadData stringValueFromDictionary:serviceRatingDict forKey:@"Id"]];
    //dateStL.text = [ReadData stringValueFromDictionary:serviceRatingDict forKey:@"BookDate"];
    //dateTimeStL.text = [ReadData stringValueFromDictionary:serviceRatingDict forKey:@"BookDate"];
    
    NSLog(@"serviceRatingDict %@", serviceRatingDict);

    //Date formate.
    //NSString *dateStr = [ReadData stringValueFromDictionary:serviceRatingDict forKey:@"BookDate"];
    //NSLog(@"dateStr %@", dateStr);
    ///
    
    
    
    //Date formate.
    NSString *dateStr = [ReadData stringValueFromDictionary:serviceRatingDict forKey:@"BookDate"];
    NSString *FNSTR = [dateStr stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    NSLog(@"FNSTR %@",FNSTR);

    //Set formate.
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:FORMAT_DATE_TIME_MILLISECONDS];
    NSDate *date  = [dateFormatter dateFromString:FNSTR];
    NSLog(@"date  %@",date);

    
    // Convert to new Date Format.
    [dateFormatter setDateFormat:@"MMMM d yyyy"];
    NSString *newDateStr = [dateFormatter stringFromDate:date];
    NSLog(@"%@",newDateStr);
    dateStL.text = newDateStr;
    
    
    // Time string.
        //Set formate.
    
    // Convert to new Date Format.
    [dateFormatter setDateFormat:FORMAT_TIME_12];
    NSString *newDateStr1 = [NSString stringWithFormat:@"@ %@",[dateFormatter stringFromDate:date]];
    dateTimeStL.text = newDateStr1;

    
    
    ///
    

//    NSString *FNSTRs = [dateStr stringByReplacingOccurrencesOfString:@"T" withString:@" "];
//    NSLog(@"FNSTRs %@", FNSTRs);
//
//    
//    // Date string.
//    NSDateFormatter *dateFormatterD = [[NSDateFormatter alloc] init];
//    [dateFormatterD setDateFormat:FORMAT_DATE_TIME];
//    NSDate *dateDR  = [dateFormatterD dateFromString:FNSTRs];
//    NSString *s = [dateFormatterD stringFromDate:dateDR];
//    
//    NSLog(@"FNSTRs %@", s);
//
//    
//    // Convert to new Date Format.
//    [dateFormatterD setDateFormat:@"MMMM d yyyy"];
//    NSString *newDateStrD = [NSString stringWithFormat:@"%@,",[dateFormatterD stringFromDate:dateDR]];
//    dateStL.text = newDateStrD;

//    
//    // Time string.
//    //Set formate.
//
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:FORMAT_DATE_TIME];
//    NSDate *date  = [dateFormatter dateFromString:FNSTR];
//    
//    // Convert to new Date Format.
//    [dateFormatter setDateFormat:FORMAT_TIME_12];
//    NSString *newDateStr = [NSString stringWithFormat:@"@ %@",[dateFormatter stringFromDate:date]];
//    dateTimeStL.text = newDateStr;
//    
    // services names
    NSArray *itemArray = [ReadData arrayFromDictionary:serviceRatingDict forKey:@"Items"];
    
    NSString *serviceStr = @"";
    
    for (NSDictionary *dict in itemArray) {
        NSString *str = [dict valueForKey:@"Name"];
        serviceStr = [NSString stringWithFormat:@"%@ +", str];
    }
    
    //NSLog(@"%@", serviceStr);

    serviceStr = [serviceStr substringToIndex:serviceStr.length-(serviceStr.length>0)];
    serviceStL.text = serviceStr;

   // NSLog(@"%@", serviceStr);
    
 
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

// Called, when click on the starrs button.
-(IBAction)ratingButtonAction:(UIButton *)sender{

    switch (sender.tag) {
        case 1:
        {
            if ([star1B.imageView.image isEqual:[UIImage imageNamed:@"service_rate_notselected.png"]])
            {
                [star1B setImage:[UIImage imageNamed:@"service_rate_selected"] forState:UIControlStateNormal];
                rateCounting = rateCounting +1;

            }
            else{
                [star1B setImage:[UIImage imageNamed:@"service_rate_notselected.png"] forState:UIControlStateNormal];
                rateCounting = rateCounting -1;

            }
            
        }
            break;
            
        case 2:
        {
       
            if ([star2B.imageView.image isEqual:[UIImage imageNamed:@"service_rate_notselected.png"]])
            {
                [star2B setImage:[UIImage imageNamed:@"service_rate_selected"] forState:UIControlStateNormal];
                rateCounting = rateCounting +1;

            }
            else{
                [star2B setImage:[UIImage imageNamed:@"service_rate_notselected.png"] forState:UIControlStateNormal];
                rateCounting = rateCounting -1;

            }
           
        }
           break;
        case 3:
        {
            
            if ([star3B.imageView.image isEqual:[UIImage imageNamed:@"service_rate_notselected.png"]])
            {
                [star3B setImage:[UIImage imageNamed:@"service_rate_selected"] forState:UIControlStateNormal];
                rateCounting = rateCounting +1;

            }
            else{
                [star3B setImage:[UIImage imageNamed:@"service_rate_notselected.png"] forState:UIControlStateNormal];
                rateCounting = rateCounting -1;

            }
            
        }
            break;
            
        case 4:
        {
            if ([star4B.imageView.image isEqual:[UIImage imageNamed:@"service_rate_notselected.png"]])
            {
                [star4B setImage:[UIImage imageNamed:@"service_rate_selected"] forState:UIControlStateNormal];
                rateCounting = rateCounting +1;

            }
            else{
                [star4B setImage:[UIImage imageNamed:@"service_rate_notselected.png"] forState:UIControlStateNormal];
                rateCounting = rateCounting -1;

            }
            break;
        }
            
        case 5:
        {
            if ([star5B.imageView.image isEqual:[UIImage imageNamed:@"service_rate_notselected.png"]])
            {
                [star5B setImage:[UIImage imageNamed:@"service_rate_selected"] forState:UIControlStateNormal];
                rateCounting = rateCounting +1;

            }
            else{
                [star5B setImage:[UIImage imageNamed:@"service_rate_notselected.png"] forState:UIControlStateNormal];
                rateCounting = rateCounting -1;

            }
            break;
            
        default:
            break;
    }
}
    }
// cancel action.
-(IBAction)ServiceCancelAction:(id)sender{
    
    
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(serviceRatingCancel)]) {
        [self.delegate serviceRatingCancel];
    }
}
// submit action.
-(IBAction)ServiceSubmitAction:(id)sender{
    
    NSLog(@"%d",rateCounting);
    NSLog(@"%@",commentTXTV.text);

    
    
    if (rateCounting <= 3) {
        if (rateCounting == 0){
            UIAlertView * rateAlertMessage = [[UIAlertView alloc]initWithTitle:nil message:@"Please select rating." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [rateAlertMessage show];
            
        }else{
            if(commentTXTV.text.length != 0 || ![commentTXTV.text isEqualToString:@""]){
                [self callingRatingService];
                
                
            }else{
                UIAlertView * rateAlertMessage = [[UIAlertView alloc]initWithTitle:nil message:@"Please tell us what went wrong." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [rateAlertMessage show];
                
            }

        }
        
    }else{
        [self callingRatingService];

    }
    
   
}


//Called the, when user click on the sumbit button and calling post rating service.
#pragma mark  Calling Post Rating service On submit button action.

-(void) callingRatingService{
    if ([InternetCheck isOnline])
    {
        // [self hudShowWithMessage:@"Loading"];
        
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject: [appD getLoggedInUserId] forKey:@"CustomerId"];
        [paramDict setObject:[ReadData stringValueFromDictionary:serviceRatingDict forKey:@"Id"] forKey:@"AppointmentId"];
        [paramDict setObject:[NSString stringWithFormat:@"%d", rateCounting] forKey:@"Rating"];
        [paramDict setObject:commentTXTV.text forKey:PARAM_REASON];
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_RATING_SERVICE  Param:paramDict];
        
    }
    else{
        //[self showErrorMessage:NETWORKERROR];
    }
    
}

// response data.
-(void)processResponse_ratingService:(NSDictionary*)dict{
    
    
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(serviceRatingSubmit)]) {
                [self.delegate serviceRatingSubmit];
    }
    
    
    /*
    if (dict == NULL|| dict == nil || dict.count == 0) {
        
    }else{
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(serviceRatingSubmit)]) {
                        [self.delegate serviceRatingSubmit];
                   }
    }
    */
    
}


# pragma mark API Delegate methods.
- (void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName{
    
    if(![ReadData isValidResponseData:response])
    {
        {
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
            [self.view setUserInteractionEnabled:YES];
            [HUD hide:YES];
        }
        return;
    }
    
    if ([apiName isEqualToString:API_RATING_SERVICE]) {
        
        [self processResponse_ratingService:[ReadData dictionaryFromDictionary:response forKey:RESPONSEKEY_DATA]];
    }
    
}



#pragma  uitextview delegate methods.
- (void)textViewDidBeginEditing:(UITextView *)textView{
    //[self setContentOffset:commentStTXTV.frame.origin animated:YES];
    if (textView == commentTXTV)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.view.frame = CGRectMake(self.view.frame.origin.x , (self.view.frame.origin.y - 100), self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    }
}


#pragma mark view tough
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan) {
        [commentTXTV resignFirstResponder];
        //[flatAddressTxtF resignFirstResponder];
        //[landMarkTxtF resignFirstResponder];
       
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.view.frame = CGRectMake(self.view.frame.origin.x ,0, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
        
    }
}



@end
