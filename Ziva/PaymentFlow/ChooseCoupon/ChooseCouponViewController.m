//
//  ChooseCouponViewController.m
//  Ziva
//
//  Created by Bharat on 03/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "ChooseCouponViewController.h"

#define CELL_SPACING 10
#define ROW_SPACING 10

#define COLUMN_TAG_BASE_INDEX 1000
#define DATA_ICON_INDEX 100
#define DATA_TITLE_INDEX 200
#define DATA_SELECT_INDEX 300

@interface ChooseCouponViewController ()<UICollectionViewDelegateFlowLayout>
{
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIView *topImageV;
    __weak IBOutlet UIImageView *mainImgV;
    
    __weak IBOutlet UITextField *promocodeTxtF;
    
    __weak IBOutlet UIView *contentV;
    
    __weak IBOutlet UIView *couponsCV;
    
    __weak IBOutlet UIScrollView *mainscrollV;
    
    __weak IBOutlet UIView *dummyRowV;
    __weak IBOutlet UIView *dummyCellV;
    
    __weak IBOutlet UIView *couponDescV;
    __weak IBOutlet UIView *couponDescCV;
    __weak IBOutlet UIImageView *couponDescImgV;
    __weak IBOutlet UILabel *couponDescL;
    
    __weak IBOutlet UIView *footerV;
    
    AppDelegate *appD;
    MyCartController *cartC;
    
    NSArray *lstCoupons;
    CGFloat noOfRows;
    
    NSString *appliedCoupon;
}
@end

@implementation ChooseCouponViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;
    
    [self setupLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods

-(void) showCouponsListing
{
    appliedCoupon = @"";
    promocodeTxtF.text = @"";
    lstCoupons = [cartC fetchCoupons];
    couponsCV.hidden = ([lstCoupons count] == 0);
    if([lstCoupons count] > 0){
        [self setupCouponsList];
    }
}

#pragma mark - Methods

- (void) setupLayout
{
    appliedCoupon = @"";
    promocodeTxtF.text = @"";
    
    //Position elements
    {
        topImageV.frame = [UpdateFrame setSizeForView:topImageV usingHeight:[ResizeToFitContent getHeightForParallaxImage:self.view.frame]];
        
        mainImgV.frame = topImageV.frame ;
        
        containerView.frame = [UpdateFrame setSizeForView:containerView usingHeight:(CGRectGetHeight(self.view.bounds))];
        
        
        contentV.frame= [UpdateFrame setPositionForView:contentV usingPositionY: CGRectGetMaxY(topImageV.frame)];
        contentV.frame = [UpdateFrame setSizeForView:contentV usingHeight: CGRectGetHeight(containerView.frame) - CGRectGetHeight(topImageV.frame) - CGRectGetHeight(footerV.frame) ];
    }
}

- (void) setupCouponsList
{
    for (UIView* v in mainscrollV.subviews){
        if(v.tag >= 0){
            [v removeFromSuperview];
        }
    }
    
    noOfRows = 0;
    NSInteger startLoc_x = -1;
    NSInteger startLoc_y = 0;
    NSInteger ctr = 0;
    while (ctr < [lstCoupons count]) {
        CGFloat tmpWidth = [self calculateCellWidth:ctr];
        
        //Add New row of data, if current cell does not fit.
        if((startLoc_x + tmpWidth > CGRectGetWidth(mainscrollV.frame))|| (startLoc_x == -1)){
            UIView *v = [self getPlaceHolderRow];
            if(noOfRows > 0){ startLoc_y += (CGRectGetHeight(v.frame) + ROW_SPACING);}
            startLoc_x = 0;
            noOfRows++;
            v.tag = noOfRows;
            v.frame = [UpdateFrame setPositionForView:v usingPositionX:startLoc_x andPositionY:startLoc_y];
            [mainscrollV addSubview:v];
        }
        
        //Fetch latest row & column cell
        UIView *dataRowV = [mainscrollV viewWithTag:noOfRows];
        UIView *columnCellV = [self getColumnCell:ctr];
        columnCellV.frame = [UpdateFrame setSizeForView:columnCellV usingWidth:tmpWidth];
        columnCellV.tag = COLUMN_TAG_BASE_INDEX + ctr;
        columnCellV.frame = [UpdateFrame setPositionForView:columnCellV usingPositionX:startLoc_x];
        
        [dataRowV addSubview:columnCellV];
        startLoc_x +=  (tmpWidth + CELL_SPACING);
        ctr ++;
    }
    [mainscrollV setContentSize:CGSizeMake(CGRectGetWidth(mainscrollV.frame), startLoc_y + CGRectGetHeight(dummyRowV.frame) + CGRectGetHeight(couponDescV.frame))];
}

- (UIView *) getPlaceHolderRow{
    NSData *tempArchive = [NSKeyedArchiver archivedDataWithRootObject:dummyRowV];
    UIView *v = (UIView *)[NSKeyedUnarchiver unarchiveObjectWithData:tempArchive];
    v.hidden = NO;
    return v;
}

- (UIView *) getColumnCell : (NSInteger) ctr {
    NSData *tempArchive = [NSKeyedArchiver archivedDataWithRootObject:dummyCellV];
    
    UIView *v = (UIView *)[NSKeyedUnarchiver unarchiveObjectWithData:tempArchive];
    UILabel *titleL =  (UILabel *)[v viewWithTag:DATA_TITLE_INDEX];
    titleL.text = [ReadData stringValueFromDictionary:[lstCoupons objectAtIndex:ctr] forKey:KEY_COUPON_CODE];
    
    UIButton *selectB =  (UIButton *)[v viewWithTag:DATA_SELECT_INDEX];
    [selectB addTarget:self action:@selector(couponselectedBPressed:) forControlEvents:UIControlEventTouchUpInside];
    v.hidden = NO;
    v.layer.cornerRadius = 3;
    return v;
}

- (CGFloat) calculateCellWidth : (NSInteger) row {
    NSDictionary *item = lstCoupons[row];
    NSString *displayTxt = [ReadData stringValueFromDictionary:item forKey:KEY_COUPON_CODE];
    CGFloat tmpWidth = [ResizeToFitContent getWidthForText:displayTxt usingFontType:FONT_SEMIBOLD FontOfSize:13 andMaxWidth:200];
    return (35 + tmpWidth);
}


- (void) selectPromoCode : (NSInteger) index {
    BOOL isFound = NO;
    NSInteger rowIdx = -1;
    for (UIView* v in mainscrollV.subviews){
        rowIdx = v.tag;
        for (UIView* cell in v.subviews){
            if(cell.tag == index){
                isFound = YES;
                break;
            }
        }
    }
    if(isFound){
        appliedCoupon = [ReadData stringValueFromDictionary: [lstCoupons objectAtIndex: index - 1000] forKey:KEY_COUPON_CODE];
        [self updateSelectionOnRow:rowIdx ColumnTag:index andState:YES];
        [self showToolTip:YES atRow:rowIdx forColumnTag:index];
    }
}

#pragma mark - Selection Events

-(void)couponselectedBPressed:(UIButton *)sender{
    [UIResponder dismissKeyboard];
    NSInteger row = [[sender superview] superview].tag;
    NSInteger columnTag = [sender superview].tag;
    [self updateSelectionOnRow:row ColumnTag:columnTag andState:!sender.selected];
    [self showToolTip:sender.selected atRow:row forColumnTag:columnTag];
}

- (void) showToolTip : (BOOL) show atRow : (NSInteger) rowIdx forColumnTag : (NSInteger) tag{
    UIView *rowV = [mainscrollV viewWithTag:rowIdx];
    UIView *cell = [rowV viewWithTag:tag];
    
    //Rearrange rows
    CGFloat positionY = -1;
    CGFloat rowOffset = CGRectGetHeight(dummyRowV.frame) + ROW_SPACING;
    for(NSInteger ctr = 1; ctr <= noOfRows; ctr++){
        positionY = (ctr - 1) * rowOffset;
        if((ctr > rowIdx) && show){
            positionY += CGRectGetHeight(couponDescV.frame);
        }
        UIView *v = [mainscrollV viewWithTag:ctr];
        v.frame = [UpdateFrame setPositionForView:v usingPositionY:positionY];
    }
    
    NSDictionary *item = [lstCoupons objectAtIndex: tag - COLUMN_TAG_BASE_INDEX];
    appliedCoupon = show ? [ReadData stringValueFromDictionary:item forKey:KEY_COUPON_CODE]: @"";
    couponDescL.text = [ReadData stringValueFromDictionary:item forKey:KEY_COUPON_TITLE];
    couponDescCV.backgroundColor = [UIColor colorWithRed:174.0/255.0 green:176.0/255.0 blue:183.0/255.0 alpha:1.0];
    couponDescV.frame = [UpdateFrame setPositionForView:couponDescV usingPositionY: CGRectGetMaxY(rowV.frame)];
    couponDescImgV.frame = [UpdateFrame setPositionForView:couponDescImgV usingPositionX:  CGRectGetMinX(cell.frame) + 35];
    couponDescV.hidden = !show;
    
}

- (void) updateSelectionOnRow : (NSInteger) rowIdx ColumnTag : (NSInteger) tag andState: (BOOL) selected{
    for (UIView* v in mainscrollV.subviews){
        for (UIView* cell in v.subviews){
            if((v.tag == rowIdx) && (cell.tag == tag)){
                [self updateColumnCell:cell selectionState:selected];
            }
            else{
                [self updateColumnCell:cell selectionState:NO];
            }
        }
    }
}

- (void) updateColumnCell : (UIView *) cellV  selectionState: (BOOL) selected{
    cellV.backgroundColor  = selected ? [UIColor brandOrangeDisplayColor] : [UIColor greyBackgroundDisplayColor];
    UIButton *iconB =  (UIButton *)[cellV viewWithTag:DATA_ICON_INDEX];
    iconB.selected = selected;
    UILabel *titleL =  (UILabel *)[cellV viewWithTag:DATA_TITLE_INDEX];
    titleL.textColor = selected ? [UIColor blackColor]  : [UIColor greyTextDisplayColor];
    UIButton *selectB =  (UIButton *)[cellV viewWithTag:DATA_SELECT_INDEX];
    selectB.selected = selected;
}

#pragma mark - Events

- (IBAction)closeBPressed:(UIButton *)sender{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(oncancel_Coupon)])
    {
        [self.delegate oncancel_Coupon] ;
    }
}

- (IBAction)searchBPressed:(UIButton *)sender{
    NSInteger index = -1;
    BOOL isFound = NO;
    if(![appD isEmptyString:promocodeTxtF.text])
    {
        for(NSInteger ctr =0; ctr < [lstCoupons count]; ctr++){
            NSDictionary *item = lstCoupons[ctr];
            if([promocodeTxtF.text isEqualToString: [ReadData stringValueFromDictionary:item forKey:KEY_COUPON_CODE]])
            {
                index = 1000 + ctr;
                isFound = YES;
                break;
            }
        }
    }
    [self selectPromoCode:index];
    if(isFound || [appD isEmptyString:promocodeTxtF.text]){
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(onapply_Coupon:)])
        {
            [self.delegate onapply_Coupon: appliedCoupon] ;
        }
    }
}

- (IBAction)applyBPressed:(UIButton *)sender{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(onapply_Coupon:)])
    {
        [self.delegate onapply_Coupon: appliedCoupon] ;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSRange lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
    
    if (lowercaseCharRange.location != NSNotFound) {
        textField.text = [textField.text stringByReplacingCharactersInRange:range
                                                                 withString:[string uppercaseString]];
        return NO;
    }
    
    return YES;
}

@end
