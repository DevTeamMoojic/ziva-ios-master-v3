//
//  PaymentFlowViewController.m
//  Ziva
//
//  Created by Bharat on 03/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//


#import "PaymentSummaryViewController.h"
#import "ChooseCouponViewController.h"
#import "PaymentModeViewController.h"
#import "GatewayPaymentViewController.h"
#import "CashPaymentViewController.h"
#import "OrderStatusViewController.h"
#import "PaymentFlowViewController.h"

@interface PaymentFlowViewController ()<PaymentSummaryDelegate,ChooseCouponDelegate,PaymentModeDelegate,CashPaymentDelegate,GatewayPaymentDelegate,OrderStatusDelegate>
{
    __weak IBOutlet UIView *paymentsummaryCV;
    __weak IBOutlet UIView *choosecouponCV;
    __weak IBOutlet UIView *choosepaymentmodeCV;
    __weak IBOutlet UIView *gatewaypaymentCV;
    __weak IBOutlet UIView *cashpaymentCV;
    __weak IBOutlet UIView *orderstatusCV;
    
    __weak PaymentSummaryViewController *paymentsummaryVC;
    __weak ChooseCouponViewController *choosecouponVC;
    __weak PaymentModeViewController *choosepaymentmodeVC;
    __weak GatewayPaymentViewController *gatewaypaymentVC;
    __weak CashPaymentViewController *cashpaymentVC;
    __weak OrderStatusViewController *orderstatusVC;
    
    AppDelegate *appD;
    MyCartController *cartC;
}
@end

@implementation PaymentFlowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;
    
    [self setupLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Methods

- (void) setupLayout{
    
    
}

- (void) paymentProcessCompleted : (BOOL) status
{
    if(status)
        [orderstatusVC showPaymentConfirmMsg];
    else
        [orderstatusVC showPaymentFailureMsg];
    [self modaltranslateView:orderstatusCV onScreen:YES];
    
}

#pragma mark - Translate View

- (void)translateView:(UIView *)view onScreen : (BOOL) show
{
    [self.view bringSubviewToFront:view];
    
    // DONT CHANGE THE ORDER
    
    // 1 - Dismiss keyboard
    [UIResponder dismissKeyboard];
    
    // 2 - Disable User Interaction
    [self.view setUserInteractionEnabled:NO];
    
    // 3 - Animate
    [UIView animateWithDuration:TIME_VIEW_TOGGLE
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         // Slide in
         view.frame = [UpdateFrame setPositionForView:view usingPositionX: show ? 0 : CGRectGetWidth(view.frame) ];
     }
                     completion:^(BOOL finished)
     {
         // Enable User Interaction
         [self.view setUserInteractionEnabled:YES];
     }];
}

- (void) modaltranslateView:(UIView *)view onScreen : (BOOL) show
{
    [self.view bringSubviewToFront:view];
    
    // DONT CHANGE THE ORDER
    
    // 1 - Dismiss keyboard
    [UIResponder dismissKeyboard];
    
    // 2 - Disable User Interaction
    [self.view setUserInteractionEnabled:NO];
    
    // 3 - Animate
    [UIView animateWithDuration:TIME_VIEW_TOGGLE
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         // Slide in
         view.frame = [UpdateFrame setPositionForView:view usingPositionY: show ? 0 : CGRectGetHeight(view.frame) ];
     }
                     completion:^(BOOL finished)
     {
         // Enable User Interaction
         [self.view setUserInteractionEnabled:YES];
     }];
}


#pragma mark - Delegates Method

- (void) oncancel_Summary
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) ondisplay_Coupon{
    [choosecouponVC showCouponsListing];
    [self modaltranslateView:choosecouponCV onScreen:YES];
}

- (void) onconfirm_Booking{
    if(cartC.payableAmount > 0.0f){
        [self modaltranslateView:choosepaymentmodeCV onScreen:YES];
    }
    else{
        // new modification for when cash is zero.
        [cartC generateOrderId];
        cartC.modeOfPayment = PAYMENT_MODE_CASH;
    }
}

- (void) oncancel_Coupon{
    [self modaltranslateView:choosecouponCV onScreen:NO];
}

- (void) onapply_Coupon : (NSString *) code
{
    [paymentsummaryVC applyCoupon:code];
    [self modaltranslateView:choosecouponCV onScreen:NO];
}

- (void) oncancel_PaymentMode{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) onapply_PaymentMode : (NSString *) mode{
    cartC.modeOfPayment = mode;
    if([cartC.modeOfPayment isEqualToString:PAYMENT_MODE_CASH]){
        [cashpaymentVC initiatePayment];
        [self translateView:cashpaymentCV onScreen:YES];
    }
    else{
        [gatewaypaymentVC initiatePayment:mode];
        [self translateView:gatewaypaymentCV onScreen:YES];
    }
}

- (void) oncancel_GatewayPayment{
    [self translateView:gatewaypaymentCV onScreen:NO];
}

- (void) oncompleted_GatewayTransaction : (BOOL) status{
    [self paymentProcessCompleted:status];
}

- (void) oncancel_CashPayment{
    [self translateView:cashpaymentCV onScreen:NO];
}

- (void) oncompleted_CashTransaction : (BOOL) status{
    [self paymentProcessCompleted:status];
}

- (void) onRetry_Booking{
    UIViewController *vc = [self.navigationController.viewControllers objectAtIndex: (self.navigationController.viewControllers.count - 2)] ;
    
    NSArray *arrV = [self.navigationController viewControllers];
    //NSLog(@"%@", arrV);
    [self.navigationController popToViewController:vc animated:YES];    
}

- (void) onCompleted_Booking{
    [appD backToHome];
}

- (void) onView_Appointment:(NSString *) orderId{
    //[appD backToHome];
    [appD backToMyTransaction];
}


#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"embed_paymentsummary"]){
        paymentsummaryVC = (PaymentSummaryViewController *)segue.destinationViewController;
        paymentsummaryVC.delegate = self;
    }
    else if([segue.identifier isEqualToString:@"embed_choosecoupon"]){
        choosecouponVC = (ChooseCouponViewController *)segue.destinationViewController;
        choosecouponVC.delegate = self;
    }
    else if([segue.identifier isEqualToString:@"embed_paymentmode"]){
        choosepaymentmodeVC = (PaymentModeViewController *)segue.destinationViewController;
        choosepaymentmodeVC.delegate = self;
    }
    else if([segue.identifier isEqualToString:@"embed_paymentbygateway"]){
        gatewaypaymentVC = (GatewayPaymentViewController *)segue.destinationViewController;
        gatewaypaymentVC.delegate = self;
    }
    else if([segue.identifier isEqualToString:@"embed_paymentbycash"]){
        cashpaymentVC = (CashPaymentViewController *)segue.destinationViewController;
        cashpaymentVC.delegate = self;
    }
    else if([segue.identifier isEqualToString:@"embed_orderstatus"]){
        orderstatusVC = (OrderStatusViewController *)segue.destinationViewController;
        orderstatusVC.delegate = self;
    }
}

@end
