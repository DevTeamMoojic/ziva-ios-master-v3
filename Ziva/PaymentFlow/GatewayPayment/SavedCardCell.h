//
//  SavedCardCell.h
//  Ziva
//
//  Created by Bharat on 22/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SavedCardCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lastDigitL;
@property (nonatomic, weak) IBOutlet  UILabel *cardNOL;
@property (nonatomic, weak) IBOutlet UILabel *cardHolderL;
@property (nonatomic, weak) IBOutlet UILabel *cardExpMonthL;
@property (nonatomic, weak) IBOutlet UILabel *cardExpYearhL;


- (void) configureDataForCell : (NSDictionary *) item;

@end
