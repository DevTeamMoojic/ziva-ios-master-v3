//
//  PreferredBanksViewController.m
//  Ziva
//
//  Created by Bharat on 12/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "GatewayPaymentViewController.h"
#import "PreferredBanksViewController.h"

#define BANK_COUNT 9
#define BANK_VIEW_TAG 500
#define SELECT_ICON_TAG 400
#define SELECT_BUTTON_TAG 100

@interface PreferredBanksViewController ()
{
    __weak IBOutlet UIView *preferredBanksV;
    
}
@end

@implementation PreferredBanksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public methods

- (void) resetSelection{
    for(NSInteger ctr = 0; ctr < BANK_COUNT ; ctr++){
        UIButton *iconB =  [preferredBanksV viewWithTag:SELECT_ICON_TAG + ctr];
        iconB.selected = NO;
        UIButton *selectionB =  [preferredBanksV viewWithTag:SELECT_BUTTON_TAG + ctr];
        selectionB.selected = NO;
    }
}

#pragma mark - Methods

- (void) setupLayout
{
    for(NSInteger ctr = 0; ctr < BANK_COUNT ; ctr++){
        UIView *containerView =  [preferredBanksV viewWithTag:BANK_VIEW_TAG + ctr];
        containerView.layer.borderWidth = 1.0f;
        containerView.layer.borderColor = [UIColor lightborderDisplayColor].CGColor;
    }
}

- (void) bankSelected : (NSInteger) index
{
    for(NSInteger ctr = 0; ctr < BANK_COUNT ; ctr++){
        UIButton *iconB =  [preferredBanksV viewWithTag:SELECT_ICON_TAG + ctr];
        iconB.selected = (ctr == index);
        UIButton *selectionB =  [preferredBanksV viewWithTag:SELECT_BUTTON_TAG + ctr];
        selectionB.selected = (ctr == index);
    }
    
    GatewayPaymentViewController *parentVC = (GatewayPaymentViewController *) self.parentViewController;
    [parentVC preferredBankSelected:index];
}

#pragma mark - Events;

- (IBAction)bankSelectedBPressed:(UIButton *)sender
{
    [self bankSelected:sender.tag - SELECT_BUTTON_TAG];
}

@end
