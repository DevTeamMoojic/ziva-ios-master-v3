//
//  PreferredBanksViewController.h
//  Ziva
//
//  Created by Bharat on 12/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreferredBanksViewController : UIViewController

- (void) resetSelection;

@end
