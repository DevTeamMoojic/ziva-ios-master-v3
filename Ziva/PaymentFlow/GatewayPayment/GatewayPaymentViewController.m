//
//  GatewayPaymentViewController.m
//  Ziva
//
//  Created by Bharat on 16/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <CitrusPay/CitrusPay.h>
#import "PreferredBanksViewController.h"
#import "ChooseBankViewController.h"
#import "GatewayPaymentViewController.h"
#import "SavedCardCell.h"
#define ROW_HEIGHT 210

#define INCORRECT_CARD_NUMBER 101
#define INCORRECT_EXPIRY_DATE 102
#define INCORRECT_CVV 103
#define INCORRECT_CARDHOLDER_NAME 104
#define INCORRECT_STORED_CARD 105
#define INCORRECT_BANK 106

#define CVV_PROMPT_TEXT_TAG 200

#define SignInId @"fx8bioa13s-signin"
#define SignInSecretKey @"777991760ffae52ea4ed32758d88890b0"
#define SubscriptionId @"fx8bioa13s-signup"
#define SubscriptionSecretKey @"b77f5430585e99aa3378273799fce439"

// URLs
#define VanityUrl @"fx8bioa13s"
#define ReturnUrl @"https://api.zivadiva.com/payment/v2/citrus/billresult"

#define ALPHABETICS @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
#define NUMERICS @"0123456789"


@interface GatewayPaymentViewController ()<MyCartDelegate,MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
    
    __weak IBOutlet UIView *optionsV;    
    __weak IBOutlet UILabel *screenTitleL;
    
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIView *topImageV;
    __weak IBOutlet UIImageView *mainImgV;
    
    __weak IBOutlet UIScrollView *scrollviewOnTop;
    
    __weak IBOutlet UIScrollView *tabcontentscrollView;
    
    __weak IBOutlet UIScrollView *mainscrollView;
    
    __weak IBOutlet UIView *addCardInfoV;
    __weak IBOutlet UIView *cardNumberV;
    __weak IBOutlet UITextField *cardNumberTxtF;
    __weak IBOutlet UIImageView *cardSchemeImage;

    __weak IBOutlet UIView *cardHolderNameV;
    __weak IBOutlet UITextField *cardHolderNameTxtF;
    
    __weak IBOutlet UIView *expiryV;
    __weak IBOutlet UITextField *expiryMonthTxtF;
    __weak IBOutlet UITextField *expiryYearTxtF;
    __weak IBOutlet UITextField *CVVNumberTxtF;
    
    __weak IBOutlet UIView *saveCardInfoV;
    __weak IBOutlet UIButton *saveCardInfoB;
    
    __weak IBOutlet UIView *savedCardsInfoV;
    
    __weak IBOutlet UIView *netBankingInfoV;
    __weak PreferredBanksViewController *preferredBankVC;
    
    __weak IBOutlet UIView *chooseBankV;
    __weak IBOutlet UIView *bankDropdownCV;
    __weak IBOutlet UILabel *bankDropdownL;
    __weak IBOutlet UIImageView *bankDropdwonImgV;
    __weak IBOutlet UIView *selectBankCV;
    __weak ChooseBankViewController *selectBankVC;
    
    __weak IBOutlet UIView *footerV;
    __weak IBOutlet UILabel *payableAmountL;
    
    NSString *tokencardName ;
    NSString *tokecardscheme;
    
    NSString *cardscheme;
    
    UIToolbar* numberToolbar;
    
    CGRect cachedImageViewSize;
    CGFloat yscrollOffset;
    
    BOOL isBackFromOtherView;
    BOOL isContentSizeUpdated;
    BOOL isEditMoving;
    
    NSInteger pageWidth;
    
    NSMutableArray *preferredBanks;
    NSArray *otherBanks;
    
    AppDelegate *appD;
    MyCartController *cartC;
    
    CGFloat contentHeight;
    
    //Payment Gateway
    CTSAuthLayer *authLayer;
    CTSProfileLayer *proifleLayer;
    CTSPaymentLayer *paymentLayer;
    CTSContactUpdate* contactInfo;
    CTSUserAddress* addressInfo;
    PaymentType *paymentType;

    NSString *selectedBankName;
    NSString *selectedBankCode;
    
    // For saved card detatils.
    NSMutableDictionary *cardsDetailsDictonary;
    NSArray *list;
    
    // For saved cards.
    NSMutableArray *cardDetailsArray;
    __weak IBOutlet  UILabel *noresultsL;
    __weak IBOutlet UITableView *listTblV;
    __weak IBOutlet UIView *saveFooterV;
    BOOL isSaveAddressTag;

}
@end

@implementation GatewayPaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;
    [cartC addDelegate:self];
    
    [self setupLayout];
    
    [self setupPreferredBanks];
    
    [self initPG];
    
    //
    //cardsDetailsDictonary = [[NSDictionary alloc]init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    cardsDetailsDictonary = [[NSMutableDictionary alloc]init];
   /*
    // initial load the table with past history location placess.
    cardDetailsArray = appD.sessionDelegate.pastHistoryLocationArray;
    
    list = cardDetailsArray;
    //listTblV.userInteractionEnabled = NO;
    
    listTblV.hidden = ([list count] == 0);
    noresultsL.hidden = !listTblV.hidden ;
    
    [listTblV reloadData];
    */
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if(!isBackFromOtherView){
        
        [cartC removeDelegate:self];
    }
}

#pragma mark - Public Methods

- (void) initiatePayment : (NSString *) mode
{
    screenTitleL.text = [mode uppercaseString];
    payableAmountL.text = [ReadData getAmountFormatted:cartC.payableAmount];
    
    cardscheme = @"";
    tokencardName = @"";
    
    cardSchemeImage.image = Nil;
    cardNumberTxtF.text = @"";
    cardHolderNameTxtF.text = @"";
    expiryMonthTxtF.text = @"";
    expiryYearTxtF.text = @"";
    CVVNumberTxtF.text = @"";
    selectedBankName = @"";
    selectedBankCode = @"";
    [preferredBankVC resetSelection];
    [selectBankVC resetSelection];
    [self setupOtherBankLayout];
    saveCardInfoB.selected = YES;
    
    [preferredBankVC resetSelection];
    [selectBankVC resetSelection];
    
    isSaveAddressTag = YES;
    
    if([screenTitleL.text isEqualToString:[PAYMENT_MODE_NETBANKING uppercaseString]]){
        addCardInfoV.hidden = YES;
        savedCardsInfoV.hidden = YES;
        netBankingInfoV.hidden = NO;
        footerV.hidden = NO;
        saveFooterV.hidden = YES;

    }
    else if([screenTitleL.text isEqualToString:[PAYMENT_MODE_DEBITCARD uppercaseString]]){
        addCardInfoV.hidden = NO;
        savedCardsInfoV.hidden = YES;
        netBankingInfoV.hidden = YES;
        footerV.hidden = NO;
        saveFooterV.hidden = YES;


    }
    else if([screenTitleL.text isEqualToString:[PAYMENT_MODE_CREDITCARD uppercaseString]]){
        addCardInfoV.hidden = NO;
        savedCardsInfoV.hidden = YES;
        netBankingInfoV.hidden = YES;
        footerV.hidden = NO;
        saveFooterV.hidden = YES;


    }
    else if([screenTitleL.text isEqualToString:[PAYMENT_MODE_CARDSSAVED uppercaseString]]){
        
        
         // initial load the table with past history location placess.
         cardDetailsArray = appD.sessionDelegate.cardDetailsArray;
        /*
        // default settin  array1
        [cardsDetailsDictonary setValue:@"45434565456787654" forKey:@"CNUMBER"];
        [cardsDetailsDictonary setValue:@"Minnarao" forKey:@"CNAME"];
        [cardsDetailsDictonary setValue:@"10" forKey:@"EXPM"];
        [cardsDetailsDictonary setValue:@"2021" forKey:@"EXPY"];
        [cardsDetailsDictonary setValue:@"Credit Card" forKey:@"CARDMODETYPE"];
        [cardsDetailsDictonary setValue:@"visa" forKey:@"CARDSCHEME"];

        [cardDetailsArray addObject:cardsDetailsDictonary];
        
        
        
        // default settin  array2
        [cardsDetailsDictonary setValue:@"1234123412341234" forKey:@"CNUMBER"];
        [cardsDetailsDictonary setValue:@"Varu Barve" forKey:@"CNAME"];
        [cardsDetailsDictonary setValue:@"09" forKey:@"EXPM"];
        [cardsDetailsDictonary setValue:@"2012" forKey:@"EXPY"];
        
        [cardsDetailsDictonary setValue:@"Debit Card" forKey:@"CARDMODETYPE"];
        [cardsDetailsDictonary setValue:@"mastercard" forKey:@"CARDSCHEME"];
        [cardDetailsArray addObject:cardsDetailsDictonary];
       
         */
       
        // To avoide duplicates.
        //NSLog(@"actuval %@",cardDetailsArray);
        NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:cardDetailsArray];
        //NSLog(@"before%@", orderedSet);
        NSArray *arrayWithoutDuplicates = [orderedSet array];
        //NSLog(@"Dulicate%@", arrayWithoutDuplicates);
        
        // Replacing the fresh array in main array.
        appD.sessionDelegate.cardDetailsArray = [(arrayWithoutDuplicates)mutableCopy];
        //NSLog(@"%@", appD.sessionDelegate.cardDetailsArray);
        
        // Set the values to actual array.
         list = appD.sessionDelegate.cardDetailsArray;
         //listTblV.userInteractionEnabled = NO;
      
        // Reload table view.
        listTblV.hidden = ([list count] == 0);
        noresultsL.hidden = !listTblV.hidden ;
        [listTblV reloadData];
         

        // Hide the rest of view and show only saved card view.
        addCardInfoV.hidden = YES;
        savedCardsInfoV.hidden = NO;
        netBankingInfoV.hidden = YES;
        footerV.hidden = YES;
        saveFooterV.hidden = NO;

    }
    
    if (contentHeight <= CGRectGetHeight(mainscrollView.bounds))
    {
        contentHeight = CGRectGetHeight(mainscrollView.bounds);
        mainscrollView.scrollEnabled = NO;
    }
    
    [self tabChanged];
}

- (void) preferredBankSelected : (NSInteger) index
{
    [selectBankVC resetSelection];
    selectedBankName = @"";
    [self setupOtherBankLayout];
    NSDictionary *item = [preferredBanks objectAtIndex:index];
    selectedBankName = [ReadData stringValueFromDictionary:item forKey:KEY_BANK_NAME];
    selectedBankCode = [ReadData stringValueFromDictionary:item forKey:KEY_BANK_ISSUERCODE];
}

- (void) dismissBankSelector
{
    [self modaltranslateView:selectBankCV onScreen:NO];
}

- (void) otherBankSelected : (NSInteger) index
{
    [preferredBankVC resetSelection];
    if(index != -1)
    {
        NSDictionary *item = [otherBanks objectAtIndex:index];
        selectedBankName = [ReadData stringValueFromDictionary:item forKey:KEY_BANK_NAME];
        selectedBankCode = [ReadData stringValueFromDictionary:item forKey:KEY_BANK_ISSUERCODE];
    }
    else{
        selectedBankName = @"";
        selectedBankCode = @"";
    }
    [self setupOtherBankLayout];
    [self modaltranslateView:selectBankCV onScreen:NO];
}

#pragma mark - Methods

- (void) setupLayout
{
    pageWidth = CGRectGetWidth(self.view.bounds);
    
    [self.view addGestureRecognizer:scrollviewOnTop.panGestureRecognizer];
    [mainscrollView removeGestureRecognizer:mainscrollView.panGestureRecognizer];
    
    //Position elements
    {
        topImageV.frame = [UpdateFrame setSizeForView:topImageV usingHeight:[ResizeToFitContent getHeightForParallaxImage:self.view.frame]];
        
        yscrollOffset = CGRectGetHeight(topImageV.frame) - IMAGE_SCROLL_OFFSET_Y - CGRectGetHeight(optionsV.frame);
        mainImgV.frame = topImageV.frame ;
        
        containerView.frame = [UpdateFrame setSizeForView:containerView usingHeight:(CGRectGetHeight(self.view.bounds) + yscrollOffset)];
        
        tabcontentscrollView.frame= [UpdateFrame setPositionForView:tabcontentscrollView usingPositionY: CGRectGetMaxY(topImageV.frame)];
        
        //NSLog(@"before tabcontentscrollView fram %f,%f,%f,%f", tabcontentscrollView.frame.origin.x, tabcontentscrollView.frame.origin.y, tabcontentscrollView.frame.size.height,tabcontentscrollView.frame.size.width);
        
        tabcontentscrollView.frame = [UpdateFrame setSizeForView:tabcontentscrollView usingHeight: CGRectGetHeight(containerView.frame) - CGRectGetMaxY(topImageV.frame) - CGRectGetHeight(footerV.frame)];
        
        //NSLog(@"after tabcontentscrollView fram %f,%f,%f,%f", tabcontentscrollView.frame.origin.x, tabcontentscrollView.frame.origin.y, tabcontentscrollView.frame.size.height,tabcontentscrollView.frame.size.width);
    }
    
    
    CGPoint scrollviewOrigin = scrollviewOnTop.frame.origin;
    scrollviewOnTop.scrollIndicatorInsets = UIEdgeInsetsMake(-scrollviewOrigin.y, 0, scrollviewOrigin.y, scrollviewOrigin.x);
    
    [self setupToolBar];
}
//

- (void) setupToolBar
{
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    numberToolbar.barTintColor = [UIColor lightGrayColor];
    //numberToolbar.tintColor = [UIColor officialMaroonColor];
    [numberToolbar sizeToFit];
    
    cardNumberTxtF.inputAccessoryView = numberToolbar;
    expiryMonthTxtF.inputAccessoryView = numberToolbar;
    expiryYearTxtF.inputAccessoryView = numberToolbar;
    CVVNumberTxtF.inputAccessoryView = numberToolbar;
}

- (void) tabChanged
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        CGPoint offset = [self getScrollContentOffset];
        offset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:offset animated:NO];
    }
    isContentSizeUpdated = NO;
}

- (void) modaltranslateView:(UIView *)view onScreen : (BOOL) show
{
    [self.view bringSubviewToFront:view];
    
    // DONT CHANGE THE ORDER
    
    // 1 - Dismiss keyboard
    [UIResponder dismissKeyboard];
    
    // 2 - Disable User Interaction
    [self.view setUserInteractionEnabled:NO];
    
    // 3 - Animate
    [UIView animateWithDuration:TIME_VIEW_TOGGLE
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         // Slide in
         view.frame = [UpdateFrame setPositionForView:view usingPositionY: show ? 0 : CGRectGetHeight(view.frame) ];
     }
                     completion:^(BOOL finished)
     {
         // Enable User Interaction
         [self.view setUserInteractionEnabled:YES];
     }];
}

- (void) setupOtherBankLayout
{
    CGFloat tmpWidth = 115;
    if(![appD isEmptyString: selectedBankName])
    {
        bankDropdownL.text = selectedBankName;
        tmpWidth = [ResizeToFitContent getWidthForText:selectedBankName usingFontType:FONT_SEMIBOLD FontOfSize:15 andMaxWidth:CGRectGetWidth(chooseBankV.frame) - 24] + 24;
    }
    else{
        bankDropdownL.text = @"Choose Bank";
    }
    bankDropdownCV.frame = [UpdateFrame setSizeForView:bankDropdownCV usingWidth:tmpWidth];
    bankDropdownCV.frame = [UpdateFrame setPositionForView:bankDropdownCV usingPositionX:(CGRectGetWidth(chooseBankV.frame) - CGRectGetWidth(bankDropdownCV.frame)) / 2.0 ];
}

#pragma mark - Cart Delegate Methods

- (void) onResponseError{
    [HUD hide:YES];
}

- (void) onOrderGenerated{
    [cartC getOrderInfo];
    if([screenTitleL.text isEqualToString:[PAYMENT_MODE_NETBANKING uppercaseString]]){
        [self processPayment_NetBanking];
    }
    else if([screenTitleL.text isEqualToString:[PAYMENT_MODE_DEBITCARD uppercaseString]]){
        [self processPayment_DebitCard];
    }
    else if([screenTitleL.text isEqualToString:[PAYMENT_MODE_CREDITCARD uppercaseString]]){
        [self processPayment_CreditCard];
    }
    // minnarao add new code.
    else if([screenTitleL.text isEqualToString:[PAYMENT_MODE_CARDSSAVED uppercaseString]]){
        //[self processPayment_CreditCard];
    }
}

- (void) onOrderDetailCompleted{
    [HUD hide:YES];
}

#pragma mark - HUD & Delegate methods

- (void)hudShowWithMessage:(NSString *)message
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    HUD.dimBackground = YES;
    HUD.labelText = message;
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    [HUD show:YES];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}

#pragma mark - Expand on Scroll animation

- (CGSize) setContentSize
{
    // New code minnarao.
    
    CGFloat height = CGRectGetHeight(mainImgV.frame) + contentHeight + CGRectGetHeight(footerV.frame);
    return CGSizeMake(pageWidth, height);
}

- (void) setScrollContentOffset: (CGPoint) offset
{
    mainscrollView.contentOffset = offset;
}

- (CGPoint) getScrollContentOffset
{
    return [mainscrollView contentOffset];
}

- (void) resetOffsetAllTabs
{
    CGPoint offset = CGPointZero;
    [self setScrollContentOffset : offset];
}

-(void) updateContentSize : (CGPoint) contentOffset;
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        contentOffset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    }
    [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    
    isContentSizeUpdated = NO;
}


#pragma mark - Scroll Events

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == scrollviewOnTop)
    {
        {
            if (isContentSizeUpdated)  return;
            
            CGRect scrolledBoundsForContainerView = containerView.bounds;
            if (scrollView.contentOffset.y <= yscrollOffset)
            {
                
                scrolledBoundsForContainerView.origin.y = scrollView.contentOffset.y ;
                containerView.bounds = scrolledBoundsForContainerView;
                
                //Reset offset for all tabs
                if (scrollView.contentOffset.y <= 0) [self resetOffsetAllTabs];
                
                
                CGFloat y = -scrollView.contentOffset.y;
                CGFloat alphaLevel = 1;
                CGFloat BLUR_MAX_Y = IMAGE_SCROLL_OFFSET_Y + CGRectGetHeight(topImageV.frame) - CGRectGetMinY(optionsV.frame);
                if (fabs(y) < BLUR_MAX_Y)
                {
                    alphaLevel = (BLUR_MAX_Y - fabs(y))/(BLUR_MAX_Y);
                }
                else
                {
                    alphaLevel = 0;
                }
                
                //[screenTitleL setAlpha:alphaLevel];
                if (y > 0)
                {
                    mainImgV.frame = CGRectInset(cachedImageViewSize, 0, -y/2);
                    mainImgV.center = CGPointMake(mainImgV.center.x, mainImgV.center.y + y/2);
                }
                else
                {
                    mainImgV.frame = [UpdateFrame setPositionForView:mainImgV usingPositionY:y];
                }
                return;
            }
            
            scrolledBoundsForContainerView.origin.y = yscrollOffset ;
            containerView.bounds = scrolledBoundsForContainerView;
            
            [self setScrollContentOffset:CGPointMake(0, scrollView.contentOffset.y - yscrollOffset)  ];
        }
    }
}


#pragma mark - Events

- (IBAction)backBPressed:(UIButton *)sender
{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(oncancel_GatewayPayment)])
    {
        [self.delegate oncancel_GatewayPayment] ;
    }
}

- (IBAction)makePaymentBPressed:(UIButton *)sender{
    BOOL isValid = NO;
    if([screenTitleL.text isEqualToString:[PAYMENT_MODE_NETBANKING uppercaseString]]){
        isValid = [self isValidNetBankingDetails];
    }
    else{
        isValid = [self isValidCardDetails];
    }
    if(isValid){
        [self hudShowWithMessage:@"Loading"];
        [cartC generateOrderId];
    }    
}

- (IBAction)saveCardInfoBPressed:(UIButton *)sender{
    sender.selected = !sender.selected;
    
    // This condition for when user remove the saved button selection the dictonary not added to saved card list.
    if (sender.selected) {
        isSaveAddressTag = YES;
    }else{
        isSaveAddressTag = NO;
    }
    
}

- (IBAction)otherBanksBPressed:(UIButton *)sender{
    [self modaltranslateView:selectBankCV onScreen:YES];
}

#pragma mark - UITextField delegates

- (UIImage *) getCardSchemeImage : (NSString *) scheme
{
    NSString *fileName = @"";
    if([scheme  isEqualToString: @"VISA"])
        fileName = @"visa";
    else if([scheme  isEqualToString: @"DISCOVER"])
        fileName = @"discover";
    else if([scheme  isEqualToString: @"MASTERCARD"])
        fileName = @"mastercard";
    else if([scheme  isEqualToString: @"AMEX"])
        fileName = @"amex";
    if ([fileName length] > 0) {
        return [UIImage imageNamed:fileName];
    }
    else
    {
        return Nil;
    }
}

- (IBAction)textFieldChanged:(id)sender
{
    NSInteger maxLen = 50;
    UITextField *txtF = (UITextField *)sender;
    
    if (txtF == cardNumberTxtF)
    {
        txtF.text = [self displayCardNumber];
        maxLen = 19;

        cardscheme = [CTSUtility fetchCardSchemeForCardNumber: [self reformatCardNumber]];
        cardSchemeImage.image = [CTSUtility getSchmeTypeImage:[self reformatCardNumber] forParentView:self.view];
        
        if (txtF.text.length == maxLen)
        {
            [cardHolderNameTxtF becomeFirstResponder];
        }
    }
    else if (txtF == cardHolderNameTxtF) {
        NSString *text = [txtF text];
        if ([text length] > 0)
        {
            txtF.text = [txtF.text capitalizedString];
        }
    }
    else if (txtF == expiryMonthTxtF)
    {
        maxLen = 2;
        if (txtF.text.length == maxLen)
        {
            [expiryYearTxtF becomeFirstResponder];
        }
    }
    else if (txtF == expiryYearTxtF)
    {
        maxLen = 4;
        if (txtF.text.length == maxLen)
        {
            [CVVNumberTxtF becomeFirstResponder];
        }
    }
    
    else if (txtF == CVVNumberTxtF)
    {
        maxLen = ([cardscheme isEqualToString:@"AMEX"]) ? 4 : 3;
        if (txtF.text.length == maxLen)
        {
            [self doneWithNumberPad];
        }
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField*)textField
{
    if (textField == cardNumberTxtF) { [cardHolderNameTxtF becomeFirstResponder];}
    if (textField == cardHolderNameTxtF) { [expiryMonthTxtF becomeFirstResponder];}
    if (textField == expiryMonthTxtF) { [expiryYearTxtF becomeFirstResponder];}
    if (textField == expiryYearTxtF) { [CVVNumberTxtF becomeFirstResponder];}
    if (textField.tag == CVV_PROMPT_TEXT_TAG ) {
        [textField resignFirstResponder];
        [self makePaymentBPressed:Nil];
    }
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if (textField.tag == CVV_PROMPT_TEXT_TAG)
    {
//        CGPoint pnt = [savedCardTblV convertPoint:textField.bounds.origin fromView:textField];
//        NSIndexPath* path = [savedCardTblV indexPathForRowAtPoint:pnt];
//        [savedCardTblV scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else if (textField == expiryMonthTxtF)
    {
        if (IS_IPHONE4) {
            isEditMoving = YES;
            [mainscrollView setContentOffset:expiryV.frame.origin animated:YES];
        }
    }
    else if (textField == expiryYearTxtF)
    {
        if (IS_IPHONE4) {
            isEditMoving = YES;
            [mainscrollView setContentOffset:expiryV.frame.origin animated:YES];
        }
    }
    else if (textField == CVVNumberTxtF)
    {
        if (IS_IPHONE4 || IS_IPHONE5) {
            isEditMoving = YES;
            [mainscrollView setContentOffset:expiryV.frame.origin animated:YES];
        }
    }
    else if (textField == cardHolderNameTxtF)
    {
        if (IS_IPHONE4 || IS_IPHONE5) {
            isEditMoving = YES;
            [mainscrollView setContentOffset:cardHolderNameV.frame.origin animated:YES];
        }
    }
    else{
        isEditMoving = YES;
        [mainscrollView setContentOffset:CGPointZero animated:YES];
    }
}

- (BOOL)textField:(UITextField*)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString*)string
{
    BOOL status;
    
    if (textField == cardNumberTxtF)
    {
        status = [self validateCardNumber:textField replacementString:string shouldChangeCharactersInRange:range];
        return status;
    }
    if (textField == expiryMonthTxtF)
    {
        status = [self validateExpiryMonth:expiryMonthTxtF replacementString:string shouldChangeCharactersInRange:range];
        return status;
        
    }
    if (textField == expiryYearTxtF)
    {
        status = [self validateExpiryYear:expiryYearTxtF replacementString:string shouldChangeCharactersInRange:range];
        return status;
        
    }
    if (textField == CVVNumberTxtF)
    {
        status = [self validateCVVNumber:textField cardScheme:cardscheme replacementString:string shouldChangeCharactersInRange:range];
        return status;
        
    }
    return YES;
}

#pragma mark - ToolBar Events

-(void)cancelNumberPad
{
    [mainscrollView setContentOffset:CGPointZero animated:NO];
    [UIResponder dismissKeyboard];
}

-(void)doneWithNumberPad
{
    [mainscrollView setContentOffset:CGPointZero animated:NO];
    [UIResponder dismissKeyboard];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"embed_preferredbanks"]){
        preferredBankVC = (PreferredBanksViewController *)segue.destinationViewController;
    }
    else if([segue.identifier isEqualToString:@"embed_otherbanks"]){
        selectBankVC = (ChooseBankViewController *)segue.destinationViewController;
    }
}

#pragma mark - Payment Gateway

- (BOOL) isPreferredBank : (NSDictionary *) dict
{
    BOOL isPreferred = NO;
    for(NSDictionary *item in preferredBanks){
        if([[ReadData stringValueFromDictionary:item forKey:KEY_BANK_ISSUERCODE] isEqualToString:[ReadData stringValueFromDictionary:dict forKey:KEY_BANK_ISSUERCODE]])
        {
            isPreferred = YES;
            break;
        }
    }
    return isPreferred;
}

- (void) setupPreferredBanks
{
    preferredBanks = [NSMutableArray array];
    NSMutableDictionary *dict;
    
    dict = [NSMutableDictionary dictionary];
    [dict setObject:@"AXIS Bank" forKey:KEY_BANK_NAME];
    [dict setObject:@"CID002" forKey:KEY_BANK_ISSUERCODE];
    [preferredBanks addObject:dict];
    
    dict = [NSMutableDictionary dictionary];
    [dict setObject:@"HDFC Bank" forKey:KEY_BANK_NAME];
    [dict setObject:@"CID010" forKey:KEY_BANK_ISSUERCODE];
    [preferredBanks addObject:dict];
    
    dict = [NSMutableDictionary dictionary];
    [dict setObject:@"ICICI Bank" forKey:KEY_BANK_NAME];
    [dict setObject:@"CID001" forKey:KEY_BANK_ISSUERCODE];
    [preferredBanks addObject:dict];
    
    dict = [NSMutableDictionary dictionary];
    [dict setObject:@"SBI Bank" forKey:KEY_BANK_NAME];
    [dict setObject:@"CID005" forKey:KEY_BANK_ISSUERCODE];
    [preferredBanks addObject:dict];
    
    dict = [NSMutableDictionary dictionary];
    [dict setObject:@"Canara Bank" forKey:KEY_BANK_NAME];
    [dict setObject:@"CID051" forKey:KEY_BANK_ISSUERCODE];
    [preferredBanks addObject:dict];
    
    dict = [NSMutableDictionary dictionary];
    [dict setObject:@"Central Bank of India" forKey:KEY_BANK_NAME];
    [dict setObject:@"CID023" forKey:KEY_BANK_ISSUERCODE];
    [preferredBanks addObject:dict];
    
    dict = [NSMutableDictionary dictionary];
    [dict setObject:@"Indian Overseas Bank" forKey:KEY_BANK_NAME];
    [dict setObject:@"CID027" forKey:KEY_BANK_ISSUERCODE];
    [preferredBanks addObject:dict];
    
    dict = [NSMutableDictionary dictionary];
    [dict setObject:@"YES Bank" forKey:KEY_BANK_NAME];
    [dict setObject:@"CID004" forKey:KEY_BANK_ISSUERCODE];
    [preferredBanks addObject:dict];
    
    dict = [NSMutableDictionary dictionary];
    [dict setObject:@"IDBI Bank" forKey:KEY_BANK_NAME];
    [dict setObject:@"CID011" forKey:KEY_BANK_ISSUERCODE];
    [preferredBanks addObject:dict];
    
}

- (void) setupOtherBanks : (NSArray *) pgBankingArray
{
    NSMutableArray * tmpArray = [NSMutableArray array];
    for(NSDictionary *item in pgBankingArray)
    {
        if(![self isPreferredBank:item]){
            [tmpArray addObject:item];
        }
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:KEY_BANK_NAME ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    otherBanks = [tmpArray sortedArrayUsingDescriptors:sortDescriptors];
    [selectBankVC loadlistForOtherBanks:otherBanks];
}

- (void) initPG
{
    CTSKeyStore *keyStore = [[CTSKeyStore alloc] init];
    keyStore.signinId = SignInId;
    keyStore.signinSecret = SignInSecretKey;
    keyStore.signUpId = SubscriptionId;
    keyStore.signUpSecret = SubscriptionSecretKey;
    keyStore.vanity = VanityUrl;
    
    [CitrusPaymentSDK initializeWithKeyStore:keyStore environment:CTSEnvProduction];
    
    [CitrusPaymentSDK enableDEBUGLogs];
    
    //[CitrusPaymentSDK enableLoader];
    
    [CitrusPaymentSDK setLoaderColor:[UIColor brandOrangeDisplayColor]];
    
    
    authLayer = [CTSAuthLayer fetchSharedAuthLayer];
    proifleLayer = [CTSProfileLayer fetchSharedProfileLayer];
    paymentLayer = [CTSPaymentLayer fetchSharedPaymentLayer];
    
    contactInfo = [[CTSContactUpdate alloc] init];
    contactInfo.firstName = [appD getLoggedInUserFirstName];
    contactInfo.lastName = [appD getLoggedInUserLastName];
    contactInfo.email = [appD getLoggedInUserEmailAddress];
    contactInfo.mobile = [appD getLoggedInUserMobileNumber];
    
    addressInfo = [[CTSUserAddress alloc] init];
//    addressInfo.city = @"Mumbai";
//    addressInfo.country = @"India";
//    addressInfo.state = @"Maharashtra";
//    addressInfo.street1 = @"Street1";
//    addressInfo.street2 = @"Street2";
//    addressInfo.zip = @"400063";
    
    
    //[self bindUser];
    
    [paymentLayer requestMerchantPgSettings:VanityUrl
                      withCompletionHandler:^(CTSPgSettings* pgSettings,NSError* error)
     {
         [self handleMerchantPgSettings:pgSettings error:error];
         
     }];
}
- (void)initPGFailedWithErrorCode : (NSString *) errorCode andMessage : (NSString *) transactMsg
{
}

- (void) handleMerchantPgSettings : (CTSPgSettings *) pgSettings error:(NSError *)error
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if(error == nil && error.code != ServerErrorWithCode)
        {
            [self setupOtherBanks:pgSettings.netBanking];
            
        }
        else
        {
            [self initPGFailedWithErrorCode: @"900"  andMessage: [error localizedDescription]];
        }
    });
}

- (void) bindUser
{
    
}

#pragma mark - Payment Methods

- (void) processPayment
{
    [paymentLayer requestSimpliPay:paymentType andParentViewController:self
                 completionHandler:^(CTSPaymentReceipt *paymentReceipt,
                                     NSError *error) {
                     [HUD hide:YES];
                     if (error) {
                         //NSLog(@"error %@", [error localizedDescription]);
                         if (self.delegate != nil && [self.delegate respondsToSelector:@selector(oncompleted_GatewayTransaction:)])
                         {
                             [self.delegate oncompleted_GatewayTransaction:NO] ;
                         }
                     }
                     else {
                         NSString * txnStatus = [[ReadData stringValueFromDictionary:paymentReceipt.toDictionary forKey:@"TxStatus"] uppercaseString];
                         if (self.delegate != nil && [self.delegate respondsToSelector:@selector(oncompleted_GatewayTransaction:)])
                         {
                             if (isSaveAddressTag) {
                                 [appD.sessionDelegate.cardDetailsArray addObject:cardsDetailsDictonary];
                             }
                            [self.delegate oncompleted_GatewayTransaction:[txnStatus isEqualToString:@"SUCCESS"]] ;
                         }
                     }
                 }];
    
}

-(void) processPayment_CreditCard
{
    CTSPaymentOptions *cardPayment = [CTSPaymentOptions  creditCardOption: [self reformatCardNumber] cardExpiryDate:[NSString stringWithFormat:@"%@/%@",expiryMonthTxtF.text, expiryYearTxtF.text]   cvv:CVVNumberTxtF.text];
    
    // Set your payment type here
    paymentType =  [PaymentType PGPayment:[NSString stringWithFormat:@"%.02f",cartC.payableAmount]
                                  billUrl: [cartC getZivaBillURL]
                            paymentOption:cardPayment
                                  contact:contactInfo
                                  address:addressInfo];
    
    //NSString *strDate = [NSString stringWithFormat:@"%@/%@", expiryMonthTxtF.text,expiryYearTxtF.text];
    
    //[cardsDetailsDictonary setValue:<#(nullable id)#> forKey:<#(nonnull NSString *)#>]
    [cardsDetailsDictonary setValue:cardNumberTxtF.text forKey:@"CNUMBER"];
    [cardsDetailsDictonary setValue:cardHolderNameTxtF.text forKey:@"CNAME"];
    [cardsDetailsDictonary setValue:expiryMonthTxtF.text forKey:@"EXPM"];
    [cardsDetailsDictonary setValue:expiryYearTxtF.text forKey:@"EXPY"];
    [cardsDetailsDictonary setValue:cartC.modeOfPayment forKey:@"CARDMODETYPE"];
    //[cardsDetailsDictonary setValue:cardscheme forKey:@"CARDSCHEME"];

    cardscheme = [CTSUtility fetchCardSchemeForCardNumber: [self reformatCardNumber]];
    cardSchemeImage.image = [CTSUtility getSchmeTypeImage:[self reformatCardNumber] forParentView:self.view];
    [cardsDetailsDictonary setValue:cardSchemeImage.image forKey:@"CARDSCHEMEIMG"];



   
   // NSLog(@"%@",cardsDetailsDictonary);
    
    [self processPayment];
}

-(void) processPayment_DebitCard
{
    CTSPaymentOptions *cardPayment =[CTSPaymentOptions  debitCardOption: [self reformatCardNumber] cardExpiryDate:[NSString stringWithFormat:@"%@/%@",expiryMonthTxtF.text, expiryYearTxtF.text]   cvv:CVVNumberTxtF.text];
    
    // Set your payment type here
    paymentType =  [PaymentType PGPayment:[NSString stringWithFormat:@"%.02f", cartC.payableAmount]
                                  billUrl: [cartC getZivaBillURL]
                            paymentOption:cardPayment
                                  contact:contactInfo
                                  address:addressInfo];
    
    //NSString *strDate = [NSString stringWithFormat:@"%@/%@", expiryMonthTxtF.text,expiryYearTxtF.text];
    [cardsDetailsDictonary setValue:cardNumberTxtF.text forKey:@"CNUMBER"];
    [cardsDetailsDictonary setValue:cardHolderNameTxtF.text forKey:@"CNAME"];
    [cardsDetailsDictonary setValue:expiryMonthTxtF.text forKey:@"EXPM"];
    [cardsDetailsDictonary setValue:expiryYearTxtF.text forKey:@"EXPY"];
    [cardsDetailsDictonary setValue:cartC.modeOfPayment forKey:@"CARDMODETYPE"];
   // [cardsDetailsDictonary setValue:cardscheme forKey:@"CARDSCHEME"];
    cardscheme = [CTSUtility fetchCardSchemeForCardNumber: [self reformatCardNumber]];
    cardSchemeImage.image = [CTSUtility getSchmeTypeImage:[self reformatCardNumber] forParentView:self.view];
    [cardsDetailsDictonary setValue:cardSchemeImage.image forKey:@"CARDSCHEMEIMG"];



   // NSLog(@"%@",cardsDetailsDictonary);

    
    [self processPayment];
}


-(void) processPayment_NetBanking
{
    CTSPaymentOptions *bankPayment = [CTSPaymentOptions netBankingOption: selectedBankName issuerCode:selectedBankCode];
    
    // Set your payment type here
    paymentType =  [PaymentType PGPayment:[NSString stringWithFormat:@"%.02f", cartC.payableAmount]
                   billUrl: [cartC getZivaBillURL]
             paymentOption:bankPayment
                   contact:contactInfo
                   address:addressInfo];
    
    [self processPayment];
}



#pragma mark - Utility Methods

- (void) showPGErrorMessage : (NSString *)message
{
    UIAlertView *errorNotice = [[UIAlertView alloc] initWithTitle:ALERT_TITLE_ERROR message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorNotice show];
}

- (void) showErrorMessage : (int)errorCode
{
    NSString *message = @"";
    switch (errorCode) {
        case NETWORKERRORCODE:
            message = NETWORKERROR;
            break;
        case INCORRECT_CARD_NUMBER:
            message = @"Please enter correct 16 digit card number.";
            break;
        case INCORRECT_EXPIRY_DATE:
            message = @"Please enter correct card expiry date.";
            break;
        case INCORRECT_CVV:
            message = @"Please enter correct CVV.";
            break;
        case INCORRECT_CARDHOLDER_NAME:
            message = @"Please enter cardholder's name.";
            break;
        case INCORRECT_STORED_CARD:
            message = @"Please choose card.";
            break;
        case INCORRECT_BANK:
            message = @"Please choose bank.";
            break;
        default:
            break;
    }
    UIAlertView *errorNotice = [[UIAlertView alloc] initWithTitle:ALERT_TITLE_ERROR message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorNotice show];
}

-(BOOL) isValidCardDetails
{
    [UIResponder dismissKeyboard];
    [mainscrollView setContentOffset:CGPointZero animated:YES];
    if (![CTSUtility validateCardNumber: [self reformatCardNumber]])
    {
        [self showErrorMessage:INCORRECT_CARD_NUMBER];
        return NO;
    }
    if ([appD isEmptyString:cardscheme])
    {
        [self showErrorMessage:INCORRECT_CARD_NUMBER];
        return NO;
    }
    if ([appD isEmptyString:cardHolderNameTxtF.text])
    {
        [self showErrorMessage:INCORRECT_CARDHOLDER_NAME];
        return  NO;
    }
    if (![CTSUtility validateExpiryDate:[NSString stringWithFormat:@"%@/%@", expiryMonthTxtF.text,expiryYearTxtF.text]])
    {
        [self showErrorMessage:INCORRECT_EXPIRY_DATE];
        return NO;
    }
    if (![CTSUtility validateCVV:CVVNumberTxtF.text cardNumber:[self reformatCardNumber]])
    {
        [self showErrorMessage:INCORRECT_CVV];
        return NO;
    }
    if (saveCardInfoB.selected) {
        NSString *trimmedString=[cardNumberTxtF.text substringFromIndex:MAX((int)[cardNumberTxtF.text length]-4, 0)];
        tokencardName = [NSString stringWithFormat:@"**** **** **** %@",trimmedString];
    }
    else{
        tokecardscheme = @"";
    }
    
    return YES;
}

-(BOOL) isValidNetBankingDetails
{
    [UIResponder dismissKeyboard];
    [mainscrollView setContentOffset:CGPointZero animated:YES];
    if ([appD isEmptyString:selectedBankCode] || [appD isEmptyString:selectedBankName] )
    {
        [self showErrorMessage:INCORRECT_BANK];
        return NO;
    }
    
    return YES;
}

- (NSString *) reformatCardNumber
{
    return [cardNumberTxtF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
}

- (NSString *) displayCardNumber
{
    NSString *filtered = [[cardNumberTxtF text] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *formatted = @"";
    
    if ([filtered length] > 12)
    {
        formatted = [NSString stringWithFormat: @"%@ %@ %@ %@", [filtered substringWithRange: NSMakeRange(0, 4)], [filtered substringWithRange: NSMakeRange(4, 4)], [filtered substringWithRange: NSMakeRange(8, 4)],[filtered substringWithRange: NSMakeRange(12, [filtered length]-12)]];
    }
    else if ([filtered length] > 8)
    {
        formatted = [NSString stringWithFormat: @"%@ %@ %@", [filtered substringWithRange: NSMakeRange(0, 4)], [filtered substringWithRange: NSMakeRange(4, 4)], [filtered substringWithRange: NSMakeRange(8, [filtered length]-8)]];
    }
    else if ([filtered length] > 4)
    {
//        NSLog(@"%lu",(unsigned long)[filtered length]);
//        NSLog(@"%@",[filtered substringWithRange: NSMakeRange(0, 4)]);
        formatted = [NSString stringWithFormat: @"%@ %@", [filtered substringWithRange: NSMakeRange(0, 4)], [filtered substringWithRange: NSMakeRange(4, [filtered length]-4)]];
    }
    else
    {
        formatted = filtered;
    }
    
    return  formatted;
}


- (BOOL)validateCardNumber:(UITextField*)textField replacementString:(NSString*)string shouldChangeCharactersInRange:(NSRange)range
{
    // 16 digits + 3 spaces
    NSInteger textfieldLength = textField.text.length - range.length + string.length;
    NSCharacterSet* myCharSet = [NSCharacterSet characterSetWithCharactersInString:NUMERICS];
    for (int i = 0; i < [string length]; i++) {
        unichar c = [string characterAtIndex:i];
        if ([myCharSet characterIsMember:c]) {
            if (textfieldLength > 19)
            {
                return NO;
            }
            else
            {
                return YES;
            }
        }
        else
        {
            return NO;
        }
    }
    return YES;
}


- (BOOL)validateExpiryMonth:(UITextField*)textField replacementString:(NSString*)string shouldChangeCharactersInRange:(NSRange)range
{
    // 2 digits
    NSInteger textfieldLength = textField.text.length - range.length + string.length;
    NSCharacterSet* myCharSet =[NSCharacterSet characterSetWithCharactersInString:NUMERICS];
    for (int i = 0; i < [string length]; i++) {
        unichar c = [string characterAtIndex:i];
        if ([myCharSet characterIsMember:c]) {
            if (textfieldLength > 2)
            {
                return NO;
            }
            else
            {
                return YES;
            }
        }
        else
        {
            return NO;
        }
    }
    return YES;
}

- (BOOL)validateExpiryYear:(UITextField*)textField replacementString:(NSString*)string shouldChangeCharactersInRange:(NSRange)range
{
    // 4 digits
    NSInteger textfieldLength = textField.text.length - range.length + string.length;
    NSCharacterSet* myCharSet =[NSCharacterSet characterSetWithCharactersInString:NUMERICS];
    for (int i = 0; i < [string length]; i++) {
        unichar c = [string characterAtIndex:i];
        if ([myCharSet characterIsMember:c]) {
            if (textfieldLength > 4)
            {
                return NO;
            }
            else
            {
                return YES;
            }
        }
        else
        {
            return NO;
        }
    }
    return YES;
}

- (BOOL)validateCVVNumber:(UITextField*)textField cardScheme:(NSString*)scheme replacementString:(NSString*)string shouldChangeCharactersInRange:(NSRange)range
{
    // CVV validation
    // if amex allow 4 digits, if non amex only 3 should allowed.
    NSInteger textfieldLength = textField.text.length - range.length + string.length;
    NSCharacterSet* myCharSet =
    [NSCharacterSet characterSetWithCharactersInString:NUMERICS];
    for (int i = 0; i < [string length]; i++) {
        unichar c = [string characterAtIndex:i];
        if ([myCharSet characterIsMember:c]) {
            if ([scheme caseInsensitiveCompare:@"amex"] == NSOrderedSame) {
                if (textfieldLength > 4) {
                    return NO;
                } else {
                    return YES;
                }
            } else if ([scheme caseInsensitiveCompare:@"amex"] !=
                       NSOrderedSame) {
                if (textfieldLength > 3) {
                    return NO;
                } else {
                    return YES;
                }
            }
            
        } else {
            return NO;
        }
    }
    return YES;
}


#pragma mark tableview methods.
//#pragma mark - Table view data source

-(CGFloat) calculateheightForRow:(NSInteger)row{
    return ROW_HEIGHT;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [list count] ;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self calculateheightForRow:indexPath.row];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //    SavedCardCell *cell = (SavedCardCell *)[tableView dequeueReusableCellWithIdentifier:@"showCardDetailscell" forIndexPath:indexPath];
    
    static NSString *MyIdentifier = @"showCardDetailscell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    NSDictionary *dict = list[indexPath.row];
    
    UILabel *cardlastnumber = (UILabel*)[cell viewWithTag:1111];
    NSString *str = [dict valueForKey:@"CNUMBER"];
    NSString *lastStr =  [str substringFromIndex:[str length] - 4];
   
    cardlastnumber.text = [NSString stringWithFormat:@"Credit Card ( %@ )",[str substringFromIndex:[str length] - 4]];
    //cardlastnumber.text = [dict valueForKey:@"CNUMBER"];
    
    // Card number.
    UILabel *cardnumber = (UILabel*)[cell viewWithTag:2222];
    cardnumber.text = [NSString stringWithFormat:@" XXXX XXXX XXXX %@", lastStr];
    //cardnumber.text = [dict valueForKey:@"CNUMBER"];
    
    // For Card name
    UILabel *cardname = (UILabel*)[cell viewWithTag:3333];
    cardname.text = [dict valueForKey:@"CNAME"];
    
    // For Month
    UITextField *mmT = (UITextField*)[cell viewWithTag:4444];
    mmT.text = [dict valueForKey:@"EXPM"];
    
    // For Year.
    UITextField *yyT = (UITextField*)[cell viewWithTag:5555];
    yyT.text = [dict valueForKey:@"EXPY"];
    
    /*
    NSString * dateStr = [NSString stringWithFormat:@"%@/%@",[dict valueForKey:@"EXPM"],[dict valueForKey:@"EXPY"]];
    UILabel *dateL = (UILabel*)[cell viewWithTag:4444];
    //dateL.text = dateStr;
    dateL.text = [NSString stringWithFormat:@"VALID THRU  %@",dateStr];
    */
//    cardscheme = [CTSUtility fetchCardSchemeForCardNumber: [self reformatCardNumber]];
//    cardSchemeImage.image = [CTSUtility getSchmeTypeImage:[self reformatCardNumber] forParentView:self.view];
//    [cardsDetailsDictonary setValue:cardSchemeImage.image forKey:@"CARDSCHEMEIMG"];

    // For images
    UIImageView *imgV = (UIImageView*)[cell viewWithTag:6666];
    //cardscheme = [CTSUtility fetchCardSchemeForCardNumber: [self reformatCardNumber]];
    imgV.image = [dict valueForKey:@"CARDSCHEMEIMG"];
   // imgV.image = [CTSUtility getSchmeTypeImage:[dict valueForKey:@"CARDSCHEMEIMG"] forParentView:self.view];

    
    // For view rounde rect
    UIView *cornerView = (UIView*)[cell viewWithTag:7777];
    cornerView.layer.cornerRadius = 10;
    cornerView.layer.masksToBounds = YES;
//    view.layer.cornerRadius = 5;
//    view.layer.masksToBounds = YES;
    //[cell configureDataForCell :item];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSDictionary *getDict = list[indexPath.row];
    //NSLog(@"%@", getDict);
    

    if ([[getDict valueForKey:@"CARDMODETYPE"] isEqualToString:PAYMENT_MODE_CREDITCARD]) {
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(onapply_PaymentMode:)])
        {
            [self.delegate onapply_PaymentMode:PAYMENT_MODE_CREDITCARD];
            //[self.delegate onapply_PaymentMode : [ReadData stringValueFromDictionary:item forKey:KEY_PAYMODE_TITLE]] ;
        }
        
    } else if ([[getDict valueForKey:@"CARDMODETYPE"] isEqualToString:PAYMENT_MODE_DEBITCARD]){
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(onapply_PaymentMode:)])
        {
            [self.delegate onapply_PaymentMode:PAYMENT_MODE_DEBITCARD];
            //[self.delegate onapply_PaymentMode : [ReadData stringValueFromDictionary:item forKey:KEY_PAYMODE_TITLE]] ;
        }

    }
    
 /*
    if ([[getDict valueForKey:@"CARDMODETYPE"] isEqualToString:PAYMENT_MODE_CREDITCARD]) {
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(onapply_PaymentMode:)])
        {
            [self.delegate onapply_PaymentMode:PAYMENT_MODE_CREDITCARD];
            //[self.delegate onapply_PaymentMode : [ReadData stringValueFromDictionary:item forKey:KEY_PAYMODE_TITLE]] ;
        }
        
    } else if ([[getDict valueForKey:@"CARDMODETYPE"] isEqualToString:@"Debit Card"]){
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(onapply_PaymentMode:)])
        {
            [self.delegate onapply_PaymentMode:PAYMENT_MODE_DEBITCARD];
            //[self.delegate onapply_PaymentMode : [ReadData stringValueFromDictionary:item forKey:KEY_PAYMODE_TITLE]] ;
        }
        
    } */
    
    cardNumberTxtF.text = [getDict valueForKey:@"CNUMBER"];
    cardHolderNameTxtF.text = [getDict valueForKey:@"CNAME"];
    expiryMonthTxtF.text = [getDict valueForKey:@"EXPM"];
    expiryYearTxtF.text = [getDict valueForKey:@"EXPY"];
    
   // cardSchemeImage.image = [CTSUtility getSchmeTypeImage:[getDict valueForKey:@"CARDSCHEME"] forParentView:self.view];
    
    cardscheme = [CTSUtility fetchCardSchemeForCardNumber: [self reformatCardNumber]];
    cardSchemeImage.image = [CTSUtility getSchmeTypeImage:[self reformatCardNumber] forParentView:self.view];
    
    /*
    
    if ([[getDict valueForKey:@"CARDMODETYPE"] isEqualToString:@"Credit Card"]) {
        addCardInfoV.hidden = NO;
        savedCardsInfoV.hidden = YES;
        netBankingInfoV.hidden = YES;
        footerV.hidden = NO;
        // [self translateView:addCardInfoV onScreen:YES];

    }
    else{
        addCardInfoV.hidden = NO;
        savedCardsInfoV.hidden = YES;
        netBankingInfoV.hidden = YES;
        footerV.hidden = NO;
       // [self translateView:addCardInfoV onScreen:YES];

    }
   */
}



@end

