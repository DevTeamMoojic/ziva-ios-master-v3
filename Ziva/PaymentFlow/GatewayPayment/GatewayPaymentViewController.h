//
//  GatewayPaymentViewController.h
//  Ziva
//
//  Created by Bharat on 16/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GatewayPaymentDelegate <NSObject>

@optional

- (void) oncancel_GatewayPayment;

- (void) oncompleted_GatewayTransaction : (BOOL) status;

- (void) onapply_PaymentMode : (NSString *) mode;


@end

@interface GatewayPaymentViewController : UIViewController

@property (nonatomic, weak) id<GatewayPaymentDelegate> delegate;



- (void) initiatePayment : (NSString *) mode;

- (void) preferredBankSelected : (NSInteger) index;

- (void) dismissBankSelector ;

- (void) otherBankSelected : (NSInteger) index;



@end
