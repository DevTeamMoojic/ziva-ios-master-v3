//
//  ProductDetailViewController.m
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "CartSummaryViewController.h"
#import "GalleryViewController.h"
#import "ProductDetailViewController.h"

@interface ProductDetailViewController ()<APIDelegate,MBProgressHUDDelegate,CartSummaryDelegate>
{
    MBProgressHUD *HUD;
    
    __weak IBOutlet UIView *optionsV;    
    __weak IBOutlet UILabel *screenTitleL;    
    
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIView *topImageV;
    __weak IBOutlet UIImageView *mainImgV;
    
    __weak IBOutlet UIScrollView *scrollviewOnTop;
    
    __weak IBOutlet UIScrollView *tabcontentscrollView;
    __weak IBOutlet UIScrollView *mainscrollView;
    
    __weak IBOutlet UIView *addToCartV;
    __weak IBOutlet UIImageView *quantityLessImgV;
    __weak IBOutlet UILabel *quantityL;
    __weak IBOutlet UIImageView *quantityAddImgV;
    
    __weak IBOutlet UILabel *titleL;
    
    __weak IBOutlet UIView *pricingV;
    __weak IBOutlet UILabel *offerPriceL;
    __weak IBOutlet UILabel *originalPriceL;
    
    __weak IBOutlet UIView *seperatorV;
    
    __weak IBOutlet UILabel *descriptionL;
    
    __weak IBOutlet UIView *additionalInfoV;
    __weak IBOutlet UILabel *sellerNameL;
    __weak IBOutlet UILabel *deliveryPeriodL;
    __weak IBOutlet UILabel *deliveryInformationL;
    
    __weak IBOutlet UIView *cartsummaryCV;
    __weak CartSummaryViewController *cartsummaryVC;
    
    CGRect cachedImageViewSize;
    CGFloat yscrollOffset;
    
    BOOL isBackFromOtherView;
    BOOL isContentSizeUpdated;
    BOOL isUpdating;
    
    NSUInteger pageIndex;
    NSInteger pageWidth;
    NSInteger pageHeight;

    CGFloat contentHeight;
    
    NSString *record_Id;
    NSDictionary *dataDict;

    AppDelegate *appD;
    MyCartController *cartC;
    
    NSInteger quantity;
    NSMutableArray *ImagesArray;

}
@end

@implementation ProductDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;
    [cartC initForProducts];
    
    quantity = 0;
    [self setupLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!isBackFromOtherView){
        cachedImageViewSize = mainImgV.frame;
    }
    else{
        isBackFromOtherView = NO;
    }
    quantity = [cartC getQuantityInCart:record_Id];
    [self updateQuantityHeading];
    [cartsummaryVC updateSummary];
}

#pragma mark - Public Methods

- (void) loadDetailView : (NSString *) recordId
{
    record_Id = recordId;
}

#pragma mark - Layout

-(void) resetLayout;
{
    mainscrollView.contentOffset = CGPointZero;
    contentHeight = CGRectGetHeight(self.view.bounds);
    
}
- (void) setupLayout
{
    [cartsummaryVC setActionOnCart:CART_ACTION_PURCHASE];
    [cartsummaryVC updateSummary];
    
    [self updateQuantityHeading];
    
    pageWidth = CGRectGetWidth(self.view.bounds);
    
    quantityAddImgV.layer.borderWidth = quantityLessImgV.layer.borderWidth = 0.5f;
    quantityAddImgV.layer.borderColor= quantityLessImgV.layer.borderColor= [UIColor borderDisplayColor].CGColor;
    
    quantityL.text = @"0";
    
    [self.view addGestureRecognizer:scrollviewOnTop.panGestureRecognizer];
    [mainscrollView removeGestureRecognizer:mainscrollView.panGestureRecognizer];
    
    //Position elements
    {
        topImageV.frame = [UpdateFrame setSizeForView:topImageV usingHeight:[ResizeToFitContent getHeightForDetailViewImage:self.view.frame]];
        
        yscrollOffset = CGRectGetHeight(topImageV.frame) - IMAGE_SCROLL_OFFSET_Y - CGRectGetHeight(optionsV.frame);
        mainImgV.frame = topImageV.frame ;
        
        containerView.frame = [UpdateFrame setSizeForView:containerView usingHeight:(CGRectGetHeight(self.view.bounds) + yscrollOffset)];
        
        tabcontentscrollView.frame= [UpdateFrame setPositionForView:tabcontentscrollView usingPositionY: CGRectGetMaxY(topImageV.frame)];
        tabcontentscrollView.frame = [UpdateFrame setSizeForView:tabcontentscrollView usingHeight: CGRectGetHeight(containerView.frame) - CGRectGetMaxY(topImageV.frame)];
    }
    
    
    CGPoint scrollviewOrigin = scrollviewOnTop.frame.origin;
    scrollviewOnTop.scrollIndicatorInsets = UIEdgeInsetsMake(-scrollviewOrigin.y, 0, scrollviewOrigin.y, scrollviewOrigin.x);
    
    [self updateQuantityHeading];
    [self reloadList];
}

- (void) updateQuantityHeading
{
    cartsummaryCV.hidden = ([cartC.cartItems count] == 0);
    quantityL.text = [NSString stringWithFormat:@"%ld",(long) quantity];
}

#pragma mark - Methods

- (void) reloadList
{
    if ([InternetCheck isOnline])
    {
        [self hudShowWithMessage:@"Loading"];
        
        NSMutableArray *replacementArray = [NSMutableArray array];
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:@"RECORD_ID" forKey:@"key"];
        [paramDict setObject:record_Id forKey:@"value"];
        
        [replacementArray addObject:paramDict];
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_DETAIL_VIEW_PRODUCT Param:nil replacementStrings:replacementArray];        
    }
}

- (void) processResponse : (NSDictionary *) dict
{
    contentHeight = 0;
    dataDict = [ReadData dictionaryFromDictionary:dict forKey:RESPONSEKEY_DATA];
    if(dataDict)
    {
        CGFloat tmpHeight = 0;
        titleL.text = [ReadData stringValueFromDictionary:dataDict forKey:KEY_PRODUCT_TITLE];
        tmpHeight = [ResizeToFitContent getHeightForText:titleL.text usingFontType:FONT_SEMIBOLD FontOfSize:13 andMaxWidth:CGRectGetWidth(titleL.frame)];
        titleL.frame = [UpdateFrame setSizeForView:titleL usingHeight:tmpHeight];
        
        pricingV.frame = [UpdateFrame setPositionForView:pricingV usingPositionY:CGRectGetMaxY(titleL.frame) + 10];
        NSString *zivaPrice = [ReadData amountInRsFromDictionary:dataDict forKey:KEY_PRODUCT_PRICE];
        NSString *originalPrice = [ReadData amountInRsFromDictionary:dataDict forKey:KEY_PRODUCT_ORIGINALPRICE];
        offerPriceL.text = zivaPrice;
        NSAttributedString * title = [[NSAttributedString alloc] initWithString: originalPrice attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle)}];
        [originalPriceL setAttributedText:title];
        originalPriceL.hidden = [zivaPrice isEqualToString:originalPrice];
        
        seperatorV.frame = [UpdateFrame setPositionForView:seperatorV usingPositionY:CGRectGetMaxY(pricingV.frame) + 15];
        
        descriptionL.frame = [UpdateFrame setPositionForView:descriptionL usingPositionY:CGRectGetMinY(seperatorV.frame) + 15];
        descriptionL.text = [ReadData stringValueFromDictionary:dataDict forKey:KEY_PRODUCT_DESCRIPTION];
        
        tmpHeight = [ResizeToFitContent getHeightForText:descriptionL.text usingFontType:FONT_SEMIBOLD FontOfSize:12 andMaxWidth:CGRectGetWidth(descriptionL.frame)];
        descriptionL.frame = [UpdateFrame setSizeForView:descriptionL usingHeight:tmpHeight];
        
        additionalInfoV.frame = [UpdateFrame setPositionForView:additionalInfoV usingPositionY:(CGRectGetMaxY(descriptionL.frame) + 15)];
        
        NSDictionary *brandDict = [ReadData dictionaryFromDictionary:dataDict forKey:KEY_PRODUCT_SELLER];
        sellerNameL.text= [ReadData stringValueFromDictionary:brandDict forKey:KEY_NAME];
        deliveryPeriodL.text= [ReadData stringValueFromDictionary:dataDict forKey:KEY_PRODUCT_DELIVERY_PERIOD];
        deliveryInformationL.text= [ReadData stringValueFromDictionary:dataDict forKey:KEY_PRODUCT_DELIVERY_INFORMATION];
        cartC.isCashPaymentAllowed =[ReadData boolValueFromDictionary:dataDict forKey:KEY_PRODUCT_PAYMENTMODE];
        

        // ImageView
        NSString *url = [ReadData stringValueFromDictionary:dataDict forKey:KEY_IMAGE_URL];
        if (![url isEqualToString:@""])
        {
            NSURL *imageURL = [NSURL URLWithString:url];
            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageURL];
            [ mainImgV setImageWithURLRequest:urlRequest
                                 placeholderImage:Nil
                                          success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
             {
                 [ mainImgV setImage:image];
                 [UIView animateWithDuration:IMAGE_VIEW_TOGGLE
                                       delay:0 options:UIViewAnimationOptionCurveLinear
                                  animations:^(void)
                  {
                    
                      [ mainImgV setAlpha:1 ];
                  }
                                  completion:^(BOOL finished)
                  {
                  }];
                 
                 
                 
             }
                                          failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
             {
                 
             }];
        }
        
        contentHeight = CGRectGetMaxY(additionalInfoV.frame) + 80;
    }

    if(contentHeight <= CGRectGetHeight(mainscrollView.frame)){
        contentHeight = CGRectGetHeight(mainscrollView.bounds);
        mainscrollView.scrollEnabled = NO;
    }
    
    [self tabChanged];
    [HUD hide:YES];
}

- (void) tabChanged
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        CGPoint offset = [self getScrollContentOffset];
        offset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:offset animated:NO];
    }
    isContentSizeUpdated = NO;
}

#pragma mark - Cart Summary Delegate

- (void) takeActionOnCart{
    isBackFromOtherView = YES;
    [self performSegueWithIdentifier:@"showcart_products" sender:Nil];
}

#pragma mark - HUD & Delegate methods

- (void)hudShowWithMessage:(NSString *)message
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    HUD.dimBackground = YES;
    HUD.labelText = message;
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    [HUD show:YES];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}

#pragma mark - Events

- (IBAction)backBPressed:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)galleryBPressed:(UIButton *)sender{
    [self performSegueWithIdentifier:@"showgallery_product" sender:Nil];
}

- (IBAction)quantityAddBPressed:(UIButton *)sender{
    if(quantity < MAX_REPEAT_QUANTITY){
        [cartC addProducts:dataDict];
        [cartsummaryVC updateSummary];
        quantity++;
        [self updateQuantityHeading];
    }
}

- (IBAction)quantityLessBPressed:(UIButton *)sender{
    if(quantity > 0){
        [cartC lessProducts:dataDict];
        [cartsummaryVC updateSummary];
        quantity--;
        [self updateQuantityHeading];
    }
}

#pragma mark - Expand on Scroll animation

- (CGSize) setContentSize
{
    CGFloat height = CGRectGetHeight(mainImgV.frame) + contentHeight;
    return CGSizeMake(pageWidth, height);
}

- (void) setScrollContentOffset: (CGPoint) offset
{
    mainscrollView.contentOffset = offset;
}

- (CGPoint) getScrollContentOffset
{
    return [mainscrollView contentOffset];
}

- (void) resetOffsetAllTabs
{
    CGPoint offset = CGPointZero;
    [self setScrollContentOffset : offset];
}

-(void) updateContentSize : (CGPoint) contentOffset;
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        contentOffset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    }
    [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    
    isContentSizeUpdated = NO;
}

#pragma mark - Scroll Events

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == scrollviewOnTop)
    {
        {
            if (isContentSizeUpdated)  return;
            
            CGRect scrolledBoundsForContainerView = containerView.bounds;
            if (scrollView.contentOffset.y <= yscrollOffset)
            {
                
                scrolledBoundsForContainerView.origin.y = scrollView.contentOffset.y ;
                containerView.bounds = scrolledBoundsForContainerView;
                
                //Reset offset for all tabs
                if (scrollView.contentOffset.y <= 0) [self resetOffsetAllTabs];
                
                
                CGFloat y = -scrollView.contentOffset.y;
//                CGFloat alphaLevel = 1;
//                CGFloat BLUR_MAX_Y = IMAGE_SCROLL_OFFSET_Y + CGRectGetHeight(topImageV.frame) - CGRectGetMinY(optionsV.frame);
//                if (fabs(y) < BLUR_MAX_Y)
//                {
//                    alphaLevel = (BLUR_MAX_Y - fabs(y))/(BLUR_MAX_Y);
//                }
//                else
//                {
//                    alphaLevel = 0;
//                }
//                
//                //[screenTitleL setAlpha:alphaLevel];
                if (y > 0)
                {
                    mainImgV.frame = CGRectInset(cachedImageViewSize, 0, -y/2);
                    mainImgV.center = CGPointMake(mainImgV.center.x, mainImgV.center.y + y/2);
                }
                else
                {
                    mainImgV.frame = [UpdateFrame setPositionForView:mainImgV usingPositionY:y];
                }
                return;
            }
            
            scrolledBoundsForContainerView.origin.y = yscrollOffset ;
            containerView.bounds = scrolledBoundsForContainerView;
            
            [self setScrollContentOffset:CGPointMake(0, scrollView.contentOffset.y - yscrollOffset)  ];
        }
    }
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"showcartsummary_products"]){
        cartsummaryVC = (CartSummaryViewController *)segue.destinationViewController;
        cartsummaryVC.delegate = self;
    }
    else if([segue.identifier isEqualToString:@"showgallery_product"]){
        GalleryViewController *galleryVC = (GalleryViewController *)segue.destinationViewController;
        ImagesArray = [[NSMutableArray alloc]init];
        
        NSMutableArray *arrayImageKeys = [self GetProductImageUrlFromDictionary:dataDict];
        
        for (NSString *URL in arrayImageKeys)
        {
            NSString *url = [ReadData stringValueFromDictionary:dataDict forKey:URL];
            if (![url isEqualToString:@""]  && [url containsString:@"https://"])
            {
                [ImagesArray addObject:url];
            }
        }
        
        [galleryVC setImageForFullScreen:ImagesArray];

        
      //  [galleryVC setImageForFullScreen:mainImgV.image];
    }
}


#pragma mark - API

-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
    if(![ReadData isValidResponseData:response])
    {
        {
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
            [self.view setUserInteractionEnabled:YES];
            [HUD hide:YES];
        }
        return;
    }
    
    if ([apiName isEqualToString:API_DETAIL_VIEW_PRODUCT]) {
        [self processResponse:response];
    }
}


// This is a method to split the images URL from Main dictonary object.
-(NSMutableArray*)GetProductImageUrlFromDictionary:(NSMutableDictionary*)dictionary
{
    NSMutableArray* result = [NSMutableArray array];
    for (NSString* key in dictionary)
    {
        if ([key hasPrefix:@"Image"])
        {
            [result addObject:key];
        }
    }
    return result;
}

@end
