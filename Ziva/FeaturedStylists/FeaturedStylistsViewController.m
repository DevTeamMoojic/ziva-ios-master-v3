//
//  FeaturedStylistsViewController.m
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "FeaturedStylistsViewController.h"
#import "FeaturedStylisCell.h"
#import "FeaturedStylistsDetailViewController.h"

#define SEGUE_DETAIL_VIEW @""

@interface FeaturedStylistsViewController ()<APIDelegate,MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
    
    NSArray *list;
    
    __weak IBOutlet UIView *optionsV;
    __weak IBOutlet UILabel *screenTitleL;
    
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIView *topImageV;
    __weak IBOutlet UIImageView *mainImgV;
    
    __weak IBOutlet UIScrollView *scrollviewOnTop;
    
    __weak IBOutlet UIScrollView *tabcontentscrollView;
    
    __weak IBOutlet UILabel *noresultsL;
    __weak IBOutlet UITableView *listTblV;
    
    
    CGRect cachedImageViewSize;
    CGFloat yscrollOffset;
    
    BOOL isBackFromOtherView;
    BOOL isContentSizeUpdated;
    BOOL isUpdating;
    
    NSUInteger pageIndex;
    NSInteger pageWidth;
    NSInteger pageHeight;
    
    AppDelegate *appD;
    
    CGFloat contentHeight;
    CGFloat maxWidth;
    FeaturedStylistsDetailViewController *fsdv;
    
}
@end

@implementation FeaturedStylistsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    fsdv = [[FeaturedStylistsDetailViewController alloc]init];
    [self setupLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!isBackFromOtherView)
        cachedImageViewSize = mainImgV.frame;
    else
        isBackFromOtherView = NO;
}


#pragma mark - Layout

- (void) setupLayout
{
    maxWidth = CGRectGetWidth(self.view.bounds) - 35 - 75;
    
    pageWidth = CGRectGetWidth(self.view.bounds);
    
    [self.view addGestureRecognizer:scrollviewOnTop.panGestureRecognizer];
    [listTblV removeGestureRecognizer:listTblV.panGestureRecognizer];
    
    //Position elements
    {
        topImageV.frame = [UpdateFrame setSizeForView:topImageV usingHeight:[ResizeToFitContent getHeightForParallaxImage:self.view.frame]];
        
        yscrollOffset = CGRectGetHeight(topImageV.frame) - IMAGE_SCROLL_OFFSET_Y - CGRectGetHeight(optionsV.frame);
        mainImgV.frame = topImageV.frame ;
        
        containerView.frame = [UpdateFrame setSizeForView:containerView usingHeight:(CGRectGetHeight(self.view.bounds) + yscrollOffset)];
        
        tabcontentscrollView.frame= [UpdateFrame setPositionForView:tabcontentscrollView usingPositionY: CGRectGetMaxY(topImageV.frame)];
        tabcontentscrollView.frame = [UpdateFrame setSizeForView:tabcontentscrollView usingHeight: CGRectGetHeight(containerView.frame) - CGRectGetMaxY(topImageV.frame)];
    }
    
    CGPoint scrollviewOrigin = scrollviewOnTop.frame.origin;
    scrollviewOnTop.scrollIndicatorInsets = UIEdgeInsetsMake(-scrollviewOrigin.y, 0, scrollviewOrigin.y, scrollviewOrigin.x);
    
    [self reloadList];
}

#pragma mark - Methods

- (void) reloadList
{
    if ([InternetCheck isOnline]){
        [self hudShowWithMessage:@"Loading"];
        
        NSMutableArray *replacementArray = [NSMutableArray array];
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:@"PAGE_ID" forKey:@"key"];
        [paramDict setObject:@"0" forKey:@"value"];
        
        [replacementArray addObject:paramDict];
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_FEATURED_STYLISTS Param:Nil replacementStrings:replacementArray];
    }
}

-(CGFloat) calculateHeightOfList
{
    CGFloat height = 0;
    for(NSInteger ctr = 0; ctr < [list count]; ctr++){
        height += [self calculateheightForRow:ctr];
    }
    return height;
}


- (void) processResponse : (NSDictionary *) dict
{
    NSArray *tmpArray = [ReadData arrayFromDictionary:dict forKey:RESPONSEKEY_DATA];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:KEY_NAME ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    list = [tmpArray sortedArrayUsingDescriptors:sortDescriptors];
    
    [listTblV reloadData];
    [listTblV setContentOffset:CGPointZero animated:NO];
    
    listTblV.hidden = ([list count] == 0);
    noresultsL.hidden = !listTblV.hidden;
    
    contentHeight = [self calculateHeightOfList];
    if(contentHeight <= CGRectGetHeight(tabcontentscrollView.bounds)){
        contentHeight = CGRectGetHeight(tabcontentscrollView.bounds);
        listTblV.scrollEnabled = NO;
    }
    
    [self tabChanged];
    [HUD hide:YES];
}

- (void) tabChanged
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        CGPoint offset = [self getScrollContentOffset];
        offset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:offset animated:NO];
    }
    isContentSizeUpdated = NO;
}


#pragma mark - HUD & Delegate methods

- (void)hudShowWithMessage:(NSString *)message
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    HUD.dimBackground = YES;
    HUD.labelText = message;
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    [HUD show:YES];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}


#pragma mark - Events

- (IBAction)backBPressed:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Expand on Scroll animation

- (CGSize) setContentSize
{
    CGFloat height = CGRectGetHeight(mainImgV.frame) + contentHeight;
    return CGSizeMake(pageWidth, height);
}

- (void) setScrollContentOffset: (CGPoint) offset
{
    listTblV.contentOffset = offset;
}

- (CGPoint) getScrollContentOffset
{
    return [listTblV contentOffset];
}

- (void) resetOffsetAllTabs
{
    CGPoint offset = CGPointZero;
    [self setScrollContentOffset : offset];
}

-(void) updateContentSize : (CGPoint) contentOffset;
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    
    if (oldOffset.y >= yscrollOffset)
    {
        contentOffset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    }
    
    [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    
    isContentSizeUpdated = NO;
}

#pragma mark - Table view data source

- (NSString *) getStartingPriceDescription : (NSDictionary *) item{
    NSString *str = @"";
    str = [ReadData amountInRsFromDictionary:item forKey:KEY_STYLIST_STARTING_PRICE];
    str = [str stringByAppendingString:@" (Starting Price)"];
    if([[ReadData stringValueFromDictionary:item forKey:KEY_STYLIST_SERVICES] length] > 0 )
    {
        str = [str stringByAppendingString : @" for "];
        str = [str stringByAppendingString : [ReadData stringValueFromDictionary:item forKey:KEY_STYLIST_SERVICES]];
    }
    return str;
}

-(CGFloat) calculateheightForRow:(NSInteger)row{
    NSDictionary *item = list[row];
    CGFloat height = 15;
    height += 15;
    height += 13;
    height += 2;
    height += [ResizeToFitContent getHeightForText:[self getStartingPriceDescription:item] usingFontType:FONT_SEMIBOLD FontOfSize:11 andMaxWidth:maxWidth];
    height += 13;
    height += 15;
    return height;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [list count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self calculateheightForRow:indexPath.row];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FeaturedStylisCell *cell = (FeaturedStylisCell *)[tableView dequeueReusableCellWithIdentifier:@"featuredstylistcell" forIndexPath:indexPath];
    cell.lblMaxWidth = maxWidth;
    NSDictionary *item = list[indexPath.row];
    [cell configureDataForCell :item];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //NSDictionary *item = list[indexPath.row];
    //int i = indexPath.row;
    //[self performSegueWithIdentifier:@"ShowFeaturedStylistsDetail" sender:item];
    
    NSDictionary *item = list[indexPath.row];
    //NSString *recordId = [ReadData recordIdFromDictionary:item forKey:KEY_ID];
    //[fsdv loadDetailView:item];
    //[fsdv processResponseTest:item];
    //[fsdv Test:item];
    //[self performSegueWithIdentifier:@"ShowFeaturedStylistsDetail" sender:item];
    
    
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"ShowFeaturedStylistsDetail"]){
        
        isBackFromOtherView = YES;
        FeaturedStylistsDetailViewController *featureStylishsdetailVC = (FeaturedStylistsDetailViewController *)segue.destinationViewController;
        [featureStylishsdetailVC loadDetailView:sender];
    }
}


#pragma mark - Scroll Events

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == scrollviewOnTop)
    {
        {
            if (isContentSizeUpdated)  return;
            
            CGRect scrolledBoundsForContainerView = containerView.bounds;
            if (scrollView.contentOffset.y <= yscrollOffset)
            {
                
                scrolledBoundsForContainerView.origin.y = scrollView.contentOffset.y ;
                containerView.bounds = scrolledBoundsForContainerView;
                
                //Reset offset for all tabs
                if (scrollView.contentOffset.y <= 0) [self resetOffsetAllTabs];
                
                
                CGFloat y = -scrollView.contentOffset.y;
                CGFloat alphaLevel = 1;
                CGFloat BLUR_MAX_Y = IMAGE_SCROLL_OFFSET_Y + CGRectGetHeight(topImageV.frame) - CGRectGetMinY(optionsV.frame);
                if (fabs(y) < BLUR_MAX_Y)
                {
                    alphaLevel = (BLUR_MAX_Y - fabs(y))/(BLUR_MAX_Y);
                }
                else
                {
                    alphaLevel = 0;
                }
                
                //[screenTitleL setAlpha:alphaLevel];
                if (y > 0)
                {
                    mainImgV.frame = CGRectInset(cachedImageViewSize, 0, -y/2);
                    mainImgV.center = CGPointMake(mainImgV.center.x, mainImgV.center.y + y/2);
                }
                else
                {
                    mainImgV.frame = [UpdateFrame setPositionForView:mainImgV usingPositionY:y];
                }
                return;
            }
            
            scrolledBoundsForContainerView.origin.y = yscrollOffset ;
            containerView.bounds = scrolledBoundsForContainerView;
            
            [self setScrollContentOffset:CGPointMake(0, scrollView.contentOffset.y - yscrollOffset)  ];
        }
    }
}


#pragma mark - API

-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
    if(![ReadData isValidResponseData:response])
    {
        {
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
            [self.view setUserInteractionEnabled:YES];
            [HUD hide:YES];
        }
        return;
    }
    
    if ([apiName isEqualToString:API_GET_FEATURED_STYLISTS]) {
        [self processResponse:response];
    }
}

@end
