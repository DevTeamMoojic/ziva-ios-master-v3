//
//  FeaturedStylistsDetailViewController.m
//  Ziva
//
//  Created by Leena on 26/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "FeaturedStylistsDetailViewController.h"
#import "GalleryViewController.h"

@interface FeaturedStylistsDetailViewController ()<APIDelegate,MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
    
    __weak IBOutlet UIView *optionsV;
    __weak IBOutlet UILabel *screenTitleL;
    __weak IBOutlet UILabel *addressL;
    
    
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIView *topImageV;
    __weak IBOutlet UIImageView *mainImgV;
    
    __weak IBOutlet UIScrollView *scrollviewOnTop;
    
    __weak IBOutlet UIScrollView *tabcontentscrollView;
    __weak IBOutlet UIScrollView *mainscrollView;
    
    __weak IBOutlet UIView *addToCartV;
    __weak IBOutlet UIImageView *quantityLessImgV;
    __weak IBOutlet UILabel *quantityL;
    __weak IBOutlet UIImageView *quantityAddImgV;
    
    __weak IBOutlet UILabel *titleL;
    
    __weak IBOutlet UIView *pricingV;
    __weak IBOutlet UILabel *offerPriceL;
    __weak IBOutlet UILabel *originalPriceL;
    
    __weak IBOutlet UILabel *professionL;
    __weak IBOutlet UILabel *serviceL;
    __weak IBOutlet UILabel *genderL;
    
    
    
    __weak IBOutlet UIView *featuredDetailsView;
    
    __weak IBOutlet UIView *seperatorV;
    
    __weak IBOutlet UILabel *descriptionL;
    
    __weak IBOutlet UIView *additionalInfoV;
    __weak IBOutlet UILabel *sellerNameL;
    __weak IBOutlet UILabel *deliveryPeriodL;
    __weak IBOutlet UILabel *deliveryInformationL;
    
    __weak IBOutlet UIView *cartsummaryCV;
    //__weak CartSummaryViewController *cartsummaryVC;
    
    CGRect cachedImageViewSize;
    CGFloat yscrollOffset;
    
    BOOL isBackFromOtherView;
    BOOL isContentSizeUpdated;
    BOOL isUpdating;
    
    NSUInteger pageIndex;
    NSInteger pageWidth;
    NSInteger pageHeight;
    
    CGFloat contentHeight;
    
    NSString *record_Id;
    NSDictionary *dataDict;
    
    AppDelegate *appD;
    MyCartController *cartC;
    
    NSInteger quantity;
    NSMutableArray *ImagesArray;

}
@end

@implementation FeaturedStylistsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!isBackFromOtherView){
        cachedImageViewSize = mainImgV.frame;
    }
    else{
        isBackFromOtherView = NO;
    }
    quantity = [cartC getQuantityInCart:record_Id];
    [self updateQuantityHeading];
    //[cartsummaryVC updateSummary];
}



#pragma mark - Layout

-(void) resetLayout;
{
    mainscrollView.contentOffset = CGPointZero;
    contentHeight = CGRectGetHeight(self.view.bounds);
    
}
- (void) setupLayout
{
    // [cartsummaryVC setActionOnCart:CART_ACTION_PURCHASE];
    //  [cartsummaryVC updateSummary];
    
    [self updateQuantityHeading];
    
    pageWidth = CGRectGetWidth(self.view.bounds);
    
    quantityAddImgV.layer.borderWidth = quantityLessImgV.layer.borderWidth = 0.5f;
    quantityAddImgV.layer.borderColor= quantityLessImgV.layer.borderColor= [UIColor borderDisplayColor].CGColor;
    
    quantityL.text = @"0";
    
    [self.view addGestureRecognizer:scrollviewOnTop.panGestureRecognizer];
    [mainscrollView removeGestureRecognizer:mainscrollView.panGestureRecognizer];
    
    //Position elements
    {
        topImageV.frame = [UpdateFrame setSizeForView:topImageV usingHeight:[ResizeToFitContent getHeightForDetailViewImage:self.view.frame]];
        
        yscrollOffset = CGRectGetHeight(topImageV.frame) - IMAGE_SCROLL_OFFSET_Y - CGRectGetHeight(optionsV.frame);
        mainImgV.frame = topImageV.frame ;
        
        containerView.frame = [UpdateFrame setSizeForView:containerView usingHeight:(CGRectGetHeight(self.view.bounds) + yscrollOffset)];
        
        tabcontentscrollView.frame= [UpdateFrame setPositionForView:tabcontentscrollView usingPositionY: CGRectGetMaxY(topImageV.frame)];
        tabcontentscrollView.frame = [UpdateFrame setSizeForView:tabcontentscrollView usingHeight: CGRectGetHeight(containerView.frame) - CGRectGetMaxY(topImageV.frame)];
    }
    
    
    CGPoint scrollviewOrigin = scrollviewOnTop.frame.origin;
    scrollviewOnTop.scrollIndicatorInsets = UIEdgeInsetsMake(-scrollviewOrigin.y, 0, scrollviewOrigin.y, scrollviewOrigin.x);
    
    [self updateQuantityHeading];
    //[self reloadList];
    [ self setDataOfFeturedStylistDetails];
}

- (void) updateQuantityHeading
{
    cartsummaryCV.hidden = ([cartC.cartItems count] == 0);
    quantityL.text = [NSString stringWithFormat:@"%ld",(long) quantity];
}



#pragma mark - Public Methods

- (void) loadDetailView : (NSDictionary *) dict
{
    dataDict = dict;
    
    //NSLog(@"%@", [dict allKeys]);
}

- (NSString *) getAddress : (NSDictionary *) item{
    NSString *str = @"";
    str = [ReadData stringValueFromDictionary:item forKey:KEY_SALON_ADDRESS];
    return str;
}
- (NSString *) getStartingPriceDescription : (NSDictionary *) item{
    NSString *str = @"";
    str = [ReadData amountInRsFromDictionary:item forKey:KEY_STYLIST_STARTING_PRICE];
    str = [str stringByAppendingString:@" (Starting Price)"];
    if([[ReadData stringValueFromDictionary:item forKey:KEY_STYLIST_SERVICES] length] > 0 )
    {
        str = [str stringByAppendingString : @" for "];
        str = [str stringByAppendingString : [ReadData stringValueFromDictionary:item forKey:KEY_STYLIST_SERVICES]];
    }
    return str;
}

- (void) setDataOfFeturedStylistDetails
{
    contentHeight = 0;
    if(dataDict)
    {
        CGFloat tmpHeight = 0;
        
        NSString *recordId = [ReadData recordIdFromDictionary:dataDict forKey:KEY_ID];
        [appD.sessionDelegate.mytrackingC trackLookById:recordId];
        
        titleL.text = [ReadData stringValueFromDictionary:dataDict forKey:KEY_NAME];
        //otherInfoL.text = [ReadData stringValueFromDictionary:dataDict forKey:KEY_LOOK_SUBTITLE];
        //NSLog(@"%@", titleL.text);
        
        
        addressL.text = [ReadData stringValueFromDictionary:dataDict forKey:KEY_STYLIST_LOCATION];
        professionL.text = [ReadData stringValueFromDictionary:dataDict forKey:KEY_STYLIST_PROFESSION];
        
        
        serviceL.text = [self getStartingPriceDescription:dataDict];
        genderL.text = [ReadData getGenderDescription:[ReadData genderIdFromDictionary:dataDict forKey:KEY_GENDER]];
        
        
        
        NSString *salonName = @"";
        if(![ReadData IsnulldictionaryFromDictionary:dataDict forKey:KEY_LOOK_SALON]){
            NSDictionary *storesDict = [ReadData dictionaryFromDictionary:dataDict forKey:KEY_LOOK_SALON];
            salonName = [ReadData stringValueFromDictionary:storesDict forKey:KEY_NAME];
        }
        
        //startingPriceL.attributedText = [self getStartingPrice:[ReadData amountInRsFromDictionary:dataDict forKey:KEY_LOOK_SILVERPRICE] withExclusiveSalon:salonName];
        
        ///
        
        descriptionL.frame = [UpdateFrame setPositionForView:descriptionL usingPositionY:CGRectGetMinY(seperatorV.frame) + 15];
        descriptionL.text = [ReadData stringValueFromDictionary:dataDict forKey:KEY_PRODUCT_DESCRIPTION];
        //descriptionL.backgroundColor = [UIColor yellowColor];
        
        tmpHeight = [ResizeToFitContent getHeightForText:descriptionL.text usingFontType:FONT_SEMIBOLD FontOfSize:12 andMaxWidth:CGRectGetWidth(descriptionL.frame)];
        descriptionL.frame = [UpdateFrame setSizeForView:descriptionL usingHeight:tmpHeight];
        additionalInfoV.frame = [UpdateFrame setPositionForView:additionalInfoV usingPositionY:(CGRectGetMaxY(descriptionL.frame) + 15)];
        
        
        // ImageView
        NSString *url = [ReadData stringValueFromDictionary:dataDict forKey:KEY_IMAGE_URL];
        if (![url isEqualToString:@""])
        {
            NSURL *imageURL = [NSURL URLWithString:url];
            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageURL];
            [ mainImgV setImageWithURLRequest:urlRequest
                             placeholderImage:Nil
                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
             {
                 [ mainImgV setImage:image];
                 [UIView animateWithDuration:IMAGE_VIEW_TOGGLE
                                       delay:0 options:UIViewAnimationOptionCurveLinear
                                  animations:^(void)
                  {
                      
                      [ mainImgV setAlpha:1 ];
                  }
                                  completion:^(BOOL finished)
                  {
                  }];
                 
                 
                 
             }
                                      failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
             {
                 
             }];
        }
        
        
        //contentHeight = CGRectGetMaxY(descriptionCV.frame) + CGRectGetHeight(footerV.frame);
    }
    
    if(contentHeight <= CGRectGetHeight(mainscrollView.frame)){
        contentHeight = CGRectGetHeight(mainscrollView.bounds);
        mainscrollView.scrollEnabled = NO;
    }
    
    [self tabChanged];
}

- (void) processResponse : (NSDictionary *) dict
{
    contentHeight = 0;
    dataDict = [ReadData dictionaryFromDictionary:dict forKey:RESPONSEKEY_DATA];
    if(dataDict)
    {
        CGFloat tmpHeight = 0;
        /*
         
         CGFloat tmpHeight = 0;
         titleL.text = [ReadData stringValueFromDictionary:dataDict forKey:KEY_PRODUCT_TITLE];
         tmpHeight = [ResizeToFitContent getHeightForText:titleL.text usingFontType:FONT_SEMIBOLD FontOfSize:13 andMaxWidth:CGRectGetWidth(titleL.frame)];
         titleL.frame = [UpdateFrame setSizeForView:titleL usingHeight:tmpHeight];
         titleL.backgroundColor = [UIColor redColor];
         
         pricingV.backgroundColor = [ UIColor greenColor];
         
         pricingV.frame = [UpdateFrame setPositionForView:pricingV usingPositionY:CGRectGetMaxY(titleL.frame) + 10];
         NSString *zivaPrice = [ReadData amountInRsFromDictionary:dataDict forKey:KEY_PRODUCT_PRICE];
         
         NSString *originalPrice = [ReadData amountInRsFromDictionary:dataDict forKey:KEY_PRODUCT_ORIGINALPRICE];
         offerPriceL.text = zivaPrice;
         NSAttributedString * title = [[NSAttributedString alloc] initWithString: originalPrice attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle)}];
         [originalPriceL setAttributedText:title];
         originalPriceL.hidden = [zivaPrice isEqualToString:originalPrice];
         
         */
        featuredDetailsView.frame = [UpdateFrame setPositionForView:featuredDetailsView usingPositionY:15];
        
        seperatorV.frame = [UpdateFrame setPositionForView:seperatorV usingPositionY:CGRectGetMaxY(featuredDetailsView.frame) + 15];
        
        descriptionL.frame = [UpdateFrame setPositionForView:descriptionL usingPositionY:CGRectGetMinY(seperatorV.frame) + 15];
        descriptionL.text = [ReadData stringValueFromDictionary:dataDict forKey:KEY_PRODUCT_DESCRIPTION];
        //descriptionL.backgroundColor = [UIColor yellowColor];
        
        tmpHeight = [ResizeToFitContent getHeightForText:descriptionL.text usingFontType:FONT_SEMIBOLD FontOfSize:12 andMaxWidth:CGRectGetWidth(descriptionL.frame)];
        descriptionL.frame = [UpdateFrame setSizeForView:descriptionL usingHeight:tmpHeight];
        
        additionalInfoV.frame = [UpdateFrame setPositionForView:additionalInfoV usingPositionY:(CGRectGetMaxY(descriptionL.frame) + 15)];
        
        NSDictionary *brandDict = [ReadData dictionaryFromDictionary:dataDict forKey:KEY_PRODUCT_SELLER];
        sellerNameL.text= [ReadData stringValueFromDictionary:brandDict forKey:KEY_NAME];
        deliveryPeriodL.text= [ReadData stringValueFromDictionary:dataDict forKey:KEY_PRODUCT_DELIVERY_PERIOD];
        deliveryInformationL.text= [ReadData stringValueFromDictionary:dataDict forKey:KEY_PRODUCT_DELIVERY_INFORMATION];
        cartC.isCashPaymentAllowed =[ReadData boolValueFromDictionary:dataDict forKey:KEY_PRODUCT_PAYMENTMODE];
        
        
        // ImageView
        NSString *url = [ReadData stringValueFromDictionary:dataDict forKey:KEY_IMAGE_URL];
        if (![url isEqualToString:@""])
        {
            NSURL *imageURL = [NSURL URLWithString:url];
            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageURL];
            [ mainImgV setImageWithURLRequest:urlRequest
                             placeholderImage:Nil
                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
             {
                 [ mainImgV setImage:image];
                 [UIView animateWithDuration:IMAGE_VIEW_TOGGLE
                                       delay:0 options:UIViewAnimationOptionCurveLinear
                                  animations:^(void)
                  {
                      
                      [ mainImgV setAlpha:1 ];
                  }
                                  completion:^(BOOL finished)
                  {
                  }];
                 
                 
                 
             }
                                      failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
             {
                 
             }];
        }
        
        contentHeight = CGRectGetMaxY(additionalInfoV.frame) + 80;
    }
    
    if(contentHeight <= CGRectGetHeight(mainscrollView.frame)){
        contentHeight = CGRectGetHeight(mainscrollView.bounds);
        mainscrollView.scrollEnabled = NO;
    }
    
    [self tabChanged];
    [HUD hide:YES];
}

- (void) tabChanged
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        CGPoint offset = [self getScrollContentOffset];
        offset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:offset animated:NO];
    }
    isContentSizeUpdated = NO;
}

#pragma mark - Cart Summary Delegate

- (void) takeActionOnCart{
    isBackFromOtherView = YES;
    [self performSegueWithIdentifier:@"showcart_products" sender:Nil];
}

#pragma mark - HUD & Delegate methods

- (void)hudShowWithMessage:(NSString *)message
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    HUD.dimBackground = YES;
    HUD.labelText = message;
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    [HUD show:YES];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}

#pragma mark - Events

- (IBAction)backBPressed:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)galleryBPressed:(UIButton *)sender{
    [self performSegueWithIdentifier:@"showgallery_look" sender:Nil];
}

/*
 - (IBAction)quantityAddBPressed:(UIButton *)sender{
 if(quantity < MAX_REPEAT_QUANTITY){
 [cartC addProducts:dataDict];
 [cartsummaryVC updateSummary];
 quantity++;
 [self updateQuantityHeading];
 }
 }
 
 - (IBAction)quantityLessBPressed:(UIButton *)sender{
 if(quantity > 0){
 [cartC lessProducts:dataDict];
 [cartsummaryVC updateSummary];
 quantity--;
 [self updateQuantityHeading];
 }
 }
 */

#pragma mark - Expand on Scroll animation

- (CGSize) setContentSize
{
    CGFloat height = CGRectGetHeight(mainImgV.frame) + contentHeight;
    return CGSizeMake(pageWidth, height);
}

- (void) setScrollContentOffset: (CGPoint) offset
{
    mainscrollView.contentOffset = offset;
}

- (CGPoint) getScrollContentOffset
{
    return [mainscrollView contentOffset];
}

- (void) resetOffsetAllTabs
{
    CGPoint offset = CGPointZero;
    [self setScrollContentOffset : offset];
}

-(void) updateContentSize : (CGPoint) contentOffset;
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        contentOffset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    }
    [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    
    isContentSizeUpdated = NO;
}

#pragma mark - Scroll Events

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == scrollviewOnTop)
    {
        {
            if (isContentSizeUpdated)  return;
            
            CGRect scrolledBoundsForContainerView = containerView.bounds;
            if (scrollView.contentOffset.y <= yscrollOffset)
            {
                
                scrolledBoundsForContainerView.origin.y = scrollView.contentOffset.y ;
                containerView.bounds = scrolledBoundsForContainerView;
                
                //Reset offset for all tabs
                if (scrollView.contentOffset.y <= 0) [self resetOffsetAllTabs];
                
                
                CGFloat y = -scrollView.contentOffset.y;
                //                CGFloat alphaLevel = 1;
                //                CGFloat BLUR_MAX_Y = IMAGE_SCROLL_OFFSET_Y + CGRectGetHeight(topImageV.frame) - CGRectGetMinY(optionsV.frame);
                //                if (fabs(y) < BLUR_MAX_Y)
                //                {
                //                    alphaLevel = (BLUR_MAX_Y - fabs(y))/(BLUR_MAX_Y);
                //                }
                //                else
                //                {
                //                    alphaLevel = 0;
                //                }
                //
                //                //[screenTitleL setAlpha:alphaLevel];
                if (y > 0)
                {
                    mainImgV.frame = CGRectInset(cachedImageViewSize, 0, -y/2);
                    mainImgV.center = CGPointMake(mainImgV.center.x, mainImgV.center.y + y/2);
                }
                else
                {
                    mainImgV.frame = [UpdateFrame setPositionForView:mainImgV usingPositionY:y];
                }
                return;
            }
            
            scrolledBoundsForContainerView.origin.y = yscrollOffset ;
            containerView.bounds = scrolledBoundsForContainerView;
            
            [self setScrollContentOffset:CGPointMake(0, scrollView.contentOffset.y - yscrollOffset)  ];
        }
    }
}

#pragma mark - API

-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
    if(![ReadData isValidResponseData:response])
    {
        {
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
            [self.view setUserInteractionEnabled:YES];
            [HUD hide:YES];
        }
        return;
    }
    
    if ([apiName isEqualToString:API_DETAIL_VIEW_PRODUCT]) {
        //[self processResponse:response];
    }
}


#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"showgallery_look"]){
        GalleryViewController *galleryVC = (GalleryViewController *)segue.destinationViewController;
        
        
        ImagesArray = [[NSMutableArray alloc]init];
        NSMutableArray *arrayImageKeys = [self GetfeturedStylistImageUrlFromDictionary:dataDict];
        // NSLog(@"The iamge urls -- %@", arrayImageKeys);
        
        // split the actual images from hole images.
        for (NSString *URL in arrayImageKeys) {
            
            NSString *url = [ReadData stringValueFromDictionary:dataDict forKey:URL];
            if (![url isEqualToString:@""] && [url containsString:@"https://"]) {
                [ImagesArray addObject:url];
            }
        }
        
        [galleryVC setImageForFullScreen:ImagesArray];
        
        //[galleryVC setImageForFullScreen:mainImgV.image];
    }
}



// This is a method to split the images URL from Main dictonary object.
-(NSMutableArray*)GetfeturedStylistImageUrlFromDictionary:(NSMutableDictionary*)dictionary
{
    NSMutableArray* result = [NSMutableArray array];
    for (NSString* key in dictionary)
    {
        if ([key hasPrefix:@"Image"])
        {
            [result addObject:key];
        }
    }
    return result;
}

@end
