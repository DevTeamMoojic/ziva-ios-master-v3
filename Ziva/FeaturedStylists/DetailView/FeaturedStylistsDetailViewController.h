//
//  FeaturedStylistsDetailViewController.h
//  Ziva
//
//  Created by Leena on 26/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeaturedStylistsDetailViewController : UIViewController
//- (void) loadDetailView : (NSString *) recordId;
- (void) processResponseTest : (NSDictionary *) dict;
//- (void) Test : (NSDictionary *) dict;
- (void) loadDetailView : (NSDictionary *) dict;

@end
